from django.apps import AppConfig


class OsmapConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'osmap'
