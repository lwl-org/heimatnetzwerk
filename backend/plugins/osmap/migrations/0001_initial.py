# Generated by Django 3.1.14 on 2023-03-30 21:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0022_auto_20180620_1551'),
    ]

    operations = [
        migrations.CreateModel(
            name='OSMap',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='osmap_osmap', serialize=False, to='cms.cmsplugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='OSMapLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('HvName1', models.CharField(blank=True, max_length=255, null=True)),
                ('HvName2', models.CharField(blank=True, max_length=255, null=True)),
                ('GH', models.CharField(blank=True, max_length=255, null=True)),
                ('OH', models.CharField(blank=True, max_length=255, null=True)),
                ('SH', models.CharField(blank=True, max_length=255, null=True)),
                ('Och', models.CharField(blank=True, max_length=255, null=True)),
                ('VHV', models.CharField(blank=True, max_length=255, null=True)),
                ('Krshpf', models.CharField(blank=True, max_length=255, null=True)),
                ('Titel', models.CharField(blank=True, max_length=255, null=True)),
                ('Vorname', models.CharField(blank=True, max_length=255, null=True)),
                ('Name1', models.CharField(blank=True, max_length=255, null=True)),
                ('Name2', models.CharField(blank=True, max_length=255, null=True)),
                ('Name3', models.CharField(blank=True, max_length=255, null=True)),
                ('Str', models.CharField(blank=True, max_length=255, null=True)),
                ('Plz', models.CharField(blank=True, max_length=255, null=True)),
                ('Ort', models.CharField(blank=True, max_length=255, null=True)),
                ('Telpriv', models.CharField(blank=True, max_length=255, null=True)),
                ('Hg', models.CharField(blank=True, max_length=255, null=True)),
                ('Krs', models.CharField(blank=True, max_length=255, null=True)),
                ('Stadt', models.CharField(blank=True, max_length=255, null=True)),
                ('Ortsteil', models.CharField(blank=True, max_length=255, null=True)),
                ('Email1', models.CharField(blank=True, max_length=255, null=True)),
                ('Email2', models.CharField(blank=True, max_length=255, null=True)),
                ('Email3', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
    ]
