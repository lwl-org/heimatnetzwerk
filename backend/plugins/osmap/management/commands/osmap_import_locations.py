from django.core.management.base import BaseCommand, CommandError

from backend.plugins.osmap.models import OSMapLocation

import csv


class Command(BaseCommand):
    help = "Import OS Map Locations from CSV file"

    def add_arguments(self, parser):
        parser.add_argument("filename", type=str)

    def handle(self, *args, **options):
        filename = options["filename"]
        self.stdout.write(f"Importing OS Map Locations from CSV file: {filename}")

        # TODO: delete old locations? overwrite existing?

        with open(filename, "r") as f:
            reader = csv.DictReader(f, delimiter=';', quotechar='|')
            locations = []
            
            for row in reader:
                location = OSMapLocation()
                for key in row.keys():
                    key = key.strip()
                    setattr(location, key, row[key])
                location.save()
                locations.append(location)
            
        self.stdout.write(
            self.style.SUCCESS(f"Imported {len(locations)} locations")
        )