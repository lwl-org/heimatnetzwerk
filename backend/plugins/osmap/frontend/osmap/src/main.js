import { createApp } from 'vue'
import App from './App.vue'

import 'leaflet/dist/leaflet.css'

import './assets/sass/main.scss'

import GlobalComponents from './globalComponents'

const app = createApp(App)

app.use(GlobalComponents)

app.mount('#osmapplugin')

export default app