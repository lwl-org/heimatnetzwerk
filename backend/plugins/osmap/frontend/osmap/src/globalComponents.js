import { LMap, LTileLayer, LPolygon, LMarker, LGeoJson } from "@vue-leaflet/vue-leaflet"

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'

/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install (Vue) {
    Vue.component('l-map', LMap)
    Vue.component('l-tile-layer', LTileLayer)
    Vue.component('l-marker', LMarker)
    Vue.component('l-polygon', LPolygon)
    Vue.component('l-geo-json', LGeoJson)
    Vue.component('v-select', vSelect)
  }
}

export default GlobalComponents
