from django.contrib import admin

from . import models


@admin.register(models.OSMapLocation)
class OSMapLocationAdmin(admin.ModelAdmin):
    model = models.OSMapLocation
