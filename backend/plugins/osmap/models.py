from cms.models import CMSPlugin
from django.db import models


# ['HvName1', 'HvName2', 'GH', 'OH', 'SH', 'Och', 'VHV', 'Krshpf', 'Titel', 'Vorname', 'Name1', 'Name2', 'Name3', 'Str', 'Plz', 'Ort', 'Telpriv', 'Hg', 'Krs', 'Stadt', 'Ortsteil', 'Email1', 'Email2', 'Email3']
class OSMapLocation(models.Model):
    HvName1 = models.CharField(max_length=255, blank=True, null=True)
    HvName2 = models.CharField(max_length=255, blank=True, null=True)
    GH = models.CharField(max_length=255, blank=True, null=True)
    OH = models.CharField(max_length=255, blank=True, null=True)
    SH = models.CharField(max_length=255, blank=True, null=True)
    Och = models.CharField(max_length=255, blank=True, null=True)
    VHV = models.CharField(max_length=255, blank=True, null=True)
    Krshpf = models.CharField(max_length=255, blank=True, null=True)
    Titel = models.CharField(max_length=255, blank=True, null=True)
    Vorname = models.CharField(max_length=255, blank=True, null=True)
    Name1 = models.CharField(max_length=255, blank=True, null=True)
    Name2 = models.CharField(max_length=255, blank=True, null=True)
    Name3 = models.CharField(max_length=255, blank=True, null=True)
    Str = models.CharField(max_length=255, blank=True, null=True) # steet
    Plz = models.CharField(max_length=255, blank=True, null=True) # postcode
    Ort = models.CharField(max_length=255, blank=True, null=True) 
    Telpriv = models.CharField(max_length=255, blank=True, null=True) # phone
    Hg = models.CharField(max_length=255, blank=True, null=True) 
    Krs = models.CharField(max_length=255, blank=True, null=True)
    Stadt = models.CharField(max_length=255, blank=True, null=True) # city
    Ortsteil = models.CharField(max_length=255, blank=True, null=True)
    Email1 = models.CharField(max_length=255, blank=True, null=True)
    Email2 = models.CharField(max_length=255, blank=True, null=True)
    Email3 = models.CharField(max_length=255, blank=True, null=True)


class OSMap(CMSPlugin):
    pass
