import 'dotenv/config'

import { fileURLToPath, URL } from 'node:url'
import { resolve } from 'path'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default ({ mode }) => {
  require('dotenv').config({ path: `./.env.${mode}` });
  // now you can access config with process.env.{configName}

  return defineConfig({
    plugins: [vue()],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    build: {
      lib: {
        // Could also be a dictionary or array of multiple entry points
        entry: resolve(__dirname, 'src/main.js'),
        name: 'osmap',
        // the proper extensions will be added
        fileName: 'osmap',
      },
      outDir: '../../static/osmap/'
    },
    define: {
      "process.env.NODE_ENV": `"${process.env.NODE_ENV}"`,
    }
  })
}

