from django.utils.translation import gettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from . import models


@plugin_pool.register_plugin
class OSMapPlugin(CMSPluginBase):
    model = models.OSMap
    name = _("OpenStreetMap")
    render_template = "osmap/osmap.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        return context
