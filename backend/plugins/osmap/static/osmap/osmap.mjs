function Uh(e, n) {
  for (var s = 0; s < n.length; s++) {
    const a = n[s];
    if (typeof a != "string" && !Array.isArray(a)) {
      for (const l in a)
        if (l !== "default" && !(l in e)) {
          const u = Object.getOwnPropertyDescriptor(a, l);
          u && Object.defineProperty(e, l, u.get ? u : {
            enumerable: !0,
            get: () => a[l]
          });
        }
    }
  }
  return Object.freeze(Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }));
}
function Ds(e, n) {
  const s = /* @__PURE__ */ Object.create(null), a = e.split(",");
  for (let l = 0; l < a.length; l++)
    s[a[l]] = !0;
  return n ? (l) => !!s[l.toLowerCase()] : (l) => !!s[l];
}
function To(e) {
  if (J(e)) {
    const n = {};
    for (let s = 0; s < e.length; s++) {
      const a = e[s], l = Zt(a) ? Gh(a) : To(a);
      if (l)
        for (const u in l)
          n[u] = l[u];
    }
    return n;
  } else {
    if (Zt(e))
      return e;
    if (St(e))
      return e;
  }
}
const Vh = /;(?![^(]*\))/g, Kh = /:([^]+)/, qh = /\/\*.*?\*\//gs;
function Gh(e) {
  const n = {};
  return e.replace(qh, "").split(Vh).forEach((s) => {
    if (s) {
      const a = s.split(Kh);
      a.length > 1 && (n[a[0].trim()] = a[1].trim());
    }
  }), n;
}
function Ai(e) {
  let n = "";
  if (Zt(e))
    n = e;
  else if (J(e))
    for (let s = 0; s < e.length; s++) {
      const a = Ai(e[s]);
      a && (n += a + " ");
    }
  else if (St(e))
    for (const s in e)
      e[s] && (n += s + " ");
  return n.trim();
}
function Le(e) {
  if (!e)
    return null;
  let { class: n, style: s } = e;
  return n && !Zt(n) && (e.class = Ai(n)), s && (e.style = To(s)), e;
}
const Yh = "itemscope,allowfullscreen,formnovalidate,ismap,nomodule,novalidate,readonly", Jh = /* @__PURE__ */ Ds(Yh);
function Qa(e) {
  return !!e || e === "";
}
const ni = (e) => Zt(e) ? e : e == null ? "" : J(e) || St(e) && (e.toString === nl || !it(e.toString)) ? JSON.stringify(e, tl, 2) : String(e), tl = (e, n) => n && n.__v_isRef ? tl(e, n.value) : Ui(n) ? {
  [`Map(${n.size})`]: [...n.entries()].reduce((s, [a, l]) => (s[`${a} =>`] = l, s), {})
} : el(n) ? {
  [`Set(${n.size})`]: [...n.values()]
} : St(n) && !J(n) && !ol(n) ? String(n) : n, Ot = {}, Wi = [], Me = () => {
}, Xh = () => !1, Qh = /^on[^a-z]/, Co = (e) => Qh.test(e), Fs = (e) => e.startsWith("onUpdate:"), Xt = Object.assign, js = (e, n) => {
  const s = e.indexOf(n);
  s > -1 && e.splice(s, 1);
}, tc = Object.prototype.hasOwnProperty, ct = (e, n) => tc.call(e, n), J = Array.isArray, Ui = (e) => Oo(e) === "[object Map]", el = (e) => Oo(e) === "[object Set]", it = (e) => typeof e == "function", Zt = (e) => typeof e == "string", Hs = (e) => typeof e == "symbol", St = (e) => e !== null && typeof e == "object", il = (e) => St(e) && it(e.then) && it(e.catch), nl = Object.prototype.toString, Oo = (e) => nl.call(e), ec = (e) => Oo(e).slice(8, -1), ol = (e) => Oo(e) === "[object Object]", $s = (e) => Zt(e) && e !== "NaN" && e[0] !== "-" && "" + parseInt(e, 10) === e, ho = /* @__PURE__ */ Ds(
  // the leading comma is intentional so empty string "" is also included
  ",key,ref,ref_for,ref_key,onVnodeBeforeMount,onVnodeMounted,onVnodeBeforeUpdate,onVnodeUpdated,onVnodeBeforeUnmount,onVnodeUnmounted"
), So = (e) => {
  const n = /* @__PURE__ */ Object.create(null);
  return (s) => n[s] || (n[s] = e(s));
}, ic = /-(\w)/g, Re = So((e) => e.replace(ic, (n, s) => s ? s.toUpperCase() : "")), nc = /\B([A-Z])/g, Yi = So((e) => e.replace(nc, "-$1").toLowerCase()), Mo = So((e) => e.charAt(0).toUpperCase() + e.slice(1)), co = So((e) => e ? `on${Mo(e)}` : ""), Pn = (e, n) => !Object.is(e, n), fo = (e, n) => {
  for (let s = 0; s < e.length; s++)
    e[s](n);
}, go = (e, n, s) => {
  Object.defineProperty(e, n, {
    configurable: !0,
    enumerable: !1,
    value: s
  });
}, xs = (e) => {
  const n = parseFloat(e);
  return isNaN(n) ? e : n;
}, oc = (e) => {
  const n = Zt(e) ? Number(e) : NaN;
  return isNaN(n) ? e : n;
};
let ca;
const sc = () => ca || (ca = typeof globalThis < "u" ? globalThis : typeof self < "u" ? self : typeof window < "u" ? window : typeof global < "u" ? global : {});
let Ce;
class rc {
  constructor(n = !1) {
    this.detached = n, this._active = !0, this.effects = [], this.cleanups = [], this.parent = Ce, !n && Ce && (this.index = (Ce.scopes || (Ce.scopes = [])).push(this) - 1);
  }
  get active() {
    return this._active;
  }
  run(n) {
    if (this._active) {
      const s = Ce;
      try {
        return Ce = this, n();
      } finally {
        Ce = s;
      }
    }
  }
  /**
   * This should only be called on non-detached scopes
   * @internal
   */
  on() {
    Ce = this;
  }
  /**
   * This should only be called on non-detached scopes
   * @internal
   */
  off() {
    Ce = this.parent;
  }
  stop(n) {
    if (this._active) {
      let s, a;
      for (s = 0, a = this.effects.length; s < a; s++)
        this.effects[s].stop();
      for (s = 0, a = this.cleanups.length; s < a; s++)
        this.cleanups[s]();
      if (this.scopes)
        for (s = 0, a = this.scopes.length; s < a; s++)
          this.scopes[s].stop(!0);
      if (!this.detached && this.parent && !n) {
        const l = this.parent.scopes.pop();
        l && l !== this && (this.parent.scopes[this.index] = l, l.index = this.index);
      }
      this.parent = void 0, this._active = !1;
    }
  }
}
function ac(e, n = Ce) {
  n && n.active && n.effects.push(e);
}
function lc() {
  return Ce;
}
const Ws = (e) => {
  const n = new Set(e);
  return n.w = 0, n.n = 0, n;
}, sl = (e) => (e.w & li) > 0, rl = (e) => (e.n & li) > 0, uc = ({ deps: e }) => {
  if (e.length)
    for (let n = 0; n < e.length; n++)
      e[n].w |= li;
}, hc = (e) => {
  const { deps: n } = e;
  if (n.length) {
    let s = 0;
    for (let a = 0; a < n.length; a++) {
      const l = n[a];
      sl(l) && !rl(l) ? l.delete(e) : n[s++] = l, l.w &= ~li, l.n &= ~li;
    }
    n.length = s;
  }
}, Ls = /* @__PURE__ */ new WeakMap();
let _n = 0, li = 1;
const Ps = 30;
let Oe;
const Mi = Symbol(""), Ts = Symbol("");
class Us {
  constructor(n, s = null, a) {
    this.fn = n, this.scheduler = s, this.active = !0, this.deps = [], this.parent = void 0, ac(this, a);
  }
  run() {
    if (!this.active)
      return this.fn();
    let n = Oe, s = ri;
    for (; n; ) {
      if (n === this)
        return;
      n = n.parent;
    }
    try {
      return this.parent = Oe, Oe = this, ri = !0, li = 1 << ++_n, _n <= Ps ? uc(this) : fa(this), this.fn();
    } finally {
      _n <= Ps && hc(this), li = 1 << --_n, Oe = this.parent, ri = s, this.parent = void 0, this.deferStop && this.stop();
    }
  }
  stop() {
    Oe === this ? this.deferStop = !0 : this.active && (fa(this), this.onStop && this.onStop(), this.active = !1);
  }
}
function fa(e) {
  const { deps: n } = e;
  if (n.length) {
    for (let s = 0; s < n.length; s++)
      n[s].delete(e);
    n.length = 0;
  }
}
let ri = !0;
const al = [];
function Ji() {
  al.push(ri), ri = !1;
}
function Xi() {
  const e = al.pop();
  ri = e === void 0 ? !0 : e;
}
function ce(e, n, s) {
  if (ri && Oe) {
    let a = Ls.get(e);
    a || Ls.set(e, a = /* @__PURE__ */ new Map());
    let l = a.get(s);
    l || a.set(s, l = Ws()), ll(l);
  }
}
function ll(e, n) {
  let s = !1;
  _n <= Ps ? rl(e) || (e.n |= li, s = !sl(e)) : s = !e.has(Oe), s && (e.add(Oe), Oe.deps.push(e));
}
function Ke(e, n, s, a, l, u) {
  const c = Ls.get(e);
  if (!c)
    return;
  let f = [];
  if (n === "clear")
    f = [...c.values()];
  else if (s === "length" && J(e)) {
    const p = Number(a);
    c.forEach((g, b) => {
      (b === "length" || b >= p) && f.push(g);
    });
  } else
    switch (s !== void 0 && f.push(c.get(s)), n) {
      case "add":
        J(e) ? $s(s) && f.push(c.get("length")) : (f.push(c.get(Mi)), Ui(e) && f.push(c.get(Ts)));
        break;
      case "delete":
        J(e) || (f.push(c.get(Mi)), Ui(e) && f.push(c.get(Ts)));
        break;
      case "set":
        Ui(e) && f.push(c.get(Mi));
        break;
    }
  if (f.length === 1)
    f[0] && Cs(f[0]);
  else {
    const p = [];
    for (const g of f)
      g && p.push(...g);
    Cs(Ws(p));
  }
}
function Cs(e, n) {
  const s = J(e) ? e : [...e];
  for (const a of s)
    a.computed && da(a);
  for (const a of s)
    a.computed || da(a);
}
function da(e, n) {
  (e !== Oe || e.allowRecurse) && (e.scheduler ? e.scheduler() : e.run());
}
const cc = /* @__PURE__ */ Ds("__proto__,__v_isRef,__isVue"), ul = new Set(
  /* @__PURE__ */ Object.getOwnPropertyNames(Symbol).filter((e) => e !== "arguments" && e !== "caller").map((e) => Symbol[e]).filter(Hs)
), fc = /* @__PURE__ */ Vs(), dc = /* @__PURE__ */ Vs(!1, !0), pc = /* @__PURE__ */ Vs(!0), pa = /* @__PURE__ */ mc();
function mc() {
  const e = {};
  return ["includes", "indexOf", "lastIndexOf"].forEach((n) => {
    e[n] = function(...s) {
      const a = pt(this);
      for (let u = 0, c = this.length; u < c; u++)
        ce(a, "get", u + "");
      const l = a[n](...s);
      return l === -1 || l === !1 ? a[n](...s.map(pt)) : l;
    };
  }), ["push", "pop", "shift", "unshift", "splice"].forEach((n) => {
    e[n] = function(...s) {
      Ji();
      const a = pt(this)[n].apply(this, s);
      return Xi(), a;
    };
  }), e;
}
function _c(e) {
  const n = pt(this);
  return ce(n, "has", e), n.hasOwnProperty(e);
}
function Vs(e = !1, n = !1) {
  return function(a, l, u) {
    if (l === "__v_isReactive")
      return !e;
    if (l === "__v_isReadonly")
      return e;
    if (l === "__v_isShallow")
      return n;
    if (l === "__v_raw" && u === (e ? n ? Ic : pl : n ? dl : fl).get(a))
      return a;
    const c = J(a);
    if (!e) {
      if (c && ct(pa, l))
        return Reflect.get(pa, l, u);
      if (l === "hasOwnProperty")
        return _c;
    }
    const f = Reflect.get(a, l, u);
    return (Hs(l) ? ul.has(l) : cc(l)) || (e || ce(a, "get", l), n) ? f : ie(f) ? c && $s(l) ? f : f.value : St(f) ? e ? ml(f) : Ao(f) : f;
  };
}
const gc = /* @__PURE__ */ hl(), vc = /* @__PURE__ */ hl(!0);
function hl(e = !1) {
  return function(s, a, l, u) {
    let c = s[a];
    if (qi(c) && ie(c) && !ie(l))
      return !1;
    if (!e && (!vo(l) && !qi(l) && (c = pt(c), l = pt(l)), !J(s) && ie(c) && !ie(l)))
      return c.value = l, !0;
    const f = J(s) && $s(a) ? Number(a) < s.length : ct(s, a), p = Reflect.set(s, a, l, u);
    return s === pt(u) && (f ? Pn(l, c) && Ke(s, "set", a, l) : Ke(s, "add", a, l)), p;
  };
}
function yc(e, n) {
  const s = ct(e, n);
  e[n];
  const a = Reflect.deleteProperty(e, n);
  return a && s && Ke(e, "delete", n, void 0), a;
}
function bc(e, n) {
  const s = Reflect.has(e, n);
  return (!Hs(n) || !ul.has(n)) && ce(e, "has", n), s;
}
function wc(e) {
  return ce(e, "iterate", J(e) ? "length" : Mi), Reflect.ownKeys(e);
}
const cl = {
  get: fc,
  set: gc,
  deleteProperty: yc,
  has: bc,
  ownKeys: wc
}, xc = {
  get: pc,
  set(e, n) {
    return !0;
  },
  deleteProperty(e, n) {
    return !0;
  }
}, Lc = /* @__PURE__ */ Xt({}, cl, {
  get: dc,
  set: vc
}), Ks = (e) => e, Eo = (e) => Reflect.getPrototypeOf(e);
function io(e, n, s = !1, a = !1) {
  e = e.__v_raw;
  const l = pt(e), u = pt(n);
  s || (n !== u && ce(l, "get", n), ce(l, "get", u));
  const { has: c } = Eo(l), f = a ? Ks : s ? Ys : Tn;
  if (c.call(l, n))
    return f(e.get(n));
  if (c.call(l, u))
    return f(e.get(u));
  e !== l && e.get(n);
}
function no(e, n = !1) {
  const s = this.__v_raw, a = pt(s), l = pt(e);
  return n || (e !== l && ce(a, "has", e), ce(a, "has", l)), e === l ? s.has(e) : s.has(e) || s.has(l);
}
function oo(e, n = !1) {
  return e = e.__v_raw, !n && ce(pt(e), "iterate", Mi), Reflect.get(e, "size", e);
}
function ma(e) {
  e = pt(e);
  const n = pt(this);
  return Eo(n).has.call(n, e) || (n.add(e), Ke(n, "add", e, e)), this;
}
function _a(e, n) {
  n = pt(n);
  const s = pt(this), { has: a, get: l } = Eo(s);
  let u = a.call(s, e);
  u || (e = pt(e), u = a.call(s, e));
  const c = l.call(s, e);
  return s.set(e, n), u ? Pn(n, c) && Ke(s, "set", e, n) : Ke(s, "add", e, n), this;
}
function ga(e) {
  const n = pt(this), { has: s, get: a } = Eo(n);
  let l = s.call(n, e);
  l || (e = pt(e), l = s.call(n, e)), a && a.call(n, e);
  const u = n.delete(e);
  return l && Ke(n, "delete", e, void 0), u;
}
function va() {
  const e = pt(this), n = e.size !== 0, s = e.clear();
  return n && Ke(e, "clear", void 0, void 0), s;
}
function so(e, n) {
  return function(a, l) {
    const u = this, c = u.__v_raw, f = pt(c), p = n ? Ks : e ? Ys : Tn;
    return !e && ce(f, "iterate", Mi), c.forEach((g, b) => a.call(l, p(g), p(b), u));
  };
}
function ro(e, n, s) {
  return function(...a) {
    const l = this.__v_raw, u = pt(l), c = Ui(u), f = e === "entries" || e === Symbol.iterator && c, p = e === "keys" && c, g = l[e](...a), b = s ? Ks : n ? Ys : Tn;
    return !n && ce(u, "iterate", p ? Ts : Mi), {
      // iterator protocol
      next() {
        const { value: x, done: T } = g.next();
        return T ? { value: x, done: T } : {
          value: f ? [b(x[0]), b(x[1])] : b(x),
          done: T
        };
      },
      // iterable protocol
      [Symbol.iterator]() {
        return this;
      }
    };
  };
}
function ti(e) {
  return function(...n) {
    return e === "delete" ? !1 : this;
  };
}
function Pc() {
  const e = {
    get(u) {
      return io(this, u);
    },
    get size() {
      return oo(this);
    },
    has: no,
    add: ma,
    set: _a,
    delete: ga,
    clear: va,
    forEach: so(!1, !1)
  }, n = {
    get(u) {
      return io(this, u, !1, !0);
    },
    get size() {
      return oo(this);
    },
    has: no,
    add: ma,
    set: _a,
    delete: ga,
    clear: va,
    forEach: so(!1, !0)
  }, s = {
    get(u) {
      return io(this, u, !0);
    },
    get size() {
      return oo(this, !0);
    },
    has(u) {
      return no.call(this, u, !0);
    },
    add: ti(
      "add"
      /* TriggerOpTypes.ADD */
    ),
    set: ti(
      "set"
      /* TriggerOpTypes.SET */
    ),
    delete: ti(
      "delete"
      /* TriggerOpTypes.DELETE */
    ),
    clear: ti(
      "clear"
      /* TriggerOpTypes.CLEAR */
    ),
    forEach: so(!0, !1)
  }, a = {
    get(u) {
      return io(this, u, !0, !0);
    },
    get size() {
      return oo(this, !0);
    },
    has(u) {
      return no.call(this, u, !0);
    },
    add: ti(
      "add"
      /* TriggerOpTypes.ADD */
    ),
    set: ti(
      "set"
      /* TriggerOpTypes.SET */
    ),
    delete: ti(
      "delete"
      /* TriggerOpTypes.DELETE */
    ),
    clear: ti(
      "clear"
      /* TriggerOpTypes.CLEAR */
    ),
    forEach: so(!0, !0)
  };
  return ["keys", "values", "entries", Symbol.iterator].forEach((u) => {
    e[u] = ro(u, !1, !1), s[u] = ro(u, !0, !1), n[u] = ro(u, !1, !0), a[u] = ro(u, !0, !0);
  }), [
    e,
    s,
    n,
    a
  ];
}
const [Tc, Cc, Oc, Sc] = /* @__PURE__ */ Pc();
function qs(e, n) {
  const s = n ? e ? Sc : Oc : e ? Cc : Tc;
  return (a, l, u) => l === "__v_isReactive" ? !e : l === "__v_isReadonly" ? e : l === "__v_raw" ? a : Reflect.get(ct(s, l) && l in a ? s : a, l, u);
}
const Mc = {
  get: /* @__PURE__ */ qs(!1, !1)
}, Ec = {
  get: /* @__PURE__ */ qs(!1, !0)
}, Ac = {
  get: /* @__PURE__ */ qs(!0, !1)
}, fl = /* @__PURE__ */ new WeakMap(), dl = /* @__PURE__ */ new WeakMap(), pl = /* @__PURE__ */ new WeakMap(), Ic = /* @__PURE__ */ new WeakMap();
function kc(e) {
  switch (e) {
    case "Object":
    case "Array":
      return 1;
    case "Map":
    case "Set":
    case "WeakMap":
    case "WeakSet":
      return 2;
    default:
      return 0;
  }
}
function Bc(e) {
  return e.__v_skip || !Object.isExtensible(e) ? 0 : kc(ec(e));
}
function Ao(e) {
  return qi(e) ? e : Gs(e, !1, cl, Mc, fl);
}
function zc(e) {
  return Gs(e, !1, Lc, Ec, dl);
}
function ml(e) {
  return Gs(e, !0, xc, Ac, pl);
}
function Gs(e, n, s, a, l) {
  if (!St(e) || e.__v_raw && !(n && e.__v_isReactive))
    return e;
  const u = l.get(e);
  if (u)
    return u;
  const c = Bc(e);
  if (c === 0)
    return e;
  const f = new Proxy(e, c === 2 ? a : s);
  return l.set(e, f), f;
}
function Vi(e) {
  return qi(e) ? Vi(e.__v_raw) : !!(e && e.__v_isReactive);
}
function qi(e) {
  return !!(e && e.__v_isReadonly);
}
function vo(e) {
  return !!(e && e.__v_isShallow);
}
function _l(e) {
  return Vi(e) || qi(e);
}
function pt(e) {
  const n = e && e.__v_raw;
  return n ? pt(n) : e;
}
function Bt(e) {
  return go(e, "__v_skip", !0), e;
}
const Tn = (e) => St(e) ? Ao(e) : e, Ys = (e) => St(e) ? ml(e) : e;
function gl(e) {
  ri && Oe && (e = pt(e), ll(e.dep || (e.dep = Ws())));
}
function vl(e, n) {
  e = pt(e);
  const s = e.dep;
  s && Cs(s);
}
function ie(e) {
  return !!(e && e.__v_isRef === !0);
}
function st(e) {
  return Nc(e, !1);
}
function Nc(e, n) {
  return ie(e) ? e : new Zc(e, n);
}
class Zc {
  constructor(n, s) {
    this.__v_isShallow = s, this.dep = void 0, this.__v_isRef = !0, this._rawValue = s ? n : pt(n), this._value = s ? n : Tn(n);
  }
  get value() {
    return gl(this), this._value;
  }
  set value(n) {
    const s = this.__v_isShallow || vo(n) || qi(n);
    n = s ? n : pt(n), Pn(n, this._rawValue) && (this._rawValue = n, this._value = s ? n : Tn(n), vl(this));
  }
}
function Rc(e) {
  return ie(e) ? e.value : e;
}
const Dc = {
  get: (e, n, s) => Rc(Reflect.get(e, n, s)),
  set: (e, n, s, a) => {
    const l = e[n];
    return ie(l) && !ie(s) ? (l.value = s, !0) : Reflect.set(e, n, s, a);
  }
};
function yl(e) {
  return Vi(e) ? e : new Proxy(e, Dc);
}
var bl;
class Fc {
  constructor(n, s, a, l) {
    this._setter = s, this.dep = void 0, this.__v_isRef = !0, this[bl] = !1, this._dirty = !0, this.effect = new Us(n, () => {
      this._dirty || (this._dirty = !0, vl(this));
    }), this.effect.computed = this, this.effect.active = this._cacheable = !l, this.__v_isReadonly = a;
  }
  get value() {
    const n = pt(this);
    return gl(n), (n._dirty || !n._cacheable) && (n._dirty = !1, n._value = n.effect.run()), n._value;
  }
  set value(n) {
    this._setter(n);
  }
}
bl = "__v_isReadonly";
function jc(e, n, s = !1) {
  let a, l;
  const u = it(e);
  return u ? (a = e, l = Me) : (a = e.get, l = e.set), new Fc(a, l, u || !l, s);
}
function ai(e, n, s, a) {
  let l;
  try {
    l = a ? e(...a) : e();
  } catch (u) {
    Io(u, n, s);
  }
  return l;
}
function ve(e, n, s, a) {
  if (it(e)) {
    const u = ai(e, n, s, a);
    return u && il(u) && u.catch((c) => {
      Io(c, n, s);
    }), u;
  }
  const l = [];
  for (let u = 0; u < e.length; u++)
    l.push(ve(e[u], n, s, a));
  return l;
}
function Io(e, n, s, a = !0) {
  const l = n ? n.vnode : null;
  if (n) {
    let u = n.parent;
    const c = n.proxy, f = s;
    for (; u; ) {
      const g = u.ec;
      if (g) {
        for (let b = 0; b < g.length; b++)
          if (g[b](e, c, f) === !1)
            return;
      }
      u = u.parent;
    }
    const p = n.appContext.config.errorHandler;
    if (p) {
      ai(p, null, 10, [e, c, f]);
      return;
    }
  }
  Hc(e, s, l, a);
}
function Hc(e, n, s, a = !0) {
  console.error(e);
}
let Cn = !1, Os = !1;
const ee = [];
let Ze = 0;
const Ki = [];
let Ve = null, Ti = 0;
const wl = /* @__PURE__ */ Promise.resolve();
let Js = null;
function Et(e) {
  const n = Js || wl;
  return e ? n.then(this ? e.bind(this) : e) : n;
}
function $c(e) {
  let n = Ze + 1, s = ee.length;
  for (; n < s; ) {
    const a = n + s >>> 1;
    On(ee[a]) < e ? n = a + 1 : s = a;
  }
  return n;
}
function Xs(e) {
  (!ee.length || !ee.includes(e, Cn && e.allowRecurse ? Ze + 1 : Ze)) && (e.id == null ? ee.push(e) : ee.splice($c(e.id), 0, e), xl());
}
function xl() {
  !Cn && !Os && (Os = !0, Js = wl.then(Pl));
}
function Wc(e) {
  const n = ee.indexOf(e);
  n > Ze && ee.splice(n, 1);
}
function Uc(e) {
  J(e) ? Ki.push(...e) : (!Ve || !Ve.includes(e, e.allowRecurse ? Ti + 1 : Ti)) && Ki.push(e), xl();
}
function ya(e, n = Cn ? Ze + 1 : 0) {
  for (; n < ee.length; n++) {
    const s = ee[n];
    s && s.pre && (ee.splice(n, 1), n--, s());
  }
}
function Ll(e) {
  if (Ki.length) {
    const n = [...new Set(Ki)];
    if (Ki.length = 0, Ve) {
      Ve.push(...n);
      return;
    }
    for (Ve = n, Ve.sort((s, a) => On(s) - On(a)), Ti = 0; Ti < Ve.length; Ti++)
      Ve[Ti]();
    Ve = null, Ti = 0;
  }
}
const On = (e) => e.id == null ? 1 / 0 : e.id, Vc = (e, n) => {
  const s = On(e) - On(n);
  if (s === 0) {
    if (e.pre && !n.pre)
      return -1;
    if (n.pre && !e.pre)
      return 1;
  }
  return s;
};
function Pl(e) {
  Os = !1, Cn = !0, ee.sort(Vc);
  const n = Me;
  try {
    for (Ze = 0; Ze < ee.length; Ze++) {
      const s = ee[Ze];
      s && s.active !== !1 && ai(
        s,
        null,
        14
        /* ErrorCodes.SCHEDULER */
      );
    }
  } finally {
    Ze = 0, ee.length = 0, Ll(), Cn = !1, Js = null, (ee.length || Ki.length) && Pl();
  }
}
function Kc(e, n, ...s) {
  if (e.isUnmounted)
    return;
  const a = e.vnode.props || Ot;
  let l = s;
  const u = n.startsWith("update:"), c = u && n.slice(7);
  if (c && c in a) {
    const b = `${c === "modelValue" ? "model" : c}Modifiers`, { number: x, trim: T } = a[b] || Ot;
    T && (l = s.map((k) => Zt(k) ? k.trim() : k)), x && (l = s.map(xs));
  }
  let f, p = a[f = co(n)] || // also try camelCase event handler (#2249)
  a[f = co(Re(n))];
  !p && u && (p = a[f = co(Yi(n))]), p && ve(p, e, 6, l);
  const g = a[f + "Once"];
  if (g) {
    if (!e.emitted)
      e.emitted = {};
    else if (e.emitted[f])
      return;
    e.emitted[f] = !0, ve(g, e, 6, l);
  }
}
function Tl(e, n, s = !1) {
  const a = n.emitsCache, l = a.get(e);
  if (l !== void 0)
    return l;
  const u = e.emits;
  let c = {}, f = !1;
  if (!it(e)) {
    const p = (g) => {
      const b = Tl(g, n, !0);
      b && (f = !0, Xt(c, b));
    };
    !s && n.mixins.length && n.mixins.forEach(p), e.extends && p(e.extends), e.mixins && e.mixins.forEach(p);
  }
  return !u && !f ? (St(e) && a.set(e, null), null) : (J(u) ? u.forEach((p) => c[p] = null) : Xt(c, u), St(e) && a.set(e, c), c);
}
function ko(e, n) {
  return !e || !Co(n) ? !1 : (n = n.slice(2).replace(/Once$/, ""), ct(e, n[0].toLowerCase() + n.slice(1)) || ct(e, Yi(n)) || ct(e, n));
}
let Jt = null, Cl = null;
function yo(e) {
  const n = Jt;
  return Jt = e, Cl = e && e.type.__scopeId || null, n;
}
function Ol(e, n = Jt, s) {
  if (!n || e._n)
    return e;
  const a = (...l) => {
    a._d && Ma(-1);
    const u = yo(n);
    let c;
    try {
      c = e(...l);
    } finally {
      yo(u), a._d && Ma(1);
    }
    return c;
  };
  return a._n = !0, a._c = !0, a._d = !0, a;
}
function ps(e) {
  const { type: n, vnode: s, proxy: a, withProxy: l, props: u, propsOptions: [c], slots: f, attrs: p, emit: g, render: b, renderCache: x, data: T, setupState: k, ctx: U, inheritAttrs: E } = e;
  let tt, X;
  const xt = yo(e);
  try {
    if (s.shapeFlag & 4) {
      const rt = l || a;
      tt = Ne(b.call(rt, rt, x, u, k, T, U)), X = p;
    } else {
      const rt = n;
      tt = Ne(rt.length > 1 ? rt(u, { attrs: p, slots: f, emit: g }) : rt(
        u,
        null
        /* we know it doesn't need it */
      )), X = n.props ? p : qc(p);
    }
  } catch (rt) {
    xn.length = 0, Io(
      rt,
      e,
      1
      /* ErrorCodes.RENDER_FUNCTION */
    ), tt = ne(ye);
  }
  let D = tt;
  if (X && E !== !1) {
    const rt = Object.keys(X), { shapeFlag: gt } = D;
    rt.length && gt & 7 && (c && rt.some(Fs) && (X = Gc(X, c)), D = ui(D, X));
  }
  return s.dirs && (D = ui(D), D.dirs = D.dirs ? D.dirs.concat(s.dirs) : s.dirs), s.transition && (D.transition = s.transition), tt = D, yo(xt), tt;
}
const qc = (e) => {
  let n;
  for (const s in e)
    (s === "class" || s === "style" || Co(s)) && ((n || (n = {}))[s] = e[s]);
  return n;
}, Gc = (e, n) => {
  const s = {};
  for (const a in e)
    (!Fs(a) || !(a.slice(9) in n)) && (s[a] = e[a]);
  return s;
};
function Yc(e, n, s) {
  const { props: a, children: l, component: u } = e, { props: c, children: f, patchFlag: p } = n, g = u.emitsOptions;
  if (n.dirs || n.transition)
    return !0;
  if (s && p >= 0) {
    if (p & 1024)
      return !0;
    if (p & 16)
      return a ? ba(a, c, g) : !!c;
    if (p & 8) {
      const b = n.dynamicProps;
      for (let x = 0; x < b.length; x++) {
        const T = b[x];
        if (c[T] !== a[T] && !ko(g, T))
          return !0;
      }
    }
  } else
    return (l || f) && (!f || !f.$stable) ? !0 : a === c ? !1 : a ? c ? ba(a, c, g) : !0 : !!c;
  return !1;
}
function ba(e, n, s) {
  const a = Object.keys(n);
  if (a.length !== Object.keys(e).length)
    return !0;
  for (let l = 0; l < a.length; l++) {
    const u = a[l];
    if (n[u] !== e[u] && !ko(s, u))
      return !0;
  }
  return !1;
}
function Jc({ vnode: e, parent: n }, s) {
  for (; n && n.subTree === e; )
    (e = n.vnode).el = s, n = n.parent;
}
const Xc = (e) => e.__isSuspense;
function Qc(e, n) {
  n && n.pendingBranch ? J(e) ? n.effects.push(...e) : n.effects.push(e) : Uc(e);
}
function ge(e, n) {
  if (Ht) {
    let s = Ht.provides;
    const a = Ht.parent && Ht.parent.provides;
    a === s && (s = Ht.provides = Object.create(a)), s[e] = n;
  }
}
function Pt(e, n, s = !1) {
  const a = Ht || Jt;
  if (a) {
    const l = a.parent == null ? a.vnode.appContext && a.vnode.appContext.provides : a.parent.provides;
    if (l && e in l)
      return l[e];
    if (arguments.length > 1)
      return s && it(n) ? n.call(a.proxy) : n;
  }
}
const ao = {};
function yn(e, n, s) {
  return Sl(e, n, s);
}
function Sl(e, n, { immediate: s, deep: a, flush: l, onTrack: u, onTrigger: c } = Ot) {
  const f = lc() === (Ht == null ? void 0 : Ht.scope) ? Ht : null;
  let p, g = !1, b = !1;
  if (ie(e) ? (p = () => e.value, g = vo(e)) : Vi(e) ? (p = () => e, a = !0) : J(e) ? (b = !0, g = e.some((D) => Vi(D) || vo(D)), p = () => e.map((D) => {
    if (ie(D))
      return D.value;
    if (Vi(D))
      return Si(D);
    if (it(D))
      return ai(
        D,
        f,
        2
        /* ErrorCodes.WATCH_GETTER */
      );
  })) : it(e) ? n ? p = () => ai(
    e,
    f,
    2
    /* ErrorCodes.WATCH_GETTER */
  ) : p = () => {
    if (!(f && f.isUnmounted))
      return x && x(), ve(e, f, 3, [T]);
  } : p = Me, n && a) {
    const D = p;
    p = () => Si(D());
  }
  let x, T = (D) => {
    x = X.onStop = () => {
      ai(
        D,
        f,
        4
        /* ErrorCodes.WATCH_CLEANUP */
      );
    };
  }, k;
  if (Mn)
    if (T = Me, n ? s && ve(n, f, 3, [
      p(),
      b ? [] : void 0,
      T
    ]) : p(), l === "sync") {
      const D = qf();
      k = D.__watcherHandles || (D.__watcherHandles = []);
    } else
      return Me;
  let U = b ? new Array(e.length).fill(ao) : ao;
  const E = () => {
    if (X.active)
      if (n) {
        const D = X.run();
        (a || g || (b ? D.some((rt, gt) => Pn(rt, U[gt])) : Pn(D, U))) && (x && x(), ve(n, f, 3, [
          D,
          // pass undefined as the old value when it's changed for the first time
          U === ao ? void 0 : b && U[0] === ao ? [] : U,
          T
        ]), U = D);
      } else
        X.run();
  };
  E.allowRecurse = !!n;
  let tt;
  l === "sync" ? tt = E : l === "post" ? tt = () => he(E, f && f.suspense) : (E.pre = !0, f && (E.id = f.uid), tt = () => Xs(E));
  const X = new Us(p, tt);
  n ? s ? E() : U = X.run() : l === "post" ? he(X.run.bind(X), f && f.suspense) : X.run();
  const xt = () => {
    X.stop(), f && f.scope && js(f.scope.effects, X);
  };
  return k && k.push(xt), xt;
}
function tf(e, n, s) {
  const a = this.proxy, l = Zt(e) ? e.includes(".") ? Ml(a, e) : () => a[e] : e.bind(a, a);
  let u;
  it(n) ? u = n : (u = n.handler, s = n);
  const c = Ht;
  Gi(this);
  const f = Sl(l, u.bind(a), s);
  return c ? Gi(c) : Ei(), f;
}
function Ml(e, n) {
  const s = n.split(".");
  return () => {
    let a = e;
    for (let l = 0; l < s.length && a; l++)
      a = a[s[l]];
    return a;
  };
}
function Si(e, n) {
  if (!St(e) || e.__v_skip || (n = n || /* @__PURE__ */ new Set(), n.has(e)))
    return e;
  if (n.add(e), ie(e))
    Si(e.value, n);
  else if (J(e))
    for (let s = 0; s < e.length; s++)
      Si(e[s], n);
  else if (el(e) || Ui(e))
    e.forEach((s) => {
      Si(s, n);
    });
  else if (ol(e))
    for (const s in e)
      Si(e[s], n);
  return e;
}
function ef() {
  const e = {
    isMounted: !1,
    isLeaving: !1,
    isUnmounting: !1,
    leavingVNodes: /* @__PURE__ */ new Map()
  };
  return At(() => {
    e.isMounted = !0;
  }), Ii(() => {
    e.isUnmounting = !0;
  }), e;
}
const _e = [Function, Array], nf = {
  name: "BaseTransition",
  props: {
    mode: String,
    appear: Boolean,
    persisted: Boolean,
    // enter
    onBeforeEnter: _e,
    onEnter: _e,
    onAfterEnter: _e,
    onEnterCancelled: _e,
    // leave
    onBeforeLeave: _e,
    onLeave: _e,
    onAfterLeave: _e,
    onLeaveCancelled: _e,
    // appear
    onBeforeAppear: _e,
    onAppear: _e,
    onAfterAppear: _e,
    onAppearCancelled: _e
  },
  setup(e, { slots: n }) {
    const s = Ff(), a = ef();
    let l;
    return () => {
      const u = n.default && Il(n.default(), !0);
      if (!u || !u.length)
        return;
      let c = u[0];
      if (u.length > 1) {
        for (const E of u)
          if (E.type !== ye) {
            c = E;
            break;
          }
      }
      const f = pt(e), { mode: p } = f;
      if (a.isLeaving)
        return ms(c);
      const g = wa(c);
      if (!g)
        return ms(c);
      const b = Ss(g, f, a, s);
      Ms(g, b);
      const x = s.subTree, T = x && wa(x);
      let k = !1;
      const { getTransitionKey: U } = g.type;
      if (U) {
        const E = U();
        l === void 0 ? l = E : E !== l && (l = E, k = !0);
      }
      if (T && T.type !== ye && (!Ci(g, T) || k)) {
        const E = Ss(T, f, a, s);
        if (Ms(T, E), p === "out-in")
          return a.isLeaving = !0, E.afterLeave = () => {
            a.isLeaving = !1, s.update.active !== !1 && s.update();
          }, ms(c);
        p === "in-out" && g.type !== ye && (E.delayLeave = (tt, X, xt) => {
          const D = Al(a, T);
          D[String(T.key)] = T, tt._leaveCb = () => {
            X(), tt._leaveCb = void 0, delete b.delayedLeave;
          }, b.delayedLeave = xt;
        });
      }
      return c;
    };
  }
}, El = nf;
function Al(e, n) {
  const { leavingVNodes: s } = e;
  let a = s.get(n.type);
  return a || (a = /* @__PURE__ */ Object.create(null), s.set(n.type, a)), a;
}
function Ss(e, n, s, a) {
  const { appear: l, mode: u, persisted: c = !1, onBeforeEnter: f, onEnter: p, onAfterEnter: g, onEnterCancelled: b, onBeforeLeave: x, onLeave: T, onAfterLeave: k, onLeaveCancelled: U, onBeforeAppear: E, onAppear: tt, onAfterAppear: X, onAppearCancelled: xt } = n, D = String(e.key), rt = Al(s, e), gt = (Q, j) => {
    Q && ve(Q, a, 9, j);
  }, Kt = (Q, j) => {
    const V = j[1];
    gt(Q, j), J(Q) ? Q.every((ut) => ut.length <= 1) && V() : Q.length <= 1 && V();
  }, qt = {
    mode: u,
    persisted: c,
    beforeEnter(Q) {
      let j = f;
      if (!s.isMounted)
        if (l)
          j = E || f;
        else
          return;
      Q._leaveCb && Q._leaveCb(
        !0
        /* cancelled */
      );
      const V = rt[D];
      V && Ci(e, V) && V.el._leaveCb && V.el._leaveCb(), gt(j, [Q]);
    },
    enter(Q) {
      let j = p, V = g, ut = b;
      if (!s.isMounted)
        if (l)
          j = tt || p, V = X || g, ut = xt || b;
        else
          return;
      let R = !1;
      const Tt = Q._enterCb = (It) => {
        R || (R = !0, It ? gt(ut, [Q]) : gt(V, [Q]), qt.delayedLeave && qt.delayedLeave(), Q._enterCb = void 0);
      };
      j ? Kt(j, [Q, Tt]) : Tt();
    },
    leave(Q, j) {
      const V = String(e.key);
      if (Q._enterCb && Q._enterCb(
        !0
        /* cancelled */
      ), s.isUnmounting)
        return j();
      gt(x, [Q]);
      let ut = !1;
      const R = Q._leaveCb = (Tt) => {
        ut || (ut = !0, j(), Tt ? gt(U, [Q]) : gt(k, [Q]), Q._leaveCb = void 0, rt[V] === e && delete rt[V]);
      };
      rt[V] = e, T ? Kt(T, [Q, R]) : R();
    },
    clone(Q) {
      return Ss(Q, n, s, a);
    }
  };
  return qt;
}
function ms(e) {
  if (Bo(e))
    return e = ui(e), e.children = null, e;
}
function wa(e) {
  return Bo(e) ? e.children ? e.children[0] : void 0 : e;
}
function Ms(e, n) {
  e.shapeFlag & 6 && e.component ? Ms(e.component.subTree, n) : e.shapeFlag & 128 ? (e.ssContent.transition = n.clone(e.ssContent), e.ssFallback.transition = n.clone(e.ssFallback)) : e.transition = n;
}
function Il(e, n = !1, s) {
  let a = [], l = 0;
  for (let u = 0; u < e.length; u++) {
    let c = e[u];
    const f = s == null ? c.key : String(s) + String(c.key != null ? c.key : u);
    c.type === ae ? (c.patchFlag & 128 && l++, a = a.concat(Il(c.children, n, f))) : (n || c.type !== ye) && a.push(f != null ? ui(c, { key: f }) : c);
  }
  if (l > 1)
    for (let u = 0; u < a.length; u++)
      a[u].patchFlag = -2;
  return a;
}
function Rt(e) {
  return it(e) ? { setup: e, name: e.name } : e;
}
const bn = (e) => !!e.type.__asyncLoader, Bo = (e) => e.type.__isKeepAlive;
function of(e, n) {
  kl(e, "a", n);
}
function sf(e, n) {
  kl(e, "da", n);
}
function kl(e, n, s = Ht) {
  const a = e.__wdc || (e.__wdc = () => {
    let l = s;
    for (; l; ) {
      if (l.isDeactivated)
        return;
      l = l.parent;
    }
    return e();
  });
  if (zo(n, a, s), s) {
    let l = s.parent;
    for (; l && l.parent; )
      Bo(l.parent.vnode) && rf(a, n, s, l), l = l.parent;
  }
}
function rf(e, n, s, a) {
  const l = zo(
    n,
    e,
    a,
    !0
    /* prepend */
  );
  En(() => {
    js(a[n], l);
  }, s);
}
function zo(e, n, s = Ht, a = !1) {
  if (s) {
    const l = s[e] || (s[e] = []), u = n.__weh || (n.__weh = (...c) => {
      if (s.isUnmounted)
        return;
      Ji(), Gi(s);
      const f = ve(n, s, e, c);
      return Ei(), Xi(), f;
    });
    return a ? l.unshift(u) : l.push(u), u;
  }
}
const qe = (e) => (n, s = Ht) => (
  // post-create lifecycle registrations are noops during SSR (except for serverPrefetch)
  (!Mn || e === "sp") && zo(e, (...a) => n(...a), s)
), af = qe(
  "bm"
  /* LifecycleHooks.BEFORE_MOUNT */
), At = qe(
  "m"
  /* LifecycleHooks.MOUNTED */
), lf = qe(
  "bu"
  /* LifecycleHooks.BEFORE_UPDATE */
), uf = qe(
  "u"
  /* LifecycleHooks.UPDATED */
), Ii = qe(
  "bum"
  /* LifecycleHooks.BEFORE_UNMOUNT */
), En = qe(
  "um"
  /* LifecycleHooks.UNMOUNTED */
), hf = qe(
  "sp"
  /* LifecycleHooks.SERVER_PREFETCH */
), cf = qe(
  "rtg"
  /* LifecycleHooks.RENDER_TRIGGERED */
), ff = qe(
  "rtc"
  /* LifecycleHooks.RENDER_TRACKED */
);
function df(e, n = Ht) {
  zo("ec", e, n);
}
function po(e, n) {
  const s = Jt;
  if (s === null)
    return e;
  const a = Ro(s) || s.proxy, l = e.dirs || (e.dirs = []);
  for (let u = 0; u < n.length; u++) {
    let [c, f, p, g = Ot] = n[u];
    c && (it(c) && (c = {
      mounted: c,
      updated: c
    }), c.deep && Si(f), l.push({
      dir: c,
      instance: a,
      value: f,
      oldValue: void 0,
      arg: p,
      modifiers: g
    }));
  }
  return e;
}
function bi(e, n, s, a) {
  const l = e.dirs, u = n && n.dirs;
  for (let c = 0; c < l.length; c++) {
    const f = l[c];
    u && (f.oldValue = u[c].value);
    let p = f.dir[a];
    p && (Ji(), ve(p, s, 8, [
      e.el,
      f,
      e,
      n
    ]), Xi());
  }
}
const Qs = "components", pf = "directives";
function mf(e, n) {
  return tr(Qs, e, !0, n) || e;
}
const Bl = Symbol();
function _s(e) {
  return Zt(e) ? tr(Qs, e, !1) || e : e || Bl;
}
function _f(e) {
  return tr(pf, e);
}
function tr(e, n, s = !0, a = !1) {
  const l = Jt || Ht;
  if (l) {
    const u = l.type;
    if (e === Qs) {
      const f = Uf(
        u,
        !1
        /* do not include inferred name to avoid breaking existing code */
      );
      if (f && (f === n || f === Re(n) || f === Mo(Re(n))))
        return u;
    }
    const c = (
      // local registration
      // check instance[type] first which is resolved for options API
      xa(l[e] || u[e], n) || // global registration
      xa(l.appContext[e], n)
    );
    return !c && a ? u : c;
  }
}
function xa(e, n) {
  return e && (e[n] || e[Re(n)] || e[Mo(Re(n))]);
}
function Es(e, n, s, a) {
  let l;
  const u = s && s[a];
  if (J(e) || Zt(e)) {
    l = new Array(e.length);
    for (let c = 0, f = e.length; c < f; c++)
      l[c] = n(e[c], c, void 0, u && u[c]);
  } else if (typeof e == "number") {
    l = new Array(e);
    for (let c = 0; c < e; c++)
      l[c] = n(c + 1, c, void 0, u && u[c]);
  } else if (St(e))
    if (e[Symbol.iterator])
      l = Array.from(e, (c, f) => n(c, f, void 0, u && u[f]));
    else {
      const c = Object.keys(e);
      l = new Array(c.length);
      for (let f = 0, p = c.length; f < p; f++) {
        const g = c[f];
        l[f] = n(e[g], g, f, u && u[f]);
      }
    }
  else
    l = [];
  return s && (s[a] = l), l;
}
function Pe(e, n, s = {}, a, l) {
  if (Jt.isCE || Jt.parent && bn(Jt.parent) && Jt.parent.isCE)
    return n !== "default" && (s.name = n), ne("slot", s, a && a());
  let u = e[n];
  u && u._c && (u._d = !1), Vt();
  const c = u && zl(u(s)), f = Ln(
    ae,
    {
      key: s.key || // slot content array of a dynamic conditional slot may have a branch
      // key attached in the `createSlots` helper, respect that
      c && c.key || `_${n}`
    },
    c || (a ? a() : []),
    c && e._ === 1 ? 64 : -2
    /* PatchFlags.BAIL */
  );
  return !l && f.scopeId && (f.slotScopeIds = [f.scopeId + "-s"]), u && u._c && (u._d = !0), f;
}
function zl(e) {
  return e.some((n) => wo(n) ? !(n.type === ye || n.type === ae && !zl(n.children)) : !0) ? e : null;
}
function gf(e, n) {
  const s = {};
  for (const a in e)
    s[n && /[A-Z]/.test(a) ? `on:${a}` : co(a)] = e[a];
  return s;
}
const As = (e) => e ? Vl(e) ? Ro(e) || e.proxy : As(e.parent) : null, wn = (
  // Move PURE marker to new line to workaround compiler discarding it
  // due to type annotation
  /* @__PURE__ */ Xt(/* @__PURE__ */ Object.create(null), {
    $: (e) => e,
    $el: (e) => e.vnode.el,
    $data: (e) => e.data,
    $props: (e) => e.props,
    $attrs: (e) => e.attrs,
    $slots: (e) => e.slots,
    $refs: (e) => e.refs,
    $parent: (e) => As(e.parent),
    $root: (e) => As(e.root),
    $emit: (e) => e.emit,
    $options: (e) => er(e),
    $forceUpdate: (e) => e.f || (e.f = () => Xs(e.update)),
    $nextTick: (e) => e.n || (e.n = Et.bind(e.proxy)),
    $watch: (e) => tf.bind(e)
  })
), gs = (e, n) => e !== Ot && !e.__isScriptSetup && ct(e, n), vf = {
  get({ _: e }, n) {
    const { ctx: s, setupState: a, data: l, props: u, accessCache: c, type: f, appContext: p } = e;
    let g;
    if (n[0] !== "$") {
      const k = c[n];
      if (k !== void 0)
        switch (k) {
          case 1:
            return a[n];
          case 2:
            return l[n];
          case 4:
            return s[n];
          case 3:
            return u[n];
        }
      else {
        if (gs(a, n))
          return c[n] = 1, a[n];
        if (l !== Ot && ct(l, n))
          return c[n] = 2, l[n];
        if (
          // only cache other properties when instance has declared (thus stable)
          // props
          (g = e.propsOptions[0]) && ct(g, n)
        )
          return c[n] = 3, u[n];
        if (s !== Ot && ct(s, n))
          return c[n] = 4, s[n];
        Is && (c[n] = 0);
      }
    }
    const b = wn[n];
    let x, T;
    if (b)
      return n === "$attrs" && ce(e, "get", n), b(e);
    if (
      // css module (injected by vue-loader)
      (x = f.__cssModules) && (x = x[n])
    )
      return x;
    if (s !== Ot && ct(s, n))
      return c[n] = 4, s[n];
    if (
      // global properties
      T = p.config.globalProperties, ct(T, n)
    )
      return T[n];
  },
  set({ _: e }, n, s) {
    const { data: a, setupState: l, ctx: u } = e;
    return gs(l, n) ? (l[n] = s, !0) : a !== Ot && ct(a, n) ? (a[n] = s, !0) : ct(e.props, n) || n[0] === "$" && n.slice(1) in e ? !1 : (u[n] = s, !0);
  },
  has({ _: { data: e, setupState: n, accessCache: s, ctx: a, appContext: l, propsOptions: u } }, c) {
    let f;
    return !!s[c] || e !== Ot && ct(e, c) || gs(n, c) || (f = u[0]) && ct(f, c) || ct(a, c) || ct(wn, c) || ct(l.config.globalProperties, c);
  },
  defineProperty(e, n, s) {
    return s.get != null ? e._.accessCache[n] = 0 : ct(s, "value") && this.set(e, n, s.value, null), Reflect.defineProperty(e, n, s);
  }
};
let Is = !0;
function yf(e) {
  const n = er(e), s = e.proxy, a = e.ctx;
  Is = !1, n.beforeCreate && La(
    n.beforeCreate,
    e,
    "bc"
    /* LifecycleHooks.BEFORE_CREATE */
  );
  const {
    // state
    data: l,
    computed: u,
    methods: c,
    watch: f,
    provide: p,
    inject: g,
    // lifecycle
    created: b,
    beforeMount: x,
    mounted: T,
    beforeUpdate: k,
    updated: U,
    activated: E,
    deactivated: tt,
    beforeDestroy: X,
    beforeUnmount: xt,
    destroyed: D,
    unmounted: rt,
    render: gt,
    renderTracked: Kt,
    renderTriggered: qt,
    errorCaptured: Q,
    serverPrefetch: j,
    // public API
    expose: V,
    inheritAttrs: ut,
    // assets
    components: R,
    directives: Tt,
    filters: It
  } = n;
  if (g && bf(g, a, null, e.appContext.config.unwrapInjectedRef), c)
    for (const bt in c) {
      const Z = c[bt];
      it(Z) && (a[bt] = Z.bind(s));
    }
  if (l) {
    const bt = l.call(s, s);
    St(bt) && (e.data = Ao(bt));
  }
  if (Is = !0, u)
    for (const bt in u) {
      const Z = u[bt], Ee = it(Z) ? Z.bind(s, s) : it(Z.get) ? Z.get.bind(s, s) : Me, K = !it(Z) && it(Z.set) ? Z.set.bind(s) : Me, mt = gn({
        get: Ee,
        set: K
      });
      Object.defineProperty(a, bt, {
        enumerable: !0,
        configurable: !0,
        get: () => mt.value,
        set: (Ct) => mt.value = Ct
      });
    }
  if (f)
    for (const bt in f)
      Nl(f[bt], a, s, bt);
  if (p) {
    const bt = it(p) ? p.call(s) : p;
    Reflect.ownKeys(bt).forEach((Z) => {
      ge(Z, bt[Z]);
    });
  }
  b && La(
    b,
    e,
    "c"
    /* LifecycleHooks.CREATED */
  );
  function ft(bt, Z) {
    J(Z) ? Z.forEach((Ee) => bt(Ee.bind(s))) : Z && bt(Z.bind(s));
  }
  if (ft(af, x), ft(At, T), ft(lf, k), ft(uf, U), ft(of, E), ft(sf, tt), ft(df, Q), ft(ff, Kt), ft(cf, qt), ft(Ii, xt), ft(En, rt), ft(hf, j), J(V))
    if (V.length) {
      const bt = e.exposed || (e.exposed = {});
      V.forEach((Z) => {
        Object.defineProperty(bt, Z, {
          get: () => s[Z],
          set: (Ee) => s[Z] = Ee
        });
      });
    } else
      e.exposed || (e.exposed = {});
  gt && e.render === Me && (e.render = gt), ut != null && (e.inheritAttrs = ut), R && (e.components = R), Tt && (e.directives = Tt);
}
function bf(e, n, s = Me, a = !1) {
  J(e) && (e = ks(e));
  for (const l in e) {
    const u = e[l];
    let c;
    St(u) ? "default" in u ? c = Pt(
      u.from || l,
      u.default,
      !0
      /* treat default function as factory */
    ) : c = Pt(u.from || l) : c = Pt(u), ie(c) && a ? Object.defineProperty(n, l, {
      enumerable: !0,
      configurable: !0,
      get: () => c.value,
      set: (f) => c.value = f
    }) : n[l] = c;
  }
}
function La(e, n, s) {
  ve(J(e) ? e.map((a) => a.bind(n.proxy)) : e.bind(n.proxy), n, s);
}
function Nl(e, n, s, a) {
  const l = a.includes(".") ? Ml(s, a) : () => s[a];
  if (Zt(e)) {
    const u = n[e];
    it(u) && yn(l, u);
  } else if (it(e))
    yn(l, e.bind(s));
  else if (St(e))
    if (J(e))
      e.forEach((u) => Nl(u, n, s, a));
    else {
      const u = it(e.handler) ? e.handler.bind(s) : n[e.handler];
      it(u) && yn(l, u, e);
    }
}
function er(e) {
  const n = e.type, { mixins: s, extends: a } = n, { mixins: l, optionsCache: u, config: { optionMergeStrategies: c } } = e.appContext, f = u.get(n);
  let p;
  return f ? p = f : !l.length && !s && !a ? p = n : (p = {}, l.length && l.forEach((g) => bo(p, g, c, !0)), bo(p, n, c)), St(n) && u.set(n, p), p;
}
function bo(e, n, s, a = !1) {
  const { mixins: l, extends: u } = n;
  u && bo(e, u, s, !0), l && l.forEach((c) => bo(e, c, s, !0));
  for (const c in n)
    if (!(a && c === "expose")) {
      const f = wf[c] || s && s[c];
      e[c] = f ? f(e[c], n[c]) : n[c];
    }
  return e;
}
const wf = {
  data: Pa,
  props: Pi,
  emits: Pi,
  // objects
  methods: Pi,
  computed: Pi,
  // lifecycle
  beforeCreate: se,
  created: se,
  beforeMount: se,
  mounted: se,
  beforeUpdate: se,
  updated: se,
  beforeDestroy: se,
  beforeUnmount: se,
  destroyed: se,
  unmounted: se,
  activated: se,
  deactivated: se,
  errorCaptured: se,
  serverPrefetch: se,
  // assets
  components: Pi,
  directives: Pi,
  // watch
  watch: Lf,
  // provide / inject
  provide: Pa,
  inject: xf
};
function Pa(e, n) {
  return n ? e ? function() {
    return Xt(it(e) ? e.call(this, this) : e, it(n) ? n.call(this, this) : n);
  } : n : e;
}
function xf(e, n) {
  return Pi(ks(e), ks(n));
}
function ks(e) {
  if (J(e)) {
    const n = {};
    for (let s = 0; s < e.length; s++)
      n[e[s]] = e[s];
    return n;
  }
  return e;
}
function se(e, n) {
  return e ? [...new Set([].concat(e, n))] : n;
}
function Pi(e, n) {
  return e ? Xt(Xt(/* @__PURE__ */ Object.create(null), e), n) : n;
}
function Lf(e, n) {
  if (!e)
    return n;
  if (!n)
    return e;
  const s = Xt(/* @__PURE__ */ Object.create(null), e);
  for (const a in n)
    s[a] = se(e[a], n[a]);
  return s;
}
function Pf(e, n, s, a = !1) {
  const l = {}, u = {};
  go(u, Zo, 1), e.propsDefaults = /* @__PURE__ */ Object.create(null), Zl(e, n, l, u);
  for (const c in e.propsOptions[0])
    c in l || (l[c] = void 0);
  s ? e.props = a ? l : zc(l) : e.type.props ? e.props = l : e.props = u, e.attrs = u;
}
function Tf(e, n, s, a) {
  const { props: l, attrs: u, vnode: { patchFlag: c } } = e, f = pt(l), [p] = e.propsOptions;
  let g = !1;
  if (
    // always force full diff in dev
    // - #1942 if hmr is enabled with sfc component
    // - vite#872 non-sfc component used by sfc component
    (a || c > 0) && !(c & 16)
  ) {
    if (c & 8) {
      const b = e.vnode.dynamicProps;
      for (let x = 0; x < b.length; x++) {
        let T = b[x];
        if (ko(e.emitsOptions, T))
          continue;
        const k = n[T];
        if (p)
          if (ct(u, T))
            k !== u[T] && (u[T] = k, g = !0);
          else {
            const U = Re(T);
            l[U] = Bs(
              p,
              f,
              U,
              k,
              e,
              !1
              /* isAbsent */
            );
          }
        else
          k !== u[T] && (u[T] = k, g = !0);
      }
    }
  } else {
    Zl(e, n, l, u) && (g = !0);
    let b;
    for (const x in f)
      (!n || // for camelCase
      !ct(n, x) && // it's possible the original props was passed in as kebab-case
      // and converted to camelCase (#955)
      ((b = Yi(x)) === x || !ct(n, b))) && (p ? s && // for camelCase
      (s[x] !== void 0 || // for kebab-case
      s[b] !== void 0) && (l[x] = Bs(
        p,
        f,
        x,
        void 0,
        e,
        !0
        /* isAbsent */
      )) : delete l[x]);
    if (u !== f)
      for (const x in u)
        (!n || !ct(n, x)) && (delete u[x], g = !0);
  }
  g && Ke(e, "set", "$attrs");
}
function Zl(e, n, s, a) {
  const [l, u] = e.propsOptions;
  let c = !1, f;
  if (n)
    for (let p in n) {
      if (ho(p))
        continue;
      const g = n[p];
      let b;
      l && ct(l, b = Re(p)) ? !u || !u.includes(b) ? s[b] = g : (f || (f = {}))[b] = g : ko(e.emitsOptions, p) || (!(p in a) || g !== a[p]) && (a[p] = g, c = !0);
    }
  if (u) {
    const p = pt(s), g = f || Ot;
    for (let b = 0; b < u.length; b++) {
      const x = u[b];
      s[x] = Bs(l, p, x, g[x], e, !ct(g, x));
    }
  }
  return c;
}
function Bs(e, n, s, a, l, u) {
  const c = e[s];
  if (c != null) {
    const f = ct(c, "default");
    if (f && a === void 0) {
      const p = c.default;
      if (c.type !== Function && it(p)) {
        const { propsDefaults: g } = l;
        s in g ? a = g[s] : (Gi(l), a = g[s] = p.call(null, n), Ei());
      } else
        a = p;
    }
    c[
      0
      /* BooleanFlags.shouldCast */
    ] && (u && !f ? a = !1 : c[
      1
      /* BooleanFlags.shouldCastTrue */
    ] && (a === "" || a === Yi(s)) && (a = !0));
  }
  return a;
}
function Rl(e, n, s = !1) {
  const a = n.propsCache, l = a.get(e);
  if (l)
    return l;
  const u = e.props, c = {}, f = [];
  let p = !1;
  if (!it(e)) {
    const b = (x) => {
      p = !0;
      const [T, k] = Rl(x, n, !0);
      Xt(c, T), k && f.push(...k);
    };
    !s && n.mixins.length && n.mixins.forEach(b), e.extends && b(e.extends), e.mixins && e.mixins.forEach(b);
  }
  if (!u && !p)
    return St(e) && a.set(e, Wi), Wi;
  if (J(u))
    for (let b = 0; b < u.length; b++) {
      const x = Re(u[b]);
      Ta(x) && (c[x] = Ot);
    }
  else if (u)
    for (const b in u) {
      const x = Re(b);
      if (Ta(x)) {
        const T = u[b], k = c[x] = J(T) || it(T) ? { type: T } : Object.assign({}, T);
        if (k) {
          const U = Sa(Boolean, k.type), E = Sa(String, k.type);
          k[
            0
            /* BooleanFlags.shouldCast */
          ] = U > -1, k[
            1
            /* BooleanFlags.shouldCastTrue */
          ] = E < 0 || U < E, (U > -1 || ct(k, "default")) && f.push(x);
        }
      }
    }
  const g = [c, f];
  return St(e) && a.set(e, g), g;
}
function Ta(e) {
  return e[0] !== "$";
}
function Ca(e) {
  const n = e && e.toString().match(/^\s*(function|class) (\w+)/);
  return n ? n[2] : e === null ? "null" : "";
}
function Oa(e, n) {
  return Ca(e) === Ca(n);
}
function Sa(e, n) {
  return J(n) ? n.findIndex((s) => Oa(s, e)) : it(n) && Oa(n, e) ? 0 : -1;
}
const Dl = (e) => e[0] === "_" || e === "$stable", ir = (e) => J(e) ? e.map(Ne) : [Ne(e)], Cf = (e, n, s) => {
  if (n._n)
    return n;
  const a = Ol((...l) => ir(n(...l)), s);
  return a._c = !1, a;
}, Fl = (e, n, s) => {
  const a = e._ctx;
  for (const l in e) {
    if (Dl(l))
      continue;
    const u = e[l];
    if (it(u))
      n[l] = Cf(l, u, a);
    else if (u != null) {
      const c = ir(u);
      n[l] = () => c;
    }
  }
}, jl = (e, n) => {
  const s = ir(n);
  e.slots.default = () => s;
}, Of = (e, n) => {
  if (e.vnode.shapeFlag & 32) {
    const s = n._;
    s ? (e.slots = pt(n), go(n, "_", s)) : Fl(n, e.slots = {});
  } else
    e.slots = {}, n && jl(e, n);
  go(e.slots, Zo, 1);
}, Sf = (e, n, s) => {
  const { vnode: a, slots: l } = e;
  let u = !0, c = Ot;
  if (a.shapeFlag & 32) {
    const f = n._;
    f ? s && f === 1 ? u = !1 : (Xt(l, n), !s && f === 1 && delete l._) : (u = !n.$stable, Fl(n, l)), c = n;
  } else
    n && (jl(e, n), c = { default: 1 });
  if (u)
    for (const f in l)
      !Dl(f) && !(f in c) && delete l[f];
};
function Hl() {
  return {
    app: null,
    config: {
      isNativeTag: Xh,
      performance: !1,
      globalProperties: {},
      optionMergeStrategies: {},
      errorHandler: void 0,
      warnHandler: void 0,
      compilerOptions: {}
    },
    mixins: [],
    components: {},
    directives: {},
    provides: /* @__PURE__ */ Object.create(null),
    optionsCache: /* @__PURE__ */ new WeakMap(),
    propsCache: /* @__PURE__ */ new WeakMap(),
    emitsCache: /* @__PURE__ */ new WeakMap()
  };
}
let Mf = 0;
function Ef(e, n) {
  return function(a, l = null) {
    it(a) || (a = Object.assign({}, a)), l != null && !St(l) && (l = null);
    const u = Hl(), c = /* @__PURE__ */ new Set();
    let f = !1;
    const p = u.app = {
      _uid: Mf++,
      _component: a,
      _props: l,
      _container: null,
      _context: u,
      _instance: null,
      version: Gf,
      get config() {
        return u.config;
      },
      set config(g) {
      },
      use(g, ...b) {
        return c.has(g) || (g && it(g.install) ? (c.add(g), g.install(p, ...b)) : it(g) && (c.add(g), g(p, ...b))), p;
      },
      mixin(g) {
        return u.mixins.includes(g) || u.mixins.push(g), p;
      },
      component(g, b) {
        return b ? (u.components[g] = b, p) : u.components[g];
      },
      directive(g, b) {
        return b ? (u.directives[g] = b, p) : u.directives[g];
      },
      mount(g, b, x) {
        if (!f) {
          const T = ne(a, l);
          return T.appContext = u, b && n ? n(T, g) : e(T, g, x), f = !0, p._container = g, g.__vue_app__ = p, Ro(T.component) || T.component.proxy;
        }
      },
      unmount() {
        f && (e(null, p._container), delete p._container.__vue_app__);
      },
      provide(g, b) {
        return u.provides[g] = b, p;
      }
    };
    return p;
  };
}
function zs(e, n, s, a, l = !1) {
  if (J(e)) {
    e.forEach((T, k) => zs(T, n && (J(n) ? n[k] : n), s, a, l));
    return;
  }
  if (bn(a) && !l)
    return;
  const u = a.shapeFlag & 4 ? Ro(a.component) || a.component.proxy : a.el, c = l ? null : u, { i: f, r: p } = e, g = n && n.r, b = f.refs === Ot ? f.refs = {} : f.refs, x = f.setupState;
  if (g != null && g !== p && (Zt(g) ? (b[g] = null, ct(x, g) && (x[g] = null)) : ie(g) && (g.value = null)), it(p))
    ai(p, f, 12, [c, b]);
  else {
    const T = Zt(p), k = ie(p);
    if (T || k) {
      const U = () => {
        if (e.f) {
          const E = T ? ct(x, p) ? x[p] : b[p] : p.value;
          l ? J(E) && js(E, u) : J(E) ? E.includes(u) || E.push(u) : T ? (b[p] = [u], ct(x, p) && (x[p] = b[p])) : (p.value = [u], e.k && (b[e.k] = p.value));
        } else
          T ? (b[p] = c, ct(x, p) && (x[p] = c)) : k && (p.value = c, e.k && (b[e.k] = c));
      };
      c ? (U.id = -1, he(U, s)) : U();
    }
  }
}
const he = Qc;
function Af(e) {
  return If(e);
}
function If(e, n) {
  const s = sc();
  s.__VUE__ = !0;
  const { insert: a, remove: l, patchProp: u, createElement: c, createText: f, createComment: p, setText: g, setElementText: b, parentNode: x, nextSibling: T, setScopeId: k = Me, insertStaticContent: U } = e, E = (_, v, P, S = null, C = null, A = null, z = !1, I = null, B = !!v.dynamicChildren) => {
    if (_ === v)
      return;
    _ && !Ci(_, v) && (S = le(_), Ct(_, C, A, !0), _ = null), v.patchFlag === -2 && (B = !1, v.dynamicChildren = null);
    const { type: M, ref: H, shapeFlag: F } = v;
    switch (M) {
      case No:
        tt(_, v, P, S);
        break;
      case ye:
        X(_, v, P, S);
        break;
      case vs:
        _ == null && xt(v, P, S, z);
        break;
      case ae:
        R(_, v, P, S, C, A, z, I, B);
        break;
      default:
        F & 1 ? gt(_, v, P, S, C, A, z, I, B) : F & 6 ? Tt(_, v, P, S, C, A, z, I, B) : (F & 64 || F & 128) && M.process(_, v, P, S, C, A, z, I, B, Fe);
    }
    H != null && C && zs(H, _ && _.ref, A, v || _, !v);
  }, tt = (_, v, P, S) => {
    if (_ == null)
      a(v.el = f(v.children), P, S);
    else {
      const C = v.el = _.el;
      v.children !== _.children && g(C, v.children);
    }
  }, X = (_, v, P, S) => {
    _ == null ? a(v.el = p(v.children || ""), P, S) : v.el = _.el;
  }, xt = (_, v, P, S) => {
    [_.el, _.anchor] = U(_.children, v, P, S, _.el, _.anchor);
  }, D = ({ el: _, anchor: v }, P, S) => {
    let C;
    for (; _ && _ !== v; )
      C = T(_), a(_, P, S), _ = C;
    a(v, P, S);
  }, rt = ({ el: _, anchor: v }) => {
    let P;
    for (; _ && _ !== v; )
      P = T(_), l(_), _ = P;
    l(v);
  }, gt = (_, v, P, S, C, A, z, I, B) => {
    z = z || v.type === "svg", _ == null ? Kt(v, P, S, C, A, z, I, B) : j(_, v, C, A, z, I, B);
  }, Kt = (_, v, P, S, C, A, z, I) => {
    let B, M;
    const { type: H, props: F, shapeFlag: W, transition: q, dirs: nt } = _;
    if (B = _.el = c(_.type, A, F && F.is, F), W & 8 ? b(B, _.children) : W & 16 && Q(_.children, B, null, S, C, A && H !== "foreignObject", z, I), nt && bi(_, null, S, "created"), qt(B, _, _.scopeId, z, S), F) {
      for (const _t in F)
        _t !== "value" && !ho(_t) && u(B, _t, null, F[_t], A, _.children, S, C, at);
      "value" in F && u(B, "value", null, F.value), (M = F.onVnodeBeforeMount) && ze(M, S, _);
    }
    nt && bi(_, null, S, "beforeMount");
    const yt = (!C || C && !C.pendingBranch) && q && !q.persisted;
    yt && q.beforeEnter(B), a(B, v, P), ((M = F && F.onVnodeMounted) || yt || nt) && he(() => {
      M && ze(M, S, _), yt && q.enter(B), nt && bi(_, null, S, "mounted");
    }, C);
  }, qt = (_, v, P, S, C) => {
    if (P && k(_, P), S)
      for (let A = 0; A < S.length; A++)
        k(_, S[A]);
    if (C) {
      let A = C.subTree;
      if (v === A) {
        const z = C.vnode;
        qt(_, z, z.scopeId, z.slotScopeIds, C.parent);
      }
    }
  }, Q = (_, v, P, S, C, A, z, I, B = 0) => {
    for (let M = B; M < _.length; M++) {
      const H = _[M] = I ? oi(_[M]) : Ne(_[M]);
      E(null, H, v, P, S, C, A, z, I);
    }
  }, j = (_, v, P, S, C, A, z) => {
    const I = v.el = _.el;
    let { patchFlag: B, dynamicChildren: M, dirs: H } = v;
    B |= _.patchFlag & 16;
    const F = _.props || Ot, W = v.props || Ot;
    let q;
    P && wi(P, !1), (q = W.onVnodeBeforeUpdate) && ze(q, P, v, _), H && bi(v, _, P, "beforeUpdate"), P && wi(P, !0);
    const nt = C && v.type !== "foreignObject";
    if (M ? V(_.dynamicChildren, M, I, P, S, nt, A) : z || Z(_, v, I, null, P, S, nt, A, !1), B > 0) {
      if (B & 16)
        ut(I, v, F, W, P, S, C);
      else if (B & 2 && F.class !== W.class && u(I, "class", null, W.class, C), B & 4 && u(I, "style", F.style, W.style, C), B & 8) {
        const yt = v.dynamicProps;
        for (let _t = 0; _t < yt.length; _t++) {
          const Nt = yt[_t], ue = F[Nt], je = W[Nt];
          (je !== ue || Nt === "value") && u(I, Nt, ue, je, C, _.children, P, S, at);
        }
      }
      B & 1 && _.children !== v.children && b(I, v.children);
    } else
      !z && M == null && ut(I, v, F, W, P, S, C);
    ((q = W.onVnodeUpdated) || H) && he(() => {
      q && ze(q, P, v, _), H && bi(v, _, P, "updated");
    }, S);
  }, V = (_, v, P, S, C, A, z) => {
    for (let I = 0; I < v.length; I++) {
      const B = _[I], M = v[I], H = (
        // oldVNode may be an errored async setup() component inside Suspense
        // which will not have a mounted element
        B.el && // - In the case of a Fragment, we need to provide the actual parent
        // of the Fragment itself so it can move its children.
        (B.type === ae || // - In the case of different nodes, there is going to be a replacement
        // which also requires the correct parent container
        !Ci(B, M) || // - In the case of a component, it could contain anything.
        B.shapeFlag & 70) ? x(B.el) : (
          // In other cases, the parent container is not actually used so we
          // just pass the block element here to avoid a DOM parentNode call.
          P
        )
      );
      E(B, M, H, null, S, C, A, z, !0);
    }
  }, ut = (_, v, P, S, C, A, z) => {
    if (P !== S) {
      if (P !== Ot)
        for (const I in P)
          !ho(I) && !(I in S) && u(_, I, P[I], null, z, v.children, C, A, at);
      for (const I in S) {
        if (ho(I))
          continue;
        const B = S[I], M = P[I];
        B !== M && I !== "value" && u(_, I, M, B, z, v.children, C, A, at);
      }
      "value" in S && u(_, "value", P.value, S.value);
    }
  }, R = (_, v, P, S, C, A, z, I, B) => {
    const M = v.el = _ ? _.el : f(""), H = v.anchor = _ ? _.anchor : f("");
    let { patchFlag: F, dynamicChildren: W, slotScopeIds: q } = v;
    q && (I = I ? I.concat(q) : q), _ == null ? (a(M, P, S), a(H, P, S), Q(v.children, P, H, C, A, z, I, B)) : F > 0 && F & 64 && W && // #2715 the previous fragment could've been a BAILed one as a result
    // of renderSlot() with no valid children
    _.dynamicChildren ? (V(_.dynamicChildren, W, P, C, A, z, I), // #2080 if the stable fragment has a key, it's a <template v-for> that may
    //  get moved around. Make sure all root level vnodes inherit el.
    // #2134 or if it's a component root, it may also get moved around
    // as the component is being moved.
    (v.key != null || C && v === C.subTree) && $l(
      _,
      v,
      !0
      /* shallow */
    )) : Z(_, v, P, H, C, A, z, I, B);
  }, Tt = (_, v, P, S, C, A, z, I, B) => {
    v.slotScopeIds = I, _ == null ? v.shapeFlag & 512 ? C.ctx.activate(v, P, S, z, B) : It(v, P, S, C, A, z, B) : ci(_, v, B);
  }, It = (_, v, P, S, C, A, z) => {
    const I = _.component = Df(_, S, C);
    if (Bo(_) && (I.ctx.renderer = Fe), jf(I), I.asyncDep) {
      if (C && C.registerDep(I, ft), !_.el) {
        const B = I.subTree = ne(ye);
        X(null, B, v, P);
      }
      return;
    }
    ft(I, _, v, P, C, A, z);
  }, ci = (_, v, P) => {
    const S = v.component = _.component;
    if (Yc(_, v, P))
      if (S.asyncDep && !S.asyncResolved) {
        bt(S, v, P);
        return;
      } else
        S.next = v, Wc(S.update), S.update();
    else
      v.el = _.el, S.vnode = v;
  }, ft = (_, v, P, S, C, A, z) => {
    const I = () => {
      if (_.isMounted) {
        let { next: H, bu: F, u: W, parent: q, vnode: nt } = _, yt = H, _t;
        wi(_, !1), H ? (H.el = nt.el, bt(_, H, z)) : H = nt, F && fo(F), (_t = H.props && H.props.onVnodeBeforeUpdate) && ze(_t, q, H, nt), wi(_, !0);
        const Nt = ps(_), ue = _.subTree;
        _.subTree = Nt, E(
          ue,
          Nt,
          // parent may have changed if it's in a teleport
          x(ue.el),
          // anchor may have changed if it's in a fragment
          le(ue),
          _,
          C,
          A
        ), H.el = Nt.el, yt === null && Jc(_, Nt.el), W && he(W, C), (_t = H.props && H.props.onVnodeUpdated) && he(() => ze(_t, q, H, nt), C);
      } else {
        let H;
        const { el: F, props: W } = v, { bm: q, m: nt, parent: yt } = _, _t = bn(v);
        if (wi(_, !1), q && fo(q), !_t && (H = W && W.onVnodeBeforeMount) && ze(H, yt, v), wi(_, !0), F && di) {
          const Nt = () => {
            _.subTree = ps(_), di(F, _.subTree, _, C, null);
          };
          _t ? v.type.__asyncLoader().then(
            // note: we are moving the render call into an async callback,
            // which means it won't track dependencies - but it's ok because
            // a server-rendered async wrapper is already in resolved state
            // and it will never need to change.
            () => !_.isUnmounted && Nt()
          ) : Nt();
        } else {
          const Nt = _.subTree = ps(_);
          E(null, Nt, P, S, _, C, A), v.el = Nt.el;
        }
        if (nt && he(nt, C), !_t && (H = W && W.onVnodeMounted)) {
          const Nt = v;
          he(() => ze(H, yt, Nt), C);
        }
        (v.shapeFlag & 256 || yt && bn(yt.vnode) && yt.vnode.shapeFlag & 256) && _.a && he(_.a, C), _.isMounted = !0, v = P = S = null;
      }
    }, B = _.effect = new Us(
      I,
      () => Xs(M),
      _.scope
      // track it in component's effect scope
    ), M = _.update = () => B.run();
    M.id = _.uid, wi(_, !0), M();
  }, bt = (_, v, P) => {
    v.component = _;
    const S = _.vnode.props;
    _.vnode = v, _.next = null, Tf(_, v.props, S, P), Sf(_, v.children, P), Ji(), ya(), Xi();
  }, Z = (_, v, P, S, C, A, z, I, B = !1) => {
    const M = _ && _.children, H = _ ? _.shapeFlag : 0, F = v.children, { patchFlag: W, shapeFlag: q } = v;
    if (W > 0) {
      if (W & 128) {
        K(M, F, P, S, C, A, z, I, B);
        return;
      } else if (W & 256) {
        Ee(M, F, P, S, C, A, z, I, B);
        return;
      }
    }
    q & 8 ? (H & 16 && at(M, C, A), F !== M && b(P, F)) : H & 16 ? q & 16 ? K(M, F, P, S, C, A, z, I, B) : at(M, C, A, !0) : (H & 8 && b(P, ""), q & 16 && Q(F, P, S, C, A, z, I, B));
  }, Ee = (_, v, P, S, C, A, z, I, B) => {
    _ = _ || Wi, v = v || Wi;
    const M = _.length, H = v.length, F = Math.min(M, H);
    let W;
    for (W = 0; W < F; W++) {
      const q = v[W] = B ? oi(v[W]) : Ne(v[W]);
      E(_[W], q, P, null, C, A, z, I, B);
    }
    M > H ? at(_, C, A, !0, !1, F) : Q(v, P, S, C, A, z, I, B, F);
  }, K = (_, v, P, S, C, A, z, I, B) => {
    let M = 0;
    const H = v.length;
    let F = _.length - 1, W = H - 1;
    for (; M <= F && M <= W; ) {
      const q = _[M], nt = v[M] = B ? oi(v[M]) : Ne(v[M]);
      if (Ci(q, nt))
        E(q, nt, P, null, C, A, z, I, B);
      else
        break;
      M++;
    }
    for (; M <= F && M <= W; ) {
      const q = _[F], nt = v[W] = B ? oi(v[W]) : Ne(v[W]);
      if (Ci(q, nt))
        E(q, nt, P, null, C, A, z, I, B);
      else
        break;
      F--, W--;
    }
    if (M > F) {
      if (M <= W) {
        const q = W + 1, nt = q < H ? v[q].el : S;
        for (; M <= W; )
          E(null, v[M] = B ? oi(v[M]) : Ne(v[M]), P, nt, C, A, z, I, B), M++;
      }
    } else if (M > W)
      for (; M <= F; )
        Ct(_[M], C, A, !0), M++;
    else {
      const q = M, nt = M, yt = /* @__PURE__ */ new Map();
      for (M = nt; M <= W; M++) {
        const te = v[M] = B ? oi(v[M]) : Ne(v[M]);
        te.key != null && yt.set(te.key, M);
      }
      let _t, Nt = 0;
      const ue = W - nt + 1;
      let je = !1, Nn = 0;
      const Ge = new Array(ue);
      for (M = 0; M < ue; M++)
        Ge[M] = 0;
      for (M = q; M <= F; M++) {
        const te = _[M];
        if (Nt >= ue) {
          Ct(te, C, A, !0);
          continue;
        }
        let fe;
        if (te.key != null)
          fe = yt.get(te.key);
        else
          for (_t = nt; _t <= W; _t++)
            if (Ge[_t - nt] === 0 && Ci(te, v[_t])) {
              fe = _t;
              break;
            }
        fe === void 0 ? Ct(te, C, A, !0) : (Ge[fe - nt] = M + 1, fe >= Nn ? Nn = fe : je = !0, E(te, v[fe], P, null, C, A, z, I, B), Nt++);
      }
      const ki = je ? kf(Ge) : Wi;
      for (_t = ki.length - 1, M = ue - 1; M >= 0; M--) {
        const te = nt + M, fe = v[te], Ye = te + 1 < H ? v[te + 1].el : S;
        Ge[M] === 0 ? E(null, fe, P, Ye, C, A, z, I, B) : je && (_t < 0 || M !== ki[_t] ? mt(
          fe,
          P,
          Ye,
          2
          /* MoveType.REORDER */
        ) : _t--);
      }
    }
  }, mt = (_, v, P, S, C = null) => {
    const { el: A, type: z, transition: I, children: B, shapeFlag: M } = _;
    if (M & 6) {
      mt(_.component.subTree, v, P, S);
      return;
    }
    if (M & 128) {
      _.suspense.move(v, P, S);
      return;
    }
    if (M & 64) {
      z.move(_, v, P, Fe);
      return;
    }
    if (z === ae) {
      a(A, v, P);
      for (let F = 0; F < B.length; F++)
        mt(B[F], v, P, S);
      a(_.anchor, v, P);
      return;
    }
    if (z === vs) {
      D(_, v, P);
      return;
    }
    if (S !== 2 && M & 1 && I)
      if (S === 0)
        I.beforeEnter(A), a(A, v, P), he(() => I.enter(A), C);
      else {
        const { leave: F, delayLeave: W, afterLeave: q } = I, nt = () => a(A, v, P), yt = () => {
          F(A, () => {
            nt(), q && q();
          });
        };
        W ? W(A, nt, yt) : yt();
      }
    else
      a(A, v, P);
  }, Ct = (_, v, P, S = !1, C = !1) => {
    const { type: A, props: z, ref: I, children: B, dynamicChildren: M, shapeFlag: H, patchFlag: F, dirs: W } = _;
    if (I != null && zs(I, null, P, _, !0), H & 256) {
      v.ctx.deactivate(_);
      return;
    }
    const q = H & 1 && W, nt = !bn(_);
    let yt;
    if (nt && (yt = z && z.onVnodeBeforeUnmount) && ze(yt, v, _), H & 6)
      vt(_.component, P, S);
    else {
      if (H & 128) {
        _.suspense.unmount(P, S);
        return;
      }
      q && bi(_, null, v, "beforeUnmount"), H & 64 ? _.type.remove(_, v, P, C, Fe, S) : M && // #1153: fast path should not be taken for non-stable (v-for) fragments
      (A !== ae || F > 0 && F & 64) ? at(M, v, P, !1, !0) : (A === ae && F & 384 || !C && H & 16) && at(B, v, P), S && Gt(_);
    }
    (nt && (yt = z && z.onVnodeUnmounted) || q) && he(() => {
      yt && ze(yt, v, _), q && bi(_, null, v, "unmounted");
    }, P);
  }, Gt = (_) => {
    const { type: v, el: P, anchor: S, transition: C } = _;
    if (v === ae) {
      $t(P, S);
      return;
    }
    if (v === vs) {
      rt(_);
      return;
    }
    const A = () => {
      l(P), C && !C.persisted && C.afterLeave && C.afterLeave();
    };
    if (_.shapeFlag & 1 && C && !C.persisted) {
      const { leave: z, delayLeave: I } = C, B = () => z(P, A);
      I ? I(_.el, A, B) : B();
    } else
      A();
  }, $t = (_, v) => {
    let P;
    for (; _ !== v; )
      P = T(_), l(_), _ = P;
    l(v);
  }, vt = (_, v, P) => {
    const { bum: S, scope: C, update: A, subTree: z, um: I } = _;
    S && fo(S), C.stop(), A && (A.active = !1, Ct(z, _, v, P)), I && he(I, v), he(() => {
      _.isUnmounted = !0;
    }, v), v && v.pendingBranch && !v.isUnmounted && _.asyncDep && !_.asyncResolved && _.suspenseId === v.pendingId && (v.deps--, v.deps === 0 && v.resolve());
  }, at = (_, v, P, S = !1, C = !1, A = 0) => {
    for (let z = A; z < _.length; z++)
      Ct(_[z], v, P, S, C);
  }, le = (_) => _.shapeFlag & 6 ? le(_.component.subTree) : _.shapeFlag & 128 ? _.suspense.next() : T(_.anchor || _.el), be = (_, v, P) => {
    _ == null ? v._vnode && Ct(v._vnode, null, null, !0) : E(v._vnode || null, _, v, null, null, null, P), ya(), Ll(), v._vnode = _;
  }, Fe = {
    p: E,
    um: Ct,
    m: mt,
    r: Gt,
    mt: It,
    mc: Q,
    pc: Z,
    pbc: V,
    n: le,
    o: e
  };
  let fi, di;
  return n && ([fi, di] = n(Fe)), {
    render: be,
    hydrate: fi,
    createApp: Ef(be, fi)
  };
}
function wi({ effect: e, update: n }, s) {
  e.allowRecurse = n.allowRecurse = s;
}
function $l(e, n, s = !1) {
  const a = e.children, l = n.children;
  if (J(a) && J(l))
    for (let u = 0; u < a.length; u++) {
      const c = a[u];
      let f = l[u];
      f.shapeFlag & 1 && !f.dynamicChildren && ((f.patchFlag <= 0 || f.patchFlag === 32) && (f = l[u] = oi(l[u]), f.el = c.el), s || $l(c, f)), f.type === No && (f.el = c.el);
    }
}
function kf(e) {
  const n = e.slice(), s = [0];
  let a, l, u, c, f;
  const p = e.length;
  for (a = 0; a < p; a++) {
    const g = e[a];
    if (g !== 0) {
      if (l = s[s.length - 1], e[l] < g) {
        n[a] = l, s.push(a);
        continue;
      }
      for (u = 0, c = s.length - 1; u < c; )
        f = u + c >> 1, e[s[f]] < g ? u = f + 1 : c = f;
      g < e[s[u]] && (u > 0 && (n[a] = s[u - 1]), s[u] = a);
    }
  }
  for (u = s.length, c = s[u - 1]; u-- > 0; )
    s[u] = c, c = n[c];
  return s;
}
const Bf = (e) => e.__isTeleport, ae = Symbol(void 0), No = Symbol(void 0), ye = Symbol(void 0), vs = Symbol(void 0), xn = [];
let Se = null;
function Vt(e = !1) {
  xn.push(Se = e ? null : []);
}
function zf() {
  xn.pop(), Se = xn[xn.length - 1] || null;
}
let Sn = 1;
function Ma(e) {
  Sn += e;
}
function Wl(e) {
  return e.dynamicChildren = Sn > 0 ? Se || Wi : null, zf(), Sn > 0 && Se && Se.push(e), e;
}
function re(e, n, s, a, l, u) {
  return Wl(Y(
    e,
    n,
    s,
    a,
    l,
    u,
    !0
    /* isBlock */
  ));
}
function Ln(e, n, s, a, l) {
  return Wl(ne(
    e,
    n,
    s,
    a,
    l,
    !0
    /* isBlock: prevent a block from tracking itself */
  ));
}
function wo(e) {
  return e ? e.__v_isVNode === !0 : !1;
}
function Ci(e, n) {
  return e.type === n.type && e.key === n.key;
}
const Zo = "__vInternal", Ul = ({ key: e }) => e ?? null, mo = ({ ref: e, ref_key: n, ref_for: s }) => e != null ? Zt(e) || ie(e) || it(e) ? { i: Jt, r: e, k: n, f: !!s } : e : null;
function Y(e, n = null, s = null, a = 0, l = null, u = e === ae ? 0 : 1, c = !1, f = !1) {
  const p = {
    __v_isVNode: !0,
    __v_skip: !0,
    type: e,
    props: n,
    key: n && Ul(n),
    ref: n && mo(n),
    scopeId: Cl,
    slotScopeIds: null,
    children: s,
    component: null,
    suspense: null,
    ssContent: null,
    ssFallback: null,
    dirs: null,
    transition: null,
    el: null,
    anchor: null,
    target: null,
    targetAnchor: null,
    staticCount: 0,
    shapeFlag: u,
    patchFlag: a,
    dynamicProps: l,
    dynamicChildren: null,
    appContext: null,
    ctx: Jt
  };
  return f ? (nr(p, s), u & 128 && e.normalize(p)) : s && (p.shapeFlag |= Zt(s) ? 8 : 16), Sn > 0 && // avoid a block node from tracking itself
  !c && // has current parent block
  Se && // presence of a patch flag indicates this node needs patching on updates.
  // component nodes also should always be patched, because even if the
  // component doesn't need to update, it needs to persist the instance on to
  // the next vnode so that it can be properly unmounted later.
  (p.patchFlag > 0 || u & 6) && // the EVENTS flag is only for hydration and if it is the only flag, the
  // vnode should not be considered dynamic due to handler caching.
  p.patchFlag !== 32 && Se.push(p), p;
}
const ne = Nf;
function Nf(e, n = null, s = null, a = 0, l = null, u = !1) {
  if ((!e || e === Bl) && (e = ye), wo(e)) {
    const f = ui(
      e,
      n,
      !0
      /* mergeRef: true */
    );
    return s && nr(f, s), Sn > 0 && !u && Se && (f.shapeFlag & 6 ? Se[Se.indexOf(e)] = f : Se.push(f)), f.patchFlag |= -2, f;
  }
  if (Vf(e) && (e = e.__vccOpts), n) {
    n = Te(n);
    let { class: f, style: p } = n;
    f && !Zt(f) && (n.class = Ai(f)), St(p) && (_l(p) && !J(p) && (p = Xt({}, p)), n.style = To(p));
  }
  const c = Zt(e) ? 1 : Xc(e) ? 128 : Bf(e) ? 64 : St(e) ? 4 : it(e) ? 2 : 0;
  return Y(e, n, s, a, l, c, u, !0);
}
function Te(e) {
  return e ? _l(e) || Zo in e ? Xt({}, e) : e : null;
}
function ui(e, n, s = !1) {
  const { props: a, ref: l, patchFlag: u, children: c } = e, f = n ? Ns(a || {}, n) : a;
  return {
    __v_isVNode: !0,
    __v_skip: !0,
    type: e.type,
    props: f,
    key: f && Ul(f),
    ref: n && n.ref ? (
      // #2078 in the case of <component :is="vnode" ref="extra"/>
      // if the vnode itself already has a ref, cloneVNode will need to merge
      // the refs so the single vnode can be set on multiple refs
      s && l ? J(l) ? l.concat(mo(n)) : [l, mo(n)] : mo(n)
    ) : l,
    scopeId: e.scopeId,
    slotScopeIds: e.slotScopeIds,
    children: c,
    target: e.target,
    targetAnchor: e.targetAnchor,
    staticCount: e.staticCount,
    shapeFlag: e.shapeFlag,
    // if the vnode is cloned with extra props, we can no longer assume its
    // existing patch flag to be reliable and need to add the FULL_PROPS flag.
    // note: preserve flag for fragments since they use the flag for children
    // fast paths only.
    patchFlag: n && e.type !== ae ? u === -1 ? 16 : u | 16 : u,
    dynamicProps: e.dynamicProps,
    dynamicChildren: e.dynamicChildren,
    appContext: e.appContext,
    dirs: e.dirs,
    transition: e.transition,
    // These should technically only be non-null on mounted VNodes. However,
    // they *should* be copied for kept-alive vnodes. So we just always copy
    // them since them being non-null during a mount doesn't affect the logic as
    // they will simply be overwritten.
    component: e.component,
    suspense: e.suspense,
    ssContent: e.ssContent && ui(e.ssContent),
    ssFallback: e.ssFallback && ui(e.ssFallback),
    el: e.el,
    anchor: e.anchor,
    ctx: e.ctx,
    ce: e.ce
  };
}
function xo(e = " ", n = 0) {
  return ne(No, null, e, n);
}
function _o(e = "", n = !1) {
  return n ? (Vt(), Ln(ye, null, e)) : ne(ye, null, e);
}
function Ne(e) {
  return e == null || typeof e == "boolean" ? ne(ye) : J(e) ? ne(
    ae,
    null,
    // #3666, avoid reference pollution when reusing vnode
    e.slice()
  ) : typeof e == "object" ? oi(e) : ne(No, null, String(e));
}
function oi(e) {
  return e.el === null && e.patchFlag !== -1 || e.memo ? e : ui(e);
}
function nr(e, n) {
  let s = 0;
  const { shapeFlag: a } = e;
  if (n == null)
    n = null;
  else if (J(n))
    s = 16;
  else if (typeof n == "object")
    if (a & 65) {
      const l = n.default;
      l && (l._c && (l._d = !1), nr(e, l()), l._c && (l._d = !0));
      return;
    } else {
      s = 32;
      const l = n._;
      !l && !(Zo in n) ? n._ctx = Jt : l === 3 && Jt && (Jt.slots._ === 1 ? n._ = 1 : (n._ = 2, e.patchFlag |= 1024));
    }
  else
    it(n) ? (n = { default: n, _ctx: Jt }, s = 32) : (n = String(n), a & 64 ? (s = 16, n = [xo(n)]) : s = 8);
  e.children = n, e.shapeFlag |= s;
}
function Ns(...e) {
  const n = {};
  for (let s = 0; s < e.length; s++) {
    const a = e[s];
    for (const l in a)
      if (l === "class")
        n.class !== a.class && (n.class = Ai([n.class, a.class]));
      else if (l === "style")
        n.style = To([n.style, a.style]);
      else if (Co(l)) {
        const u = n[l], c = a[l];
        c && u !== c && !(J(u) && u.includes(c)) && (n[l] = u ? [].concat(u, c) : c);
      } else
        l !== "" && (n[l] = a[l]);
  }
  return n;
}
function ze(e, n, s, a = null) {
  ve(e, n, 7, [
    s,
    a
  ]);
}
const Zf = Hl();
let Rf = 0;
function Df(e, n, s) {
  const a = e.type, l = (n ? n.appContext : e.appContext) || Zf, u = {
    uid: Rf++,
    vnode: e,
    type: a,
    parent: n,
    appContext: l,
    root: null,
    next: null,
    subTree: null,
    effect: null,
    update: null,
    scope: new rc(
      !0
      /* detached */
    ),
    render: null,
    proxy: null,
    exposed: null,
    exposeProxy: null,
    withProxy: null,
    provides: n ? n.provides : Object.create(l.provides),
    accessCache: null,
    renderCache: [],
    // local resolved assets
    components: null,
    directives: null,
    // resolved props and emits options
    propsOptions: Rl(a, l),
    emitsOptions: Tl(a, l),
    // emit
    emit: null,
    emitted: null,
    // props default value
    propsDefaults: Ot,
    // inheritAttrs
    inheritAttrs: a.inheritAttrs,
    // state
    ctx: Ot,
    data: Ot,
    props: Ot,
    attrs: Ot,
    slots: Ot,
    refs: Ot,
    setupState: Ot,
    setupContext: null,
    // suspense related
    suspense: s,
    suspenseId: s ? s.pendingId : 0,
    asyncDep: null,
    asyncResolved: !1,
    // lifecycle hooks
    // not using enums here because it results in computed properties
    isMounted: !1,
    isUnmounted: !1,
    isDeactivated: !1,
    bc: null,
    c: null,
    bm: null,
    m: null,
    bu: null,
    u: null,
    um: null,
    bum: null,
    da: null,
    a: null,
    rtg: null,
    rtc: null,
    ec: null,
    sp: null
  };
  return u.ctx = { _: u }, u.root = n ? n.root : u, u.emit = Kc.bind(null, u), e.ce && e.ce(u), u;
}
let Ht = null;
const Ff = () => Ht || Jt, Gi = (e) => {
  Ht = e, e.scope.on();
}, Ei = () => {
  Ht && Ht.scope.off(), Ht = null;
};
function Vl(e) {
  return e.vnode.shapeFlag & 4;
}
let Mn = !1;
function jf(e, n = !1) {
  Mn = n;
  const { props: s, children: a } = e.vnode, l = Vl(e);
  Pf(e, s, l, n), Of(e, a);
  const u = l ? Hf(e, n) : void 0;
  return Mn = !1, u;
}
function Hf(e, n) {
  const s = e.type;
  e.accessCache = /* @__PURE__ */ Object.create(null), e.proxy = Bt(new Proxy(e.ctx, vf));
  const { setup: a } = s;
  if (a) {
    const l = e.setupContext = a.length > 1 ? Wf(e) : null;
    Gi(e), Ji();
    const u = ai(a, e, 0, [e.props, l]);
    if (Xi(), Ei(), il(u)) {
      if (u.then(Ei, Ei), n)
        return u.then((c) => {
          Ea(e, c, n);
        }).catch((c) => {
          Io(
            c,
            e,
            0
            /* ErrorCodes.SETUP_FUNCTION */
          );
        });
      e.asyncDep = u;
    } else
      Ea(e, u, n);
  } else
    Kl(e, n);
}
function Ea(e, n, s) {
  it(n) ? e.type.__ssrInlineRender ? e.ssrRender = n : e.render = n : St(n) && (e.setupState = yl(n)), Kl(e, s);
}
let Aa;
function Kl(e, n, s) {
  const a = e.type;
  if (!e.render) {
    if (!n && Aa && !a.render) {
      const l = a.template || er(e).template;
      if (l) {
        const { isCustomElement: u, compilerOptions: c } = e.appContext.config, { delimiters: f, compilerOptions: p } = a, g = Xt(Xt({
          isCustomElement: u,
          delimiters: f
        }, c), p);
        a.render = Aa(l, g);
      }
    }
    e.render = a.render || Me;
  }
  Gi(e), Ji(), yf(e), Xi(), Ei();
}
function $f(e) {
  return new Proxy(e.attrs, {
    get(n, s) {
      return ce(e, "get", "$attrs"), n[s];
    }
  });
}
function Wf(e) {
  const n = (a) => {
    e.exposed = a || {};
  };
  let s;
  return {
    get attrs() {
      return s || (s = $f(e));
    },
    slots: e.slots,
    emit: e.emit,
    expose: n
  };
}
function Ro(e) {
  if (e.exposed)
    return e.exposeProxy || (e.exposeProxy = new Proxy(yl(Bt(e.exposed)), {
      get(n, s) {
        if (s in n)
          return n[s];
        if (s in wn)
          return wn[s](e);
      },
      has(n, s) {
        return s in n || s in wn;
      }
    }));
}
function Uf(e, n = !0) {
  return it(e) ? e.displayName || e.name : e.name || n && e.__name;
}
function Vf(e) {
  return it(e) && "__vccOpts" in e;
}
const gn = (e, n) => jc(e, n, Mn);
function hi(e, n, s) {
  const a = arguments.length;
  return a === 2 ? St(n) && !J(n) ? wo(n) ? ne(e, null, [n]) : ne(e, n) : ne(e, null, n) : (a > 3 ? s = Array.prototype.slice.call(arguments, 2) : a === 3 && wo(s) && (s = [s]), ne(e, n, s));
}
const Kf = Symbol(""), qf = () => Pt(Kf), Gf = "3.2.47", Yf = "http://www.w3.org/2000/svg", Oi = typeof document < "u" ? document : null, Ia = Oi && /* @__PURE__ */ Oi.createElement("template"), Jf = {
  insert: (e, n, s) => {
    n.insertBefore(e, s || null);
  },
  remove: (e) => {
    const n = e.parentNode;
    n && n.removeChild(e);
  },
  createElement: (e, n, s, a) => {
    const l = n ? Oi.createElementNS(Yf, e) : Oi.createElement(e, s ? { is: s } : void 0);
    return e === "select" && a && a.multiple != null && l.setAttribute("multiple", a.multiple), l;
  },
  createText: (e) => Oi.createTextNode(e),
  createComment: (e) => Oi.createComment(e),
  setText: (e, n) => {
    e.nodeValue = n;
  },
  setElementText: (e, n) => {
    e.textContent = n;
  },
  parentNode: (e) => e.parentNode,
  nextSibling: (e) => e.nextSibling,
  querySelector: (e) => Oi.querySelector(e),
  setScopeId(e, n) {
    e.setAttribute(n, "");
  },
  // __UNSAFE__
  // Reason: innerHTML.
  // Static content here can only come from compiled templates.
  // As long as the user only uses trusted templates, this is safe.
  insertStaticContent(e, n, s, a, l, u) {
    const c = s ? s.previousSibling : n.lastChild;
    if (l && (l === u || l.nextSibling))
      for (; n.insertBefore(l.cloneNode(!0), s), !(l === u || !(l = l.nextSibling)); )
        ;
    else {
      Ia.innerHTML = a ? `<svg>${e}</svg>` : e;
      const f = Ia.content;
      if (a) {
        const p = f.firstChild;
        for (; p.firstChild; )
          f.appendChild(p.firstChild);
        f.removeChild(p);
      }
      n.insertBefore(f, s);
    }
    return [
      // first
      c ? c.nextSibling : n.firstChild,
      // last
      s ? s.previousSibling : n.lastChild
    ];
  }
};
function Xf(e, n, s) {
  const a = e._vtc;
  a && (n = (n ? [n, ...a] : [...a]).join(" ")), n == null ? e.removeAttribute("class") : s ? e.setAttribute("class", n) : e.className = n;
}
function Qf(e, n, s) {
  const a = e.style, l = Zt(s);
  if (s && !l) {
    if (n && !Zt(n))
      for (const u in n)
        s[u] == null && Zs(a, u, "");
    for (const u in s)
      Zs(a, u, s[u]);
  } else {
    const u = a.display;
    l ? n !== s && (a.cssText = s) : n && e.removeAttribute("style"), "_vod" in e && (a.display = u);
  }
}
const ka = /\s*!important$/;
function Zs(e, n, s) {
  if (J(s))
    s.forEach((a) => Zs(e, n, a));
  else if (s == null && (s = ""), n.startsWith("--"))
    e.setProperty(n, s);
  else {
    const a = td(e, n);
    ka.test(s) ? e.setProperty(Yi(a), s.replace(ka, ""), "important") : e[a] = s;
  }
}
const Ba = ["Webkit", "Moz", "ms"], ys = {};
function td(e, n) {
  const s = ys[n];
  if (s)
    return s;
  let a = Re(n);
  if (a !== "filter" && a in e)
    return ys[n] = a;
  a = Mo(a);
  for (let l = 0; l < Ba.length; l++) {
    const u = Ba[l] + a;
    if (u in e)
      return ys[n] = u;
  }
  return n;
}
const za = "http://www.w3.org/1999/xlink";
function ed(e, n, s, a, l) {
  if (a && n.startsWith("xlink:"))
    s == null ? e.removeAttributeNS(za, n.slice(6, n.length)) : e.setAttributeNS(za, n, s);
  else {
    const u = Jh(n);
    s == null || u && !Qa(s) ? e.removeAttribute(n) : e.setAttribute(n, u ? "" : s);
  }
}
function id(e, n, s, a, l, u, c) {
  if (n === "innerHTML" || n === "textContent") {
    a && c(a, l, u), e[n] = s ?? "";
    return;
  }
  if (n === "value" && e.tagName !== "PROGRESS" && // custom elements may use _value internally
  !e.tagName.includes("-")) {
    e._value = s;
    const p = s ?? "";
    (e.value !== p || // #4956: always set for OPTION elements because its value falls back to
    // textContent if no value attribute is present. And setting .value for
    // OPTION has no side effect
    e.tagName === "OPTION") && (e.value = p), s == null && e.removeAttribute(n);
    return;
  }
  let f = !1;
  if (s === "" || s == null) {
    const p = typeof e[n];
    p === "boolean" ? s = Qa(s) : s == null && p === "string" ? (s = "", f = !0) : p === "number" && (s = 0, f = !0);
  }
  try {
    e[n] = s;
  } catch {
  }
  f && e.removeAttribute(n);
}
function $i(e, n, s, a) {
  e.addEventListener(n, s, a);
}
function nd(e, n, s, a) {
  e.removeEventListener(n, s, a);
}
function od(e, n, s, a, l = null) {
  const u = e._vei || (e._vei = {}), c = u[n];
  if (a && c)
    c.value = a;
  else {
    const [f, p] = sd(n);
    if (a) {
      const g = u[n] = ld(a, l);
      $i(e, f, g, p);
    } else
      c && (nd(e, f, c, p), u[n] = void 0);
  }
}
const Na = /(?:Once|Passive|Capture)$/;
function sd(e) {
  let n;
  if (Na.test(e)) {
    n = {};
    let a;
    for (; a = e.match(Na); )
      e = e.slice(0, e.length - a[0].length), n[a[0].toLowerCase()] = !0;
  }
  return [e[2] === ":" ? e.slice(3) : Yi(e.slice(2)), n];
}
let bs = 0;
const rd = /* @__PURE__ */ Promise.resolve(), ad = () => bs || (rd.then(() => bs = 0), bs = Date.now());
function ld(e, n) {
  const s = (a) => {
    if (!a._vts)
      a._vts = Date.now();
    else if (a._vts <= s.attached)
      return;
    ve(ud(a, s.value), n, 5, [a]);
  };
  return s.value = e, s.attached = ad(), s;
}
function ud(e, n) {
  if (J(n)) {
    const s = e.stopImmediatePropagation;
    return e.stopImmediatePropagation = () => {
      s.call(e), e._stopped = !0;
    }, n.map((a) => (l) => !l._stopped && a && a(l));
  } else
    return n;
}
const Za = /^on[a-z]/, hd = (e, n, s, a, l = !1, u, c, f, p) => {
  n === "class" ? Xf(e, a, l) : n === "style" ? Qf(e, s, a) : Co(n) ? Fs(n) || od(e, n, s, a, c) : (n[0] === "." ? (n = n.slice(1), !0) : n[0] === "^" ? (n = n.slice(1), !1) : cd(e, n, a, l)) ? id(e, n, a, u, c, f, p) : (n === "true-value" ? e._trueValue = a : n === "false-value" && (e._falseValue = a), ed(e, n, a, l));
};
function cd(e, n, s, a) {
  return a ? !!(n === "innerHTML" || n === "textContent" || n in e && Za.test(n) && it(s)) : n === "spellcheck" || n === "draggable" || n === "translate" || n === "form" || n === "list" && e.tagName === "INPUT" || n === "type" && e.tagName === "TEXTAREA" || Za.test(n) && Zt(s) ? !1 : n in e;
}
const ei = "transition", pn = "animation", or = (e, { slots: n }) => hi(El, fd(e), n);
or.displayName = "Transition";
const ql = {
  name: String,
  type: String,
  css: {
    type: Boolean,
    default: !0
  },
  duration: [String, Number, Object],
  enterFromClass: String,
  enterActiveClass: String,
  enterToClass: String,
  appearFromClass: String,
  appearActiveClass: String,
  appearToClass: String,
  leaveFromClass: String,
  leaveActiveClass: String,
  leaveToClass: String
};
or.props = /* @__PURE__ */ Xt({}, El.props, ql);
const xi = (e, n = []) => {
  J(e) ? e.forEach((s) => s(...n)) : e && e(...n);
}, Ra = (e) => e ? J(e) ? e.some((n) => n.length > 1) : e.length > 1 : !1;
function fd(e) {
  const n = {};
  for (const R in e)
    R in ql || (n[R] = e[R]);
  if (e.css === !1)
    return n;
  const { name: s = "v", type: a, duration: l, enterFromClass: u = `${s}-enter-from`, enterActiveClass: c = `${s}-enter-active`, enterToClass: f = `${s}-enter-to`, appearFromClass: p = u, appearActiveClass: g = c, appearToClass: b = f, leaveFromClass: x = `${s}-leave-from`, leaveActiveClass: T = `${s}-leave-active`, leaveToClass: k = `${s}-leave-to` } = e, U = dd(l), E = U && U[0], tt = U && U[1], { onBeforeEnter: X, onEnter: xt, onEnterCancelled: D, onLeave: rt, onLeaveCancelled: gt, onBeforeAppear: Kt = X, onAppear: qt = xt, onAppearCancelled: Q = D } = n, j = (R, Tt, It) => {
    Li(R, Tt ? b : f), Li(R, Tt ? g : c), It && It();
  }, V = (R, Tt) => {
    R._isLeaving = !1, Li(R, x), Li(R, k), Li(R, T), Tt && Tt();
  }, ut = (R) => (Tt, It) => {
    const ci = R ? qt : xt, ft = () => j(Tt, R, It);
    xi(ci, [Tt, ft]), Da(() => {
      Li(Tt, R ? p : u), ii(Tt, R ? b : f), Ra(ci) || Fa(Tt, a, E, ft);
    });
  };
  return Xt(n, {
    onBeforeEnter(R) {
      xi(X, [R]), ii(R, u), ii(R, c);
    },
    onBeforeAppear(R) {
      xi(Kt, [R]), ii(R, p), ii(R, g);
    },
    onEnter: ut(!1),
    onAppear: ut(!0),
    onLeave(R, Tt) {
      R._isLeaving = !0;
      const It = () => V(R, Tt);
      ii(R, x), _d(), ii(R, T), Da(() => {
        R._isLeaving && (Li(R, x), ii(R, k), Ra(rt) || Fa(R, a, tt, It));
      }), xi(rt, [R, It]);
    },
    onEnterCancelled(R) {
      j(R, !1), xi(D, [R]);
    },
    onAppearCancelled(R) {
      j(R, !0), xi(Q, [R]);
    },
    onLeaveCancelled(R) {
      V(R), xi(gt, [R]);
    }
  });
}
function dd(e) {
  if (e == null)
    return null;
  if (St(e))
    return [ws(e.enter), ws(e.leave)];
  {
    const n = ws(e);
    return [n, n];
  }
}
function ws(e) {
  return oc(e);
}
function ii(e, n) {
  n.split(/\s+/).forEach((s) => s && e.classList.add(s)), (e._vtc || (e._vtc = /* @__PURE__ */ new Set())).add(n);
}
function Li(e, n) {
  n.split(/\s+/).forEach((a) => a && e.classList.remove(a));
  const { _vtc: s } = e;
  s && (s.delete(n), s.size || (e._vtc = void 0));
}
function Da(e) {
  requestAnimationFrame(() => {
    requestAnimationFrame(e);
  });
}
let pd = 0;
function Fa(e, n, s, a) {
  const l = e._endId = ++pd, u = () => {
    l === e._endId && a();
  };
  if (s)
    return setTimeout(u, s);
  const { type: c, timeout: f, propCount: p } = md(e, n);
  if (!c)
    return a();
  const g = c + "end";
  let b = 0;
  const x = () => {
    e.removeEventListener(g, T), u();
  }, T = (k) => {
    k.target === e && ++b >= p && x();
  };
  setTimeout(() => {
    b < p && x();
  }, f + 1), e.addEventListener(g, T);
}
function md(e, n) {
  const s = window.getComputedStyle(e), a = (U) => (s[U] || "").split(", "), l = a(`${ei}Delay`), u = a(`${ei}Duration`), c = ja(l, u), f = a(`${pn}Delay`), p = a(`${pn}Duration`), g = ja(f, p);
  let b = null, x = 0, T = 0;
  n === ei ? c > 0 && (b = ei, x = c, T = u.length) : n === pn ? g > 0 && (b = pn, x = g, T = p.length) : (x = Math.max(c, g), b = x > 0 ? c > g ? ei : pn : null, T = b ? b === ei ? u.length : p.length : 0);
  const k = b === ei && /\b(transform|all)(,|$)/.test(a(`${ei}Property`).toString());
  return {
    type: b,
    timeout: x,
    propCount: T,
    hasTransform: k
  };
}
function ja(e, n) {
  for (; e.length < n.length; )
    e = e.concat(e);
  return Math.max(...n.map((s, a) => Ha(s) + Ha(e[a])));
}
function Ha(e) {
  return Number(e.slice(0, -1).replace(",", ".")) * 1e3;
}
function _d() {
  return document.body.offsetHeight;
}
const $a = (e) => {
  const n = e.props["onUpdate:modelValue"] || !1;
  return J(n) ? (s) => fo(n, s) : n;
};
function gd(e) {
  e.target.composing = !0;
}
function Wa(e) {
  const n = e.target;
  n.composing && (n.composing = !1, n.dispatchEvent(new Event("input")));
}
const vd = {
  created(e, { modifiers: { lazy: n, trim: s, number: a } }, l) {
    e._assign = $a(l);
    const u = a || l.props && l.props.type === "number";
    $i(e, n ? "change" : "input", (c) => {
      if (c.target.composing)
        return;
      let f = e.value;
      s && (f = f.trim()), u && (f = xs(f)), e._assign(f);
    }), s && $i(e, "change", () => {
      e.value = e.value.trim();
    }), n || ($i(e, "compositionstart", gd), $i(e, "compositionend", Wa), $i(e, "change", Wa));
  },
  // set value on mounted so it's after min/max for type="range"
  mounted(e, { value: n }) {
    e.value = n ?? "";
  },
  beforeUpdate(e, { value: n, modifiers: { lazy: s, trim: a, number: l } }, u) {
    if (e._assign = $a(u), e.composing || document.activeElement === e && e.type !== "range" && (s || a && e.value.trim() === n || (l || e.type === "number") && xs(e.value) === n))
      return;
    const c = n ?? "";
    e.value !== c && (e.value = c);
  }
}, yd = ["ctrl", "shift", "alt", "meta"], bd = {
  stop: (e) => e.stopPropagation(),
  prevent: (e) => e.preventDefault(),
  self: (e) => e.target !== e.currentTarget,
  ctrl: (e) => !e.ctrlKey,
  shift: (e) => !e.shiftKey,
  alt: (e) => !e.altKey,
  meta: (e) => !e.metaKey,
  left: (e) => "button" in e && e.button !== 0,
  middle: (e) => "button" in e && e.button !== 1,
  right: (e) => "button" in e && e.button !== 2,
  exact: (e, n) => yd.some((s) => e[`${s}Key`] && !n.includes(s))
}, Lo = (e, n) => (s, ...a) => {
  for (let l = 0; l < n.length; l++) {
    const u = bd[n[l]];
    if (u && u(s, n))
      return;
  }
  return e(s, ...a);
}, Ua = {
  beforeMount(e, { value: n }, { transition: s }) {
    e._vod = e.style.display === "none" ? "" : e.style.display, s && n ? s.beforeEnter(e) : mn(e, n);
  },
  mounted(e, { value: n }, { transition: s }) {
    s && n && s.enter(e);
  },
  updated(e, { value: n, oldValue: s }, { transition: a }) {
    !n != !s && (a ? n ? (a.beforeEnter(e), mn(e, !0), a.enter(e)) : a.leave(e, () => {
      mn(e, !1);
    }) : mn(e, n));
  },
  beforeUnmount(e, { value: n }) {
    mn(e, n);
  }
};
function mn(e, n) {
  e.style.display = n ? e._vod : "none";
}
const wd = /* @__PURE__ */ Xt({ patchProp: hd }, Jf);
let Va;
function Gl() {
  return Va || (Va = Af(wd));
}
const xd = (...e) => {
  Gl().render(...e);
}, Ld = (...e) => {
  const n = Gl().createApp(...e), { mount: s } = n;
  return n.mount = (a) => {
    const l = Pd(a);
    if (!l)
      return;
    const u = n._component;
    !it(u) && !u.render && !u.template && (u.template = l.innerHTML), l.innerHTML = "";
    const c = s(l, !1, l instanceof SVGElement);
    return l instanceof Element && (l.removeAttribute("v-cloak"), l.setAttribute("data-v-app", "")), c;
  }, n;
};
function Pd(e) {
  return Zt(e) ? document.querySelector(e) : e;
}
var Td = typeof globalThis < "u" ? globalThis : typeof window < "u" ? window : typeof global < "u" ? global : typeof self < "u" ? self : {};
function Cd(e) {
  return e && e.__esModule && Object.prototype.hasOwnProperty.call(e, "default") ? e.default : e;
}
var Rs = { exports: {} };
/* @preserve
 * Leaflet 1.9.3, a JS library for interactive maps. https://leafletjs.com
 * (c) 2010-2022 Vladimir Agafonkin, (c) 2010-2011 CloudMade
 */
(function(e, n) {
  (function(s, a) {
    a(n);
  })(Td, function(s) {
    var a = "1.9.3";
    function l(t) {
      var i, o, r, h;
      for (o = 1, r = arguments.length; o < r; o++) {
        h = arguments[o];
        for (i in h)
          t[i] = h[i];
      }
      return t;
    }
    var u = Object.create || function() {
      function t() {
      }
      return function(i) {
        return t.prototype = i, new t();
      };
    }();
    function c(t, i) {
      var o = Array.prototype.slice;
      if (t.bind)
        return t.bind.apply(t, o.call(arguments, 1));
      var r = o.call(arguments, 2);
      return function() {
        return t.apply(i, r.length ? r.concat(o.call(arguments)) : arguments);
      };
    }
    var f = 0;
    function p(t) {
      return "_leaflet_id" in t || (t._leaflet_id = ++f), t._leaflet_id;
    }
    function g(t, i, o) {
      var r, h, d, m;
      return m = function() {
        r = !1, h && (d.apply(o, h), h = !1);
      }, d = function() {
        r ? h = arguments : (t.apply(o, arguments), setTimeout(m, i), r = !0);
      }, d;
    }
    function b(t, i, o) {
      var r = i[1], h = i[0], d = r - h;
      return t === r && o ? t : ((t - h) % d + d) % d + h;
    }
    function x() {
      return !1;
    }
    function T(t, i) {
      if (i === !1)
        return t;
      var o = Math.pow(10, i === void 0 ? 6 : i);
      return Math.round(t * o) / o;
    }
    function k(t) {
      return t.trim ? t.trim() : t.replace(/^\s+|\s+$/g, "");
    }
    function U(t) {
      return k(t).split(/\s+/);
    }
    function E(t, i) {
      Object.prototype.hasOwnProperty.call(t, "options") || (t.options = t.options ? u(t.options) : {});
      for (var o in i)
        t.options[o] = i[o];
      return t.options;
    }
    function tt(t, i, o) {
      var r = [];
      for (var h in t)
        r.push(encodeURIComponent(o ? h.toUpperCase() : h) + "=" + encodeURIComponent(t[h]));
      return (!i || i.indexOf("?") === -1 ? "?" : "&") + r.join("&");
    }
    var X = /\{ *([\w_ -]+) *\}/g;
    function xt(t, i) {
      return t.replace(X, function(o, r) {
        var h = i[r];
        if (h === void 0)
          throw new Error("No value provided for variable " + o);
        return typeof h == "function" && (h = h(i)), h;
      });
    }
    var D = Array.isArray || function(t) {
      return Object.prototype.toString.call(t) === "[object Array]";
    };
    function rt(t, i) {
      for (var o = 0; o < t.length; o++)
        if (t[o] === i)
          return o;
      return -1;
    }
    var gt = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=";
    function Kt(t) {
      return window["webkit" + t] || window["moz" + t] || window["ms" + t];
    }
    var qt = 0;
    function Q(t) {
      var i = +/* @__PURE__ */ new Date(), o = Math.max(0, 16 - (i - qt));
      return qt = i + o, window.setTimeout(t, o);
    }
    var j = window.requestAnimationFrame || Kt("RequestAnimationFrame") || Q, V = window.cancelAnimationFrame || Kt("CancelAnimationFrame") || Kt("CancelRequestAnimationFrame") || function(t) {
      window.clearTimeout(t);
    };
    function ut(t, i, o) {
      if (o && j === Q)
        t.call(i);
      else
        return j.call(window, c(t, i));
    }
    function R(t) {
      t && V.call(window, t);
    }
    var Tt = {
      __proto__: null,
      extend: l,
      create: u,
      bind: c,
      get lastId() {
        return f;
      },
      stamp: p,
      throttle: g,
      wrapNum: b,
      falseFn: x,
      formatNum: T,
      trim: k,
      splitWords: U,
      setOptions: E,
      getParamString: tt,
      template: xt,
      isArray: D,
      indexOf: rt,
      emptyImageUrl: gt,
      requestFn: j,
      cancelFn: V,
      requestAnimFrame: ut,
      cancelAnimFrame: R
    };
    function It() {
    }
    It.extend = function(t) {
      var i = function() {
        E(this), this.initialize && this.initialize.apply(this, arguments), this.callInitHooks();
      }, o = i.__super__ = this.prototype, r = u(o);
      r.constructor = i, i.prototype = r;
      for (var h in this)
        Object.prototype.hasOwnProperty.call(this, h) && h !== "prototype" && h !== "__super__" && (i[h] = this[h]);
      return t.statics && l(i, t.statics), t.includes && (ci(t.includes), l.apply(null, [r].concat(t.includes))), l(r, t), delete r.statics, delete r.includes, r.options && (r.options = o.options ? u(o.options) : {}, l(r.options, t.options)), r._initHooks = [], r.callInitHooks = function() {
        if (!this._initHooksCalled) {
          o.callInitHooks && o.callInitHooks.call(this), this._initHooksCalled = !0;
          for (var d = 0, m = r._initHooks.length; d < m; d++)
            r._initHooks[d].call(this);
        }
      }, i;
    }, It.include = function(t) {
      var i = this.prototype.options;
      return l(this.prototype, t), t.options && (this.prototype.options = i, this.mergeOptions(t.options)), this;
    }, It.mergeOptions = function(t) {
      return l(this.prototype.options, t), this;
    }, It.addInitHook = function(t) {
      var i = Array.prototype.slice.call(arguments, 1), o = typeof t == "function" ? t : function() {
        this[t].apply(this, i);
      };
      return this.prototype._initHooks = this.prototype._initHooks || [], this.prototype._initHooks.push(o), this;
    };
    function ci(t) {
      if (!(typeof L > "u" || !L || !L.Mixin)) {
        t = D(t) ? t : [t];
        for (var i = 0; i < t.length; i++)
          t[i] === L.Mixin.Events && console.warn("Deprecated include of L.Mixin.Events: this property will be removed in future releases, please inherit from L.Evented instead.", new Error().stack);
      }
    }
    var ft = {
      /* @method on(type: String, fn: Function, context?: Object): this
       * Adds a listener function (`fn`) to a particular event type of the object. You can optionally specify the context of the listener (object the this keyword will point to). You can also pass several space-separated types (e.g. `'click dblclick'`).
       *
       * @alternative
       * @method on(eventMap: Object): this
       * Adds a set of type/listener pairs, e.g. `{click: onClick, mousemove: onMouseMove}`
       */
      on: function(t, i, o) {
        if (typeof t == "object")
          for (var r in t)
            this._on(r, t[r], i);
        else {
          t = U(t);
          for (var h = 0, d = t.length; h < d; h++)
            this._on(t[h], i, o);
        }
        return this;
      },
      /* @method off(type: String, fn?: Function, context?: Object): this
       * Removes a previously added listener function. If no function is specified, it will remove all the listeners of that particular event from the object. Note that if you passed a custom context to `on`, you must pass the same context to `off` in order to remove the listener.
       *
       * @alternative
       * @method off(eventMap: Object): this
       * Removes a set of type/listener pairs.
       *
       * @alternative
       * @method off: this
       * Removes all listeners to all events on the object. This includes implicitly attached events.
       */
      off: function(t, i, o) {
        if (!arguments.length)
          delete this._events;
        else if (typeof t == "object")
          for (var r in t)
            this._off(r, t[r], i);
        else {
          t = U(t);
          for (var h = arguments.length === 1, d = 0, m = t.length; d < m; d++)
            h ? this._off(t[d]) : this._off(t[d], i, o);
        }
        return this;
      },
      // attach listener (without syntactic sugar now)
      _on: function(t, i, o, r) {
        if (typeof i != "function") {
          console.warn("wrong listener type: " + typeof i);
          return;
        }
        if (this._listens(t, i, o) === !1) {
          o === this && (o = void 0);
          var h = { fn: i, ctx: o };
          r && (h.once = !0), this._events = this._events || {}, this._events[t] = this._events[t] || [], this._events[t].push(h);
        }
      },
      _off: function(t, i, o) {
        var r, h, d;
        if (this._events && (r = this._events[t], !!r)) {
          if (arguments.length === 1) {
            if (this._firingCount)
              for (h = 0, d = r.length; h < d; h++)
                r[h].fn = x;
            delete this._events[t];
            return;
          }
          if (typeof i != "function") {
            console.warn("wrong listener type: " + typeof i);
            return;
          }
          var m = this._listens(t, i, o);
          if (m !== !1) {
            var y = r[m];
            this._firingCount && (y.fn = x, this._events[t] = r = r.slice()), r.splice(m, 1);
          }
        }
      },
      // @method fire(type: String, data?: Object, propagate?: Boolean): this
      // Fires an event of the specified type. You can optionally provide a data
      // object — the first argument of the listener function will contain its
      // properties. The event can optionally be propagated to event parents.
      fire: function(t, i, o) {
        if (!this.listens(t, o))
          return this;
        var r = l({}, i, {
          type: t,
          target: this,
          sourceTarget: i && i.sourceTarget || this
        });
        if (this._events) {
          var h = this._events[t];
          if (h) {
            this._firingCount = this._firingCount + 1 || 1;
            for (var d = 0, m = h.length; d < m; d++) {
              var y = h[d], w = y.fn;
              y.once && this.off(t, w, y.ctx), w.call(y.ctx || this, r);
            }
            this._firingCount--;
          }
        }
        return o && this._propagateEvent(r), this;
      },
      // @method listens(type: String, propagate?: Boolean): Boolean
      // @method listens(type: String, fn: Function, context?: Object, propagate?: Boolean): Boolean
      // Returns `true` if a particular event type has any listeners attached to it.
      // The verification can optionally be propagated, it will return `true` if parents have the listener attached to it.
      listens: function(t, i, o, r) {
        typeof t != "string" && console.warn('"string" type argument expected');
        var h = i;
        typeof i != "function" && (r = !!i, h = void 0, o = void 0);
        var d = this._events && this._events[t];
        if (d && d.length && this._listens(t, h, o) !== !1)
          return !0;
        if (r) {
          for (var m in this._eventParents)
            if (this._eventParents[m].listens(t, i, o, r))
              return !0;
        }
        return !1;
      },
      // returns the index (number) or false
      _listens: function(t, i, o) {
        if (!this._events)
          return !1;
        var r = this._events[t] || [];
        if (!i)
          return !!r.length;
        o === this && (o = void 0);
        for (var h = 0, d = r.length; h < d; h++)
          if (r[h].fn === i && r[h].ctx === o)
            return h;
        return !1;
      },
      // @method once(…): this
      // Behaves as [`on(…)`](#evented-on), except the listener will only get fired once and then removed.
      once: function(t, i, o) {
        if (typeof t == "object")
          for (var r in t)
            this._on(r, t[r], i, !0);
        else {
          t = U(t);
          for (var h = 0, d = t.length; h < d; h++)
            this._on(t[h], i, o, !0);
        }
        return this;
      },
      // @method addEventParent(obj: Evented): this
      // Adds an event parent - an `Evented` that will receive propagated events
      addEventParent: function(t) {
        return this._eventParents = this._eventParents || {}, this._eventParents[p(t)] = t, this;
      },
      // @method removeEventParent(obj: Evented): this
      // Removes an event parent, so it will stop receiving propagated events
      removeEventParent: function(t) {
        return this._eventParents && delete this._eventParents[p(t)], this;
      },
      _propagateEvent: function(t) {
        for (var i in this._eventParents)
          this._eventParents[i].fire(t.type, l({
            layer: t.target,
            propagatedFrom: t.target
          }, t), !0);
      }
    };
    ft.addEventListener = ft.on, ft.removeEventListener = ft.clearAllEventListeners = ft.off, ft.addOneTimeEventListener = ft.once, ft.fireEvent = ft.fire, ft.hasEventListeners = ft.listens;
    var bt = It.extend(ft);
    function Z(t, i, o) {
      this.x = o ? Math.round(t) : t, this.y = o ? Math.round(i) : i;
    }
    var Ee = Math.trunc || function(t) {
      return t > 0 ? Math.floor(t) : Math.ceil(t);
    };
    Z.prototype = {
      // @method clone(): Point
      // Returns a copy of the current point.
      clone: function() {
        return new Z(this.x, this.y);
      },
      // @method add(otherPoint: Point): Point
      // Returns the result of addition of the current and the given points.
      add: function(t) {
        return this.clone()._add(K(t));
      },
      _add: function(t) {
        return this.x += t.x, this.y += t.y, this;
      },
      // @method subtract(otherPoint: Point): Point
      // Returns the result of subtraction of the given point from the current.
      subtract: function(t) {
        return this.clone()._subtract(K(t));
      },
      _subtract: function(t) {
        return this.x -= t.x, this.y -= t.y, this;
      },
      // @method divideBy(num: Number): Point
      // Returns the result of division of the current point by the given number.
      divideBy: function(t) {
        return this.clone()._divideBy(t);
      },
      _divideBy: function(t) {
        return this.x /= t, this.y /= t, this;
      },
      // @method multiplyBy(num: Number): Point
      // Returns the result of multiplication of the current point by the given number.
      multiplyBy: function(t) {
        return this.clone()._multiplyBy(t);
      },
      _multiplyBy: function(t) {
        return this.x *= t, this.y *= t, this;
      },
      // @method scaleBy(scale: Point): Point
      // Multiply each coordinate of the current point by each coordinate of
      // `scale`. In linear algebra terms, multiply the point by the
      // [scaling matrix](https://en.wikipedia.org/wiki/Scaling_%28geometry%29#Matrix_representation)
      // defined by `scale`.
      scaleBy: function(t) {
        return new Z(this.x * t.x, this.y * t.y);
      },
      // @method unscaleBy(scale: Point): Point
      // Inverse of `scaleBy`. Divide each coordinate of the current point by
      // each coordinate of `scale`.
      unscaleBy: function(t) {
        return new Z(this.x / t.x, this.y / t.y);
      },
      // @method round(): Point
      // Returns a copy of the current point with rounded coordinates.
      round: function() {
        return this.clone()._round();
      },
      _round: function() {
        return this.x = Math.round(this.x), this.y = Math.round(this.y), this;
      },
      // @method floor(): Point
      // Returns a copy of the current point with floored coordinates (rounded down).
      floor: function() {
        return this.clone()._floor();
      },
      _floor: function() {
        return this.x = Math.floor(this.x), this.y = Math.floor(this.y), this;
      },
      // @method ceil(): Point
      // Returns a copy of the current point with ceiled coordinates (rounded up).
      ceil: function() {
        return this.clone()._ceil();
      },
      _ceil: function() {
        return this.x = Math.ceil(this.x), this.y = Math.ceil(this.y), this;
      },
      // @method trunc(): Point
      // Returns a copy of the current point with truncated coordinates (rounded towards zero).
      trunc: function() {
        return this.clone()._trunc();
      },
      _trunc: function() {
        return this.x = Ee(this.x), this.y = Ee(this.y), this;
      },
      // @method distanceTo(otherPoint: Point): Number
      // Returns the cartesian distance between the current and the given points.
      distanceTo: function(t) {
        t = K(t);
        var i = t.x - this.x, o = t.y - this.y;
        return Math.sqrt(i * i + o * o);
      },
      // @method equals(otherPoint: Point): Boolean
      // Returns `true` if the given point has the same coordinates.
      equals: function(t) {
        return t = K(t), t.x === this.x && t.y === this.y;
      },
      // @method contains(otherPoint: Point): Boolean
      // Returns `true` if both coordinates of the given point are less than the corresponding current point coordinates (in absolute values).
      contains: function(t) {
        return t = K(t), Math.abs(t.x) <= Math.abs(this.x) && Math.abs(t.y) <= Math.abs(this.y);
      },
      // @method toString(): String
      // Returns a string representation of the point for debugging purposes.
      toString: function() {
        return "Point(" + T(this.x) + ", " + T(this.y) + ")";
      }
    };
    function K(t, i, o) {
      return t instanceof Z ? t : D(t) ? new Z(t[0], t[1]) : t == null ? t : typeof t == "object" && "x" in t && "y" in t ? new Z(t.x, t.y) : new Z(t, i, o);
    }
    function mt(t, i) {
      if (t)
        for (var o = i ? [t, i] : t, r = 0, h = o.length; r < h; r++)
          this.extend(o[r]);
    }
    mt.prototype = {
      // @method extend(point: Point): this
      // Extends the bounds to contain the given point.
      // @alternative
      // @method extend(otherBounds: Bounds): this
      // Extend the bounds to contain the given bounds
      extend: function(t) {
        var i, o;
        if (!t)
          return this;
        if (t instanceof Z || typeof t[0] == "number" || "x" in t)
          i = o = K(t);
        else if (t = Ct(t), i = t.min, o = t.max, !i || !o)
          return this;
        return !this.min && !this.max ? (this.min = i.clone(), this.max = o.clone()) : (this.min.x = Math.min(i.x, this.min.x), this.max.x = Math.max(o.x, this.max.x), this.min.y = Math.min(i.y, this.min.y), this.max.y = Math.max(o.y, this.max.y)), this;
      },
      // @method getCenter(round?: Boolean): Point
      // Returns the center point of the bounds.
      getCenter: function(t) {
        return K(
          (this.min.x + this.max.x) / 2,
          (this.min.y + this.max.y) / 2,
          t
        );
      },
      // @method getBottomLeft(): Point
      // Returns the bottom-left point of the bounds.
      getBottomLeft: function() {
        return K(this.min.x, this.max.y);
      },
      // @method getTopRight(): Point
      // Returns the top-right point of the bounds.
      getTopRight: function() {
        return K(this.max.x, this.min.y);
      },
      // @method getTopLeft(): Point
      // Returns the top-left point of the bounds (i.e. [`this.min`](#bounds-min)).
      getTopLeft: function() {
        return this.min;
      },
      // @method getBottomRight(): Point
      // Returns the bottom-right point of the bounds (i.e. [`this.max`](#bounds-max)).
      getBottomRight: function() {
        return this.max;
      },
      // @method getSize(): Point
      // Returns the size of the given bounds
      getSize: function() {
        return this.max.subtract(this.min);
      },
      // @method contains(otherBounds: Bounds): Boolean
      // Returns `true` if the rectangle contains the given one.
      // @alternative
      // @method contains(point: Point): Boolean
      // Returns `true` if the rectangle contains the given point.
      contains: function(t) {
        var i, o;
        return typeof t[0] == "number" || t instanceof Z ? t = K(t) : t = Ct(t), t instanceof mt ? (i = t.min, o = t.max) : i = o = t, i.x >= this.min.x && o.x <= this.max.x && i.y >= this.min.y && o.y <= this.max.y;
      },
      // @method intersects(otherBounds: Bounds): Boolean
      // Returns `true` if the rectangle intersects the given bounds. Two bounds
      // intersect if they have at least one point in common.
      intersects: function(t) {
        t = Ct(t);
        var i = this.min, o = this.max, r = t.min, h = t.max, d = h.x >= i.x && r.x <= o.x, m = h.y >= i.y && r.y <= o.y;
        return d && m;
      },
      // @method overlaps(otherBounds: Bounds): Boolean
      // Returns `true` if the rectangle overlaps the given bounds. Two bounds
      // overlap if their intersection is an area.
      overlaps: function(t) {
        t = Ct(t);
        var i = this.min, o = this.max, r = t.min, h = t.max, d = h.x > i.x && r.x < o.x, m = h.y > i.y && r.y < o.y;
        return d && m;
      },
      // @method isValid(): Boolean
      // Returns `true` if the bounds are properly initialized.
      isValid: function() {
        return !!(this.min && this.max);
      },
      // @method pad(bufferRatio: Number): Bounds
      // Returns bounds created by extending or retracting the current bounds by a given ratio in each direction.
      // For example, a ratio of 0.5 extends the bounds by 50% in each direction.
      // Negative values will retract the bounds.
      pad: function(t) {
        var i = this.min, o = this.max, r = Math.abs(i.x - o.x) * t, h = Math.abs(i.y - o.y) * t;
        return Ct(
          K(i.x - r, i.y - h),
          K(o.x + r, o.y + h)
        );
      },
      // @method equals(otherBounds: Bounds): Boolean
      // Returns `true` if the rectangle is equivalent to the given bounds.
      equals: function(t) {
        return t ? (t = Ct(t), this.min.equals(t.getTopLeft()) && this.max.equals(t.getBottomRight())) : !1;
      }
    };
    function Ct(t, i) {
      return !t || t instanceof mt ? t : new mt(t, i);
    }
    function Gt(t, i) {
      if (t)
        for (var o = i ? [t, i] : t, r = 0, h = o.length; r < h; r++)
          this.extend(o[r]);
    }
    Gt.prototype = {
      // @method extend(latlng: LatLng): this
      // Extend the bounds to contain the given point
      // @alternative
      // @method extend(otherBounds: LatLngBounds): this
      // Extend the bounds to contain the given bounds
      extend: function(t) {
        var i = this._southWest, o = this._northEast, r, h;
        if (t instanceof vt)
          r = t, h = t;
        else if (t instanceof Gt) {
          if (r = t._southWest, h = t._northEast, !r || !h)
            return this;
        } else
          return t ? this.extend(at(t) || $t(t)) : this;
        return !i && !o ? (this._southWest = new vt(r.lat, r.lng), this._northEast = new vt(h.lat, h.lng)) : (i.lat = Math.min(r.lat, i.lat), i.lng = Math.min(r.lng, i.lng), o.lat = Math.max(h.lat, o.lat), o.lng = Math.max(h.lng, o.lng)), this;
      },
      // @method pad(bufferRatio: Number): LatLngBounds
      // Returns bounds created by extending or retracting the current bounds by a given ratio in each direction.
      // For example, a ratio of 0.5 extends the bounds by 50% in each direction.
      // Negative values will retract the bounds.
      pad: function(t) {
        var i = this._southWest, o = this._northEast, r = Math.abs(i.lat - o.lat) * t, h = Math.abs(i.lng - o.lng) * t;
        return new Gt(
          new vt(i.lat - r, i.lng - h),
          new vt(o.lat + r, o.lng + h)
        );
      },
      // @method getCenter(): LatLng
      // Returns the center point of the bounds.
      getCenter: function() {
        return new vt(
          (this._southWest.lat + this._northEast.lat) / 2,
          (this._southWest.lng + this._northEast.lng) / 2
        );
      },
      // @method getSouthWest(): LatLng
      // Returns the south-west point of the bounds.
      getSouthWest: function() {
        return this._southWest;
      },
      // @method getNorthEast(): LatLng
      // Returns the north-east point of the bounds.
      getNorthEast: function() {
        return this._northEast;
      },
      // @method getNorthWest(): LatLng
      // Returns the north-west point of the bounds.
      getNorthWest: function() {
        return new vt(this.getNorth(), this.getWest());
      },
      // @method getSouthEast(): LatLng
      // Returns the south-east point of the bounds.
      getSouthEast: function() {
        return new vt(this.getSouth(), this.getEast());
      },
      // @method getWest(): Number
      // Returns the west longitude of the bounds
      getWest: function() {
        return this._southWest.lng;
      },
      // @method getSouth(): Number
      // Returns the south latitude of the bounds
      getSouth: function() {
        return this._southWest.lat;
      },
      // @method getEast(): Number
      // Returns the east longitude of the bounds
      getEast: function() {
        return this._northEast.lng;
      },
      // @method getNorth(): Number
      // Returns the north latitude of the bounds
      getNorth: function() {
        return this._northEast.lat;
      },
      // @method contains(otherBounds: LatLngBounds): Boolean
      // Returns `true` if the rectangle contains the given one.
      // @alternative
      // @method contains (latlng: LatLng): Boolean
      // Returns `true` if the rectangle contains the given point.
      contains: function(t) {
        typeof t[0] == "number" || t instanceof vt || "lat" in t ? t = at(t) : t = $t(t);
        var i = this._southWest, o = this._northEast, r, h;
        return t instanceof Gt ? (r = t.getSouthWest(), h = t.getNorthEast()) : r = h = t, r.lat >= i.lat && h.lat <= o.lat && r.lng >= i.lng && h.lng <= o.lng;
      },
      // @method intersects(otherBounds: LatLngBounds): Boolean
      // Returns `true` if the rectangle intersects the given bounds. Two bounds intersect if they have at least one point in common.
      intersects: function(t) {
        t = $t(t);
        var i = this._southWest, o = this._northEast, r = t.getSouthWest(), h = t.getNorthEast(), d = h.lat >= i.lat && r.lat <= o.lat, m = h.lng >= i.lng && r.lng <= o.lng;
        return d && m;
      },
      // @method overlaps(otherBounds: LatLngBounds): Boolean
      // Returns `true` if the rectangle overlaps the given bounds. Two bounds overlap if their intersection is an area.
      overlaps: function(t) {
        t = $t(t);
        var i = this._southWest, o = this._northEast, r = t.getSouthWest(), h = t.getNorthEast(), d = h.lat > i.lat && r.lat < o.lat, m = h.lng > i.lng && r.lng < o.lng;
        return d && m;
      },
      // @method toBBoxString(): String
      // Returns a string with bounding box coordinates in a 'southwest_lng,southwest_lat,northeast_lng,northeast_lat' format. Useful for sending requests to web services that return geo data.
      toBBoxString: function() {
        return [this.getWest(), this.getSouth(), this.getEast(), this.getNorth()].join(",");
      },
      // @method equals(otherBounds: LatLngBounds, maxMargin?: Number): Boolean
      // Returns `true` if the rectangle is equivalent (within a small margin of error) to the given bounds. The margin of error can be overridden by setting `maxMargin` to a small number.
      equals: function(t, i) {
        return t ? (t = $t(t), this._southWest.equals(t.getSouthWest(), i) && this._northEast.equals(t.getNorthEast(), i)) : !1;
      },
      // @method isValid(): Boolean
      // Returns `true` if the bounds are properly initialized.
      isValid: function() {
        return !!(this._southWest && this._northEast);
      }
    };
    function $t(t, i) {
      return t instanceof Gt ? t : new Gt(t, i);
    }
    function vt(t, i, o) {
      if (isNaN(t) || isNaN(i))
        throw new Error("Invalid LatLng object: (" + t + ", " + i + ")");
      this.lat = +t, this.lng = +i, o !== void 0 && (this.alt = +o);
    }
    vt.prototype = {
      // @method equals(otherLatLng: LatLng, maxMargin?: Number): Boolean
      // Returns `true` if the given `LatLng` point is at the same position (within a small margin of error). The margin of error can be overridden by setting `maxMargin` to a small number.
      equals: function(t, i) {
        if (!t)
          return !1;
        t = at(t);
        var o = Math.max(
          Math.abs(this.lat - t.lat),
          Math.abs(this.lng - t.lng)
        );
        return o <= (i === void 0 ? 1e-9 : i);
      },
      // @method toString(): String
      // Returns a string representation of the point (for debugging purposes).
      toString: function(t) {
        return "LatLng(" + T(this.lat, t) + ", " + T(this.lng, t) + ")";
      },
      // @method distanceTo(otherLatLng: LatLng): Number
      // Returns the distance (in meters) to the given `LatLng` calculated using the [Spherical Law of Cosines](https://en.wikipedia.org/wiki/Spherical_law_of_cosines).
      distanceTo: function(t) {
        return be.distance(this, at(t));
      },
      // @method wrap(): LatLng
      // Returns a new `LatLng` object with the longitude wrapped so it's always between -180 and +180 degrees.
      wrap: function() {
        return be.wrapLatLng(this);
      },
      // @method toBounds(sizeInMeters: Number): LatLngBounds
      // Returns a new `LatLngBounds` object in which each boundary is `sizeInMeters/2` meters apart from the `LatLng`.
      toBounds: function(t) {
        var i = 180 * t / 40075017, o = i / Math.cos(Math.PI / 180 * this.lat);
        return $t(
          [this.lat - i, this.lng - o],
          [this.lat + i, this.lng + o]
        );
      },
      clone: function() {
        return new vt(this.lat, this.lng, this.alt);
      }
    };
    function at(t, i, o) {
      return t instanceof vt ? t : D(t) && typeof t[0] != "object" ? t.length === 3 ? new vt(t[0], t[1], t[2]) : t.length === 2 ? new vt(t[0], t[1]) : null : t == null ? t : typeof t == "object" && "lat" in t ? new vt(t.lat, "lng" in t ? t.lng : t.lon, t.alt) : i === void 0 ? null : new vt(t, i, o);
    }
    var le = {
      // @method latLngToPoint(latlng: LatLng, zoom: Number): Point
      // Projects geographical coordinates into pixel coordinates for a given zoom.
      latLngToPoint: function(t, i) {
        var o = this.projection.project(t), r = this.scale(i);
        return this.transformation._transform(o, r);
      },
      // @method pointToLatLng(point: Point, zoom: Number): LatLng
      // The inverse of `latLngToPoint`. Projects pixel coordinates on a given
      // zoom into geographical coordinates.
      pointToLatLng: function(t, i) {
        var o = this.scale(i), r = this.transformation.untransform(t, o);
        return this.projection.unproject(r);
      },
      // @method project(latlng: LatLng): Point
      // Projects geographical coordinates into coordinates in units accepted for
      // this CRS (e.g. meters for EPSG:3857, for passing it to WMS services).
      project: function(t) {
        return this.projection.project(t);
      },
      // @method unproject(point: Point): LatLng
      // Given a projected coordinate returns the corresponding LatLng.
      // The inverse of `project`.
      unproject: function(t) {
        return this.projection.unproject(t);
      },
      // @method scale(zoom: Number): Number
      // Returns the scale used when transforming projected coordinates into
      // pixel coordinates for a particular zoom. For example, it returns
      // `256 * 2^zoom` for Mercator-based CRS.
      scale: function(t) {
        return 256 * Math.pow(2, t);
      },
      // @method zoom(scale: Number): Number
      // Inverse of `scale()`, returns the zoom level corresponding to a scale
      // factor of `scale`.
      zoom: function(t) {
        return Math.log(t / 256) / Math.LN2;
      },
      // @method getProjectedBounds(zoom: Number): Bounds
      // Returns the projection's bounds scaled and transformed for the provided `zoom`.
      getProjectedBounds: function(t) {
        if (this.infinite)
          return null;
        var i = this.projection.bounds, o = this.scale(t), r = this.transformation.transform(i.min, o), h = this.transformation.transform(i.max, o);
        return new mt(r, h);
      },
      // @method distance(latlng1: LatLng, latlng2: LatLng): Number
      // Returns the distance between two geographical coordinates.
      // @property code: String
      // Standard code name of the CRS passed into WMS services (e.g. `'EPSG:3857'`)
      //
      // @property wrapLng: Number[]
      // An array of two numbers defining whether the longitude (horizontal) coordinate
      // axis wraps around a given range and how. Defaults to `[-180, 180]` in most
      // geographical CRSs. If `undefined`, the longitude axis does not wrap around.
      //
      // @property wrapLat: Number[]
      // Like `wrapLng`, but for the latitude (vertical) axis.
      // wrapLng: [min, max],
      // wrapLat: [min, max],
      // @property infinite: Boolean
      // If true, the coordinate space will be unbounded (infinite in both axes)
      infinite: !1,
      // @method wrapLatLng(latlng: LatLng): LatLng
      // Returns a `LatLng` where lat and lng has been wrapped according to the
      // CRS's `wrapLat` and `wrapLng` properties, if they are outside the CRS's bounds.
      wrapLatLng: function(t) {
        var i = this.wrapLng ? b(t.lng, this.wrapLng, !0) : t.lng, o = this.wrapLat ? b(t.lat, this.wrapLat, !0) : t.lat, r = t.alt;
        return new vt(o, i, r);
      },
      // @method wrapLatLngBounds(bounds: LatLngBounds): LatLngBounds
      // Returns a `LatLngBounds` with the same size as the given one, ensuring
      // that its center is within the CRS's bounds.
      // Only accepts actual `L.LatLngBounds` instances, not arrays.
      wrapLatLngBounds: function(t) {
        var i = t.getCenter(), o = this.wrapLatLng(i), r = i.lat - o.lat, h = i.lng - o.lng;
        if (r === 0 && h === 0)
          return t;
        var d = t.getSouthWest(), m = t.getNorthEast(), y = new vt(d.lat - r, d.lng - h), w = new vt(m.lat - r, m.lng - h);
        return new Gt(y, w);
      }
    }, be = l({}, le, {
      wrapLng: [-180, 180],
      // Mean Earth Radius, as recommended for use by
      // the International Union of Geodesy and Geophysics,
      // see https://rosettacode.org/wiki/Haversine_formula
      R: 6371e3,
      // distance between two geographical points using spherical law of cosines approximation
      distance: function(t, i) {
        var o = Math.PI / 180, r = t.lat * o, h = i.lat * o, d = Math.sin((i.lat - t.lat) * o / 2), m = Math.sin((i.lng - t.lng) * o / 2), y = d * d + Math.cos(r) * Math.cos(h) * m * m, w = 2 * Math.atan2(Math.sqrt(y), Math.sqrt(1 - y));
        return this.R * w;
      }
    }), Fe = 6378137, fi = {
      R: Fe,
      MAX_LATITUDE: 85.0511287798,
      project: function(t) {
        var i = Math.PI / 180, o = this.MAX_LATITUDE, r = Math.max(Math.min(o, t.lat), -o), h = Math.sin(r * i);
        return new Z(
          this.R * t.lng * i,
          this.R * Math.log((1 + h) / (1 - h)) / 2
        );
      },
      unproject: function(t) {
        var i = 180 / Math.PI;
        return new vt(
          (2 * Math.atan(Math.exp(t.y / this.R)) - Math.PI / 2) * i,
          t.x * i / this.R
        );
      },
      bounds: function() {
        var t = Fe * Math.PI;
        return new mt([-t, -t], [t, t]);
      }()
    };
    function di(t, i, o, r) {
      if (D(t)) {
        this._a = t[0], this._b = t[1], this._c = t[2], this._d = t[3];
        return;
      }
      this._a = t, this._b = i, this._c = o, this._d = r;
    }
    di.prototype = {
      // @method transform(point: Point, scale?: Number): Point
      // Returns a transformed point, optionally multiplied by the given scale.
      // Only accepts actual `L.Point` instances, not arrays.
      transform: function(t, i) {
        return this._transform(t.clone(), i);
      },
      // destructive transform (faster)
      _transform: function(t, i) {
        return i = i || 1, t.x = i * (this._a * t.x + this._b), t.y = i * (this._c * t.y + this._d), t;
      },
      // @method untransform(point: Point, scale?: Number): Point
      // Returns the reverse transformation of the given point, optionally divided
      // by the given scale. Only accepts actual `L.Point` instances, not arrays.
      untransform: function(t, i) {
        return i = i || 1, new Z(
          (t.x / i - this._b) / this._a,
          (t.y / i - this._d) / this._c
        );
      }
    };
    function _(t, i, o, r) {
      return new di(t, i, o, r);
    }
    var v = l({}, be, {
      code: "EPSG:3857",
      projection: fi,
      transformation: function() {
        var t = 0.5 / (Math.PI * fi.R);
        return _(t, 0.5, -t, 0.5);
      }()
    }), P = l({}, v, {
      code: "EPSG:900913"
    });
    function S(t) {
      return document.createElementNS("http://www.w3.org/2000/svg", t);
    }
    function C(t, i) {
      var o = "", r, h, d, m, y, w;
      for (r = 0, d = t.length; r < d; r++) {
        for (y = t[r], h = 0, m = y.length; h < m; h++)
          w = y[h], o += (h ? "L" : "M") + w.x + " " + w.y;
        o += i ? $.svg ? "z" : "x" : "";
      }
      return o || "M0 0";
    }
    var A = document.documentElement.style, z = "ActiveXObject" in window, I = z && !document.addEventListener, B = "msLaunchUri" in navigator && !("documentMode" in document), M = Ae("webkit"), H = Ae("android"), F = Ae("android 2") || Ae("android 3"), W = parseInt(/WebKit\/([0-9]+)|$/.exec(navigator.userAgent)[1], 10), q = H && Ae("Google") && W < 537 && !("AudioNode" in window), nt = !!window.opera, yt = !B && Ae("chrome"), _t = Ae("gecko") && !M && !nt && !z, Nt = !yt && Ae("safari"), ue = Ae("phantom"), je = "OTransition" in A, Nn = navigator.platform.indexOf("Win") === 0, Ge = z && "transition" in A, ki = "WebKitCSSMatrix" in window && "m11" in new window.WebKitCSSMatrix() && !F, te = "MozPerspective" in A, fe = !window.L_DISABLE_3D && (Ge || ki || te) && !je && !ue, Ye = typeof orientation < "u" || Ae("mobile"), Su = Ye && M, Mu = Ye && ki, dr = !window.PointerEvent && window.MSPointerEvent, pr = !!(window.PointerEvent || dr), mr = "ontouchstart" in window || !!window.TouchEvent, Eu = !window.L_NO_TOUCH && (mr || pr), Au = Ye && nt, Iu = Ye && _t, ku = (window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI) > 1, Bu = function() {
      var t = !1;
      try {
        var i = Object.defineProperty({}, "passive", {
          get: function() {
            t = !0;
          }
        });
        window.addEventListener("testPassiveEventSupport", x, i), window.removeEventListener("testPassiveEventSupport", x, i);
      } catch {
      }
      return t;
    }(), zu = function() {
      return !!document.createElement("canvas").getContext;
    }(), jo = !!(document.createElementNS && S("svg").createSVGRect), Nu = !!jo && function() {
      var t = document.createElement("div");
      return t.innerHTML = "<svg/>", (t.firstChild && t.firstChild.namespaceURI) === "http://www.w3.org/2000/svg";
    }(), Zu = !jo && function() {
      try {
        var t = document.createElement("div");
        t.innerHTML = '<v:shape adj="1"/>';
        var i = t.firstChild;
        return i.style.behavior = "url(#default#VML)", i && typeof i.adj == "object";
      } catch {
        return !1;
      }
    }(), Ru = navigator.platform.indexOf("Mac") === 0, Du = navigator.platform.indexOf("Linux") === 0;
    function Ae(t) {
      return navigator.userAgent.toLowerCase().indexOf(t) >= 0;
    }
    var $ = {
      ie: z,
      ielt9: I,
      edge: B,
      webkit: M,
      android: H,
      android23: F,
      androidStock: q,
      opera: nt,
      chrome: yt,
      gecko: _t,
      safari: Nt,
      phantom: ue,
      opera12: je,
      win: Nn,
      ie3d: Ge,
      webkit3d: ki,
      gecko3d: te,
      any3d: fe,
      mobile: Ye,
      mobileWebkit: Su,
      mobileWebkit3d: Mu,
      msPointer: dr,
      pointer: pr,
      touch: Eu,
      touchNative: mr,
      mobileOpera: Au,
      mobileGecko: Iu,
      retina: ku,
      passiveEvents: Bu,
      canvas: zu,
      svg: jo,
      vml: Zu,
      inlineSvg: Nu,
      mac: Ru,
      linux: Du
    }, _r = $.msPointer ? "MSPointerDown" : "pointerdown", gr = $.msPointer ? "MSPointerMove" : "pointermove", vr = $.msPointer ? "MSPointerUp" : "pointerup", yr = $.msPointer ? "MSPointerCancel" : "pointercancel", Ho = {
      touchstart: _r,
      touchmove: gr,
      touchend: vr,
      touchcancel: yr
    }, br = {
      touchstart: Uu,
      touchmove: Zn,
      touchend: Zn,
      touchcancel: Zn
    }, Bi = {}, wr = !1;
    function Fu(t, i, o) {
      return i === "touchstart" && Wu(), br[i] ? (o = br[i].bind(this, o), t.addEventListener(Ho[i], o, !1), o) : (console.warn("wrong event specified:", i), x);
    }
    function ju(t, i, o) {
      if (!Ho[i]) {
        console.warn("wrong event specified:", i);
        return;
      }
      t.removeEventListener(Ho[i], o, !1);
    }
    function Hu(t) {
      Bi[t.pointerId] = t;
    }
    function $u(t) {
      Bi[t.pointerId] && (Bi[t.pointerId] = t);
    }
    function xr(t) {
      delete Bi[t.pointerId];
    }
    function Wu() {
      wr || (document.addEventListener(_r, Hu, !0), document.addEventListener(gr, $u, !0), document.addEventListener(vr, xr, !0), document.addEventListener(yr, xr, !0), wr = !0);
    }
    function Zn(t, i) {
      if (i.pointerType !== (i.MSPOINTER_TYPE_MOUSE || "mouse")) {
        i.touches = [];
        for (var o in Bi)
          i.touches.push(Bi[o]);
        i.changedTouches = [i], t(i);
      }
    }
    function Uu(t, i) {
      i.MSPOINTER_TYPE_TOUCH && i.pointerType === i.MSPOINTER_TYPE_TOUCH && Yt(i), Zn(t, i);
    }
    function Vu(t) {
      var i = {}, o, r;
      for (r in t)
        o = t[r], i[r] = o && o.bind ? o.bind(t) : o;
      return t = i, i.type = "dblclick", i.detail = 2, i.isTrusted = !1, i._simulated = !0, i;
    }
    var Ku = 200;
    function qu(t, i) {
      t.addEventListener("dblclick", i);
      var o = 0, r;
      function h(d) {
        if (d.detail !== 1) {
          r = d.detail;
          return;
        }
        if (!(d.pointerType === "mouse" || d.sourceCapabilities && !d.sourceCapabilities.firesTouchEvents)) {
          var m = Or(d);
          if (!(m.some(function(w) {
            return w instanceof HTMLLabelElement && w.attributes.for;
          }) && !m.some(function(w) {
            return w instanceof HTMLInputElement || w instanceof HTMLSelectElement;
          }))) {
            var y = Date.now();
            y - o <= Ku ? (r++, r === 2 && i(Vu(d))) : r = 1, o = y;
          }
        }
      }
      return t.addEventListener("click", h), {
        dblclick: i,
        simDblclick: h
      };
    }
    function Gu(t, i) {
      t.removeEventListener("dblclick", i.dblclick), t.removeEventListener("click", i.simDblclick);
    }
    var $o = Fn(
      ["transform", "webkitTransform", "OTransform", "MozTransform", "msTransform"]
    ), en = Fn(
      ["webkitTransition", "transition", "OTransition", "MozTransition", "msTransition"]
    ), Lr = en === "webkitTransition" || en === "OTransition" ? en + "End" : "transitionend";
    function Pr(t) {
      return typeof t == "string" ? document.getElementById(t) : t;
    }
    function nn(t, i) {
      var o = t.style[i] || t.currentStyle && t.currentStyle[i];
      if ((!o || o === "auto") && document.defaultView) {
        var r = document.defaultView.getComputedStyle(t, null);
        o = r ? r[i] : null;
      }
      return o === "auto" ? null : o;
    }
    function dt(t, i, o) {
      var r = document.createElement(t);
      return r.className = i || "", o && o.appendChild(r), r;
    }
    function kt(t) {
      var i = t.parentNode;
      i && i.removeChild(t);
    }
    function Rn(t) {
      for (; t.firstChild; )
        t.removeChild(t.firstChild);
    }
    function zi(t) {
      var i = t.parentNode;
      i && i.lastChild !== t && i.appendChild(t);
    }
    function Ni(t) {
      var i = t.parentNode;
      i && i.firstChild !== t && i.insertBefore(t, i.firstChild);
    }
    function Wo(t, i) {
      if (t.classList !== void 0)
        return t.classList.contains(i);
      var o = Dn(t);
      return o.length > 0 && new RegExp("(^|\\s)" + i + "(\\s|$)").test(o);
    }
    function ot(t, i) {
      if (t.classList !== void 0)
        for (var o = U(i), r = 0, h = o.length; r < h; r++)
          t.classList.add(o[r]);
      else if (!Wo(t, i)) {
        var d = Dn(t);
        Uo(t, (d ? d + " " : "") + i);
      }
    }
    function jt(t, i) {
      t.classList !== void 0 ? t.classList.remove(i) : Uo(t, k((" " + Dn(t) + " ").replace(" " + i + " ", " ")));
    }
    function Uo(t, i) {
      t.className.baseVal === void 0 ? t.className = i : t.className.baseVal = i;
    }
    function Dn(t) {
      return t.correspondingElement && (t = t.correspondingElement), t.className.baseVal === void 0 ? t.className : t.className.baseVal;
    }
    function de(t, i) {
      "opacity" in t.style ? t.style.opacity = i : "filter" in t.style && Yu(t, i);
    }
    function Yu(t, i) {
      var o = !1, r = "DXImageTransform.Microsoft.Alpha";
      try {
        o = t.filters.item(r);
      } catch {
        if (i === 1)
          return;
      }
      i = Math.round(i * 100), o ? (o.Enabled = i !== 100, o.Opacity = i) : t.style.filter += " progid:" + r + "(opacity=" + i + ")";
    }
    function Fn(t) {
      for (var i = document.documentElement.style, o = 0; o < t.length; o++)
        if (t[o] in i)
          return t[o];
      return !1;
    }
    function pi(t, i, o) {
      var r = i || new Z(0, 0);
      t.style[$o] = ($.ie3d ? "translate(" + r.x + "px," + r.y + "px)" : "translate3d(" + r.x + "px," + r.y + "px,0)") + (o ? " scale(" + o + ")" : "");
    }
    function Wt(t, i) {
      t._leaflet_pos = i, $.any3d ? pi(t, i) : (t.style.left = i.x + "px", t.style.top = i.y + "px");
    }
    function mi(t) {
      return t._leaflet_pos || new Z(0, 0);
    }
    var on, sn, Vo;
    if ("onselectstart" in document)
      on = function() {
        et(window, "selectstart", Yt);
      }, sn = function() {
        Lt(window, "selectstart", Yt);
      };
    else {
      var rn = Fn(
        ["userSelect", "WebkitUserSelect", "OUserSelect", "MozUserSelect", "msUserSelect"]
      );
      on = function() {
        if (rn) {
          var t = document.documentElement.style;
          Vo = t[rn], t[rn] = "none";
        }
      }, sn = function() {
        rn && (document.documentElement.style[rn] = Vo, Vo = void 0);
      };
    }
    function Ko() {
      et(window, "dragstart", Yt);
    }
    function qo() {
      Lt(window, "dragstart", Yt);
    }
    var jn, Go;
    function Yo(t) {
      for (; t.tabIndex === -1; )
        t = t.parentNode;
      t.style && (Hn(), jn = t, Go = t.style.outline, t.style.outline = "none", et(window, "keydown", Hn));
    }
    function Hn() {
      jn && (jn.style.outline = Go, jn = void 0, Go = void 0, Lt(window, "keydown", Hn));
    }
    function Tr(t) {
      do
        t = t.parentNode;
      while ((!t.offsetWidth || !t.offsetHeight) && t !== document.body);
      return t;
    }
    function Jo(t) {
      var i = t.getBoundingClientRect();
      return {
        x: i.width / t.offsetWidth || 1,
        y: i.height / t.offsetHeight || 1,
        boundingClientRect: i
      };
    }
    var Ju = {
      __proto__: null,
      TRANSFORM: $o,
      TRANSITION: en,
      TRANSITION_END: Lr,
      get: Pr,
      getStyle: nn,
      create: dt,
      remove: kt,
      empty: Rn,
      toFront: zi,
      toBack: Ni,
      hasClass: Wo,
      addClass: ot,
      removeClass: jt,
      setClass: Uo,
      getClass: Dn,
      setOpacity: de,
      testProp: Fn,
      setTransform: pi,
      setPosition: Wt,
      getPosition: mi,
      get disableTextSelection() {
        return on;
      },
      get enableTextSelection() {
        return sn;
      },
      disableImageDrag: Ko,
      enableImageDrag: qo,
      preventOutline: Yo,
      restoreOutline: Hn,
      getSizedParentNode: Tr,
      getScale: Jo
    };
    function et(t, i, o, r) {
      if (i && typeof i == "object")
        for (var h in i)
          Qo(t, h, i[h], o);
      else {
        i = U(i);
        for (var d = 0, m = i.length; d < m; d++)
          Qo(t, i[d], o, r);
      }
      return this;
    }
    var Ie = "_leaflet_events";
    function Lt(t, i, o, r) {
      if (arguments.length === 1)
        Cr(t), delete t[Ie];
      else if (i && typeof i == "object")
        for (var h in i)
          ts(t, h, i[h], o);
      else if (i = U(i), arguments.length === 2)
        Cr(t, function(y) {
          return rt(i, y) !== -1;
        });
      else
        for (var d = 0, m = i.length; d < m; d++)
          ts(t, i[d], o, r);
      return this;
    }
    function Cr(t, i) {
      for (var o in t[Ie]) {
        var r = o.split(/\d/)[0];
        (!i || i(r)) && ts(t, r, null, null, o);
      }
    }
    var Xo = {
      mouseenter: "mouseover",
      mouseleave: "mouseout",
      wheel: !("onwheel" in window) && "mousewheel"
    };
    function Qo(t, i, o, r) {
      var h = i + p(o) + (r ? "_" + p(r) : "");
      if (t[Ie] && t[Ie][h])
        return this;
      var d = function(y) {
        return o.call(r || t, y || window.event);
      }, m = d;
      !$.touchNative && $.pointer && i.indexOf("touch") === 0 ? d = Fu(t, i, d) : $.touch && i === "dblclick" ? d = qu(t, d) : "addEventListener" in t ? i === "touchstart" || i === "touchmove" || i === "wheel" || i === "mousewheel" ? t.addEventListener(Xo[i] || i, d, $.passiveEvents ? { passive: !1 } : !1) : i === "mouseenter" || i === "mouseleave" ? (d = function(y) {
        y = y || window.event, is(t, y) && m(y);
      }, t.addEventListener(Xo[i], d, !1)) : t.addEventListener(i, m, !1) : t.attachEvent("on" + i, d), t[Ie] = t[Ie] || {}, t[Ie][h] = d;
    }
    function ts(t, i, o, r, h) {
      h = h || i + p(o) + (r ? "_" + p(r) : "");
      var d = t[Ie] && t[Ie][h];
      if (!d)
        return this;
      !$.touchNative && $.pointer && i.indexOf("touch") === 0 ? ju(t, i, d) : $.touch && i === "dblclick" ? Gu(t, d) : "removeEventListener" in t ? t.removeEventListener(Xo[i] || i, d, !1) : t.detachEvent("on" + i, d), t[Ie][h] = null;
    }
    function _i(t) {
      return t.stopPropagation ? t.stopPropagation() : t.originalEvent ? t.originalEvent._stopped = !0 : t.cancelBubble = !0, this;
    }
    function es(t) {
      return Qo(t, "wheel", _i), this;
    }
    function an(t) {
      return et(t, "mousedown touchstart dblclick contextmenu", _i), t._leaflet_disable_click = !0, this;
    }
    function Yt(t) {
      return t.preventDefault ? t.preventDefault() : t.returnValue = !1, this;
    }
    function gi(t) {
      return Yt(t), _i(t), this;
    }
    function Or(t) {
      if (t.composedPath)
        return t.composedPath();
      for (var i = [], o = t.target; o; )
        i.push(o), o = o.parentNode;
      return i;
    }
    function Sr(t, i) {
      if (!i)
        return new Z(t.clientX, t.clientY);
      var o = Jo(i), r = o.boundingClientRect;
      return new Z(
        // offset.left/top values are in page scale (like clientX/Y),
        // whereas clientLeft/Top (border width) values are the original values (before CSS scale applies).
        (t.clientX - r.left) / o.x - i.clientLeft,
        (t.clientY - r.top) / o.y - i.clientTop
      );
    }
    var Xu = $.linux && $.chrome ? window.devicePixelRatio : $.mac ? window.devicePixelRatio * 3 : window.devicePixelRatio > 0 ? 2 * window.devicePixelRatio : 1;
    function Mr(t) {
      return $.edge ? t.wheelDeltaY / 2 : (
        // Don't trust window-geometry-based delta
        t.deltaY && t.deltaMode === 0 ? -t.deltaY / Xu : (
          // Pixels
          t.deltaY && t.deltaMode === 1 ? -t.deltaY * 20 : (
            // Lines
            t.deltaY && t.deltaMode === 2 ? -t.deltaY * 60 : (
              // Pages
              t.deltaX || t.deltaZ ? 0 : (
                // Skip horizontal/depth wheel events
                t.wheelDelta ? (t.wheelDeltaY || t.wheelDelta) / 2 : (
                  // Legacy IE pixels
                  t.detail && Math.abs(t.detail) < 32765 ? -t.detail * 20 : (
                    // Legacy Moz lines
                    t.detail ? t.detail / -32765 * 60 : (
                      // Legacy Moz pages
                      0
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
    function is(t, i) {
      var o = i.relatedTarget;
      if (!o)
        return !0;
      try {
        for (; o && o !== t; )
          o = o.parentNode;
      } catch {
        return !1;
      }
      return o !== t;
    }
    var Qu = {
      __proto__: null,
      on: et,
      off: Lt,
      stopPropagation: _i,
      disableScrollPropagation: es,
      disableClickPropagation: an,
      preventDefault: Yt,
      stop: gi,
      getPropagationPath: Or,
      getMousePosition: Sr,
      getWheelDelta: Mr,
      isExternalTarget: is,
      addListener: et,
      removeListener: Lt
    }, Er = bt.extend({
      // @method run(el: HTMLElement, newPos: Point, duration?: Number, easeLinearity?: Number)
      // Run an animation of a given element to a new position, optionally setting
      // duration in seconds (`0.25` by default) and easing linearity factor (3rd
      // argument of the [cubic bezier curve](https://cubic-bezier.com/#0,0,.5,1),
      // `0.5` by default).
      run: function(t, i, o, r) {
        this.stop(), this._el = t, this._inProgress = !0, this._duration = o || 0.25, this._easeOutPower = 1 / Math.max(r || 0.5, 0.2), this._startPos = mi(t), this._offset = i.subtract(this._startPos), this._startTime = +/* @__PURE__ */ new Date(), this.fire("start"), this._animate();
      },
      // @method stop()
      // Stops the animation (if currently running).
      stop: function() {
        this._inProgress && (this._step(!0), this._complete());
      },
      _animate: function() {
        this._animId = ut(this._animate, this), this._step();
      },
      _step: function(t) {
        var i = +/* @__PURE__ */ new Date() - this._startTime, o = this._duration * 1e3;
        i < o ? this._runFrame(this._easeOut(i / o), t) : (this._runFrame(1), this._complete());
      },
      _runFrame: function(t, i) {
        var o = this._startPos.add(this._offset.multiplyBy(t));
        i && o._round(), Wt(this._el, o), this.fire("step");
      },
      _complete: function() {
        R(this._animId), this._inProgress = !1, this.fire("end");
      },
      _easeOut: function(t) {
        return 1 - Math.pow(1 - t, this._easeOutPower);
      }
    }), ht = bt.extend({
      options: {
        // @section Map State Options
        // @option crs: CRS = L.CRS.EPSG3857
        // The [Coordinate Reference System](#crs) to use. Don't change this if you're not
        // sure what it means.
        crs: v,
        // @option center: LatLng = undefined
        // Initial geographic center of the map
        center: void 0,
        // @option zoom: Number = undefined
        // Initial map zoom level
        zoom: void 0,
        // @option minZoom: Number = *
        // Minimum zoom level of the map.
        // If not specified and at least one `GridLayer` or `TileLayer` is in the map,
        // the lowest of their `minZoom` options will be used instead.
        minZoom: void 0,
        // @option maxZoom: Number = *
        // Maximum zoom level of the map.
        // If not specified and at least one `GridLayer` or `TileLayer` is in the map,
        // the highest of their `maxZoom` options will be used instead.
        maxZoom: void 0,
        // @option layers: Layer[] = []
        // Array of layers that will be added to the map initially
        layers: [],
        // @option maxBounds: LatLngBounds = null
        // When this option is set, the map restricts the view to the given
        // geographical bounds, bouncing the user back if the user tries to pan
        // outside the view. To set the restriction dynamically, use
        // [`setMaxBounds`](#map-setmaxbounds) method.
        maxBounds: void 0,
        // @option renderer: Renderer = *
        // The default method for drawing vector layers on the map. `L.SVG`
        // or `L.Canvas` by default depending on browser support.
        renderer: void 0,
        // @section Animation Options
        // @option zoomAnimation: Boolean = true
        // Whether the map zoom animation is enabled. By default it's enabled
        // in all browsers that support CSS3 Transitions except Android.
        zoomAnimation: !0,
        // @option zoomAnimationThreshold: Number = 4
        // Won't animate zoom if the zoom difference exceeds this value.
        zoomAnimationThreshold: 4,
        // @option fadeAnimation: Boolean = true
        // Whether the tile fade animation is enabled. By default it's enabled
        // in all browsers that support CSS3 Transitions except Android.
        fadeAnimation: !0,
        // @option markerZoomAnimation: Boolean = true
        // Whether markers animate their zoom with the zoom animation, if disabled
        // they will disappear for the length of the animation. By default it's
        // enabled in all browsers that support CSS3 Transitions except Android.
        markerZoomAnimation: !0,
        // @option transform3DLimit: Number = 2^23
        // Defines the maximum size of a CSS translation transform. The default
        // value should not be changed unless a web browser positions layers in
        // the wrong place after doing a large `panBy`.
        transform3DLimit: 8388608,
        // Precision limit of a 32-bit float
        // @section Interaction Options
        // @option zoomSnap: Number = 1
        // Forces the map's zoom level to always be a multiple of this, particularly
        // right after a [`fitBounds()`](#map-fitbounds) or a pinch-zoom.
        // By default, the zoom level snaps to the nearest integer; lower values
        // (e.g. `0.5` or `0.1`) allow for greater granularity. A value of `0`
        // means the zoom level will not be snapped after `fitBounds` or a pinch-zoom.
        zoomSnap: 1,
        // @option zoomDelta: Number = 1
        // Controls how much the map's zoom level will change after a
        // [`zoomIn()`](#map-zoomin), [`zoomOut()`](#map-zoomout), pressing `+`
        // or `-` on the keyboard, or using the [zoom controls](#control-zoom).
        // Values smaller than `1` (e.g. `0.5`) allow for greater granularity.
        zoomDelta: 1,
        // @option trackResize: Boolean = true
        // Whether the map automatically handles browser window resize to update itself.
        trackResize: !0
      },
      initialize: function(t, i) {
        i = E(this, i), this._handlers = [], this._layers = {}, this._zoomBoundLayers = {}, this._sizeChanged = !0, this._initContainer(t), this._initLayout(), this._onResize = c(this._onResize, this), this._initEvents(), i.maxBounds && this.setMaxBounds(i.maxBounds), i.zoom !== void 0 && (this._zoom = this._limitZoom(i.zoom)), i.center && i.zoom !== void 0 && this.setView(at(i.center), i.zoom, { reset: !0 }), this.callInitHooks(), this._zoomAnimated = en && $.any3d && !$.mobileOpera && this.options.zoomAnimation, this._zoomAnimated && (this._createAnimProxy(), et(this._proxy, Lr, this._catchTransitionEnd, this)), this._addLayers(this.options.layers);
      },
      // @section Methods for modifying map state
      // @method setView(center: LatLng, zoom: Number, options?: Zoom/pan options): this
      // Sets the view of the map (geographical center and zoom) with the given
      // animation options.
      setView: function(t, i, o) {
        if (i = i === void 0 ? this._zoom : this._limitZoom(i), t = this._limitCenter(at(t), i, this.options.maxBounds), o = o || {}, this._stop(), this._loaded && !o.reset && o !== !0) {
          o.animate !== void 0 && (o.zoom = l({ animate: o.animate }, o.zoom), o.pan = l({ animate: o.animate, duration: o.duration }, o.pan));
          var r = this._zoom !== i ? this._tryAnimatedZoom && this._tryAnimatedZoom(t, i, o.zoom) : this._tryAnimatedPan(t, o.pan);
          if (r)
            return clearTimeout(this._sizeTimer), this;
        }
        return this._resetView(t, i, o.pan && o.pan.noMoveStart), this;
      },
      // @method setZoom(zoom: Number, options?: Zoom/pan options): this
      // Sets the zoom of the map.
      setZoom: function(t, i) {
        return this._loaded ? this.setView(this.getCenter(), t, { zoom: i }) : (this._zoom = t, this);
      },
      // @method zoomIn(delta?: Number, options?: Zoom options): this
      // Increases the zoom of the map by `delta` ([`zoomDelta`](#map-zoomdelta) by default).
      zoomIn: function(t, i) {
        return t = t || ($.any3d ? this.options.zoomDelta : 1), this.setZoom(this._zoom + t, i);
      },
      // @method zoomOut(delta?: Number, options?: Zoom options): this
      // Decreases the zoom of the map by `delta` ([`zoomDelta`](#map-zoomdelta) by default).
      zoomOut: function(t, i) {
        return t = t || ($.any3d ? this.options.zoomDelta : 1), this.setZoom(this._zoom - t, i);
      },
      // @method setZoomAround(latlng: LatLng, zoom: Number, options: Zoom options): this
      // Zooms the map while keeping a specified geographical point on the map
      // stationary (e.g. used internally for scroll zoom and double-click zoom).
      // @alternative
      // @method setZoomAround(offset: Point, zoom: Number, options: Zoom options): this
      // Zooms the map while keeping a specified pixel on the map (relative to the top-left corner) stationary.
      setZoomAround: function(t, i, o) {
        var r = this.getZoomScale(i), h = this.getSize().divideBy(2), d = t instanceof Z ? t : this.latLngToContainerPoint(t), m = d.subtract(h).multiplyBy(1 - 1 / r), y = this.containerPointToLatLng(h.add(m));
        return this.setView(y, i, { zoom: o });
      },
      _getBoundsCenterZoom: function(t, i) {
        i = i || {}, t = t.getBounds ? t.getBounds() : $t(t);
        var o = K(i.paddingTopLeft || i.padding || [0, 0]), r = K(i.paddingBottomRight || i.padding || [0, 0]), h = this.getBoundsZoom(t, !1, o.add(r));
        if (h = typeof i.maxZoom == "number" ? Math.min(i.maxZoom, h) : h, h === 1 / 0)
          return {
            center: t.getCenter(),
            zoom: h
          };
        var d = r.subtract(o).divideBy(2), m = this.project(t.getSouthWest(), h), y = this.project(t.getNorthEast(), h), w = this.unproject(m.add(y).divideBy(2).add(d), h);
        return {
          center: w,
          zoom: h
        };
      },
      // @method fitBounds(bounds: LatLngBounds, options?: fitBounds options): this
      // Sets a map view that contains the given geographical bounds with the
      // maximum zoom level possible.
      fitBounds: function(t, i) {
        if (t = $t(t), !t.isValid())
          throw new Error("Bounds are not valid.");
        var o = this._getBoundsCenterZoom(t, i);
        return this.setView(o.center, o.zoom, i);
      },
      // @method fitWorld(options?: fitBounds options): this
      // Sets a map view that mostly contains the whole world with the maximum
      // zoom level possible.
      fitWorld: function(t) {
        return this.fitBounds([[-90, -180], [90, 180]], t);
      },
      // @method panTo(latlng: LatLng, options?: Pan options): this
      // Pans the map to a given center.
      panTo: function(t, i) {
        return this.setView(t, this._zoom, { pan: i });
      },
      // @method panBy(offset: Point, options?: Pan options): this
      // Pans the map by a given number of pixels (animated).
      panBy: function(t, i) {
        if (t = K(t).round(), i = i || {}, !t.x && !t.y)
          return this.fire("moveend");
        if (i.animate !== !0 && !this.getSize().contains(t))
          return this._resetView(this.unproject(this.project(this.getCenter()).add(t)), this.getZoom()), this;
        if (this._panAnim || (this._panAnim = new Er(), this._panAnim.on({
          step: this._onPanTransitionStep,
          end: this._onPanTransitionEnd
        }, this)), i.noMoveStart || this.fire("movestart"), i.animate !== !1) {
          ot(this._mapPane, "leaflet-pan-anim");
          var o = this._getMapPanePos().subtract(t).round();
          this._panAnim.run(this._mapPane, o, i.duration || 0.25, i.easeLinearity);
        } else
          this._rawPanBy(t), this.fire("move").fire("moveend");
        return this;
      },
      // @method flyTo(latlng: LatLng, zoom?: Number, options?: Zoom/pan options): this
      // Sets the view of the map (geographical center and zoom) performing a smooth
      // pan-zoom animation.
      flyTo: function(t, i, o) {
        if (o = o || {}, o.animate === !1 || !$.any3d)
          return this.setView(t, i, o);
        this._stop();
        var r = this.project(this.getCenter()), h = this.project(t), d = this.getSize(), m = this._zoom;
        t = at(t), i = i === void 0 ? m : i;
        var y = Math.max(d.x, d.y), w = y * this.getZoomScale(m, i), O = h.distanceTo(r) || 1, N = 1.42, G = N * N;
        function lt(Ut) {
          var eo = Ut ? -1 : 1, jh = Ut ? w : y, Hh = w * w - y * y + eo * G * G * O * O, $h = 2 * jh * G * O, ds = Hh / $h, ha = Math.sqrt(ds * ds + 1) - ds, Wh = ha < 1e-9 ? -18 : Math.log(ha);
          return Wh;
        }
        function me(Ut) {
          return (Math.exp(Ut) - Math.exp(-Ut)) / 2;
        }
        function yi(Ut) {
          return (Math.exp(Ut) + Math.exp(-Ut)) / 2;
        }
        function to(Ut) {
          return me(Ut) / yi(Ut);
        }
        var Qe = lt(0);
        function fs(Ut) {
          return y * (yi(Qe) / yi(Qe + N * Ut));
        }
        function Zh(Ut) {
          return y * (yi(Qe) * to(Qe + N * Ut) - me(Qe)) / G;
        }
        function Rh(Ut) {
          return 1 - Math.pow(1 - Ut, 1.5);
        }
        var Dh = Date.now(), la = (lt(1) - Qe) / N, Fh = o.duration ? 1e3 * o.duration : 1e3 * la * 0.8;
        function ua() {
          var Ut = (Date.now() - Dh) / Fh, eo = Rh(Ut) * la;
          Ut <= 1 ? (this._flyToFrame = ut(ua, this), this._move(
            this.unproject(r.add(h.subtract(r).multiplyBy(Zh(eo) / O)), m),
            this.getScaleZoom(y / fs(eo), m),
            { flyTo: !0 }
          )) : this._move(t, i)._moveEnd(!0);
        }
        return this._moveStart(!0, o.noMoveStart), ua.call(this), this;
      },
      // @method flyToBounds(bounds: LatLngBounds, options?: fitBounds options): this
      // Sets the view of the map with a smooth animation like [`flyTo`](#map-flyto),
      // but takes a bounds parameter like [`fitBounds`](#map-fitbounds).
      flyToBounds: function(t, i) {
        var o = this._getBoundsCenterZoom(t, i);
        return this.flyTo(o.center, o.zoom, i);
      },
      // @method setMaxBounds(bounds: LatLngBounds): this
      // Restricts the map view to the given bounds (see the [maxBounds](#map-maxbounds) option).
      setMaxBounds: function(t) {
        return t = $t(t), this.listens("moveend", this._panInsideMaxBounds) && this.off("moveend", this._panInsideMaxBounds), t.isValid() ? (this.options.maxBounds = t, this._loaded && this._panInsideMaxBounds(), this.on("moveend", this._panInsideMaxBounds)) : (this.options.maxBounds = null, this);
      },
      // @method setMinZoom(zoom: Number): this
      // Sets the lower limit for the available zoom levels (see the [minZoom](#map-minzoom) option).
      setMinZoom: function(t) {
        var i = this.options.minZoom;
        return this.options.minZoom = t, this._loaded && i !== t && (this.fire("zoomlevelschange"), this.getZoom() < this.options.minZoom) ? this.setZoom(t) : this;
      },
      // @method setMaxZoom(zoom: Number): this
      // Sets the upper limit for the available zoom levels (see the [maxZoom](#map-maxzoom) option).
      setMaxZoom: function(t) {
        var i = this.options.maxZoom;
        return this.options.maxZoom = t, this._loaded && i !== t && (this.fire("zoomlevelschange"), this.getZoom() > this.options.maxZoom) ? this.setZoom(t) : this;
      },
      // @method panInsideBounds(bounds: LatLngBounds, options?: Pan options): this
      // Pans the map to the closest view that would lie inside the given bounds (if it's not already), controlling the animation using the options specific, if any.
      panInsideBounds: function(t, i) {
        this._enforcingBounds = !0;
        var o = this.getCenter(), r = this._limitCenter(o, this._zoom, $t(t));
        return o.equals(r) || this.panTo(r, i), this._enforcingBounds = !1, this;
      },
      // @method panInside(latlng: LatLng, options?: padding options): this
      // Pans the map the minimum amount to make the `latlng` visible. Use
      // padding options to fit the display to more restricted bounds.
      // If `latlng` is already within the (optionally padded) display bounds,
      // the map will not be panned.
      panInside: function(t, i) {
        i = i || {};
        var o = K(i.paddingTopLeft || i.padding || [0, 0]), r = K(i.paddingBottomRight || i.padding || [0, 0]), h = this.project(this.getCenter()), d = this.project(t), m = this.getPixelBounds(), y = Ct([m.min.add(o), m.max.subtract(r)]), w = y.getSize();
        if (!y.contains(d)) {
          this._enforcingBounds = !0;
          var O = d.subtract(y.getCenter()), N = y.extend(d).getSize().subtract(w);
          h.x += O.x < 0 ? -N.x : N.x, h.y += O.y < 0 ? -N.y : N.y, this.panTo(this.unproject(h), i), this._enforcingBounds = !1;
        }
        return this;
      },
      // @method invalidateSize(options: Zoom/pan options): this
      // Checks if the map container size changed and updates the map if so —
      // call it after you've changed the map size dynamically, also animating
      // pan by default. If `options.pan` is `false`, panning will not occur.
      // If `options.debounceMoveend` is `true`, it will delay `moveend` event so
      // that it doesn't happen often even if the method is called many
      // times in a row.
      // @alternative
      // @method invalidateSize(animate: Boolean): this
      // Checks if the map container size changed and updates the map if so —
      // call it after you've changed the map size dynamically, also animating
      // pan by default.
      invalidateSize: function(t) {
        if (!this._loaded)
          return this;
        t = l({
          animate: !1,
          pan: !0
        }, t === !0 ? { animate: !0 } : t);
        var i = this.getSize();
        this._sizeChanged = !0, this._lastCenter = null;
        var o = this.getSize(), r = i.divideBy(2).round(), h = o.divideBy(2).round(), d = r.subtract(h);
        return !d.x && !d.y ? this : (t.animate && t.pan ? this.panBy(d) : (t.pan && this._rawPanBy(d), this.fire("move"), t.debounceMoveend ? (clearTimeout(this._sizeTimer), this._sizeTimer = setTimeout(c(this.fire, this, "moveend"), 200)) : this.fire("moveend")), this.fire("resize", {
          oldSize: i,
          newSize: o
        }));
      },
      // @section Methods for modifying map state
      // @method stop(): this
      // Stops the currently running `panTo` or `flyTo` animation, if any.
      stop: function() {
        return this.setZoom(this._limitZoom(this._zoom)), this.options.zoomSnap || this.fire("viewreset"), this._stop();
      },
      // @section Geolocation methods
      // @method locate(options?: Locate options): this
      // Tries to locate the user using the Geolocation API, firing a [`locationfound`](#map-locationfound)
      // event with location data on success or a [`locationerror`](#map-locationerror) event on failure,
      // and optionally sets the map view to the user's location with respect to
      // detection accuracy (or to the world view if geolocation failed).
      // Note that, if your page doesn't use HTTPS, this method will fail in
      // modern browsers ([Chrome 50 and newer](https://sites.google.com/a/chromium.org/dev/Home/chromium-security/deprecating-powerful-features-on-insecure-origins))
      // See `Locate options` for more details.
      locate: function(t) {
        if (t = this._locateOptions = l({
          timeout: 1e4,
          watch: !1
          // setView: false
          // maxZoom: <Number>
          // maximumAge: 0
          // enableHighAccuracy: false
        }, t), !("geolocation" in navigator))
          return this._handleGeolocationError({
            code: 0,
            message: "Geolocation not supported."
          }), this;
        var i = c(this._handleGeolocationResponse, this), o = c(this._handleGeolocationError, this);
        return t.watch ? this._locationWatchId = navigator.geolocation.watchPosition(i, o, t) : navigator.geolocation.getCurrentPosition(i, o, t), this;
      },
      // @method stopLocate(): this
      // Stops watching location previously initiated by `map.locate({watch: true})`
      // and aborts resetting the map view if map.locate was called with
      // `{setView: true}`.
      stopLocate: function() {
        return navigator.geolocation && navigator.geolocation.clearWatch && navigator.geolocation.clearWatch(this._locationWatchId), this._locateOptions && (this._locateOptions.setView = !1), this;
      },
      _handleGeolocationError: function(t) {
        if (this._container._leaflet_id) {
          var i = t.code, o = t.message || (i === 1 ? "permission denied" : i === 2 ? "position unavailable" : "timeout");
          this._locateOptions.setView && !this._loaded && this.fitWorld(), this.fire("locationerror", {
            code: i,
            message: "Geolocation error: " + o + "."
          });
        }
      },
      _handleGeolocationResponse: function(t) {
        if (this._container._leaflet_id) {
          var i = t.coords.latitude, o = t.coords.longitude, r = new vt(i, o), h = r.toBounds(t.coords.accuracy * 2), d = this._locateOptions;
          if (d.setView) {
            var m = this.getBoundsZoom(h);
            this.setView(r, d.maxZoom ? Math.min(m, d.maxZoom) : m);
          }
          var y = {
            latlng: r,
            bounds: h,
            timestamp: t.timestamp
          };
          for (var w in t.coords)
            typeof t.coords[w] == "number" && (y[w] = t.coords[w]);
          this.fire("locationfound", y);
        }
      },
      // TODO Appropriate docs section?
      // @section Other Methods
      // @method addHandler(name: String, HandlerClass: Function): this
      // Adds a new `Handler` to the map, given its name and constructor function.
      addHandler: function(t, i) {
        if (!i)
          return this;
        var o = this[t] = new i(this);
        return this._handlers.push(o), this.options[t] && o.enable(), this;
      },
      // @method remove(): this
      // Destroys the map and clears all related event listeners.
      remove: function() {
        if (this._initEvents(!0), this.options.maxBounds && this.off("moveend", this._panInsideMaxBounds), this._containerId !== this._container._leaflet_id)
          throw new Error("Map container is being reused by another instance");
        try {
          delete this._container._leaflet_id, delete this._containerId;
        } catch {
          this._container._leaflet_id = void 0, this._containerId = void 0;
        }
        this._locationWatchId !== void 0 && this.stopLocate(), this._stop(), kt(this._mapPane), this._clearControlPos && this._clearControlPos(), this._resizeRequest && (R(this._resizeRequest), this._resizeRequest = null), this._clearHandlers(), this._loaded && this.fire("unload");
        var t;
        for (t in this._layers)
          this._layers[t].remove();
        for (t in this._panes)
          kt(this._panes[t]);
        return this._layers = [], this._panes = [], delete this._mapPane, delete this._renderer, this;
      },
      // @section Other Methods
      // @method createPane(name: String, container?: HTMLElement): HTMLElement
      // Creates a new [map pane](#map-pane) with the given name if it doesn't exist already,
      // then returns it. The pane is created as a child of `container`, or
      // as a child of the main map pane if not set.
      createPane: function(t, i) {
        var o = "leaflet-pane" + (t ? " leaflet-" + t.replace("Pane", "") + "-pane" : ""), r = dt("div", o, i || this._mapPane);
        return t && (this._panes[t] = r), r;
      },
      // @section Methods for Getting Map State
      // @method getCenter(): LatLng
      // Returns the geographical center of the map view
      getCenter: function() {
        return this._checkIfLoaded(), this._lastCenter && !this._moved() ? this._lastCenter.clone() : this.layerPointToLatLng(this._getCenterLayerPoint());
      },
      // @method getZoom(): Number
      // Returns the current zoom level of the map view
      getZoom: function() {
        return this._zoom;
      },
      // @method getBounds(): LatLngBounds
      // Returns the geographical bounds visible in the current map view
      getBounds: function() {
        var t = this.getPixelBounds(), i = this.unproject(t.getBottomLeft()), o = this.unproject(t.getTopRight());
        return new Gt(i, o);
      },
      // @method getMinZoom(): Number
      // Returns the minimum zoom level of the map (if set in the `minZoom` option of the map or of any layers), or `0` by default.
      getMinZoom: function() {
        return this.options.minZoom === void 0 ? this._layersMinZoom || 0 : this.options.minZoom;
      },
      // @method getMaxZoom(): Number
      // Returns the maximum zoom level of the map (if set in the `maxZoom` option of the map or of any layers).
      getMaxZoom: function() {
        return this.options.maxZoom === void 0 ? this._layersMaxZoom === void 0 ? 1 / 0 : this._layersMaxZoom : this.options.maxZoom;
      },
      // @method getBoundsZoom(bounds: LatLngBounds, inside?: Boolean, padding?: Point): Number
      // Returns the maximum zoom level on which the given bounds fit to the map
      // view in its entirety. If `inside` (optional) is set to `true`, the method
      // instead returns the minimum zoom level on which the map view fits into
      // the given bounds in its entirety.
      getBoundsZoom: function(t, i, o) {
        t = $t(t), o = K(o || [0, 0]);
        var r = this.getZoom() || 0, h = this.getMinZoom(), d = this.getMaxZoom(), m = t.getNorthWest(), y = t.getSouthEast(), w = this.getSize().subtract(o), O = Ct(this.project(y, r), this.project(m, r)).getSize(), N = $.any3d ? this.options.zoomSnap : 1, G = w.x / O.x, lt = w.y / O.y, me = i ? Math.max(G, lt) : Math.min(G, lt);
        return r = this.getScaleZoom(me, r), N && (r = Math.round(r / (N / 100)) * (N / 100), r = i ? Math.ceil(r / N) * N : Math.floor(r / N) * N), Math.max(h, Math.min(d, r));
      },
      // @method getSize(): Point
      // Returns the current size of the map container (in pixels).
      getSize: function() {
        return (!this._size || this._sizeChanged) && (this._size = new Z(
          this._container.clientWidth || 0,
          this._container.clientHeight || 0
        ), this._sizeChanged = !1), this._size.clone();
      },
      // @method getPixelBounds(): Bounds
      // Returns the bounds of the current map view in projected pixel
      // coordinates (sometimes useful in layer and overlay implementations).
      getPixelBounds: function(t, i) {
        var o = this._getTopLeftPoint(t, i);
        return new mt(o, o.add(this.getSize()));
      },
      // TODO: Check semantics - isn't the pixel origin the 0,0 coord relative to
      // the map pane? "left point of the map layer" can be confusing, specially
      // since there can be negative offsets.
      // @method getPixelOrigin(): Point
      // Returns the projected pixel coordinates of the top left point of
      // the map layer (useful in custom layer and overlay implementations).
      getPixelOrigin: function() {
        return this._checkIfLoaded(), this._pixelOrigin;
      },
      // @method getPixelWorldBounds(zoom?: Number): Bounds
      // Returns the world's bounds in pixel coordinates for zoom level `zoom`.
      // If `zoom` is omitted, the map's current zoom level is used.
      getPixelWorldBounds: function(t) {
        return this.options.crs.getProjectedBounds(t === void 0 ? this.getZoom() : t);
      },
      // @section Other Methods
      // @method getPane(pane: String|HTMLElement): HTMLElement
      // Returns a [map pane](#map-pane), given its name or its HTML element (its identity).
      getPane: function(t) {
        return typeof t == "string" ? this._panes[t] : t;
      },
      // @method getPanes(): Object
      // Returns a plain object containing the names of all [panes](#map-pane) as keys and
      // the panes as values.
      getPanes: function() {
        return this._panes;
      },
      // @method getContainer: HTMLElement
      // Returns the HTML element that contains the map.
      getContainer: function() {
        return this._container;
      },
      // @section Conversion Methods
      // @method getZoomScale(toZoom: Number, fromZoom: Number): Number
      // Returns the scale factor to be applied to a map transition from zoom level
      // `fromZoom` to `toZoom`. Used internally to help with zoom animations.
      getZoomScale: function(t, i) {
        var o = this.options.crs;
        return i = i === void 0 ? this._zoom : i, o.scale(t) / o.scale(i);
      },
      // @method getScaleZoom(scale: Number, fromZoom: Number): Number
      // Returns the zoom level that the map would end up at, if it is at `fromZoom`
      // level and everything is scaled by a factor of `scale`. Inverse of
      // [`getZoomScale`](#map-getZoomScale).
      getScaleZoom: function(t, i) {
        var o = this.options.crs;
        i = i === void 0 ? this._zoom : i;
        var r = o.zoom(t * o.scale(i));
        return isNaN(r) ? 1 / 0 : r;
      },
      // @method project(latlng: LatLng, zoom: Number): Point
      // Projects a geographical coordinate `LatLng` according to the projection
      // of the map's CRS, then scales it according to `zoom` and the CRS's
      // `Transformation`. The result is pixel coordinate relative to
      // the CRS origin.
      project: function(t, i) {
        return i = i === void 0 ? this._zoom : i, this.options.crs.latLngToPoint(at(t), i);
      },
      // @method unproject(point: Point, zoom: Number): LatLng
      // Inverse of [`project`](#map-project).
      unproject: function(t, i) {
        return i = i === void 0 ? this._zoom : i, this.options.crs.pointToLatLng(K(t), i);
      },
      // @method layerPointToLatLng(point: Point): LatLng
      // Given a pixel coordinate relative to the [origin pixel](#map-getpixelorigin),
      // returns the corresponding geographical coordinate (for the current zoom level).
      layerPointToLatLng: function(t) {
        var i = K(t).add(this.getPixelOrigin());
        return this.unproject(i);
      },
      // @method latLngToLayerPoint(latlng: LatLng): Point
      // Given a geographical coordinate, returns the corresponding pixel coordinate
      // relative to the [origin pixel](#map-getpixelorigin).
      latLngToLayerPoint: function(t) {
        var i = this.project(at(t))._round();
        return i._subtract(this.getPixelOrigin());
      },
      // @method wrapLatLng(latlng: LatLng): LatLng
      // Returns a `LatLng` where `lat` and `lng` has been wrapped according to the
      // map's CRS's `wrapLat` and `wrapLng` properties, if they are outside the
      // CRS's bounds.
      // By default this means longitude is wrapped around the dateline so its
      // value is between -180 and +180 degrees.
      wrapLatLng: function(t) {
        return this.options.crs.wrapLatLng(at(t));
      },
      // @method wrapLatLngBounds(bounds: LatLngBounds): LatLngBounds
      // Returns a `LatLngBounds` with the same size as the given one, ensuring that
      // its center is within the CRS's bounds.
      // By default this means the center longitude is wrapped around the dateline so its
      // value is between -180 and +180 degrees, and the majority of the bounds
      // overlaps the CRS's bounds.
      wrapLatLngBounds: function(t) {
        return this.options.crs.wrapLatLngBounds($t(t));
      },
      // @method distance(latlng1: LatLng, latlng2: LatLng): Number
      // Returns the distance between two geographical coordinates according to
      // the map's CRS. By default this measures distance in meters.
      distance: function(t, i) {
        return this.options.crs.distance(at(t), at(i));
      },
      // @method containerPointToLayerPoint(point: Point): Point
      // Given a pixel coordinate relative to the map container, returns the corresponding
      // pixel coordinate relative to the [origin pixel](#map-getpixelorigin).
      containerPointToLayerPoint: function(t) {
        return K(t).subtract(this._getMapPanePos());
      },
      // @method layerPointToContainerPoint(point: Point): Point
      // Given a pixel coordinate relative to the [origin pixel](#map-getpixelorigin),
      // returns the corresponding pixel coordinate relative to the map container.
      layerPointToContainerPoint: function(t) {
        return K(t).add(this._getMapPanePos());
      },
      // @method containerPointToLatLng(point: Point): LatLng
      // Given a pixel coordinate relative to the map container, returns
      // the corresponding geographical coordinate (for the current zoom level).
      containerPointToLatLng: function(t) {
        var i = this.containerPointToLayerPoint(K(t));
        return this.layerPointToLatLng(i);
      },
      // @method latLngToContainerPoint(latlng: LatLng): Point
      // Given a geographical coordinate, returns the corresponding pixel coordinate
      // relative to the map container.
      latLngToContainerPoint: function(t) {
        return this.layerPointToContainerPoint(this.latLngToLayerPoint(at(t)));
      },
      // @method mouseEventToContainerPoint(ev: MouseEvent): Point
      // Given a MouseEvent object, returns the pixel coordinate relative to the
      // map container where the event took place.
      mouseEventToContainerPoint: function(t) {
        return Sr(t, this._container);
      },
      // @method mouseEventToLayerPoint(ev: MouseEvent): Point
      // Given a MouseEvent object, returns the pixel coordinate relative to
      // the [origin pixel](#map-getpixelorigin) where the event took place.
      mouseEventToLayerPoint: function(t) {
        return this.containerPointToLayerPoint(this.mouseEventToContainerPoint(t));
      },
      // @method mouseEventToLatLng(ev: MouseEvent): LatLng
      // Given a MouseEvent object, returns geographical coordinate where the
      // event took place.
      mouseEventToLatLng: function(t) {
        return this.layerPointToLatLng(this.mouseEventToLayerPoint(t));
      },
      // map initialization methods
      _initContainer: function(t) {
        var i = this._container = Pr(t);
        if (i) {
          if (i._leaflet_id)
            throw new Error("Map container is already initialized.");
        } else
          throw new Error("Map container not found.");
        et(i, "scroll", this._onScroll, this), this._containerId = p(i);
      },
      _initLayout: function() {
        var t = this._container;
        this._fadeAnimated = this.options.fadeAnimation && $.any3d, ot(t, "leaflet-container" + ($.touch ? " leaflet-touch" : "") + ($.retina ? " leaflet-retina" : "") + ($.ielt9 ? " leaflet-oldie" : "") + ($.safari ? " leaflet-safari" : "") + (this._fadeAnimated ? " leaflet-fade-anim" : ""));
        var i = nn(t, "position");
        i !== "absolute" && i !== "relative" && i !== "fixed" && i !== "sticky" && (t.style.position = "relative"), this._initPanes(), this._initControlPos && this._initControlPos();
      },
      _initPanes: function() {
        var t = this._panes = {};
        this._paneRenderers = {}, this._mapPane = this.createPane("mapPane", this._container), Wt(this._mapPane, new Z(0, 0)), this.createPane("tilePane"), this.createPane("overlayPane"), this.createPane("shadowPane"), this.createPane("markerPane"), this.createPane("tooltipPane"), this.createPane("popupPane"), this.options.markerZoomAnimation || (ot(t.markerPane, "leaflet-zoom-hide"), ot(t.shadowPane, "leaflet-zoom-hide"));
      },
      // private methods that modify map state
      // @section Map state change events
      _resetView: function(t, i, o) {
        Wt(this._mapPane, new Z(0, 0));
        var r = !this._loaded;
        this._loaded = !0, i = this._limitZoom(i), this.fire("viewprereset");
        var h = this._zoom !== i;
        this._moveStart(h, o)._move(t, i)._moveEnd(h), this.fire("viewreset"), r && this.fire("load");
      },
      _moveStart: function(t, i) {
        return t && this.fire("zoomstart"), i || this.fire("movestart"), this;
      },
      _move: function(t, i, o, r) {
        i === void 0 && (i = this._zoom);
        var h = this._zoom !== i;
        return this._zoom = i, this._lastCenter = t, this._pixelOrigin = this._getNewPixelOrigin(t), r ? o && o.pinch && this.fire("zoom", o) : ((h || o && o.pinch) && this.fire("zoom", o), this.fire("move", o)), this;
      },
      _moveEnd: function(t) {
        return t && this.fire("zoomend"), this.fire("moveend");
      },
      _stop: function() {
        return R(this._flyToFrame), this._panAnim && this._panAnim.stop(), this;
      },
      _rawPanBy: function(t) {
        Wt(this._mapPane, this._getMapPanePos().subtract(t));
      },
      _getZoomSpan: function() {
        return this.getMaxZoom() - this.getMinZoom();
      },
      _panInsideMaxBounds: function() {
        this._enforcingBounds || this.panInsideBounds(this.options.maxBounds);
      },
      _checkIfLoaded: function() {
        if (!this._loaded)
          throw new Error("Set map center and zoom first.");
      },
      // DOM event handling
      // @section Interaction events
      _initEvents: function(t) {
        this._targets = {}, this._targets[p(this._container)] = this;
        var i = t ? Lt : et;
        i(this._container, "click dblclick mousedown mouseup mouseover mouseout mousemove contextmenu keypress keydown keyup", this._handleDOMEvent, this), this.options.trackResize && i(window, "resize", this._onResize, this), $.any3d && this.options.transform3DLimit && (t ? this.off : this.on).call(this, "moveend", this._onMoveEnd);
      },
      _onResize: function() {
        R(this._resizeRequest), this._resizeRequest = ut(
          function() {
            this.invalidateSize({ debounceMoveend: !0 });
          },
          this
        );
      },
      _onScroll: function() {
        this._container.scrollTop = 0, this._container.scrollLeft = 0;
      },
      _onMoveEnd: function() {
        var t = this._getMapPanePos();
        Math.max(Math.abs(t.x), Math.abs(t.y)) >= this.options.transform3DLimit && this._resetView(this.getCenter(), this.getZoom());
      },
      _findEventTargets: function(t, i) {
        for (var o = [], r, h = i === "mouseout" || i === "mouseover", d = t.target || t.srcElement, m = !1; d; ) {
          if (r = this._targets[p(d)], r && (i === "click" || i === "preclick") && this._draggableMoved(r)) {
            m = !0;
            break;
          }
          if (r && r.listens(i, !0) && (h && !is(d, t) || (o.push(r), h)) || d === this._container)
            break;
          d = d.parentNode;
        }
        return !o.length && !m && !h && this.listens(i, !0) && (o = [this]), o;
      },
      _isClickDisabled: function(t) {
        for (; t && t !== this._container; ) {
          if (t._leaflet_disable_click)
            return !0;
          t = t.parentNode;
        }
      },
      _handleDOMEvent: function(t) {
        var i = t.target || t.srcElement;
        if (!(!this._loaded || i._leaflet_disable_events || t.type === "click" && this._isClickDisabled(i))) {
          var o = t.type;
          o === "mousedown" && Yo(i), this._fireDOMEvent(t, o);
        }
      },
      _mouseEvents: ["click", "dblclick", "mouseover", "mouseout", "contextmenu"],
      _fireDOMEvent: function(t, i, o) {
        if (t.type === "click") {
          var r = l({}, t);
          r.type = "preclick", this._fireDOMEvent(r, r.type, o);
        }
        var h = this._findEventTargets(t, i);
        if (o) {
          for (var d = [], m = 0; m < o.length; m++)
            o[m].listens(i, !0) && d.push(o[m]);
          h = d.concat(h);
        }
        if (h.length) {
          i === "contextmenu" && Yt(t);
          var y = h[0], w = {
            originalEvent: t
          };
          if (t.type !== "keypress" && t.type !== "keydown" && t.type !== "keyup") {
            var O = y.getLatLng && (!y._radius || y._radius <= 10);
            w.containerPoint = O ? this.latLngToContainerPoint(y.getLatLng()) : this.mouseEventToContainerPoint(t), w.layerPoint = this.containerPointToLayerPoint(w.containerPoint), w.latlng = O ? y.getLatLng() : this.layerPointToLatLng(w.layerPoint);
          }
          for (m = 0; m < h.length; m++)
            if (h[m].fire(i, w, !0), w.originalEvent._stopped || h[m].options.bubblingMouseEvents === !1 && rt(this._mouseEvents, i) !== -1)
              return;
        }
      },
      _draggableMoved: function(t) {
        return t = t.dragging && t.dragging.enabled() ? t : this, t.dragging && t.dragging.moved() || this.boxZoom && this.boxZoom.moved();
      },
      _clearHandlers: function() {
        for (var t = 0, i = this._handlers.length; t < i; t++)
          this._handlers[t].disable();
      },
      // @section Other Methods
      // @method whenReady(fn: Function, context?: Object): this
      // Runs the given function `fn` when the map gets initialized with
      // a view (center and zoom) and at least one layer, or immediately
      // if it's already initialized, optionally passing a function context.
      whenReady: function(t, i) {
        return this._loaded ? t.call(i || this, { target: this }) : this.on("load", t, i), this;
      },
      // private methods for getting map state
      _getMapPanePos: function() {
        return mi(this._mapPane) || new Z(0, 0);
      },
      _moved: function() {
        var t = this._getMapPanePos();
        return t && !t.equals([0, 0]);
      },
      _getTopLeftPoint: function(t, i) {
        var o = t && i !== void 0 ? this._getNewPixelOrigin(t, i) : this.getPixelOrigin();
        return o.subtract(this._getMapPanePos());
      },
      _getNewPixelOrigin: function(t, i) {
        var o = this.getSize()._divideBy(2);
        return this.project(t, i)._subtract(o)._add(this._getMapPanePos())._round();
      },
      _latLngToNewLayerPoint: function(t, i, o) {
        var r = this._getNewPixelOrigin(o, i);
        return this.project(t, i)._subtract(r);
      },
      _latLngBoundsToNewLayerBounds: function(t, i, o) {
        var r = this._getNewPixelOrigin(o, i);
        return Ct([
          this.project(t.getSouthWest(), i)._subtract(r),
          this.project(t.getNorthWest(), i)._subtract(r),
          this.project(t.getSouthEast(), i)._subtract(r),
          this.project(t.getNorthEast(), i)._subtract(r)
        ]);
      },
      // layer point of the current center
      _getCenterLayerPoint: function() {
        return this.containerPointToLayerPoint(this.getSize()._divideBy(2));
      },
      // offset of the specified place to the current center in pixels
      _getCenterOffset: function(t) {
        return this.latLngToLayerPoint(t).subtract(this._getCenterLayerPoint());
      },
      // adjust center for view to get inside bounds
      _limitCenter: function(t, i, o) {
        if (!o)
          return t;
        var r = this.project(t, i), h = this.getSize().divideBy(2), d = new mt(r.subtract(h), r.add(h)), m = this._getBoundsOffset(d, o, i);
        return Math.abs(m.x) <= 1 && Math.abs(m.y) <= 1 ? t : this.unproject(r.add(m), i);
      },
      // adjust offset for view to get inside bounds
      _limitOffset: function(t, i) {
        if (!i)
          return t;
        var o = this.getPixelBounds(), r = new mt(o.min.add(t), o.max.add(t));
        return t.add(this._getBoundsOffset(r, i));
      },
      // returns offset needed for pxBounds to get inside maxBounds at a specified zoom
      _getBoundsOffset: function(t, i, o) {
        var r = Ct(
          this.project(i.getNorthEast(), o),
          this.project(i.getSouthWest(), o)
        ), h = r.min.subtract(t.min), d = r.max.subtract(t.max), m = this._rebound(h.x, -d.x), y = this._rebound(h.y, -d.y);
        return new Z(m, y);
      },
      _rebound: function(t, i) {
        return t + i > 0 ? Math.round(t - i) / 2 : Math.max(0, Math.ceil(t)) - Math.max(0, Math.floor(i));
      },
      _limitZoom: function(t) {
        var i = this.getMinZoom(), o = this.getMaxZoom(), r = $.any3d ? this.options.zoomSnap : 1;
        return r && (t = Math.round(t / r) * r), Math.max(i, Math.min(o, t));
      },
      _onPanTransitionStep: function() {
        this.fire("move");
      },
      _onPanTransitionEnd: function() {
        jt(this._mapPane, "leaflet-pan-anim"), this.fire("moveend");
      },
      _tryAnimatedPan: function(t, i) {
        var o = this._getCenterOffset(t)._trunc();
        return (i && i.animate) !== !0 && !this.getSize().contains(o) ? !1 : (this.panBy(o, i), !0);
      },
      _createAnimProxy: function() {
        var t = this._proxy = dt("div", "leaflet-proxy leaflet-zoom-animated");
        this._panes.mapPane.appendChild(t), this.on("zoomanim", function(i) {
          var o = $o, r = this._proxy.style[o];
          pi(this._proxy, this.project(i.center, i.zoom), this.getZoomScale(i.zoom, 1)), r === this._proxy.style[o] && this._animatingZoom && this._onZoomTransitionEnd();
        }, this), this.on("load moveend", this._animMoveEnd, this), this._on("unload", this._destroyAnimProxy, this);
      },
      _destroyAnimProxy: function() {
        kt(this._proxy), this.off("load moveend", this._animMoveEnd, this), delete this._proxy;
      },
      _animMoveEnd: function() {
        var t = this.getCenter(), i = this.getZoom();
        pi(this._proxy, this.project(t, i), this.getZoomScale(i, 1));
      },
      _catchTransitionEnd: function(t) {
        this._animatingZoom && t.propertyName.indexOf("transform") >= 0 && this._onZoomTransitionEnd();
      },
      _nothingToAnimate: function() {
        return !this._container.getElementsByClassName("leaflet-zoom-animated").length;
      },
      _tryAnimatedZoom: function(t, i, o) {
        if (this._animatingZoom)
          return !0;
        if (o = o || {}, !this._zoomAnimated || o.animate === !1 || this._nothingToAnimate() || Math.abs(i - this._zoom) > this.options.zoomAnimationThreshold)
          return !1;
        var r = this.getZoomScale(i), h = this._getCenterOffset(t)._divideBy(1 - 1 / r);
        return o.animate !== !0 && !this.getSize().contains(h) ? !1 : (ut(function() {
          this._moveStart(!0, !1)._animateZoom(t, i, !0);
        }, this), !0);
      },
      _animateZoom: function(t, i, o, r) {
        this._mapPane && (o && (this._animatingZoom = !0, this._animateToCenter = t, this._animateToZoom = i, ot(this._mapPane, "leaflet-zoom-anim")), this.fire("zoomanim", {
          center: t,
          zoom: i,
          noUpdate: r
        }), this._tempFireZoomEvent || (this._tempFireZoomEvent = this._zoom !== this._animateToZoom), this._move(this._animateToCenter, this._animateToZoom, void 0, !0), setTimeout(c(this._onZoomTransitionEnd, this), 250));
      },
      _onZoomTransitionEnd: function() {
        this._animatingZoom && (this._mapPane && jt(this._mapPane, "leaflet-zoom-anim"), this._animatingZoom = !1, this._move(this._animateToCenter, this._animateToZoom, void 0, !0), this._tempFireZoomEvent && this.fire("zoom"), delete this._tempFireZoomEvent, this.fire("move"), this._moveEnd(!0));
      }
    });
    function th(t, i) {
      return new ht(t, i);
    }
    var we = It.extend({
      // @section
      // @aka Control Options
      options: {
        // @option position: String = 'topright'
        // The position of the control (one of the map corners). Possible values are `'topleft'`,
        // `'topright'`, `'bottomleft'` or `'bottomright'`
        position: "topright"
      },
      initialize: function(t) {
        E(this, t);
      },
      /* @section
       * Classes extending L.Control will inherit the following methods:
       *
       * @method getPosition: string
       * Returns the position of the control.
       */
      getPosition: function() {
        return this.options.position;
      },
      // @method setPosition(position: string): this
      // Sets the position of the control.
      setPosition: function(t) {
        var i = this._map;
        return i && i.removeControl(this), this.options.position = t, i && i.addControl(this), this;
      },
      // @method getContainer: HTMLElement
      // Returns the HTMLElement that contains the control.
      getContainer: function() {
        return this._container;
      },
      // @method addTo(map: Map): this
      // Adds the control to the given map.
      addTo: function(t) {
        this.remove(), this._map = t;
        var i = this._container = this.onAdd(t), o = this.getPosition(), r = t._controlCorners[o];
        return ot(i, "leaflet-control"), o.indexOf("bottom") !== -1 ? r.insertBefore(i, r.firstChild) : r.appendChild(i), this._map.on("unload", this.remove, this), this;
      },
      // @method remove: this
      // Removes the control from the map it is currently active on.
      remove: function() {
        return this._map ? (kt(this._container), this.onRemove && this.onRemove(this._map), this._map.off("unload", this.remove, this), this._map = null, this) : this;
      },
      _refocusOnMap: function(t) {
        this._map && t && t.screenX > 0 && t.screenY > 0 && this._map.getContainer().focus();
      }
    }), ln = function(t) {
      return new we(t);
    };
    ht.include({
      // @method addControl(control: Control): this
      // Adds the given control to the map
      addControl: function(t) {
        return t.addTo(this), this;
      },
      // @method removeControl(control: Control): this
      // Removes the given control from the map
      removeControl: function(t) {
        return t.remove(), this;
      },
      _initControlPos: function() {
        var t = this._controlCorners = {}, i = "leaflet-", o = this._controlContainer = dt("div", i + "control-container", this._container);
        function r(h, d) {
          var m = i + h + " " + i + d;
          t[h + d] = dt("div", m, o);
        }
        r("top", "left"), r("top", "right"), r("bottom", "left"), r("bottom", "right");
      },
      _clearControlPos: function() {
        for (var t in this._controlCorners)
          kt(this._controlCorners[t]);
        kt(this._controlContainer), delete this._controlCorners, delete this._controlContainer;
      }
    });
    var Ar = we.extend({
      // @section
      // @aka Control.Layers options
      options: {
        // @option collapsed: Boolean = true
        // If `true`, the control will be collapsed into an icon and expanded on mouse hover, touch, or keyboard activation.
        collapsed: !0,
        position: "topright",
        // @option autoZIndex: Boolean = true
        // If `true`, the control will assign zIndexes in increasing order to all of its layers so that the order is preserved when switching them on/off.
        autoZIndex: !0,
        // @option hideSingleBase: Boolean = false
        // If `true`, the base layers in the control will be hidden when there is only one.
        hideSingleBase: !1,
        // @option sortLayers: Boolean = false
        // Whether to sort the layers. When `false`, layers will keep the order
        // in which they were added to the control.
        sortLayers: !1,
        // @option sortFunction: Function = *
        // A [compare function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)
        // that will be used for sorting the layers, when `sortLayers` is `true`.
        // The function receives both the `L.Layer` instances and their names, as in
        // `sortFunction(layerA, layerB, nameA, nameB)`.
        // By default, it sorts layers alphabetically by their name.
        sortFunction: function(t, i, o, r) {
          return o < r ? -1 : r < o ? 1 : 0;
        }
      },
      initialize: function(t, i, o) {
        E(this, o), this._layerControlInputs = [], this._layers = [], this._lastZIndex = 0, this._handlingClick = !1;
        for (var r in t)
          this._addLayer(t[r], r);
        for (r in i)
          this._addLayer(i[r], r, !0);
      },
      onAdd: function(t) {
        this._initLayout(), this._update(), this._map = t, t.on("zoomend", this._checkDisabledLayers, this);
        for (var i = 0; i < this._layers.length; i++)
          this._layers[i].layer.on("add remove", this._onLayerChange, this);
        return this._container;
      },
      addTo: function(t) {
        return we.prototype.addTo.call(this, t), this._expandIfNotCollapsed();
      },
      onRemove: function() {
        this._map.off("zoomend", this._checkDisabledLayers, this);
        for (var t = 0; t < this._layers.length; t++)
          this._layers[t].layer.off("add remove", this._onLayerChange, this);
      },
      // @method addBaseLayer(layer: Layer, name: String): this
      // Adds a base layer (radio button entry) with the given name to the control.
      addBaseLayer: function(t, i) {
        return this._addLayer(t, i), this._map ? this._update() : this;
      },
      // @method addOverlay(layer: Layer, name: String): this
      // Adds an overlay (checkbox entry) with the given name to the control.
      addOverlay: function(t, i) {
        return this._addLayer(t, i, !0), this._map ? this._update() : this;
      },
      // @method removeLayer(layer: Layer): this
      // Remove the given layer from the control.
      removeLayer: function(t) {
        t.off("add remove", this._onLayerChange, this);
        var i = this._getLayer(p(t));
        return i && this._layers.splice(this._layers.indexOf(i), 1), this._map ? this._update() : this;
      },
      // @method expand(): this
      // Expand the control container if collapsed.
      expand: function() {
        ot(this._container, "leaflet-control-layers-expanded"), this._section.style.height = null;
        var t = this._map.getSize().y - (this._container.offsetTop + 50);
        return t < this._section.clientHeight ? (ot(this._section, "leaflet-control-layers-scrollbar"), this._section.style.height = t + "px") : jt(this._section, "leaflet-control-layers-scrollbar"), this._checkDisabledLayers(), this;
      },
      // @method collapse(): this
      // Collapse the control container if expanded.
      collapse: function() {
        return jt(this._container, "leaflet-control-layers-expanded"), this;
      },
      _initLayout: function() {
        var t = "leaflet-control-layers", i = this._container = dt("div", t), o = this.options.collapsed;
        i.setAttribute("aria-haspopup", !0), an(i), es(i);
        var r = this._section = dt("section", t + "-list");
        o && (this._map.on("click", this.collapse, this), et(i, {
          mouseenter: this._expandSafely,
          mouseleave: this.collapse
        }, this));
        var h = this._layersLink = dt("a", t + "-toggle", i);
        h.href = "#", h.title = "Layers", h.setAttribute("role", "button"), et(h, {
          keydown: function(d) {
            d.keyCode === 13 && this._expandSafely();
          },
          // Certain screen readers intercept the key event and instead send a click event
          click: function(d) {
            Yt(d), this._expandSafely();
          }
        }, this), o || this.expand(), this._baseLayersList = dt("div", t + "-base", r), this._separator = dt("div", t + "-separator", r), this._overlaysList = dt("div", t + "-overlays", r), i.appendChild(r);
      },
      _getLayer: function(t) {
        for (var i = 0; i < this._layers.length; i++)
          if (this._layers[i] && p(this._layers[i].layer) === t)
            return this._layers[i];
      },
      _addLayer: function(t, i, o) {
        this._map && t.on("add remove", this._onLayerChange, this), this._layers.push({
          layer: t,
          name: i,
          overlay: o
        }), this.options.sortLayers && this._layers.sort(c(function(r, h) {
          return this.options.sortFunction(r.layer, h.layer, r.name, h.name);
        }, this)), this.options.autoZIndex && t.setZIndex && (this._lastZIndex++, t.setZIndex(this._lastZIndex)), this._expandIfNotCollapsed();
      },
      _update: function() {
        if (!this._container)
          return this;
        Rn(this._baseLayersList), Rn(this._overlaysList), this._layerControlInputs = [];
        var t, i, o, r, h = 0;
        for (o = 0; o < this._layers.length; o++)
          r = this._layers[o], this._addItem(r), i = i || r.overlay, t = t || !r.overlay, h += r.overlay ? 0 : 1;
        return this.options.hideSingleBase && (t = t && h > 1, this._baseLayersList.style.display = t ? "" : "none"), this._separator.style.display = i && t ? "" : "none", this;
      },
      _onLayerChange: function(t) {
        this._handlingClick || this._update();
        var i = this._getLayer(p(t.target)), o = i.overlay ? t.type === "add" ? "overlayadd" : "overlayremove" : t.type === "add" ? "baselayerchange" : null;
        o && this._map.fire(o, i);
      },
      // IE7 bugs out if you create a radio dynamically, so you have to do it this hacky way (see https://stackoverflow.com/a/119079)
      _createRadioElement: function(t, i) {
        var o = '<input type="radio" class="leaflet-control-layers-selector" name="' + t + '"' + (i ? ' checked="checked"' : "") + "/>", r = document.createElement("div");
        return r.innerHTML = o, r.firstChild;
      },
      _addItem: function(t) {
        var i = document.createElement("label"), o = this._map.hasLayer(t.layer), r;
        t.overlay ? (r = document.createElement("input"), r.type = "checkbox", r.className = "leaflet-control-layers-selector", r.defaultChecked = o) : r = this._createRadioElement("leaflet-base-layers_" + p(this), o), this._layerControlInputs.push(r), r.layerId = p(t.layer), et(r, "click", this._onInputClick, this);
        var h = document.createElement("span");
        h.innerHTML = " " + t.name;
        var d = document.createElement("span");
        i.appendChild(d), d.appendChild(r), d.appendChild(h);
        var m = t.overlay ? this._overlaysList : this._baseLayersList;
        return m.appendChild(i), this._checkDisabledLayers(), i;
      },
      _onInputClick: function() {
        var t = this._layerControlInputs, i, o, r = [], h = [];
        this._handlingClick = !0;
        for (var d = t.length - 1; d >= 0; d--)
          i = t[d], o = this._getLayer(i.layerId).layer, i.checked ? r.push(o) : i.checked || h.push(o);
        for (d = 0; d < h.length; d++)
          this._map.hasLayer(h[d]) && this._map.removeLayer(h[d]);
        for (d = 0; d < r.length; d++)
          this._map.hasLayer(r[d]) || this._map.addLayer(r[d]);
        this._handlingClick = !1, this._refocusOnMap();
      },
      _checkDisabledLayers: function() {
        for (var t = this._layerControlInputs, i, o, r = this._map.getZoom(), h = t.length - 1; h >= 0; h--)
          i = t[h], o = this._getLayer(i.layerId).layer, i.disabled = o.options.minZoom !== void 0 && r < o.options.minZoom || o.options.maxZoom !== void 0 && r > o.options.maxZoom;
      },
      _expandIfNotCollapsed: function() {
        return this._map && !this.options.collapsed && this.expand(), this;
      },
      _expandSafely: function() {
        var t = this._section;
        et(t, "click", Yt), this.expand(), setTimeout(function() {
          Lt(t, "click", Yt);
        });
      }
    }), eh = function(t, i, o) {
      return new Ar(t, i, o);
    }, ns = we.extend({
      // @section
      // @aka Control.Zoom options
      options: {
        position: "topleft",
        // @option zoomInText: String = '<span aria-hidden="true">+</span>'
        // The text set on the 'zoom in' button.
        zoomInText: '<span aria-hidden="true">+</span>',
        // @option zoomInTitle: String = 'Zoom in'
        // The title set on the 'zoom in' button.
        zoomInTitle: "Zoom in",
        // @option zoomOutText: String = '<span aria-hidden="true">&#x2212;</span>'
        // The text set on the 'zoom out' button.
        zoomOutText: '<span aria-hidden="true">&#x2212;</span>',
        // @option zoomOutTitle: String = 'Zoom out'
        // The title set on the 'zoom out' button.
        zoomOutTitle: "Zoom out"
      },
      onAdd: function(t) {
        var i = "leaflet-control-zoom", o = dt("div", i + " leaflet-bar"), r = this.options;
        return this._zoomInButton = this._createButton(
          r.zoomInText,
          r.zoomInTitle,
          i + "-in",
          o,
          this._zoomIn
        ), this._zoomOutButton = this._createButton(
          r.zoomOutText,
          r.zoomOutTitle,
          i + "-out",
          o,
          this._zoomOut
        ), this._updateDisabled(), t.on("zoomend zoomlevelschange", this._updateDisabled, this), o;
      },
      onRemove: function(t) {
        t.off("zoomend zoomlevelschange", this._updateDisabled, this);
      },
      disable: function() {
        return this._disabled = !0, this._updateDisabled(), this;
      },
      enable: function() {
        return this._disabled = !1, this._updateDisabled(), this;
      },
      _zoomIn: function(t) {
        !this._disabled && this._map._zoom < this._map.getMaxZoom() && this._map.zoomIn(this._map.options.zoomDelta * (t.shiftKey ? 3 : 1));
      },
      _zoomOut: function(t) {
        !this._disabled && this._map._zoom > this._map.getMinZoom() && this._map.zoomOut(this._map.options.zoomDelta * (t.shiftKey ? 3 : 1));
      },
      _createButton: function(t, i, o, r, h) {
        var d = dt("a", o, r);
        return d.innerHTML = t, d.href = "#", d.title = i, d.setAttribute("role", "button"), d.setAttribute("aria-label", i), an(d), et(d, "click", gi), et(d, "click", h, this), et(d, "click", this._refocusOnMap, this), d;
      },
      _updateDisabled: function() {
        var t = this._map, i = "leaflet-disabled";
        jt(this._zoomInButton, i), jt(this._zoomOutButton, i), this._zoomInButton.setAttribute("aria-disabled", "false"), this._zoomOutButton.setAttribute("aria-disabled", "false"), (this._disabled || t._zoom === t.getMinZoom()) && (ot(this._zoomOutButton, i), this._zoomOutButton.setAttribute("aria-disabled", "true")), (this._disabled || t._zoom === t.getMaxZoom()) && (ot(this._zoomInButton, i), this._zoomInButton.setAttribute("aria-disabled", "true"));
      }
    });
    ht.mergeOptions({
      zoomControl: !0
    }), ht.addInitHook(function() {
      this.options.zoomControl && (this.zoomControl = new ns(), this.addControl(this.zoomControl));
    });
    var ih = function(t) {
      return new ns(t);
    }, Ir = we.extend({
      // @section
      // @aka Control.Scale options
      options: {
        position: "bottomleft",
        // @option maxWidth: Number = 100
        // Maximum width of the control in pixels. The width is set dynamically to show round values (e.g. 100, 200, 500).
        maxWidth: 100,
        // @option metric: Boolean = True
        // Whether to show the metric scale line (m/km).
        metric: !0,
        // @option imperial: Boolean = True
        // Whether to show the imperial scale line (mi/ft).
        imperial: !0
        // @option updateWhenIdle: Boolean = false
        // If `true`, the control is updated on [`moveend`](#map-moveend), otherwise it's always up-to-date (updated on [`move`](#map-move)).
      },
      onAdd: function(t) {
        var i = "leaflet-control-scale", o = dt("div", i), r = this.options;
        return this._addScales(r, i + "-line", o), t.on(r.updateWhenIdle ? "moveend" : "move", this._update, this), t.whenReady(this._update, this), o;
      },
      onRemove: function(t) {
        t.off(this.options.updateWhenIdle ? "moveend" : "move", this._update, this);
      },
      _addScales: function(t, i, o) {
        t.metric && (this._mScale = dt("div", i, o)), t.imperial && (this._iScale = dt("div", i, o));
      },
      _update: function() {
        var t = this._map, i = t.getSize().y / 2, o = t.distance(
          t.containerPointToLatLng([0, i]),
          t.containerPointToLatLng([this.options.maxWidth, i])
        );
        this._updateScales(o);
      },
      _updateScales: function(t) {
        this.options.metric && t && this._updateMetric(t), this.options.imperial && t && this._updateImperial(t);
      },
      _updateMetric: function(t) {
        var i = this._getRoundNum(t), o = i < 1e3 ? i + " m" : i / 1e3 + " km";
        this._updateScale(this._mScale, o, i / t);
      },
      _updateImperial: function(t) {
        var i = t * 3.2808399, o, r, h;
        i > 5280 ? (o = i / 5280, r = this._getRoundNum(o), this._updateScale(this._iScale, r + " mi", r / o)) : (h = this._getRoundNum(i), this._updateScale(this._iScale, h + " ft", h / i));
      },
      _updateScale: function(t, i, o) {
        t.style.width = Math.round(this.options.maxWidth * o) + "px", t.innerHTML = i;
      },
      _getRoundNum: function(t) {
        var i = Math.pow(10, (Math.floor(t) + "").length - 1), o = t / i;
        return o = o >= 10 ? 10 : o >= 5 ? 5 : o >= 3 ? 3 : o >= 2 ? 2 : 1, i * o;
      }
    }), nh = function(t) {
      return new Ir(t);
    }, oh = '<svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="12" height="8" viewBox="0 0 12 8" class="leaflet-attribution-flag"><path fill="#4C7BE1" d="M0 0h12v4H0z"/><path fill="#FFD500" d="M0 4h12v3H0z"/><path fill="#E0BC00" d="M0 7h12v1H0z"/></svg>', os = we.extend({
      // @section
      // @aka Control.Attribution options
      options: {
        position: "bottomright",
        // @option prefix: String|false = 'Leaflet'
        // The HTML text shown before the attributions. Pass `false` to disable.
        prefix: '<a href="https://leafletjs.com" title="A JavaScript library for interactive maps">' + ($.inlineSvg ? oh + " " : "") + "Leaflet</a>"
      },
      initialize: function(t) {
        E(this, t), this._attributions = {};
      },
      onAdd: function(t) {
        t.attributionControl = this, this._container = dt("div", "leaflet-control-attribution"), an(this._container);
        for (var i in t._layers)
          t._layers[i].getAttribution && this.addAttribution(t._layers[i].getAttribution());
        return this._update(), t.on("layeradd", this._addAttribution, this), this._container;
      },
      onRemove: function(t) {
        t.off("layeradd", this._addAttribution, this);
      },
      _addAttribution: function(t) {
        t.layer.getAttribution && (this.addAttribution(t.layer.getAttribution()), t.layer.once("remove", function() {
          this.removeAttribution(t.layer.getAttribution());
        }, this));
      },
      // @method setPrefix(prefix: String|false): this
      // The HTML text shown before the attributions. Pass `false` to disable.
      setPrefix: function(t) {
        return this.options.prefix = t, this._update(), this;
      },
      // @method addAttribution(text: String): this
      // Adds an attribution text (e.g. `'&copy; OpenStreetMap contributors'`).
      addAttribution: function(t) {
        return t ? (this._attributions[t] || (this._attributions[t] = 0), this._attributions[t]++, this._update(), this) : this;
      },
      // @method removeAttribution(text: String): this
      // Removes an attribution text.
      removeAttribution: function(t) {
        return t ? (this._attributions[t] && (this._attributions[t]--, this._update()), this) : this;
      },
      _update: function() {
        if (this._map) {
          var t = [];
          for (var i in this._attributions)
            this._attributions[i] && t.push(i);
          var o = [];
          this.options.prefix && o.push(this.options.prefix), t.length && o.push(t.join(", ")), this._container.innerHTML = o.join(' <span aria-hidden="true">|</span> ');
        }
      }
    });
    ht.mergeOptions({
      attributionControl: !0
    }), ht.addInitHook(function() {
      this.options.attributionControl && new os().addTo(this);
    });
    var sh = function(t) {
      return new os(t);
    };
    we.Layers = Ar, we.Zoom = ns, we.Scale = Ir, we.Attribution = os, ln.layers = eh, ln.zoom = ih, ln.scale = nh, ln.attribution = sh;
    var ke = It.extend({
      initialize: function(t) {
        this._map = t;
      },
      // @method enable(): this
      // Enables the handler
      enable: function() {
        return this._enabled ? this : (this._enabled = !0, this.addHooks(), this);
      },
      // @method disable(): this
      // Disables the handler
      disable: function() {
        return this._enabled ? (this._enabled = !1, this.removeHooks(), this) : this;
      },
      // @method enabled(): Boolean
      // Returns `true` if the handler is enabled
      enabled: function() {
        return !!this._enabled;
      }
      // @section Extension methods
      // Classes inheriting from `Handler` must implement the two following methods:
      // @method addHooks()
      // Called when the handler is enabled, should add event hooks.
      // @method removeHooks()
      // Called when the handler is disabled, should remove the event hooks added previously.
    });
    ke.addTo = function(t, i) {
      return t.addHandler(i, this), this;
    };
    var rh = { Events: ft }, kr = $.touch ? "touchstart mousedown" : "mousedown", Je = bt.extend({
      options: {
        // @section
        // @aka Draggable options
        // @option clickTolerance: Number = 3
        // The max number of pixels a user can shift the mouse pointer during a click
        // for it to be considered a valid click (as opposed to a mouse drag).
        clickTolerance: 3
      },
      // @constructor L.Draggable(el: HTMLElement, dragHandle?: HTMLElement, preventOutline?: Boolean, options?: Draggable options)
      // Creates a `Draggable` object for moving `el` when you start dragging the `dragHandle` element (equals `el` itself by default).
      initialize: function(t, i, o, r) {
        E(this, r), this._element = t, this._dragStartTarget = i || t, this._preventOutline = o;
      },
      // @method enable()
      // Enables the dragging ability
      enable: function() {
        this._enabled || (et(this._dragStartTarget, kr, this._onDown, this), this._enabled = !0);
      },
      // @method disable()
      // Disables the dragging ability
      disable: function() {
        this._enabled && (Je._dragging === this && this.finishDrag(!0), Lt(this._dragStartTarget, kr, this._onDown, this), this._enabled = !1, this._moved = !1);
      },
      _onDown: function(t) {
        if (this._enabled && (this._moved = !1, !Wo(this._element, "leaflet-zoom-anim"))) {
          if (t.touches && t.touches.length !== 1) {
            Je._dragging === this && this.finishDrag();
            return;
          }
          if (!(Je._dragging || t.shiftKey || t.which !== 1 && t.button !== 1 && !t.touches) && (Je._dragging = this, this._preventOutline && Yo(this._element), Ko(), on(), !this._moving)) {
            this.fire("down");
            var i = t.touches ? t.touches[0] : t, o = Tr(this._element);
            this._startPoint = new Z(i.clientX, i.clientY), this._startPos = mi(this._element), this._parentScale = Jo(o);
            var r = t.type === "mousedown";
            et(document, r ? "mousemove" : "touchmove", this._onMove, this), et(document, r ? "mouseup" : "touchend touchcancel", this._onUp, this);
          }
        }
      },
      _onMove: function(t) {
        if (this._enabled) {
          if (t.touches && t.touches.length > 1) {
            this._moved = !0;
            return;
          }
          var i = t.touches && t.touches.length === 1 ? t.touches[0] : t, o = new Z(i.clientX, i.clientY)._subtract(this._startPoint);
          !o.x && !o.y || Math.abs(o.x) + Math.abs(o.y) < this.options.clickTolerance || (o.x /= this._parentScale.x, o.y /= this._parentScale.y, Yt(t), this._moved || (this.fire("dragstart"), this._moved = !0, ot(document.body, "leaflet-dragging"), this._lastTarget = t.target || t.srcElement, window.SVGElementInstance && this._lastTarget instanceof window.SVGElementInstance && (this._lastTarget = this._lastTarget.correspondingUseElement), ot(this._lastTarget, "leaflet-drag-target")), this._newPos = this._startPos.add(o), this._moving = !0, this._lastEvent = t, this._updatePosition());
        }
      },
      _updatePosition: function() {
        var t = { originalEvent: this._lastEvent };
        this.fire("predrag", t), Wt(this._element, this._newPos), this.fire("drag", t);
      },
      _onUp: function() {
        this._enabled && this.finishDrag();
      },
      finishDrag: function(t) {
        jt(document.body, "leaflet-dragging"), this._lastTarget && (jt(this._lastTarget, "leaflet-drag-target"), this._lastTarget = null), Lt(document, "mousemove touchmove", this._onMove, this), Lt(document, "mouseup touchend touchcancel", this._onUp, this), qo(), sn(), this._moved && this._moving && this.fire("dragend", {
          noInertia: t,
          distance: this._newPos.distanceTo(this._startPos)
        }), this._moving = !1, Je._dragging = !1;
      }
    });
    function Br(t, i) {
      if (!i || !t.length)
        return t.slice();
      var o = i * i;
      return t = uh(t, o), t = lh(t, o), t;
    }
    function zr(t, i, o) {
      return Math.sqrt(un(t, i, o, !0));
    }
    function ah(t, i, o) {
      return un(t, i, o);
    }
    function lh(t, i) {
      var o = t.length, r = typeof Uint8Array != void 0 + "" ? Uint8Array : Array, h = new r(o);
      h[0] = h[o - 1] = 1, ss(t, h, i, 0, o - 1);
      var d, m = [];
      for (d = 0; d < o; d++)
        h[d] && m.push(t[d]);
      return m;
    }
    function ss(t, i, o, r, h) {
      var d = 0, m, y, w;
      for (y = r + 1; y <= h - 1; y++)
        w = un(t[y], t[r], t[h], !0), w > d && (m = y, d = w);
      d > o && (i[m] = 1, ss(t, i, o, r, m), ss(t, i, o, m, h));
    }
    function uh(t, i) {
      for (var o = [t[0]], r = 1, h = 0, d = t.length; r < d; r++)
        hh(t[r], t[h]) > i && (o.push(t[r]), h = r);
      return h < d - 1 && o.push(t[d - 1]), o;
    }
    var Nr;
    function Zr(t, i, o, r, h) {
      var d = r ? Nr : vi(t, o), m = vi(i, o), y, w, O;
      for (Nr = m; ; ) {
        if (!(d | m))
          return [t, i];
        if (d & m)
          return !1;
        y = d || m, w = $n(t, i, y, o, h), O = vi(w, o), y === d ? (t = w, d = O) : (i = w, m = O);
      }
    }
    function $n(t, i, o, r, h) {
      var d = i.x - t.x, m = i.y - t.y, y = r.min, w = r.max, O, N;
      return o & 8 ? (O = t.x + d * (w.y - t.y) / m, N = w.y) : o & 4 ? (O = t.x + d * (y.y - t.y) / m, N = y.y) : o & 2 ? (O = w.x, N = t.y + m * (w.x - t.x) / d) : o & 1 && (O = y.x, N = t.y + m * (y.x - t.x) / d), new Z(O, N, h);
    }
    function vi(t, i) {
      var o = 0;
      return t.x < i.min.x ? o |= 1 : t.x > i.max.x && (o |= 2), t.y < i.min.y ? o |= 4 : t.y > i.max.y && (o |= 8), o;
    }
    function hh(t, i) {
      var o = i.x - t.x, r = i.y - t.y;
      return o * o + r * r;
    }
    function un(t, i, o, r) {
      var h = i.x, d = i.y, m = o.x - h, y = o.y - d, w = m * m + y * y, O;
      return w > 0 && (O = ((t.x - h) * m + (t.y - d) * y) / w, O > 1 ? (h = o.x, d = o.y) : O > 0 && (h += m * O, d += y * O)), m = t.x - h, y = t.y - d, r ? m * m + y * y : new Z(h, d);
    }
    function pe(t) {
      return !D(t[0]) || typeof t[0][0] != "object" && typeof t[0][0] < "u";
    }
    function Rr(t) {
      return console.warn("Deprecated use of _flat, please use L.LineUtil.isFlat instead."), pe(t);
    }
    function Dr(t, i) {
      var o, r, h, d, m, y, w, O;
      if (!t || t.length === 0)
        throw new Error("latlngs not passed");
      pe(t) || (console.warn("latlngs are not flat! Only the first ring will be used"), t = t[0]);
      var N = [];
      for (var G in t)
        N.push(i.project(at(t[G])));
      var lt = N.length;
      for (o = 0, r = 0; o < lt - 1; o++)
        r += N[o].distanceTo(N[o + 1]) / 2;
      if (r === 0)
        O = N[0];
      else
        for (o = 0, d = 0; o < lt - 1; o++)
          if (m = N[o], y = N[o + 1], h = m.distanceTo(y), d += h, d > r) {
            w = (d - r) / h, O = [
              y.x - w * (y.x - m.x),
              y.y - w * (y.y - m.y)
            ];
            break;
          }
      return i.unproject(K(O));
    }
    var ch = {
      __proto__: null,
      simplify: Br,
      pointToSegmentDistance: zr,
      closestPointOnSegment: ah,
      clipSegment: Zr,
      _getEdgeIntersection: $n,
      _getBitCode: vi,
      _sqClosestPointOnSegment: un,
      isFlat: pe,
      _flat: Rr,
      polylineCenter: Dr
    };
    function Fr(t, i, o) {
      var r, h = [1, 4, 2, 8], d, m, y, w, O, N, G, lt;
      for (d = 0, N = t.length; d < N; d++)
        t[d]._code = vi(t[d], i);
      for (y = 0; y < 4; y++) {
        for (G = h[y], r = [], d = 0, N = t.length, m = N - 1; d < N; m = d++)
          w = t[d], O = t[m], w._code & G ? O._code & G || (lt = $n(O, w, G, i, o), lt._code = vi(lt, i), r.push(lt)) : (O._code & G && (lt = $n(O, w, G, i, o), lt._code = vi(lt, i), r.push(lt)), r.push(w));
        t = r;
      }
      return t;
    }
    function jr(t, i) {
      var o, r, h, d, m, y, w, O, N;
      if (!t || t.length === 0)
        throw new Error("latlngs not passed");
      pe(t) || (console.warn("latlngs are not flat! Only the first ring will be used"), t = t[0]);
      var G = [];
      for (var lt in t)
        G.push(i.project(at(t[lt])));
      var me = G.length;
      for (y = w = O = 0, o = 0, r = me - 1; o < me; r = o++)
        h = G[o], d = G[r], m = h.y * d.x - d.y * h.x, w += (h.x + d.x) * m, O += (h.y + d.y) * m, y += m * 3;
      return y === 0 ? N = G[0] : N = [w / y, O / y], i.unproject(K(N));
    }
    var fh = {
      __proto__: null,
      clipPolygon: Fr,
      polygonCenter: jr
    }, rs = {
      project: function(t) {
        return new Z(t.lng, t.lat);
      },
      unproject: function(t) {
        return new vt(t.y, t.x);
      },
      bounds: new mt([-180, -90], [180, 90])
    }, as = {
      R: 6378137,
      R_MINOR: 6356752314245179e-9,
      bounds: new mt([-2003750834279e-5, -1549657073972e-5], [2003750834279e-5, 1876465623138e-5]),
      project: function(t) {
        var i = Math.PI / 180, o = this.R, r = t.lat * i, h = this.R_MINOR / o, d = Math.sqrt(1 - h * h), m = d * Math.sin(r), y = Math.tan(Math.PI / 4 - r / 2) / Math.pow((1 - m) / (1 + m), d / 2);
        return r = -o * Math.log(Math.max(y, 1e-10)), new Z(t.lng * i * o, r);
      },
      unproject: function(t) {
        for (var i = 180 / Math.PI, o = this.R, r = this.R_MINOR / o, h = Math.sqrt(1 - r * r), d = Math.exp(-t.y / o), m = Math.PI / 2 - 2 * Math.atan(d), y = 0, w = 0.1, O; y < 15 && Math.abs(w) > 1e-7; y++)
          O = h * Math.sin(m), O = Math.pow((1 - O) / (1 + O), h / 2), w = Math.PI / 2 - 2 * Math.atan(d * O) - m, m += w;
        return new vt(m * i, t.x * i / o);
      }
    }, dh = {
      __proto__: null,
      LonLat: rs,
      Mercator: as,
      SphericalMercator: fi
    }, ph = l({}, be, {
      code: "EPSG:3395",
      projection: as,
      transformation: function() {
        var t = 0.5 / (Math.PI * as.R);
        return _(t, 0.5, -t, 0.5);
      }()
    }), Hr = l({}, be, {
      code: "EPSG:4326",
      projection: rs,
      transformation: _(1 / 180, 1, -1 / 180, 0.5)
    }), mh = l({}, le, {
      projection: rs,
      transformation: _(1, 0, -1, 0),
      scale: function(t) {
        return Math.pow(2, t);
      },
      zoom: function(t) {
        return Math.log(t) / Math.LN2;
      },
      distance: function(t, i) {
        var o = i.lng - t.lng, r = i.lat - t.lat;
        return Math.sqrt(o * o + r * r);
      },
      infinite: !0
    });
    le.Earth = be, le.EPSG3395 = ph, le.EPSG3857 = v, le.EPSG900913 = P, le.EPSG4326 = Hr, le.Simple = mh;
    var xe = bt.extend({
      // Classes extending `L.Layer` will inherit the following options:
      options: {
        // @option pane: String = 'overlayPane'
        // By default the layer will be added to the map's [overlay pane](#map-overlaypane). Overriding this option will cause the layer to be placed on another pane by default.
        pane: "overlayPane",
        // @option attribution: String = null
        // String to be shown in the attribution control, e.g. "© OpenStreetMap contributors". It describes the layer data and is often a legal obligation towards copyright holders and tile providers.
        attribution: null,
        bubblingMouseEvents: !0
      },
      /* @section
       * Classes extending `L.Layer` will inherit the following methods:
       *
       * @method addTo(map: Map|LayerGroup): this
       * Adds the layer to the given map or layer group.
       */
      addTo: function(t) {
        return t.addLayer(this), this;
      },
      // @method remove: this
      // Removes the layer from the map it is currently active on.
      remove: function() {
        return this.removeFrom(this._map || this._mapToAdd);
      },
      // @method removeFrom(map: Map): this
      // Removes the layer from the given map
      //
      // @alternative
      // @method removeFrom(group: LayerGroup): this
      // Removes the layer from the given `LayerGroup`
      removeFrom: function(t) {
        return t && t.removeLayer(this), this;
      },
      // @method getPane(name? : String): HTMLElement
      // Returns the `HTMLElement` representing the named pane on the map. If `name` is omitted, returns the pane for this layer.
      getPane: function(t) {
        return this._map.getPane(t ? this.options[t] || t : this.options.pane);
      },
      addInteractiveTarget: function(t) {
        return this._map._targets[p(t)] = this, this;
      },
      removeInteractiveTarget: function(t) {
        return delete this._map._targets[p(t)], this;
      },
      // @method getAttribution: String
      // Used by the `attribution control`, returns the [attribution option](#gridlayer-attribution).
      getAttribution: function() {
        return this.options.attribution;
      },
      _layerAdd: function(t) {
        var i = t.target;
        if (i.hasLayer(this)) {
          if (this._map = i, this._zoomAnimated = i._zoomAnimated, this.getEvents) {
            var o = this.getEvents();
            i.on(o, this), this.once("remove", function() {
              i.off(o, this);
            }, this);
          }
          this.onAdd(i), this.fire("add"), i.fire("layeradd", { layer: this });
        }
      }
    });
    ht.include({
      // @method addLayer(layer: Layer): this
      // Adds the given layer to the map
      addLayer: function(t) {
        if (!t._layerAdd)
          throw new Error("The provided object is not a Layer.");
        var i = p(t);
        return this._layers[i] ? this : (this._layers[i] = t, t._mapToAdd = this, t.beforeAdd && t.beforeAdd(this), this.whenReady(t._layerAdd, t), this);
      },
      // @method removeLayer(layer: Layer): this
      // Removes the given layer from the map.
      removeLayer: function(t) {
        var i = p(t);
        return this._layers[i] ? (this._loaded && t.onRemove(this), delete this._layers[i], this._loaded && (this.fire("layerremove", { layer: t }), t.fire("remove")), t._map = t._mapToAdd = null, this) : this;
      },
      // @method hasLayer(layer: Layer): Boolean
      // Returns `true` if the given layer is currently added to the map
      hasLayer: function(t) {
        return p(t) in this._layers;
      },
      /* @method eachLayer(fn: Function, context?: Object): this
       * Iterates over the layers of the map, optionally specifying context of the iterator function.
       * ```
       * map.eachLayer(function(layer){
       *     layer.bindPopup('Hello');
       * });
       * ```
       */
      eachLayer: function(t, i) {
        for (var o in this._layers)
          t.call(i, this._layers[o]);
        return this;
      },
      _addLayers: function(t) {
        t = t ? D(t) ? t : [t] : [];
        for (var i = 0, o = t.length; i < o; i++)
          this.addLayer(t[i]);
      },
      _addZoomLimit: function(t) {
        (!isNaN(t.options.maxZoom) || !isNaN(t.options.minZoom)) && (this._zoomBoundLayers[p(t)] = t, this._updateZoomLevels());
      },
      _removeZoomLimit: function(t) {
        var i = p(t);
        this._zoomBoundLayers[i] && (delete this._zoomBoundLayers[i], this._updateZoomLevels());
      },
      _updateZoomLevels: function() {
        var t = 1 / 0, i = -1 / 0, o = this._getZoomSpan();
        for (var r in this._zoomBoundLayers) {
          var h = this._zoomBoundLayers[r].options;
          t = h.minZoom === void 0 ? t : Math.min(t, h.minZoom), i = h.maxZoom === void 0 ? i : Math.max(i, h.maxZoom);
        }
        this._layersMaxZoom = i === -1 / 0 ? void 0 : i, this._layersMinZoom = t === 1 / 0 ? void 0 : t, o !== this._getZoomSpan() && this.fire("zoomlevelschange"), this.options.maxZoom === void 0 && this._layersMaxZoom && this.getZoom() > this._layersMaxZoom && this.setZoom(this._layersMaxZoom), this.options.minZoom === void 0 && this._layersMinZoom && this.getZoom() < this._layersMinZoom && this.setZoom(this._layersMinZoom);
      }
    });
    var Zi = xe.extend({
      initialize: function(t, i) {
        E(this, i), this._layers = {};
        var o, r;
        if (t)
          for (o = 0, r = t.length; o < r; o++)
            this.addLayer(t[o]);
      },
      // @method addLayer(layer: Layer): this
      // Adds the given layer to the group.
      addLayer: function(t) {
        var i = this.getLayerId(t);
        return this._layers[i] = t, this._map && this._map.addLayer(t), this;
      },
      // @method removeLayer(layer: Layer): this
      // Removes the given layer from the group.
      // @alternative
      // @method removeLayer(id: Number): this
      // Removes the layer with the given internal ID from the group.
      removeLayer: function(t) {
        var i = t in this._layers ? t : this.getLayerId(t);
        return this._map && this._layers[i] && this._map.removeLayer(this._layers[i]), delete this._layers[i], this;
      },
      // @method hasLayer(layer: Layer): Boolean
      // Returns `true` if the given layer is currently added to the group.
      // @alternative
      // @method hasLayer(id: Number): Boolean
      // Returns `true` if the given internal ID is currently added to the group.
      hasLayer: function(t) {
        var i = typeof t == "number" ? t : this.getLayerId(t);
        return i in this._layers;
      },
      // @method clearLayers(): this
      // Removes all the layers from the group.
      clearLayers: function() {
        return this.eachLayer(this.removeLayer, this);
      },
      // @method invoke(methodName: String, …): this
      // Calls `methodName` on every layer contained in this group, passing any
      // additional parameters. Has no effect if the layers contained do not
      // implement `methodName`.
      invoke: function(t) {
        var i = Array.prototype.slice.call(arguments, 1), o, r;
        for (o in this._layers)
          r = this._layers[o], r[t] && r[t].apply(r, i);
        return this;
      },
      onAdd: function(t) {
        this.eachLayer(t.addLayer, t);
      },
      onRemove: function(t) {
        this.eachLayer(t.removeLayer, t);
      },
      // @method eachLayer(fn: Function, context?: Object): this
      // Iterates over the layers of the group, optionally specifying context of the iterator function.
      // ```js
      // group.eachLayer(function (layer) {
      // 	layer.bindPopup('Hello');
      // });
      // ```
      eachLayer: function(t, i) {
        for (var o in this._layers)
          t.call(i, this._layers[o]);
        return this;
      },
      // @method getLayer(id: Number): Layer
      // Returns the layer with the given internal ID.
      getLayer: function(t) {
        return this._layers[t];
      },
      // @method getLayers(): Layer[]
      // Returns an array of all the layers added to the group.
      getLayers: function() {
        var t = [];
        return this.eachLayer(t.push, t), t;
      },
      // @method setZIndex(zIndex: Number): this
      // Calls `setZIndex` on every layer contained in this group, passing the z-index.
      setZIndex: function(t) {
        return this.invoke("setZIndex", t);
      },
      // @method getLayerId(layer: Layer): Number
      // Returns the internal ID for a layer
      getLayerId: function(t) {
        return p(t);
      }
    }), _h = function(t, i) {
      return new Zi(t, i);
    }, He = Zi.extend({
      addLayer: function(t) {
        return this.hasLayer(t) ? this : (t.addEventParent(this), Zi.prototype.addLayer.call(this, t), this.fire("layeradd", { layer: t }));
      },
      removeLayer: function(t) {
        return this.hasLayer(t) ? (t in this._layers && (t = this._layers[t]), t.removeEventParent(this), Zi.prototype.removeLayer.call(this, t), this.fire("layerremove", { layer: t })) : this;
      },
      // @method setStyle(style: Path options): this
      // Sets the given path options to each layer of the group that has a `setStyle` method.
      setStyle: function(t) {
        return this.invoke("setStyle", t);
      },
      // @method bringToFront(): this
      // Brings the layer group to the top of all other layers
      bringToFront: function() {
        return this.invoke("bringToFront");
      },
      // @method bringToBack(): this
      // Brings the layer group to the back of all other layers
      bringToBack: function() {
        return this.invoke("bringToBack");
      },
      // @method getBounds(): LatLngBounds
      // Returns the LatLngBounds of the Feature Group (created from bounds and coordinates of its children).
      getBounds: function() {
        var t = new Gt();
        for (var i in this._layers) {
          var o = this._layers[i];
          t.extend(o.getBounds ? o.getBounds() : o.getLatLng());
        }
        return t;
      }
    }), gh = function(t, i) {
      return new He(t, i);
    }, Ri = It.extend({
      /* @section
       * @aka Icon options
       *
       * @option iconUrl: String = null
       * **(required)** The URL to the icon image (absolute or relative to your script path).
       *
       * @option iconRetinaUrl: String = null
       * The URL to a retina sized version of the icon image (absolute or relative to your
       * script path). Used for Retina screen devices.
       *
       * @option iconSize: Point = null
       * Size of the icon image in pixels.
       *
       * @option iconAnchor: Point = null
       * The coordinates of the "tip" of the icon (relative to its top left corner). The icon
       * will be aligned so that this point is at the marker's geographical location. Centered
       * by default if size is specified, also can be set in CSS with negative margins.
       *
       * @option popupAnchor: Point = [0, 0]
       * The coordinates of the point from which popups will "open", relative to the icon anchor.
       *
       * @option tooltipAnchor: Point = [0, 0]
       * The coordinates of the point from which tooltips will "open", relative to the icon anchor.
       *
       * @option shadowUrl: String = null
       * The URL to the icon shadow image. If not specified, no shadow image will be created.
       *
       * @option shadowRetinaUrl: String = null
       *
       * @option shadowSize: Point = null
       * Size of the shadow image in pixels.
       *
       * @option shadowAnchor: Point = null
       * The coordinates of the "tip" of the shadow (relative to its top left corner) (the same
       * as iconAnchor if not specified).
       *
       * @option className: String = ''
       * A custom class name to assign to both icon and shadow images. Empty by default.
       */
      options: {
        popupAnchor: [0, 0],
        tooltipAnchor: [0, 0],
        // @option crossOrigin: Boolean|String = false
        // Whether the crossOrigin attribute will be added to the tiles.
        // If a String is provided, all tiles will have their crossOrigin attribute set to the String provided. This is needed if you want to access tile pixel data.
        // Refer to [CORS Settings](https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_settings_attributes) for valid String values.
        crossOrigin: !1
      },
      initialize: function(t) {
        E(this, t);
      },
      // @method createIcon(oldIcon?: HTMLElement): HTMLElement
      // Called internally when the icon has to be shown, returns a `<img>` HTML element
      // styled according to the options.
      createIcon: function(t) {
        return this._createIcon("icon", t);
      },
      // @method createShadow(oldIcon?: HTMLElement): HTMLElement
      // As `createIcon`, but for the shadow beneath it.
      createShadow: function(t) {
        return this._createIcon("shadow", t);
      },
      _createIcon: function(t, i) {
        var o = this._getIconUrl(t);
        if (!o) {
          if (t === "icon")
            throw new Error("iconUrl not set in Icon options (see the docs).");
          return null;
        }
        var r = this._createImg(o, i && i.tagName === "IMG" ? i : null);
        return this._setIconStyles(r, t), (this.options.crossOrigin || this.options.crossOrigin === "") && (r.crossOrigin = this.options.crossOrigin === !0 ? "" : this.options.crossOrigin), r;
      },
      _setIconStyles: function(t, i) {
        var o = this.options, r = o[i + "Size"];
        typeof r == "number" && (r = [r, r]);
        var h = K(r), d = K(i === "shadow" && o.shadowAnchor || o.iconAnchor || h && h.divideBy(2, !0));
        t.className = "leaflet-marker-" + i + " " + (o.className || ""), d && (t.style.marginLeft = -d.x + "px", t.style.marginTop = -d.y + "px"), h && (t.style.width = h.x + "px", t.style.height = h.y + "px");
      },
      _createImg: function(t, i) {
        return i = i || document.createElement("img"), i.src = t, i;
      },
      _getIconUrl: function(t) {
        return $.retina && this.options[t + "RetinaUrl"] || this.options[t + "Url"];
      }
    });
    function vh(t) {
      return new Ri(t);
    }
    var hn = Ri.extend({
      options: {
        iconUrl: "marker-icon.png",
        iconRetinaUrl: "marker-icon-2x.png",
        shadowUrl: "marker-shadow.png",
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      },
      _getIconUrl: function(t) {
        return typeof hn.imagePath != "string" && (hn.imagePath = this._detectIconPath()), (this.options.imagePath || hn.imagePath) + Ri.prototype._getIconUrl.call(this, t);
      },
      _stripUrl: function(t) {
        var i = function(o, r, h) {
          var d = r.exec(o);
          return d && d[h];
        };
        return t = i(t, /^url\((['"])?(.+)\1\)$/, 2), t && i(t, /^(.*)marker-icon\.png$/, 1);
      },
      _detectIconPath: function() {
        var t = dt("div", "leaflet-default-icon-path", document.body), i = nn(t, "background-image") || nn(t, "backgroundImage");
        if (document.body.removeChild(t), i = this._stripUrl(i), i)
          return i;
        var o = document.querySelector('link[href$="leaflet.css"]');
        return o ? o.href.substring(0, o.href.length - 11 - 1) : "";
      }
    }), $r = ke.extend({
      initialize: function(t) {
        this._marker = t;
      },
      addHooks: function() {
        var t = this._marker._icon;
        this._draggable || (this._draggable = new Je(t, t, !0)), this._draggable.on({
          dragstart: this._onDragStart,
          predrag: this._onPreDrag,
          drag: this._onDrag,
          dragend: this._onDragEnd
        }, this).enable(), ot(t, "leaflet-marker-draggable");
      },
      removeHooks: function() {
        this._draggable.off({
          dragstart: this._onDragStart,
          predrag: this._onPreDrag,
          drag: this._onDrag,
          dragend: this._onDragEnd
        }, this).disable(), this._marker._icon && jt(this._marker._icon, "leaflet-marker-draggable");
      },
      moved: function() {
        return this._draggable && this._draggable._moved;
      },
      _adjustPan: function(t) {
        var i = this._marker, o = i._map, r = this._marker.options.autoPanSpeed, h = this._marker.options.autoPanPadding, d = mi(i._icon), m = o.getPixelBounds(), y = o.getPixelOrigin(), w = Ct(
          m.min._subtract(y).add(h),
          m.max._subtract(y).subtract(h)
        );
        if (!w.contains(d)) {
          var O = K(
            (Math.max(w.max.x, d.x) - w.max.x) / (m.max.x - w.max.x) - (Math.min(w.min.x, d.x) - w.min.x) / (m.min.x - w.min.x),
            (Math.max(w.max.y, d.y) - w.max.y) / (m.max.y - w.max.y) - (Math.min(w.min.y, d.y) - w.min.y) / (m.min.y - w.min.y)
          ).multiplyBy(r);
          o.panBy(O, { animate: !1 }), this._draggable._newPos._add(O), this._draggable._startPos._add(O), Wt(i._icon, this._draggable._newPos), this._onDrag(t), this._panRequest = ut(this._adjustPan.bind(this, t));
        }
      },
      _onDragStart: function() {
        this._oldLatLng = this._marker.getLatLng(), this._marker.closePopup && this._marker.closePopup(), this._marker.fire("movestart").fire("dragstart");
      },
      _onPreDrag: function(t) {
        this._marker.options.autoPan && (R(this._panRequest), this._panRequest = ut(this._adjustPan.bind(this, t)));
      },
      _onDrag: function(t) {
        var i = this._marker, o = i._shadow, r = mi(i._icon), h = i._map.layerPointToLatLng(r);
        o && Wt(o, r), i._latlng = h, t.latlng = h, t.oldLatLng = this._oldLatLng, i.fire("move", t).fire("drag", t);
      },
      _onDragEnd: function(t) {
        R(this._panRequest), delete this._oldLatLng, this._marker.fire("moveend").fire("dragend", t);
      }
    }), Wn = xe.extend({
      // @section
      // @aka Marker options
      options: {
        // @option icon: Icon = *
        // Icon instance to use for rendering the marker.
        // See [Icon documentation](#L.Icon) for details on how to customize the marker icon.
        // If not specified, a common instance of `L.Icon.Default` is used.
        icon: new hn(),
        // Option inherited from "Interactive layer" abstract class
        interactive: !0,
        // @option keyboard: Boolean = true
        // Whether the marker can be tabbed to with a keyboard and clicked by pressing enter.
        keyboard: !0,
        // @option title: String = ''
        // Text for the browser tooltip that appear on marker hover (no tooltip by default).
        // [Useful for accessibility](https://leafletjs.com/examples/accessibility/#markers-must-be-labelled).
        title: "",
        // @option alt: String = 'Marker'
        // Text for the `alt` attribute of the icon image.
        // [Useful for accessibility](https://leafletjs.com/examples/accessibility/#markers-must-be-labelled).
        alt: "Marker",
        // @option zIndexOffset: Number = 0
        // By default, marker images zIndex is set automatically based on its latitude. Use this option if you want to put the marker on top of all others (or below), specifying a high value like `1000` (or high negative value, respectively).
        zIndexOffset: 0,
        // @option opacity: Number = 1.0
        // The opacity of the marker.
        opacity: 1,
        // @option riseOnHover: Boolean = false
        // If `true`, the marker will get on top of others when you hover the mouse over it.
        riseOnHover: !1,
        // @option riseOffset: Number = 250
        // The z-index offset used for the `riseOnHover` feature.
        riseOffset: 250,
        // @option pane: String = 'markerPane'
        // `Map pane` where the markers icon will be added.
        pane: "markerPane",
        // @option shadowPane: String = 'shadowPane'
        // `Map pane` where the markers shadow will be added.
        shadowPane: "shadowPane",
        // @option bubblingMouseEvents: Boolean = false
        // When `true`, a mouse event on this marker will trigger the same event on the map
        // (unless [`L.DomEvent.stopPropagation`](#domevent-stoppropagation) is used).
        bubblingMouseEvents: !1,
        // @option autoPanOnFocus: Boolean = true
        // When `true`, the map will pan whenever the marker is focused (via
        // e.g. pressing `tab` on the keyboard) to ensure the marker is
        // visible within the map's bounds
        autoPanOnFocus: !0,
        // @section Draggable marker options
        // @option draggable: Boolean = false
        // Whether the marker is draggable with mouse/touch or not.
        draggable: !1,
        // @option autoPan: Boolean = false
        // Whether to pan the map when dragging this marker near its edge or not.
        autoPan: !1,
        // @option autoPanPadding: Point = Point(50, 50)
        // Distance (in pixels to the left/right and to the top/bottom) of the
        // map edge to start panning the map.
        autoPanPadding: [50, 50],
        // @option autoPanSpeed: Number = 10
        // Number of pixels the map should pan by.
        autoPanSpeed: 10
      },
      /* @section
       *
       * In addition to [shared layer methods](#Layer) like `addTo()` and `remove()` and [popup methods](#Popup) like bindPopup() you can also use the following methods:
       */
      initialize: function(t, i) {
        E(this, i), this._latlng = at(t);
      },
      onAdd: function(t) {
        this._zoomAnimated = this._zoomAnimated && t.options.markerZoomAnimation, this._zoomAnimated && t.on("zoomanim", this._animateZoom, this), this._initIcon(), this.update();
      },
      onRemove: function(t) {
        this.dragging && this.dragging.enabled() && (this.options.draggable = !0, this.dragging.removeHooks()), delete this.dragging, this._zoomAnimated && t.off("zoomanim", this._animateZoom, this), this._removeIcon(), this._removeShadow();
      },
      getEvents: function() {
        return {
          zoom: this.update,
          viewreset: this.update
        };
      },
      // @method getLatLng: LatLng
      // Returns the current geographical position of the marker.
      getLatLng: function() {
        return this._latlng;
      },
      // @method setLatLng(latlng: LatLng): this
      // Changes the marker position to the given point.
      setLatLng: function(t) {
        var i = this._latlng;
        return this._latlng = at(t), this.update(), this.fire("move", { oldLatLng: i, latlng: this._latlng });
      },
      // @method setZIndexOffset(offset: Number): this
      // Changes the [zIndex offset](#marker-zindexoffset) of the marker.
      setZIndexOffset: function(t) {
        return this.options.zIndexOffset = t, this.update();
      },
      // @method getIcon: Icon
      // Returns the current icon used by the marker
      getIcon: function() {
        return this.options.icon;
      },
      // @method setIcon(icon: Icon): this
      // Changes the marker icon.
      setIcon: function(t) {
        return this.options.icon = t, this._map && (this._initIcon(), this.update()), this._popup && this.bindPopup(this._popup, this._popup.options), this;
      },
      getElement: function() {
        return this._icon;
      },
      update: function() {
        if (this._icon && this._map) {
          var t = this._map.latLngToLayerPoint(this._latlng).round();
          this._setPos(t);
        }
        return this;
      },
      _initIcon: function() {
        var t = this.options, i = "leaflet-zoom-" + (this._zoomAnimated ? "animated" : "hide"), o = t.icon.createIcon(this._icon), r = !1;
        o !== this._icon && (this._icon && this._removeIcon(), r = !0, t.title && (o.title = t.title), o.tagName === "IMG" && (o.alt = t.alt || "")), ot(o, i), t.keyboard && (o.tabIndex = "0", o.setAttribute("role", "button")), this._icon = o, t.riseOnHover && this.on({
          mouseover: this._bringToFront,
          mouseout: this._resetZIndex
        }), this.options.autoPanOnFocus && et(o, "focus", this._panOnFocus, this);
        var h = t.icon.createShadow(this._shadow), d = !1;
        h !== this._shadow && (this._removeShadow(), d = !0), h && (ot(h, i), h.alt = ""), this._shadow = h, t.opacity < 1 && this._updateOpacity(), r && this.getPane().appendChild(this._icon), this._initInteraction(), h && d && this.getPane(t.shadowPane).appendChild(this._shadow);
      },
      _removeIcon: function() {
        this.options.riseOnHover && this.off({
          mouseover: this._bringToFront,
          mouseout: this._resetZIndex
        }), this.options.autoPanOnFocus && Lt(this._icon, "focus", this._panOnFocus, this), kt(this._icon), this.removeInteractiveTarget(this._icon), this._icon = null;
      },
      _removeShadow: function() {
        this._shadow && kt(this._shadow), this._shadow = null;
      },
      _setPos: function(t) {
        this._icon && Wt(this._icon, t), this._shadow && Wt(this._shadow, t), this._zIndex = t.y + this.options.zIndexOffset, this._resetZIndex();
      },
      _updateZIndex: function(t) {
        this._icon && (this._icon.style.zIndex = this._zIndex + t);
      },
      _animateZoom: function(t) {
        var i = this._map._latLngToNewLayerPoint(this._latlng, t.zoom, t.center).round();
        this._setPos(i);
      },
      _initInteraction: function() {
        if (this.options.interactive && (ot(this._icon, "leaflet-interactive"), this.addInteractiveTarget(this._icon), $r)) {
          var t = this.options.draggable;
          this.dragging && (t = this.dragging.enabled(), this.dragging.disable()), this.dragging = new $r(this), t && this.dragging.enable();
        }
      },
      // @method setOpacity(opacity: Number): this
      // Changes the opacity of the marker.
      setOpacity: function(t) {
        return this.options.opacity = t, this._map && this._updateOpacity(), this;
      },
      _updateOpacity: function() {
        var t = this.options.opacity;
        this._icon && de(this._icon, t), this._shadow && de(this._shadow, t);
      },
      _bringToFront: function() {
        this._updateZIndex(this.options.riseOffset);
      },
      _resetZIndex: function() {
        this._updateZIndex(0);
      },
      _panOnFocus: function() {
        var t = this._map;
        if (t) {
          var i = this.options.icon.options, o = i.iconSize ? K(i.iconSize) : K(0, 0), r = i.iconAnchor ? K(i.iconAnchor) : K(0, 0);
          t.panInside(this._latlng, {
            paddingTopLeft: r,
            paddingBottomRight: o.subtract(r)
          });
        }
      },
      _getPopupAnchor: function() {
        return this.options.icon.options.popupAnchor;
      },
      _getTooltipAnchor: function() {
        return this.options.icon.options.tooltipAnchor;
      }
    });
    function yh(t, i) {
      return new Wn(t, i);
    }
    var Xe = xe.extend({
      // @section
      // @aka Path options
      options: {
        // @option stroke: Boolean = true
        // Whether to draw stroke along the path. Set it to `false` to disable borders on polygons or circles.
        stroke: !0,
        // @option color: String = '#3388ff'
        // Stroke color
        color: "#3388ff",
        // @option weight: Number = 3
        // Stroke width in pixels
        weight: 3,
        // @option opacity: Number = 1.0
        // Stroke opacity
        opacity: 1,
        // @option lineCap: String= 'round'
        // A string that defines [shape to be used at the end](https://developer.mozilla.org/docs/Web/SVG/Attribute/stroke-linecap) of the stroke.
        lineCap: "round",
        // @option lineJoin: String = 'round'
        // A string that defines [shape to be used at the corners](https://developer.mozilla.org/docs/Web/SVG/Attribute/stroke-linejoin) of the stroke.
        lineJoin: "round",
        // @option dashArray: String = null
        // A string that defines the stroke [dash pattern](https://developer.mozilla.org/docs/Web/SVG/Attribute/stroke-dasharray). Doesn't work on `Canvas`-powered layers in [some old browsers](https://developer.mozilla.org/docs/Web/API/CanvasRenderingContext2D/setLineDash#Browser_compatibility).
        dashArray: null,
        // @option dashOffset: String = null
        // A string that defines the [distance into the dash pattern to start the dash](https://developer.mozilla.org/docs/Web/SVG/Attribute/stroke-dashoffset). Doesn't work on `Canvas`-powered layers in [some old browsers](https://developer.mozilla.org/docs/Web/API/CanvasRenderingContext2D/setLineDash#Browser_compatibility).
        dashOffset: null,
        // @option fill: Boolean = depends
        // Whether to fill the path with color. Set it to `false` to disable filling on polygons or circles.
        fill: !1,
        // @option fillColor: String = *
        // Fill color. Defaults to the value of the [`color`](#path-color) option
        fillColor: null,
        // @option fillOpacity: Number = 0.2
        // Fill opacity.
        fillOpacity: 0.2,
        // @option fillRule: String = 'evenodd'
        // A string that defines [how the inside of a shape](https://developer.mozilla.org/docs/Web/SVG/Attribute/fill-rule) is determined.
        fillRule: "evenodd",
        // className: '',
        // Option inherited from "Interactive layer" abstract class
        interactive: !0,
        // @option bubblingMouseEvents: Boolean = true
        // When `true`, a mouse event on this path will trigger the same event on the map
        // (unless [`L.DomEvent.stopPropagation`](#domevent-stoppropagation) is used).
        bubblingMouseEvents: !0
      },
      beforeAdd: function(t) {
        this._renderer = t.getRenderer(this);
      },
      onAdd: function() {
        this._renderer._initPath(this), this._reset(), this._renderer._addPath(this);
      },
      onRemove: function() {
        this._renderer._removePath(this);
      },
      // @method redraw(): this
      // Redraws the layer. Sometimes useful after you changed the coordinates that the path uses.
      redraw: function() {
        return this._map && this._renderer._updatePath(this), this;
      },
      // @method setStyle(style: Path options): this
      // Changes the appearance of a Path based on the options in the `Path options` object.
      setStyle: function(t) {
        return E(this, t), this._renderer && (this._renderer._updateStyle(this), this.options.stroke && t && Object.prototype.hasOwnProperty.call(t, "weight") && this._updateBounds()), this;
      },
      // @method bringToFront(): this
      // Brings the layer to the top of all path layers.
      bringToFront: function() {
        return this._renderer && this._renderer._bringToFront(this), this;
      },
      // @method bringToBack(): this
      // Brings the layer to the bottom of all path layers.
      bringToBack: function() {
        return this._renderer && this._renderer._bringToBack(this), this;
      },
      getElement: function() {
        return this._path;
      },
      _reset: function() {
        this._project(), this._update();
      },
      _clickTolerance: function() {
        return (this.options.stroke ? this.options.weight / 2 : 0) + (this._renderer.options.tolerance || 0);
      }
    }), Un = Xe.extend({
      // @section
      // @aka CircleMarker options
      options: {
        fill: !0,
        // @option radius: Number = 10
        // Radius of the circle marker, in pixels
        radius: 10
      },
      initialize: function(t, i) {
        E(this, i), this._latlng = at(t), this._radius = this.options.radius;
      },
      // @method setLatLng(latLng: LatLng): this
      // Sets the position of a circle marker to a new location.
      setLatLng: function(t) {
        var i = this._latlng;
        return this._latlng = at(t), this.redraw(), this.fire("move", { oldLatLng: i, latlng: this._latlng });
      },
      // @method getLatLng(): LatLng
      // Returns the current geographical position of the circle marker
      getLatLng: function() {
        return this._latlng;
      },
      // @method setRadius(radius: Number): this
      // Sets the radius of a circle marker. Units are in pixels.
      setRadius: function(t) {
        return this.options.radius = this._radius = t, this.redraw();
      },
      // @method getRadius(): Number
      // Returns the current radius of the circle
      getRadius: function() {
        return this._radius;
      },
      setStyle: function(t) {
        var i = t && t.radius || this._radius;
        return Xe.prototype.setStyle.call(this, t), this.setRadius(i), this;
      },
      _project: function() {
        this._point = this._map.latLngToLayerPoint(this._latlng), this._updateBounds();
      },
      _updateBounds: function() {
        var t = this._radius, i = this._radiusY || t, o = this._clickTolerance(), r = [t + o, i + o];
        this._pxBounds = new mt(this._point.subtract(r), this._point.add(r));
      },
      _update: function() {
        this._map && this._updatePath();
      },
      _updatePath: function() {
        this._renderer._updateCircle(this);
      },
      _empty: function() {
        return this._radius && !this._renderer._bounds.intersects(this._pxBounds);
      },
      // Needed by the `Canvas` renderer for interactivity
      _containsPoint: function(t) {
        return t.distanceTo(this._point) <= this._radius + this._clickTolerance();
      }
    });
    function bh(t, i) {
      return new Un(t, i);
    }
    var ls = Un.extend({
      initialize: function(t, i, o) {
        if (typeof i == "number" && (i = l({}, o, { radius: i })), E(this, i), this._latlng = at(t), isNaN(this.options.radius))
          throw new Error("Circle radius cannot be NaN");
        this._mRadius = this.options.radius;
      },
      // @method setRadius(radius: Number): this
      // Sets the radius of a circle. Units are in meters.
      setRadius: function(t) {
        return this._mRadius = t, this.redraw();
      },
      // @method getRadius(): Number
      // Returns the current radius of a circle. Units are in meters.
      getRadius: function() {
        return this._mRadius;
      },
      // @method getBounds(): LatLngBounds
      // Returns the `LatLngBounds` of the path.
      getBounds: function() {
        var t = [this._radius, this._radiusY || this._radius];
        return new Gt(
          this._map.layerPointToLatLng(this._point.subtract(t)),
          this._map.layerPointToLatLng(this._point.add(t))
        );
      },
      setStyle: Xe.prototype.setStyle,
      _project: function() {
        var t = this._latlng.lng, i = this._latlng.lat, o = this._map, r = o.options.crs;
        if (r.distance === be.distance) {
          var h = Math.PI / 180, d = this._mRadius / be.R / h, m = o.project([i + d, t]), y = o.project([i - d, t]), w = m.add(y).divideBy(2), O = o.unproject(w).lat, N = Math.acos((Math.cos(d * h) - Math.sin(i * h) * Math.sin(O * h)) / (Math.cos(i * h) * Math.cos(O * h))) / h;
          (isNaN(N) || N === 0) && (N = d / Math.cos(Math.PI / 180 * i)), this._point = w.subtract(o.getPixelOrigin()), this._radius = isNaN(N) ? 0 : w.x - o.project([O, t - N]).x, this._radiusY = w.y - m.y;
        } else {
          var G = r.unproject(r.project(this._latlng).subtract([this._mRadius, 0]));
          this._point = o.latLngToLayerPoint(this._latlng), this._radius = this._point.x - o.latLngToLayerPoint(G).x;
        }
        this._updateBounds();
      }
    });
    function wh(t, i, o) {
      return new ls(t, i, o);
    }
    var $e = Xe.extend({
      // @section
      // @aka Polyline options
      options: {
        // @option smoothFactor: Number = 1.0
        // How much to simplify the polyline on each zoom level. More means
        // better performance and smoother look, and less means more accurate representation.
        smoothFactor: 1,
        // @option noClip: Boolean = false
        // Disable polyline clipping.
        noClip: !1
      },
      initialize: function(t, i) {
        E(this, i), this._setLatLngs(t);
      },
      // @method getLatLngs(): LatLng[]
      // Returns an array of the points in the path, or nested arrays of points in case of multi-polyline.
      getLatLngs: function() {
        return this._latlngs;
      },
      // @method setLatLngs(latlngs: LatLng[]): this
      // Replaces all the points in the polyline with the given array of geographical points.
      setLatLngs: function(t) {
        return this._setLatLngs(t), this.redraw();
      },
      // @method isEmpty(): Boolean
      // Returns `true` if the Polyline has no LatLngs.
      isEmpty: function() {
        return !this._latlngs.length;
      },
      // @method closestLayerPoint(p: Point): Point
      // Returns the point closest to `p` on the Polyline.
      closestLayerPoint: function(t) {
        for (var i = 1 / 0, o = null, r = un, h, d, m = 0, y = this._parts.length; m < y; m++)
          for (var w = this._parts[m], O = 1, N = w.length; O < N; O++) {
            h = w[O - 1], d = w[O];
            var G = r(t, h, d, !0);
            G < i && (i = G, o = r(t, h, d));
          }
        return o && (o.distance = Math.sqrt(i)), o;
      },
      // @method getCenter(): LatLng
      // Returns the center ([centroid](https://en.wikipedia.org/wiki/Centroid)) of the polyline.
      getCenter: function() {
        if (!this._map)
          throw new Error("Must add layer to map before using getCenter()");
        return Dr(this._defaultShape(), this._map.options.crs);
      },
      // @method getBounds(): LatLngBounds
      // Returns the `LatLngBounds` of the path.
      getBounds: function() {
        return this._bounds;
      },
      // @method addLatLng(latlng: LatLng, latlngs?: LatLng[]): this
      // Adds a given point to the polyline. By default, adds to the first ring of
      // the polyline in case of a multi-polyline, but can be overridden by passing
      // a specific ring as a LatLng array (that you can earlier access with [`getLatLngs`](#polyline-getlatlngs)).
      addLatLng: function(t, i) {
        return i = i || this._defaultShape(), t = at(t), i.push(t), this._bounds.extend(t), this.redraw();
      },
      _setLatLngs: function(t) {
        this._bounds = new Gt(), this._latlngs = this._convertLatLngs(t);
      },
      _defaultShape: function() {
        return pe(this._latlngs) ? this._latlngs : this._latlngs[0];
      },
      // recursively convert latlngs input into actual LatLng instances; calculate bounds along the way
      _convertLatLngs: function(t) {
        for (var i = [], o = pe(t), r = 0, h = t.length; r < h; r++)
          o ? (i[r] = at(t[r]), this._bounds.extend(i[r])) : i[r] = this._convertLatLngs(t[r]);
        return i;
      },
      _project: function() {
        var t = new mt();
        this._rings = [], this._projectLatlngs(this._latlngs, this._rings, t), this._bounds.isValid() && t.isValid() && (this._rawPxBounds = t, this._updateBounds());
      },
      _updateBounds: function() {
        var t = this._clickTolerance(), i = new Z(t, t);
        this._rawPxBounds && (this._pxBounds = new mt([
          this._rawPxBounds.min.subtract(i),
          this._rawPxBounds.max.add(i)
        ]));
      },
      // recursively turns latlngs into a set of rings with projected coordinates
      _projectLatlngs: function(t, i, o) {
        var r = t[0] instanceof vt, h = t.length, d, m;
        if (r) {
          for (m = [], d = 0; d < h; d++)
            m[d] = this._map.latLngToLayerPoint(t[d]), o.extend(m[d]);
          i.push(m);
        } else
          for (d = 0; d < h; d++)
            this._projectLatlngs(t[d], i, o);
      },
      // clip polyline by renderer bounds so that we have less to render for performance
      _clipPoints: function() {
        var t = this._renderer._bounds;
        if (this._parts = [], !(!this._pxBounds || !this._pxBounds.intersects(t))) {
          if (this.options.noClip) {
            this._parts = this._rings;
            return;
          }
          var i = this._parts, o, r, h, d, m, y, w;
          for (o = 0, h = 0, d = this._rings.length; o < d; o++)
            for (w = this._rings[o], r = 0, m = w.length; r < m - 1; r++)
              y = Zr(w[r], w[r + 1], t, r, !0), y && (i[h] = i[h] || [], i[h].push(y[0]), (y[1] !== w[r + 1] || r === m - 2) && (i[h].push(y[1]), h++));
        }
      },
      // simplify each clipped part of the polyline for performance
      _simplifyPoints: function() {
        for (var t = this._parts, i = this.options.smoothFactor, o = 0, r = t.length; o < r; o++)
          t[o] = Br(t[o], i);
      },
      _update: function() {
        this._map && (this._clipPoints(), this._simplifyPoints(), this._updatePath());
      },
      _updatePath: function() {
        this._renderer._updatePoly(this);
      },
      // Needed by the `Canvas` renderer for interactivity
      _containsPoint: function(t, i) {
        var o, r, h, d, m, y, w = this._clickTolerance();
        if (!this._pxBounds || !this._pxBounds.contains(t))
          return !1;
        for (o = 0, d = this._parts.length; o < d; o++)
          for (y = this._parts[o], r = 0, m = y.length, h = m - 1; r < m; h = r++)
            if (!(!i && r === 0) && zr(t, y[h], y[r]) <= w)
              return !0;
        return !1;
      }
    });
    function xh(t, i) {
      return new $e(t, i);
    }
    $e._flat = Rr;
    var Di = $e.extend({
      options: {
        fill: !0
      },
      isEmpty: function() {
        return !this._latlngs.length || !this._latlngs[0].length;
      },
      // @method getCenter(): LatLng
      // Returns the center ([centroid](http://en.wikipedia.org/wiki/Centroid)) of the Polygon.
      getCenter: function() {
        if (!this._map)
          throw new Error("Must add layer to map before using getCenter()");
        return jr(this._defaultShape(), this._map.options.crs);
      },
      _convertLatLngs: function(t) {
        var i = $e.prototype._convertLatLngs.call(this, t), o = i.length;
        return o >= 2 && i[0] instanceof vt && i[0].equals(i[o - 1]) && i.pop(), i;
      },
      _setLatLngs: function(t) {
        $e.prototype._setLatLngs.call(this, t), pe(this._latlngs) && (this._latlngs = [this._latlngs]);
      },
      _defaultShape: function() {
        return pe(this._latlngs[0]) ? this._latlngs[0] : this._latlngs[0][0];
      },
      _clipPoints: function() {
        var t = this._renderer._bounds, i = this.options.weight, o = new Z(i, i);
        if (t = new mt(t.min.subtract(o), t.max.add(o)), this._parts = [], !(!this._pxBounds || !this._pxBounds.intersects(t))) {
          if (this.options.noClip) {
            this._parts = this._rings;
            return;
          }
          for (var r = 0, h = this._rings.length, d; r < h; r++)
            d = Fr(this._rings[r], t, !0), d.length && this._parts.push(d);
        }
      },
      _updatePath: function() {
        this._renderer._updatePoly(this, !0);
      },
      // Needed by the `Canvas` renderer for interactivity
      _containsPoint: function(t) {
        var i = !1, o, r, h, d, m, y, w, O;
        if (!this._pxBounds || !this._pxBounds.contains(t))
          return !1;
        for (d = 0, w = this._parts.length; d < w; d++)
          for (o = this._parts[d], m = 0, O = o.length, y = O - 1; m < O; y = m++)
            r = o[m], h = o[y], r.y > t.y != h.y > t.y && t.x < (h.x - r.x) * (t.y - r.y) / (h.y - r.y) + r.x && (i = !i);
        return i || $e.prototype._containsPoint.call(this, t, !0);
      }
    });
    function Lh(t, i) {
      return new Di(t, i);
    }
    var We = He.extend({
      /* @section
       * @aka GeoJSON options
       *
       * @option pointToLayer: Function = *
       * A `Function` defining how GeoJSON points spawn Leaflet layers. It is internally
       * called when data is added, passing the GeoJSON point feature and its `LatLng`.
       * The default is to spawn a default `Marker`:
       * ```js
       * function(geoJsonPoint, latlng) {
       * 	return L.marker(latlng);
       * }
       * ```
       *
       * @option style: Function = *
       * A `Function` defining the `Path options` for styling GeoJSON lines and polygons,
       * called internally when data is added.
       * The default value is to not override any defaults:
       * ```js
       * function (geoJsonFeature) {
       * 	return {}
       * }
       * ```
       *
       * @option onEachFeature: Function = *
       * A `Function` that will be called once for each created `Feature`, after it has
       * been created and styled. Useful for attaching events and popups to features.
       * The default is to do nothing with the newly created layers:
       * ```js
       * function (feature, layer) {}
       * ```
       *
       * @option filter: Function = *
       * A `Function` that will be used to decide whether to include a feature or not.
       * The default is to include all features:
       * ```js
       * function (geoJsonFeature) {
       * 	return true;
       * }
       * ```
       * Note: dynamically changing the `filter` option will have effect only on newly
       * added data. It will _not_ re-evaluate already included features.
       *
       * @option coordsToLatLng: Function = *
       * A `Function` that will be used for converting GeoJSON coordinates to `LatLng`s.
       * The default is the `coordsToLatLng` static method.
       *
       * @option markersInheritOptions: Boolean = false
       * Whether default Markers for "Point" type Features inherit from group options.
       */
      initialize: function(t, i) {
        E(this, i), this._layers = {}, t && this.addData(t);
      },
      // @method addData( <GeoJSON> data ): this
      // Adds a GeoJSON object to the layer.
      addData: function(t) {
        var i = D(t) ? t : t.features, o, r, h;
        if (i) {
          for (o = 0, r = i.length; o < r; o++)
            h = i[o], (h.geometries || h.geometry || h.features || h.coordinates) && this.addData(h);
          return this;
        }
        var d = this.options;
        if (d.filter && !d.filter(t))
          return this;
        var m = Vn(t, d);
        return m ? (m.feature = Gn(t), m.defaultOptions = m.options, this.resetStyle(m), d.onEachFeature && d.onEachFeature(t, m), this.addLayer(m)) : this;
      },
      // @method resetStyle( <Path> layer? ): this
      // Resets the given vector layer's style to the original GeoJSON style, useful for resetting style after hover events.
      // If `layer` is omitted, the style of all features in the current layer is reset.
      resetStyle: function(t) {
        return t === void 0 ? this.eachLayer(this.resetStyle, this) : (t.options = l({}, t.defaultOptions), this._setLayerStyle(t, this.options.style), this);
      },
      // @method setStyle( <Function> style ): this
      // Changes styles of GeoJSON vector layers with the given style function.
      setStyle: function(t) {
        return this.eachLayer(function(i) {
          this._setLayerStyle(i, t);
        }, this);
      },
      _setLayerStyle: function(t, i) {
        t.setStyle && (typeof i == "function" && (i = i(t.feature)), t.setStyle(i));
      }
    });
    function Vn(t, i) {
      var o = t.type === "Feature" ? t.geometry : t, r = o ? o.coordinates : null, h = [], d = i && i.pointToLayer, m = i && i.coordsToLatLng || us, y, w, O, N;
      if (!r && !o)
        return null;
      switch (o.type) {
        case "Point":
          return y = m(r), Wr(d, t, y, i);
        case "MultiPoint":
          for (O = 0, N = r.length; O < N; O++)
            y = m(r[O]), h.push(Wr(d, t, y, i));
          return new He(h);
        case "LineString":
        case "MultiLineString":
          return w = Kn(r, o.type === "LineString" ? 0 : 1, m), new $e(w, i);
        case "Polygon":
        case "MultiPolygon":
          return w = Kn(r, o.type === "Polygon" ? 1 : 2, m), new Di(w, i);
        case "GeometryCollection":
          for (O = 0, N = o.geometries.length; O < N; O++) {
            var G = Vn({
              geometry: o.geometries[O],
              type: "Feature",
              properties: t.properties
            }, i);
            G && h.push(G);
          }
          return new He(h);
        case "FeatureCollection":
          for (O = 0, N = o.features.length; O < N; O++) {
            var lt = Vn(o.features[O], i);
            lt && h.push(lt);
          }
          return new He(h);
        default:
          throw new Error("Invalid GeoJSON object.");
      }
    }
    function Wr(t, i, o, r) {
      return t ? t(i, o) : new Wn(o, r && r.markersInheritOptions && r);
    }
    function us(t) {
      return new vt(t[1], t[0], t[2]);
    }
    function Kn(t, i, o) {
      for (var r = [], h = 0, d = t.length, m; h < d; h++)
        m = i ? Kn(t[h], i - 1, o) : (o || us)(t[h]), r.push(m);
      return r;
    }
    function hs(t, i) {
      return t = at(t), t.alt !== void 0 ? [T(t.lng, i), T(t.lat, i), T(t.alt, i)] : [T(t.lng, i), T(t.lat, i)];
    }
    function qn(t, i, o, r) {
      for (var h = [], d = 0, m = t.length; d < m; d++)
        h.push(i ? qn(t[d], pe(t[d]) ? 0 : i - 1, o, r) : hs(t[d], r));
      return !i && o && h.push(h[0].slice()), h;
    }
    function Fi(t, i) {
      return t.feature ? l({}, t.feature, { geometry: i }) : Gn(i);
    }
    function Gn(t) {
      return t.type === "Feature" || t.type === "FeatureCollection" ? t : {
        type: "Feature",
        properties: {},
        geometry: t
      };
    }
    var cs = {
      toGeoJSON: function(t) {
        return Fi(this, {
          type: "Point",
          coordinates: hs(this.getLatLng(), t)
        });
      }
    };
    Wn.include(cs), ls.include(cs), Un.include(cs), $e.include({
      toGeoJSON: function(t) {
        var i = !pe(this._latlngs), o = qn(this._latlngs, i ? 1 : 0, !1, t);
        return Fi(this, {
          type: (i ? "Multi" : "") + "LineString",
          coordinates: o
        });
      }
    }), Di.include({
      toGeoJSON: function(t) {
        var i = !pe(this._latlngs), o = i && !pe(this._latlngs[0]), r = qn(this._latlngs, o ? 2 : i ? 1 : 0, !0, t);
        return i || (r = [r]), Fi(this, {
          type: (o ? "Multi" : "") + "Polygon",
          coordinates: r
        });
      }
    }), Zi.include({
      toMultiPoint: function(t) {
        var i = [];
        return this.eachLayer(function(o) {
          i.push(o.toGeoJSON(t).geometry.coordinates);
        }), Fi(this, {
          type: "MultiPoint",
          coordinates: i
        });
      },
      // @method toGeoJSON(precision?: Number|false): Object
      // Coordinates values are rounded with [`formatNum`](#util-formatnum) function with given `precision`.
      // Returns a [`GeoJSON`](https://en.wikipedia.org/wiki/GeoJSON) representation of the layer group (as a GeoJSON `FeatureCollection`, `GeometryCollection`, or `MultiPoint`).
      toGeoJSON: function(t) {
        var i = this.feature && this.feature.geometry && this.feature.geometry.type;
        if (i === "MultiPoint")
          return this.toMultiPoint(t);
        var o = i === "GeometryCollection", r = [];
        return this.eachLayer(function(h) {
          if (h.toGeoJSON) {
            var d = h.toGeoJSON(t);
            if (o)
              r.push(d.geometry);
            else {
              var m = Gn(d);
              m.type === "FeatureCollection" ? r.push.apply(r, m.features) : r.push(m);
            }
          }
        }), o ? Fi(this, {
          geometries: r,
          type: "GeometryCollection"
        }) : {
          type: "FeatureCollection",
          features: r
        };
      }
    });
    function Ur(t, i) {
      return new We(t, i);
    }
    var Ph = Ur, Yn = xe.extend({
      // @section
      // @aka ImageOverlay options
      options: {
        // @option opacity: Number = 1.0
        // The opacity of the image overlay.
        opacity: 1,
        // @option alt: String = ''
        // Text for the `alt` attribute of the image (useful for accessibility).
        alt: "",
        // @option interactive: Boolean = false
        // If `true`, the image overlay will emit [mouse events](#interactive-layer) when clicked or hovered.
        interactive: !1,
        // @option crossOrigin: Boolean|String = false
        // Whether the crossOrigin attribute will be added to the image.
        // If a String is provided, the image will have its crossOrigin attribute set to the String provided. This is needed if you want to access image pixel data.
        // Refer to [CORS Settings](https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_settings_attributes) for valid String values.
        crossOrigin: !1,
        // @option errorOverlayUrl: String = ''
        // URL to the overlay image to show in place of the overlay that failed to load.
        errorOverlayUrl: "",
        // @option zIndex: Number = 1
        // The explicit [zIndex](https://developer.mozilla.org/docs/Web/CSS/CSS_Positioning/Understanding_z_index) of the overlay layer.
        zIndex: 1,
        // @option className: String = ''
        // A custom class name to assign to the image. Empty by default.
        className: ""
      },
      initialize: function(t, i, o) {
        this._url = t, this._bounds = $t(i), E(this, o);
      },
      onAdd: function() {
        this._image || (this._initImage(), this.options.opacity < 1 && this._updateOpacity()), this.options.interactive && (ot(this._image, "leaflet-interactive"), this.addInteractiveTarget(this._image)), this.getPane().appendChild(this._image), this._reset();
      },
      onRemove: function() {
        kt(this._image), this.options.interactive && this.removeInteractiveTarget(this._image);
      },
      // @method setOpacity(opacity: Number): this
      // Sets the opacity of the overlay.
      setOpacity: function(t) {
        return this.options.opacity = t, this._image && this._updateOpacity(), this;
      },
      setStyle: function(t) {
        return t.opacity && this.setOpacity(t.opacity), this;
      },
      // @method bringToFront(): this
      // Brings the layer to the top of all overlays.
      bringToFront: function() {
        return this._map && zi(this._image), this;
      },
      // @method bringToBack(): this
      // Brings the layer to the bottom of all overlays.
      bringToBack: function() {
        return this._map && Ni(this._image), this;
      },
      // @method setUrl(url: String): this
      // Changes the URL of the image.
      setUrl: function(t) {
        return this._url = t, this._image && (this._image.src = t), this;
      },
      // @method setBounds(bounds: LatLngBounds): this
      // Update the bounds that this ImageOverlay covers
      setBounds: function(t) {
        return this._bounds = $t(t), this._map && this._reset(), this;
      },
      getEvents: function() {
        var t = {
          zoom: this._reset,
          viewreset: this._reset
        };
        return this._zoomAnimated && (t.zoomanim = this._animateZoom), t;
      },
      // @method setZIndex(value: Number): this
      // Changes the [zIndex](#imageoverlay-zindex) of the image overlay.
      setZIndex: function(t) {
        return this.options.zIndex = t, this._updateZIndex(), this;
      },
      // @method getBounds(): LatLngBounds
      // Get the bounds that this ImageOverlay covers
      getBounds: function() {
        return this._bounds;
      },
      // @method getElement(): HTMLElement
      // Returns the instance of [`HTMLImageElement`](https://developer.mozilla.org/docs/Web/API/HTMLImageElement)
      // used by this overlay.
      getElement: function() {
        return this._image;
      },
      _initImage: function() {
        var t = this._url.tagName === "IMG", i = this._image = t ? this._url : dt("img");
        if (ot(i, "leaflet-image-layer"), this._zoomAnimated && ot(i, "leaflet-zoom-animated"), this.options.className && ot(i, this.options.className), i.onselectstart = x, i.onmousemove = x, i.onload = c(this.fire, this, "load"), i.onerror = c(this._overlayOnError, this, "error"), (this.options.crossOrigin || this.options.crossOrigin === "") && (i.crossOrigin = this.options.crossOrigin === !0 ? "" : this.options.crossOrigin), this.options.zIndex && this._updateZIndex(), t) {
          this._url = i.src;
          return;
        }
        i.src = this._url, i.alt = this.options.alt;
      },
      _animateZoom: function(t) {
        var i = this._map.getZoomScale(t.zoom), o = this._map._latLngBoundsToNewLayerBounds(this._bounds, t.zoom, t.center).min;
        pi(this._image, o, i);
      },
      _reset: function() {
        var t = this._image, i = new mt(
          this._map.latLngToLayerPoint(this._bounds.getNorthWest()),
          this._map.latLngToLayerPoint(this._bounds.getSouthEast())
        ), o = i.getSize();
        Wt(t, i.min), t.style.width = o.x + "px", t.style.height = o.y + "px";
      },
      _updateOpacity: function() {
        de(this._image, this.options.opacity);
      },
      _updateZIndex: function() {
        this._image && this.options.zIndex !== void 0 && this.options.zIndex !== null && (this._image.style.zIndex = this.options.zIndex);
      },
      _overlayOnError: function() {
        this.fire("error");
        var t = this.options.errorOverlayUrl;
        t && this._url !== t && (this._url = t, this._image.src = t);
      },
      // @method getCenter(): LatLng
      // Returns the center of the ImageOverlay.
      getCenter: function() {
        return this._bounds.getCenter();
      }
    }), Th = function(t, i, o) {
      return new Yn(t, i, o);
    }, Vr = Yn.extend({
      // @section
      // @aka VideoOverlay options
      options: {
        // @option autoplay: Boolean = true
        // Whether the video starts playing automatically when loaded.
        // On some browsers autoplay will only work with `muted: true`
        autoplay: !0,
        // @option loop: Boolean = true
        // Whether the video will loop back to the beginning when played.
        loop: !0,
        // @option keepAspectRatio: Boolean = true
        // Whether the video will save aspect ratio after the projection.
        // Relevant for supported browsers. See [browser compatibility](https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit)
        keepAspectRatio: !0,
        // @option muted: Boolean = false
        // Whether the video starts on mute when loaded.
        muted: !1,
        // @option playsInline: Boolean = true
        // Mobile browsers will play the video right where it is instead of open it up in fullscreen mode.
        playsInline: !0
      },
      _initImage: function() {
        var t = this._url.tagName === "VIDEO", i = this._image = t ? this._url : dt("video");
        if (ot(i, "leaflet-image-layer"), this._zoomAnimated && ot(i, "leaflet-zoom-animated"), this.options.className && ot(i, this.options.className), i.onselectstart = x, i.onmousemove = x, i.onloadeddata = c(this.fire, this, "load"), t) {
          for (var o = i.getElementsByTagName("source"), r = [], h = 0; h < o.length; h++)
            r.push(o[h].src);
          this._url = o.length > 0 ? r : [i.src];
          return;
        }
        D(this._url) || (this._url = [this._url]), !this.options.keepAspectRatio && Object.prototype.hasOwnProperty.call(i.style, "objectFit") && (i.style.objectFit = "fill"), i.autoplay = !!this.options.autoplay, i.loop = !!this.options.loop, i.muted = !!this.options.muted, i.playsInline = !!this.options.playsInline;
        for (var d = 0; d < this._url.length; d++) {
          var m = dt("source");
          m.src = this._url[d], i.appendChild(m);
        }
      }
      // @method getElement(): HTMLVideoElement
      // Returns the instance of [`HTMLVideoElement`](https://developer.mozilla.org/docs/Web/API/HTMLVideoElement)
      // used by this overlay.
    });
    function Ch(t, i, o) {
      return new Vr(t, i, o);
    }
    var Kr = Yn.extend({
      _initImage: function() {
        var t = this._image = this._url;
        ot(t, "leaflet-image-layer"), this._zoomAnimated && ot(t, "leaflet-zoom-animated"), this.options.className && ot(t, this.options.className), t.onselectstart = x, t.onmousemove = x;
      }
      // @method getElement(): SVGElement
      // Returns the instance of [`SVGElement`](https://developer.mozilla.org/docs/Web/API/SVGElement)
      // used by this overlay.
    });
    function Oh(t, i, o) {
      return new Kr(t, i, o);
    }
    var Be = xe.extend({
      // @section
      // @aka DivOverlay options
      options: {
        // @option interactive: Boolean = false
        // If true, the popup/tooltip will listen to the mouse events.
        interactive: !1,
        // @option offset: Point = Point(0, 0)
        // The offset of the overlay position.
        offset: [0, 0],
        // @option className: String = ''
        // A custom CSS class name to assign to the overlay.
        className: "",
        // @option pane: String = undefined
        // `Map pane` where the overlay will be added.
        pane: void 0,
        // @option content: String|HTMLElement|Function = ''
        // Sets the HTML content of the overlay while initializing. If a function is passed the source layer will be
        // passed to the function. The function should return a `String` or `HTMLElement` to be used in the overlay.
        content: ""
      },
      initialize: function(t, i) {
        t && (t instanceof vt || D(t)) ? (this._latlng = at(t), E(this, i)) : (E(this, t), this._source = i), this.options.content && (this._content = this.options.content);
      },
      // @method openOn(map: Map): this
      // Adds the overlay to the map.
      // Alternative to `map.openPopup(popup)`/`.openTooltip(tooltip)`.
      openOn: function(t) {
        return t = arguments.length ? t : this._source._map, t.hasLayer(this) || t.addLayer(this), this;
      },
      // @method close(): this
      // Closes the overlay.
      // Alternative to `map.closePopup(popup)`/`.closeTooltip(tooltip)`
      // and `layer.closePopup()`/`.closeTooltip()`.
      close: function() {
        return this._map && this._map.removeLayer(this), this;
      },
      // @method toggle(layer?: Layer): this
      // Opens or closes the overlay bound to layer depending on its current state.
      // Argument may be omitted only for overlay bound to layer.
      // Alternative to `layer.togglePopup()`/`.toggleTooltip()`.
      toggle: function(t) {
        return this._map ? this.close() : (arguments.length ? this._source = t : t = this._source, this._prepareOpen(), this.openOn(t._map)), this;
      },
      onAdd: function(t) {
        this._zoomAnimated = t._zoomAnimated, this._container || this._initLayout(), t._fadeAnimated && de(this._container, 0), clearTimeout(this._removeTimeout), this.getPane().appendChild(this._container), this.update(), t._fadeAnimated && de(this._container, 1), this.bringToFront(), this.options.interactive && (ot(this._container, "leaflet-interactive"), this.addInteractiveTarget(this._container));
      },
      onRemove: function(t) {
        t._fadeAnimated ? (de(this._container, 0), this._removeTimeout = setTimeout(c(kt, void 0, this._container), 200)) : kt(this._container), this.options.interactive && (jt(this._container, "leaflet-interactive"), this.removeInteractiveTarget(this._container));
      },
      // @namespace DivOverlay
      // @method getLatLng: LatLng
      // Returns the geographical point of the overlay.
      getLatLng: function() {
        return this._latlng;
      },
      // @method setLatLng(latlng: LatLng): this
      // Sets the geographical point where the overlay will open.
      setLatLng: function(t) {
        return this._latlng = at(t), this._map && (this._updatePosition(), this._adjustPan()), this;
      },
      // @method getContent: String|HTMLElement
      // Returns the content of the overlay.
      getContent: function() {
        return this._content;
      },
      // @method setContent(htmlContent: String|HTMLElement|Function): this
      // Sets the HTML content of the overlay. If a function is passed the source layer will be passed to the function.
      // The function should return a `String` or `HTMLElement` to be used in the overlay.
      setContent: function(t) {
        return this._content = t, this.update(), this;
      },
      // @method getElement: String|HTMLElement
      // Returns the HTML container of the overlay.
      getElement: function() {
        return this._container;
      },
      // @method update: null
      // Updates the overlay content, layout and position. Useful for updating the overlay after something inside changed, e.g. image loaded.
      update: function() {
        this._map && (this._container.style.visibility = "hidden", this._updateContent(), this._updateLayout(), this._updatePosition(), this._container.style.visibility = "", this._adjustPan());
      },
      getEvents: function() {
        var t = {
          zoom: this._updatePosition,
          viewreset: this._updatePosition
        };
        return this._zoomAnimated && (t.zoomanim = this._animateZoom), t;
      },
      // @method isOpen: Boolean
      // Returns `true` when the overlay is visible on the map.
      isOpen: function() {
        return !!this._map && this._map.hasLayer(this);
      },
      // @method bringToFront: this
      // Brings this overlay in front of other overlays (in the same map pane).
      bringToFront: function() {
        return this._map && zi(this._container), this;
      },
      // @method bringToBack: this
      // Brings this overlay to the back of other overlays (in the same map pane).
      bringToBack: function() {
        return this._map && Ni(this._container), this;
      },
      // prepare bound overlay to open: update latlng pos / content source (for FeatureGroup)
      _prepareOpen: function(t) {
        var i = this._source;
        if (!i._map)
          return !1;
        if (i instanceof He) {
          i = null;
          var o = this._source._layers;
          for (var r in o)
            if (o[r]._map) {
              i = o[r];
              break;
            }
          if (!i)
            return !1;
          this._source = i;
        }
        if (!t)
          if (i.getCenter)
            t = i.getCenter();
          else if (i.getLatLng)
            t = i.getLatLng();
          else if (i.getBounds)
            t = i.getBounds().getCenter();
          else
            throw new Error("Unable to get source layer LatLng.");
        return this.setLatLng(t), this._map && this.update(), !0;
      },
      _updateContent: function() {
        if (this._content) {
          var t = this._contentNode, i = typeof this._content == "function" ? this._content(this._source || this) : this._content;
          if (typeof i == "string")
            t.innerHTML = i;
          else {
            for (; t.hasChildNodes(); )
              t.removeChild(t.firstChild);
            t.appendChild(i);
          }
          this.fire("contentupdate");
        }
      },
      _updatePosition: function() {
        if (this._map) {
          var t = this._map.latLngToLayerPoint(this._latlng), i = K(this.options.offset), o = this._getAnchor();
          this._zoomAnimated ? Wt(this._container, t.add(o)) : i = i.add(t).add(o);
          var r = this._containerBottom = -i.y, h = this._containerLeft = -Math.round(this._containerWidth / 2) + i.x;
          this._container.style.bottom = r + "px", this._container.style.left = h + "px";
        }
      },
      _getAnchor: function() {
        return [0, 0];
      }
    });
    ht.include({
      _initOverlay: function(t, i, o, r) {
        var h = i;
        return h instanceof t || (h = new t(r).setContent(i)), o && h.setLatLng(o), h;
      }
    }), xe.include({
      _initOverlay: function(t, i, o, r) {
        var h = o;
        return h instanceof t ? (E(h, r), h._source = this) : (h = i && !r ? i : new t(r, this), h.setContent(o)), h;
      }
    });
    var Jn = Be.extend({
      // @section
      // @aka Popup options
      options: {
        // @option pane: String = 'popupPane'
        // `Map pane` where the popup will be added.
        pane: "popupPane",
        // @option offset: Point = Point(0, 7)
        // The offset of the popup position.
        offset: [0, 7],
        // @option maxWidth: Number = 300
        // Max width of the popup, in pixels.
        maxWidth: 300,
        // @option minWidth: Number = 50
        // Min width of the popup, in pixels.
        minWidth: 50,
        // @option maxHeight: Number = null
        // If set, creates a scrollable container of the given height
        // inside a popup if its content exceeds it.
        // The scrollable container can be styled using the
        // `leaflet-popup-scrolled` CSS class selector.
        maxHeight: null,
        // @option autoPan: Boolean = true
        // Set it to `false` if you don't want the map to do panning animation
        // to fit the opened popup.
        autoPan: !0,
        // @option autoPanPaddingTopLeft: Point = null
        // The margin between the popup and the top left corner of the map
        // view after autopanning was performed.
        autoPanPaddingTopLeft: null,
        // @option autoPanPaddingBottomRight: Point = null
        // The margin between the popup and the bottom right corner of the map
        // view after autopanning was performed.
        autoPanPaddingBottomRight: null,
        // @option autoPanPadding: Point = Point(5, 5)
        // Equivalent of setting both top left and bottom right autopan padding to the same value.
        autoPanPadding: [5, 5],
        // @option keepInView: Boolean = false
        // Set it to `true` if you want to prevent users from panning the popup
        // off of the screen while it is open.
        keepInView: !1,
        // @option closeButton: Boolean = true
        // Controls the presence of a close button in the popup.
        closeButton: !0,
        // @option autoClose: Boolean = true
        // Set it to `false` if you want to override the default behavior of
        // the popup closing when another popup is opened.
        autoClose: !0,
        // @option closeOnEscapeKey: Boolean = true
        // Set it to `false` if you want to override the default behavior of
        // the ESC key for closing of the popup.
        closeOnEscapeKey: !0,
        // @option closeOnClick: Boolean = *
        // Set it if you want to override the default behavior of the popup closing when user clicks
        // on the map. Defaults to the map's [`closePopupOnClick`](#map-closepopuponclick) option.
        // @option className: String = ''
        // A custom CSS class name to assign to the popup.
        className: ""
      },
      // @namespace Popup
      // @method openOn(map: Map): this
      // Alternative to `map.openPopup(popup)`.
      // Adds the popup to the map and closes the previous one.
      openOn: function(t) {
        return t = arguments.length ? t : this._source._map, !t.hasLayer(this) && t._popup && t._popup.options.autoClose && t.removeLayer(t._popup), t._popup = this, Be.prototype.openOn.call(this, t);
      },
      onAdd: function(t) {
        Be.prototype.onAdd.call(this, t), t.fire("popupopen", { popup: this }), this._source && (this._source.fire("popupopen", { popup: this }, !0), this._source instanceof Xe || this._source.on("preclick", _i));
      },
      onRemove: function(t) {
        Be.prototype.onRemove.call(this, t), t.fire("popupclose", { popup: this }), this._source && (this._source.fire("popupclose", { popup: this }, !0), this._source instanceof Xe || this._source.off("preclick", _i));
      },
      getEvents: function() {
        var t = Be.prototype.getEvents.call(this);
        return (this.options.closeOnClick !== void 0 ? this.options.closeOnClick : this._map.options.closePopupOnClick) && (t.preclick = this.close), this.options.keepInView && (t.moveend = this._adjustPan), t;
      },
      _initLayout: function() {
        var t = "leaflet-popup", i = this._container = dt(
          "div",
          t + " " + (this.options.className || "") + " leaflet-zoom-animated"
        ), o = this._wrapper = dt("div", t + "-content-wrapper", i);
        if (this._contentNode = dt("div", t + "-content", o), an(i), es(this._contentNode), et(i, "contextmenu", _i), this._tipContainer = dt("div", t + "-tip-container", i), this._tip = dt("div", t + "-tip", this._tipContainer), this.options.closeButton) {
          var r = this._closeButton = dt("a", t + "-close-button", i);
          r.setAttribute("role", "button"), r.setAttribute("aria-label", "Close popup"), r.href = "#close", r.innerHTML = '<span aria-hidden="true">&#215;</span>', et(r, "click", function(h) {
            Yt(h), this.close();
          }, this);
        }
      },
      _updateLayout: function() {
        var t = this._contentNode, i = t.style;
        i.width = "", i.whiteSpace = "nowrap";
        var o = t.offsetWidth;
        o = Math.min(o, this.options.maxWidth), o = Math.max(o, this.options.minWidth), i.width = o + 1 + "px", i.whiteSpace = "", i.height = "";
        var r = t.offsetHeight, h = this.options.maxHeight, d = "leaflet-popup-scrolled";
        h && r > h ? (i.height = h + "px", ot(t, d)) : jt(t, d), this._containerWidth = this._container.offsetWidth;
      },
      _animateZoom: function(t) {
        var i = this._map._latLngToNewLayerPoint(this._latlng, t.zoom, t.center), o = this._getAnchor();
        Wt(this._container, i.add(o));
      },
      _adjustPan: function() {
        if (this.options.autoPan) {
          if (this._map._panAnim && this._map._panAnim.stop(), this._autopanning) {
            this._autopanning = !1;
            return;
          }
          var t = this._map, i = parseInt(nn(this._container, "marginBottom"), 10) || 0, o = this._container.offsetHeight + i, r = this._containerWidth, h = new Z(this._containerLeft, -o - this._containerBottom);
          h._add(mi(this._container));
          var d = t.layerPointToContainerPoint(h), m = K(this.options.autoPanPadding), y = K(this.options.autoPanPaddingTopLeft || m), w = K(this.options.autoPanPaddingBottomRight || m), O = t.getSize(), N = 0, G = 0;
          d.x + r + w.x > O.x && (N = d.x + r - O.x + w.x), d.x - N - y.x < 0 && (N = d.x - y.x), d.y + o + w.y > O.y && (G = d.y + o - O.y + w.y), d.y - G - y.y < 0 && (G = d.y - y.y), (N || G) && (this.options.keepInView && (this._autopanning = !0), t.fire("autopanstart").panBy([N, G]));
        }
      },
      _getAnchor: function() {
        return K(this._source && this._source._getPopupAnchor ? this._source._getPopupAnchor() : [0, 0]);
      }
    }), Sh = function(t, i) {
      return new Jn(t, i);
    };
    ht.mergeOptions({
      closePopupOnClick: !0
    }), ht.include({
      // @method openPopup(popup: Popup): this
      // Opens the specified popup while closing the previously opened (to make sure only one is opened at one time for usability).
      // @alternative
      // @method openPopup(content: String|HTMLElement, latlng: LatLng, options?: Popup options): this
      // Creates a popup with the specified content and options and opens it in the given point on a map.
      openPopup: function(t, i, o) {
        return this._initOverlay(Jn, t, i, o).openOn(this), this;
      },
      // @method closePopup(popup?: Popup): this
      // Closes the popup previously opened with [openPopup](#map-openpopup) (or the given one).
      closePopup: function(t) {
        return t = arguments.length ? t : this._popup, t && t.close(), this;
      }
    }), xe.include({
      // @method bindPopup(content: String|HTMLElement|Function|Popup, options?: Popup options): this
      // Binds a popup to the layer with the passed `content` and sets up the
      // necessary event listeners. If a `Function` is passed it will receive
      // the layer as the first argument and should return a `String` or `HTMLElement`.
      bindPopup: function(t, i) {
        return this._popup = this._initOverlay(Jn, this._popup, t, i), this._popupHandlersAdded || (this.on({
          click: this._openPopup,
          keypress: this._onKeyPress,
          remove: this.closePopup,
          move: this._movePopup
        }), this._popupHandlersAdded = !0), this;
      },
      // @method unbindPopup(): this
      // Removes the popup previously bound with `bindPopup`.
      unbindPopup: function() {
        return this._popup && (this.off({
          click: this._openPopup,
          keypress: this._onKeyPress,
          remove: this.closePopup,
          move: this._movePopup
        }), this._popupHandlersAdded = !1, this._popup = null), this;
      },
      // @method openPopup(latlng?: LatLng): this
      // Opens the bound popup at the specified `latlng` or at the default popup anchor if no `latlng` is passed.
      openPopup: function(t) {
        return this._popup && (this instanceof He || (this._popup._source = this), this._popup._prepareOpen(t || this._latlng) && this._popup.openOn(this._map)), this;
      },
      // @method closePopup(): this
      // Closes the popup bound to this layer if it is open.
      closePopup: function() {
        return this._popup && this._popup.close(), this;
      },
      // @method togglePopup(): this
      // Opens or closes the popup bound to this layer depending on its current state.
      togglePopup: function() {
        return this._popup && this._popup.toggle(this), this;
      },
      // @method isPopupOpen(): boolean
      // Returns `true` if the popup bound to this layer is currently open.
      isPopupOpen: function() {
        return this._popup ? this._popup.isOpen() : !1;
      },
      // @method setPopupContent(content: String|HTMLElement|Popup): this
      // Sets the content of the popup bound to this layer.
      setPopupContent: function(t) {
        return this._popup && this._popup.setContent(t), this;
      },
      // @method getPopup(): Popup
      // Returns the popup bound to this layer.
      getPopup: function() {
        return this._popup;
      },
      _openPopup: function(t) {
        if (!(!this._popup || !this._map)) {
          gi(t);
          var i = t.layer || t.target;
          if (this._popup._source === i && !(i instanceof Xe)) {
            this._map.hasLayer(this._popup) ? this.closePopup() : this.openPopup(t.latlng);
            return;
          }
          this._popup._source = i, this.openPopup(t.latlng);
        }
      },
      _movePopup: function(t) {
        this._popup.setLatLng(t.latlng);
      },
      _onKeyPress: function(t) {
        t.originalEvent.keyCode === 13 && this._openPopup(t);
      }
    });
    var Xn = Be.extend({
      // @section
      // @aka Tooltip options
      options: {
        // @option pane: String = 'tooltipPane'
        // `Map pane` where the tooltip will be added.
        pane: "tooltipPane",
        // @option offset: Point = Point(0, 0)
        // Optional offset of the tooltip position.
        offset: [0, 0],
        // @option direction: String = 'auto'
        // Direction where to open the tooltip. Possible values are: `right`, `left`,
        // `top`, `bottom`, `center`, `auto`.
        // `auto` will dynamically switch between `right` and `left` according to the tooltip
        // position on the map.
        direction: "auto",
        // @option permanent: Boolean = false
        // Whether to open the tooltip permanently or only on mouseover.
        permanent: !1,
        // @option sticky: Boolean = false
        // If true, the tooltip will follow the mouse instead of being fixed at the feature center.
        sticky: !1,
        // @option opacity: Number = 0.9
        // Tooltip container opacity.
        opacity: 0.9
      },
      onAdd: function(t) {
        Be.prototype.onAdd.call(this, t), this.setOpacity(this.options.opacity), t.fire("tooltipopen", { tooltip: this }), this._source && (this.addEventParent(this._source), this._source.fire("tooltipopen", { tooltip: this }, !0));
      },
      onRemove: function(t) {
        Be.prototype.onRemove.call(this, t), t.fire("tooltipclose", { tooltip: this }), this._source && (this.removeEventParent(this._source), this._source.fire("tooltipclose", { tooltip: this }, !0));
      },
      getEvents: function() {
        var t = Be.prototype.getEvents.call(this);
        return this.options.permanent || (t.preclick = this.close), t;
      },
      _initLayout: function() {
        var t = "leaflet-tooltip", i = t + " " + (this.options.className || "") + " leaflet-zoom-" + (this._zoomAnimated ? "animated" : "hide");
        this._contentNode = this._container = dt("div", i), this._container.setAttribute("role", "tooltip"), this._container.setAttribute("id", "leaflet-tooltip-" + p(this));
      },
      _updateLayout: function() {
      },
      _adjustPan: function() {
      },
      _setPosition: function(t) {
        var i, o, r = this._map, h = this._container, d = r.latLngToContainerPoint(r.getCenter()), m = r.layerPointToContainerPoint(t), y = this.options.direction, w = h.offsetWidth, O = h.offsetHeight, N = K(this.options.offset), G = this._getAnchor();
        y === "top" ? (i = w / 2, o = O) : y === "bottom" ? (i = w / 2, o = 0) : y === "center" ? (i = w / 2, o = O / 2) : y === "right" ? (i = 0, o = O / 2) : y === "left" ? (i = w, o = O / 2) : m.x < d.x ? (y = "right", i = 0, o = O / 2) : (y = "left", i = w + (N.x + G.x) * 2, o = O / 2), t = t.subtract(K(i, o, !0)).add(N).add(G), jt(h, "leaflet-tooltip-right"), jt(h, "leaflet-tooltip-left"), jt(h, "leaflet-tooltip-top"), jt(h, "leaflet-tooltip-bottom"), ot(h, "leaflet-tooltip-" + y), Wt(h, t);
      },
      _updatePosition: function() {
        var t = this._map.latLngToLayerPoint(this._latlng);
        this._setPosition(t);
      },
      setOpacity: function(t) {
        this.options.opacity = t, this._container && de(this._container, t);
      },
      _animateZoom: function(t) {
        var i = this._map._latLngToNewLayerPoint(this._latlng, t.zoom, t.center);
        this._setPosition(i);
      },
      _getAnchor: function() {
        return K(this._source && this._source._getTooltipAnchor && !this.options.sticky ? this._source._getTooltipAnchor() : [0, 0]);
      }
    }), Mh = function(t, i) {
      return new Xn(t, i);
    };
    ht.include({
      // @method openTooltip(tooltip: Tooltip): this
      // Opens the specified tooltip.
      // @alternative
      // @method openTooltip(content: String|HTMLElement, latlng: LatLng, options?: Tooltip options): this
      // Creates a tooltip with the specified content and options and open it.
      openTooltip: function(t, i, o) {
        return this._initOverlay(Xn, t, i, o).openOn(this), this;
      },
      // @method closeTooltip(tooltip: Tooltip): this
      // Closes the tooltip given as parameter.
      closeTooltip: function(t) {
        return t.close(), this;
      }
    }), xe.include({
      // @method bindTooltip(content: String|HTMLElement|Function|Tooltip, options?: Tooltip options): this
      // Binds a tooltip to the layer with the passed `content` and sets up the
      // necessary event listeners. If a `Function` is passed it will receive
      // the layer as the first argument and should return a `String` or `HTMLElement`.
      bindTooltip: function(t, i) {
        return this._tooltip && this.isTooltipOpen() && this.unbindTooltip(), this._tooltip = this._initOverlay(Xn, this._tooltip, t, i), this._initTooltipInteractions(), this._tooltip.options.permanent && this._map && this._map.hasLayer(this) && this.openTooltip(), this;
      },
      // @method unbindTooltip(): this
      // Removes the tooltip previously bound with `bindTooltip`.
      unbindTooltip: function() {
        return this._tooltip && (this._initTooltipInteractions(!0), this.closeTooltip(), this._tooltip = null), this;
      },
      _initTooltipInteractions: function(t) {
        if (!(!t && this._tooltipHandlersAdded)) {
          var i = t ? "off" : "on", o = {
            remove: this.closeTooltip,
            move: this._moveTooltip
          };
          this._tooltip.options.permanent ? o.add = this._openTooltip : (o.mouseover = this._openTooltip, o.mouseout = this.closeTooltip, o.click = this._openTooltip, this._map ? this._addFocusListeners() : o.add = this._addFocusListeners), this._tooltip.options.sticky && (o.mousemove = this._moveTooltip), this[i](o), this._tooltipHandlersAdded = !t;
        }
      },
      // @method openTooltip(latlng?: LatLng): this
      // Opens the bound tooltip at the specified `latlng` or at the default tooltip anchor if no `latlng` is passed.
      openTooltip: function(t) {
        return this._tooltip && (this instanceof He || (this._tooltip._source = this), this._tooltip._prepareOpen(t) && (this._tooltip.openOn(this._map), this.getElement ? this._setAriaDescribedByOnLayer(this) : this.eachLayer && this.eachLayer(this._setAriaDescribedByOnLayer, this))), this;
      },
      // @method closeTooltip(): this
      // Closes the tooltip bound to this layer if it is open.
      closeTooltip: function() {
        if (this._tooltip)
          return this._tooltip.close();
      },
      // @method toggleTooltip(): this
      // Opens or closes the tooltip bound to this layer depending on its current state.
      toggleTooltip: function() {
        return this._tooltip && this._tooltip.toggle(this), this;
      },
      // @method isTooltipOpen(): boolean
      // Returns `true` if the tooltip bound to this layer is currently open.
      isTooltipOpen: function() {
        return this._tooltip.isOpen();
      },
      // @method setTooltipContent(content: String|HTMLElement|Tooltip): this
      // Sets the content of the tooltip bound to this layer.
      setTooltipContent: function(t) {
        return this._tooltip && this._tooltip.setContent(t), this;
      },
      // @method getTooltip(): Tooltip
      // Returns the tooltip bound to this layer.
      getTooltip: function() {
        return this._tooltip;
      },
      _addFocusListeners: function() {
        this.getElement ? this._addFocusListenersOnLayer(this) : this.eachLayer && this.eachLayer(this._addFocusListenersOnLayer, this);
      },
      _addFocusListenersOnLayer: function(t) {
        var i = t.getElement();
        i && (et(i, "focus", function() {
          this._tooltip._source = t, this.openTooltip();
        }, this), et(i, "blur", this.closeTooltip, this));
      },
      _setAriaDescribedByOnLayer: function(t) {
        var i = t.getElement();
        i && i.setAttribute("aria-describedby", this._tooltip._container.id);
      },
      _openTooltip: function(t) {
        !this._tooltip || !this._map || this._map.dragging && this._map.dragging.moving() || (this._tooltip._source = t.layer || t.target, this.openTooltip(this._tooltip.options.sticky ? t.latlng : void 0));
      },
      _moveTooltip: function(t) {
        var i = t.latlng, o, r;
        this._tooltip.options.sticky && t.originalEvent && (o = this._map.mouseEventToContainerPoint(t.originalEvent), r = this._map.containerPointToLayerPoint(o), i = this._map.layerPointToLatLng(r)), this._tooltip.setLatLng(i);
      }
    });
    var qr = Ri.extend({
      options: {
        // @section
        // @aka DivIcon options
        iconSize: [12, 12],
        // also can be set through CSS
        // iconAnchor: (Point),
        // popupAnchor: (Point),
        // @option html: String|HTMLElement = ''
        // Custom HTML code to put inside the div element, empty by default. Alternatively,
        // an instance of `HTMLElement`.
        html: !1,
        // @option bgPos: Point = [0, 0]
        // Optional relative position of the background, in pixels
        bgPos: null,
        className: "leaflet-div-icon"
      },
      createIcon: function(t) {
        var i = t && t.tagName === "DIV" ? t : document.createElement("div"), o = this.options;
        if (o.html instanceof Element ? (Rn(i), i.appendChild(o.html)) : i.innerHTML = o.html !== !1 ? o.html : "", o.bgPos) {
          var r = K(o.bgPos);
          i.style.backgroundPosition = -r.x + "px " + -r.y + "px";
        }
        return this._setIconStyles(i, "icon"), i;
      },
      createShadow: function() {
        return null;
      }
    });
    function Eh(t) {
      return new qr(t);
    }
    Ri.Default = hn;
    var cn = xe.extend({
      // @section
      // @aka GridLayer options
      options: {
        // @option tileSize: Number|Point = 256
        // Width and height of tiles in the grid. Use a number if width and height are equal, or `L.point(width, height)` otherwise.
        tileSize: 256,
        // @option opacity: Number = 1.0
        // Opacity of the tiles. Can be used in the `createTile()` function.
        opacity: 1,
        // @option updateWhenIdle: Boolean = (depends)
        // Load new tiles only when panning ends.
        // `true` by default on mobile browsers, in order to avoid too many requests and keep smooth navigation.
        // `false` otherwise in order to display new tiles _during_ panning, since it is easy to pan outside the
        // [`keepBuffer`](#gridlayer-keepbuffer) option in desktop browsers.
        updateWhenIdle: $.mobile,
        // @option updateWhenZooming: Boolean = true
        // By default, a smooth zoom animation (during a [touch zoom](#map-touchzoom) or a [`flyTo()`](#map-flyto)) will update grid layers every integer zoom level. Setting this option to `false` will update the grid layer only when the smooth animation ends.
        updateWhenZooming: !0,
        // @option updateInterval: Number = 200
        // Tiles will not update more than once every `updateInterval` milliseconds when panning.
        updateInterval: 200,
        // @option zIndex: Number = 1
        // The explicit zIndex of the tile layer.
        zIndex: 1,
        // @option bounds: LatLngBounds = undefined
        // If set, tiles will only be loaded inside the set `LatLngBounds`.
        bounds: null,
        // @option minZoom: Number = 0
        // The minimum zoom level down to which this layer will be displayed (inclusive).
        minZoom: 0,
        // @option maxZoom: Number = undefined
        // The maximum zoom level up to which this layer will be displayed (inclusive).
        maxZoom: void 0,
        // @option maxNativeZoom: Number = undefined
        // Maximum zoom number the tile source has available. If it is specified,
        // the tiles on all zoom levels higher than `maxNativeZoom` will be loaded
        // from `maxNativeZoom` level and auto-scaled.
        maxNativeZoom: void 0,
        // @option minNativeZoom: Number = undefined
        // Minimum zoom number the tile source has available. If it is specified,
        // the tiles on all zoom levels lower than `minNativeZoom` will be loaded
        // from `minNativeZoom` level and auto-scaled.
        minNativeZoom: void 0,
        // @option noWrap: Boolean = false
        // Whether the layer is wrapped around the antimeridian. If `true`, the
        // GridLayer will only be displayed once at low zoom levels. Has no
        // effect when the [map CRS](#map-crs) doesn't wrap around. Can be used
        // in combination with [`bounds`](#gridlayer-bounds) to prevent requesting
        // tiles outside the CRS limits.
        noWrap: !1,
        // @option pane: String = 'tilePane'
        // `Map pane` where the grid layer will be added.
        pane: "tilePane",
        // @option className: String = ''
        // A custom class name to assign to the tile layer. Empty by default.
        className: "",
        // @option keepBuffer: Number = 2
        // When panning the map, keep this many rows and columns of tiles before unloading them.
        keepBuffer: 2
      },
      initialize: function(t) {
        E(this, t);
      },
      onAdd: function() {
        this._initContainer(), this._levels = {}, this._tiles = {}, this._resetView();
      },
      beforeAdd: function(t) {
        t._addZoomLimit(this);
      },
      onRemove: function(t) {
        this._removeAllTiles(), kt(this._container), t._removeZoomLimit(this), this._container = null, this._tileZoom = void 0;
      },
      // @method bringToFront: this
      // Brings the tile layer to the top of all tile layers.
      bringToFront: function() {
        return this._map && (zi(this._container), this._setAutoZIndex(Math.max)), this;
      },
      // @method bringToBack: this
      // Brings the tile layer to the bottom of all tile layers.
      bringToBack: function() {
        return this._map && (Ni(this._container), this._setAutoZIndex(Math.min)), this;
      },
      // @method getContainer: HTMLElement
      // Returns the HTML element that contains the tiles for this layer.
      getContainer: function() {
        return this._container;
      },
      // @method setOpacity(opacity: Number): this
      // Changes the [opacity](#gridlayer-opacity) of the grid layer.
      setOpacity: function(t) {
        return this.options.opacity = t, this._updateOpacity(), this;
      },
      // @method setZIndex(zIndex: Number): this
      // Changes the [zIndex](#gridlayer-zindex) of the grid layer.
      setZIndex: function(t) {
        return this.options.zIndex = t, this._updateZIndex(), this;
      },
      // @method isLoading: Boolean
      // Returns `true` if any tile in the grid layer has not finished loading.
      isLoading: function() {
        return this._loading;
      },
      // @method redraw: this
      // Causes the layer to clear all the tiles and request them again.
      redraw: function() {
        if (this._map) {
          this._removeAllTiles();
          var t = this._clampZoom(this._map.getZoom());
          t !== this._tileZoom && (this._tileZoom = t, this._updateLevels()), this._update();
        }
        return this;
      },
      getEvents: function() {
        var t = {
          viewprereset: this._invalidateAll,
          viewreset: this._resetView,
          zoom: this._resetView,
          moveend: this._onMoveEnd
        };
        return this.options.updateWhenIdle || (this._onMove || (this._onMove = g(this._onMoveEnd, this.options.updateInterval, this)), t.move = this._onMove), this._zoomAnimated && (t.zoomanim = this._animateZoom), t;
      },
      // @section Extension methods
      // Layers extending `GridLayer` shall reimplement the following method.
      // @method createTile(coords: Object, done?: Function): HTMLElement
      // Called only internally, must be overridden by classes extending `GridLayer`.
      // Returns the `HTMLElement` corresponding to the given `coords`. If the `done` callback
      // is specified, it must be called when the tile has finished loading and drawing.
      createTile: function() {
        return document.createElement("div");
      },
      // @section
      // @method getTileSize: Point
      // Normalizes the [tileSize option](#gridlayer-tilesize) into a point. Used by the `createTile()` method.
      getTileSize: function() {
        var t = this.options.tileSize;
        return t instanceof Z ? t : new Z(t, t);
      },
      _updateZIndex: function() {
        this._container && this.options.zIndex !== void 0 && this.options.zIndex !== null && (this._container.style.zIndex = this.options.zIndex);
      },
      _setAutoZIndex: function(t) {
        for (var i = this.getPane().children, o = -t(-1 / 0, 1 / 0), r = 0, h = i.length, d; r < h; r++)
          d = i[r].style.zIndex, i[r] !== this._container && d && (o = t(o, +d));
        isFinite(o) && (this.options.zIndex = o + t(-1, 1), this._updateZIndex());
      },
      _updateOpacity: function() {
        if (this._map && !$.ielt9) {
          de(this._container, this.options.opacity);
          var t = +/* @__PURE__ */ new Date(), i = !1, o = !1;
          for (var r in this._tiles) {
            var h = this._tiles[r];
            if (!(!h.current || !h.loaded)) {
              var d = Math.min(1, (t - h.loaded) / 200);
              de(h.el, d), d < 1 ? i = !0 : (h.active ? o = !0 : this._onOpaqueTile(h), h.active = !0);
            }
          }
          o && !this._noPrune && this._pruneTiles(), i && (R(this._fadeFrame), this._fadeFrame = ut(this._updateOpacity, this));
        }
      },
      _onOpaqueTile: x,
      _initContainer: function() {
        this._container || (this._container = dt("div", "leaflet-layer " + (this.options.className || "")), this._updateZIndex(), this.options.opacity < 1 && this._updateOpacity(), this.getPane().appendChild(this._container));
      },
      _updateLevels: function() {
        var t = this._tileZoom, i = this.options.maxZoom;
        if (t !== void 0) {
          for (var o in this._levels)
            o = Number(o), this._levels[o].el.children.length || o === t ? (this._levels[o].el.style.zIndex = i - Math.abs(t - o), this._onUpdateLevel(o)) : (kt(this._levels[o].el), this._removeTilesAtZoom(o), this._onRemoveLevel(o), delete this._levels[o]);
          var r = this._levels[t], h = this._map;
          return r || (r = this._levels[t] = {}, r.el = dt("div", "leaflet-tile-container leaflet-zoom-animated", this._container), r.el.style.zIndex = i, r.origin = h.project(h.unproject(h.getPixelOrigin()), t).round(), r.zoom = t, this._setZoomTransform(r, h.getCenter(), h.getZoom()), x(r.el.offsetWidth), this._onCreateLevel(r)), this._level = r, r;
        }
      },
      _onUpdateLevel: x,
      _onRemoveLevel: x,
      _onCreateLevel: x,
      _pruneTiles: function() {
        if (this._map) {
          var t, i, o = this._map.getZoom();
          if (o > this.options.maxZoom || o < this.options.minZoom) {
            this._removeAllTiles();
            return;
          }
          for (t in this._tiles)
            i = this._tiles[t], i.retain = i.current;
          for (t in this._tiles)
            if (i = this._tiles[t], i.current && !i.active) {
              var r = i.coords;
              this._retainParent(r.x, r.y, r.z, r.z - 5) || this._retainChildren(r.x, r.y, r.z, r.z + 2);
            }
          for (t in this._tiles)
            this._tiles[t].retain || this._removeTile(t);
        }
      },
      _removeTilesAtZoom: function(t) {
        for (var i in this._tiles)
          this._tiles[i].coords.z === t && this._removeTile(i);
      },
      _removeAllTiles: function() {
        for (var t in this._tiles)
          this._removeTile(t);
      },
      _invalidateAll: function() {
        for (var t in this._levels)
          kt(this._levels[t].el), this._onRemoveLevel(Number(t)), delete this._levels[t];
        this._removeAllTiles(), this._tileZoom = void 0;
      },
      _retainParent: function(t, i, o, r) {
        var h = Math.floor(t / 2), d = Math.floor(i / 2), m = o - 1, y = new Z(+h, +d);
        y.z = +m;
        var w = this._tileCoordsToKey(y), O = this._tiles[w];
        return O && O.active ? (O.retain = !0, !0) : (O && O.loaded && (O.retain = !0), m > r ? this._retainParent(h, d, m, r) : !1);
      },
      _retainChildren: function(t, i, o, r) {
        for (var h = 2 * t; h < 2 * t + 2; h++)
          for (var d = 2 * i; d < 2 * i + 2; d++) {
            var m = new Z(h, d);
            m.z = o + 1;
            var y = this._tileCoordsToKey(m), w = this._tiles[y];
            if (w && w.active) {
              w.retain = !0;
              continue;
            } else
              w && w.loaded && (w.retain = !0);
            o + 1 < r && this._retainChildren(h, d, o + 1, r);
          }
      },
      _resetView: function(t) {
        var i = t && (t.pinch || t.flyTo);
        this._setView(this._map.getCenter(), this._map.getZoom(), i, i);
      },
      _animateZoom: function(t) {
        this._setView(t.center, t.zoom, !0, t.noUpdate);
      },
      _clampZoom: function(t) {
        var i = this.options;
        return i.minNativeZoom !== void 0 && t < i.minNativeZoom ? i.minNativeZoom : i.maxNativeZoom !== void 0 && i.maxNativeZoom < t ? i.maxNativeZoom : t;
      },
      _setView: function(t, i, o, r) {
        var h = Math.round(i);
        this.options.maxZoom !== void 0 && h > this.options.maxZoom || this.options.minZoom !== void 0 && h < this.options.minZoom ? h = void 0 : h = this._clampZoom(h);
        var d = this.options.updateWhenZooming && h !== this._tileZoom;
        (!r || d) && (this._tileZoom = h, this._abortLoading && this._abortLoading(), this._updateLevels(), this._resetGrid(), h !== void 0 && this._update(t), o || this._pruneTiles(), this._noPrune = !!o), this._setZoomTransforms(t, i);
      },
      _setZoomTransforms: function(t, i) {
        for (var o in this._levels)
          this._setZoomTransform(this._levels[o], t, i);
      },
      _setZoomTransform: function(t, i, o) {
        var r = this._map.getZoomScale(o, t.zoom), h = t.origin.multiplyBy(r).subtract(this._map._getNewPixelOrigin(i, o)).round();
        $.any3d ? pi(t.el, h, r) : Wt(t.el, h);
      },
      _resetGrid: function() {
        var t = this._map, i = t.options.crs, o = this._tileSize = this.getTileSize(), r = this._tileZoom, h = this._map.getPixelWorldBounds(this._tileZoom);
        h && (this._globalTileRange = this._pxBoundsToTileRange(h)), this._wrapX = i.wrapLng && !this.options.noWrap && [
          Math.floor(t.project([0, i.wrapLng[0]], r).x / o.x),
          Math.ceil(t.project([0, i.wrapLng[1]], r).x / o.y)
        ], this._wrapY = i.wrapLat && !this.options.noWrap && [
          Math.floor(t.project([i.wrapLat[0], 0], r).y / o.x),
          Math.ceil(t.project([i.wrapLat[1], 0], r).y / o.y)
        ];
      },
      _onMoveEnd: function() {
        !this._map || this._map._animatingZoom || this._update();
      },
      _getTiledPixelBounds: function(t) {
        var i = this._map, o = i._animatingZoom ? Math.max(i._animateToZoom, i.getZoom()) : i.getZoom(), r = i.getZoomScale(o, this._tileZoom), h = i.project(t, this._tileZoom).floor(), d = i.getSize().divideBy(r * 2);
        return new mt(h.subtract(d), h.add(d));
      },
      // Private method to load tiles in the grid's active zoom level according to map bounds
      _update: function(t) {
        var i = this._map;
        if (i) {
          var o = this._clampZoom(i.getZoom());
          if (t === void 0 && (t = i.getCenter()), this._tileZoom !== void 0) {
            var r = this._getTiledPixelBounds(t), h = this._pxBoundsToTileRange(r), d = h.getCenter(), m = [], y = this.options.keepBuffer, w = new mt(
              h.getBottomLeft().subtract([y, -y]),
              h.getTopRight().add([y, -y])
            );
            if (!(isFinite(h.min.x) && isFinite(h.min.y) && isFinite(h.max.x) && isFinite(h.max.y)))
              throw new Error("Attempted to load an infinite number of tiles");
            for (var O in this._tiles) {
              var N = this._tiles[O].coords;
              (N.z !== this._tileZoom || !w.contains(new Z(N.x, N.y))) && (this._tiles[O].current = !1);
            }
            if (Math.abs(o - this._tileZoom) > 1) {
              this._setView(t, o);
              return;
            }
            for (var G = h.min.y; G <= h.max.y; G++)
              for (var lt = h.min.x; lt <= h.max.x; lt++) {
                var me = new Z(lt, G);
                if (me.z = this._tileZoom, !!this._isValidTile(me)) {
                  var yi = this._tiles[this._tileCoordsToKey(me)];
                  yi ? yi.current = !0 : m.push(me);
                }
              }
            if (m.sort(function(Qe, fs) {
              return Qe.distanceTo(d) - fs.distanceTo(d);
            }), m.length !== 0) {
              this._loading || (this._loading = !0, this.fire("loading"));
              var to = document.createDocumentFragment();
              for (lt = 0; lt < m.length; lt++)
                this._addTile(m[lt], to);
              this._level.el.appendChild(to);
            }
          }
        }
      },
      _isValidTile: function(t) {
        var i = this._map.options.crs;
        if (!i.infinite) {
          var o = this._globalTileRange;
          if (!i.wrapLng && (t.x < o.min.x || t.x > o.max.x) || !i.wrapLat && (t.y < o.min.y || t.y > o.max.y))
            return !1;
        }
        if (!this.options.bounds)
          return !0;
        var r = this._tileCoordsToBounds(t);
        return $t(this.options.bounds).overlaps(r);
      },
      _keyToBounds: function(t) {
        return this._tileCoordsToBounds(this._keyToTileCoords(t));
      },
      _tileCoordsToNwSe: function(t) {
        var i = this._map, o = this.getTileSize(), r = t.scaleBy(o), h = r.add(o), d = i.unproject(r, t.z), m = i.unproject(h, t.z);
        return [d, m];
      },
      // converts tile coordinates to its geographical bounds
      _tileCoordsToBounds: function(t) {
        var i = this._tileCoordsToNwSe(t), o = new Gt(i[0], i[1]);
        return this.options.noWrap || (o = this._map.wrapLatLngBounds(o)), o;
      },
      // converts tile coordinates to key for the tile cache
      _tileCoordsToKey: function(t) {
        return t.x + ":" + t.y + ":" + t.z;
      },
      // converts tile cache key to coordinates
      _keyToTileCoords: function(t) {
        var i = t.split(":"), o = new Z(+i[0], +i[1]);
        return o.z = +i[2], o;
      },
      _removeTile: function(t) {
        var i = this._tiles[t];
        i && (kt(i.el), delete this._tiles[t], this.fire("tileunload", {
          tile: i.el,
          coords: this._keyToTileCoords(t)
        }));
      },
      _initTile: function(t) {
        ot(t, "leaflet-tile");
        var i = this.getTileSize();
        t.style.width = i.x + "px", t.style.height = i.y + "px", t.onselectstart = x, t.onmousemove = x, $.ielt9 && this.options.opacity < 1 && de(t, this.options.opacity);
      },
      _addTile: function(t, i) {
        var o = this._getTilePos(t), r = this._tileCoordsToKey(t), h = this.createTile(this._wrapCoords(t), c(this._tileReady, this, t));
        this._initTile(h), this.createTile.length < 2 && ut(c(this._tileReady, this, t, null, h)), Wt(h, o), this._tiles[r] = {
          el: h,
          coords: t,
          current: !0
        }, i.appendChild(h), this.fire("tileloadstart", {
          tile: h,
          coords: t
        });
      },
      _tileReady: function(t, i, o) {
        i && this.fire("tileerror", {
          error: i,
          tile: o,
          coords: t
        });
        var r = this._tileCoordsToKey(t);
        o = this._tiles[r], o && (o.loaded = +/* @__PURE__ */ new Date(), this._map._fadeAnimated ? (de(o.el, 0), R(this._fadeFrame), this._fadeFrame = ut(this._updateOpacity, this)) : (o.active = !0, this._pruneTiles()), i || (ot(o.el, "leaflet-tile-loaded"), this.fire("tileload", {
          tile: o.el,
          coords: t
        })), this._noTilesToLoad() && (this._loading = !1, this.fire("load"), $.ielt9 || !this._map._fadeAnimated ? ut(this._pruneTiles, this) : setTimeout(c(this._pruneTiles, this), 250)));
      },
      _getTilePos: function(t) {
        return t.scaleBy(this.getTileSize()).subtract(this._level.origin);
      },
      _wrapCoords: function(t) {
        var i = new Z(
          this._wrapX ? b(t.x, this._wrapX) : t.x,
          this._wrapY ? b(t.y, this._wrapY) : t.y
        );
        return i.z = t.z, i;
      },
      _pxBoundsToTileRange: function(t) {
        var i = this.getTileSize();
        return new mt(
          t.min.unscaleBy(i).floor(),
          t.max.unscaleBy(i).ceil().subtract([1, 1])
        );
      },
      _noTilesToLoad: function() {
        for (var t in this._tiles)
          if (!this._tiles[t].loaded)
            return !1;
        return !0;
      }
    });
    function Ah(t) {
      return new cn(t);
    }
    var ji = cn.extend({
      // @section
      // @aka TileLayer options
      options: {
        // @option minZoom: Number = 0
        // The minimum zoom level down to which this layer will be displayed (inclusive).
        minZoom: 0,
        // @option maxZoom: Number = 18
        // The maximum zoom level up to which this layer will be displayed (inclusive).
        maxZoom: 18,
        // @option subdomains: String|String[] = 'abc'
        // Subdomains of the tile service. Can be passed in the form of one string (where each letter is a subdomain name) or an array of strings.
        subdomains: "abc",
        // @option errorTileUrl: String = ''
        // URL to the tile image to show in place of the tile that failed to load.
        errorTileUrl: "",
        // @option zoomOffset: Number = 0
        // The zoom number used in tile URLs will be offset with this value.
        zoomOffset: 0,
        // @option tms: Boolean = false
        // If `true`, inverses Y axis numbering for tiles (turn this on for [TMS](https://en.wikipedia.org/wiki/Tile_Map_Service) services).
        tms: !1,
        // @option zoomReverse: Boolean = false
        // If set to true, the zoom number used in tile URLs will be reversed (`maxZoom - zoom` instead of `zoom`)
        zoomReverse: !1,
        // @option detectRetina: Boolean = false
        // If `true` and user is on a retina display, it will request four tiles of half the specified size and a bigger zoom level in place of one to utilize the high resolution.
        detectRetina: !1,
        // @option crossOrigin: Boolean|String = false
        // Whether the crossOrigin attribute will be added to the tiles.
        // If a String is provided, all tiles will have their crossOrigin attribute set to the String provided. This is needed if you want to access tile pixel data.
        // Refer to [CORS Settings](https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_settings_attributes) for valid String values.
        crossOrigin: !1,
        // @option referrerPolicy: Boolean|String = false
        // Whether the referrerPolicy attribute will be added to the tiles.
        // If a String is provided, all tiles will have their referrerPolicy attribute set to the String provided.
        // This may be needed if your map's rendering context has a strict default but your tile provider expects a valid referrer
        // (e.g. to validate an API token).
        // Refer to [HTMLImageElement.referrerPolicy](https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/referrerPolicy) for valid String values.
        referrerPolicy: !1
      },
      initialize: function(t, i) {
        this._url = t, i = E(this, i), i.detectRetina && $.retina && i.maxZoom > 0 ? (i.tileSize = Math.floor(i.tileSize / 2), i.zoomReverse ? (i.zoomOffset--, i.minZoom = Math.min(i.maxZoom, i.minZoom + 1)) : (i.zoomOffset++, i.maxZoom = Math.max(i.minZoom, i.maxZoom - 1)), i.minZoom = Math.max(0, i.minZoom)) : i.zoomReverse ? i.minZoom = Math.min(i.maxZoom, i.minZoom) : i.maxZoom = Math.max(i.minZoom, i.maxZoom), typeof i.subdomains == "string" && (i.subdomains = i.subdomains.split("")), this.on("tileunload", this._onTileRemove);
      },
      // @method setUrl(url: String, noRedraw?: Boolean): this
      // Updates the layer's URL template and redraws it (unless `noRedraw` is set to `true`).
      // If the URL does not change, the layer will not be redrawn unless
      // the noRedraw parameter is set to false.
      setUrl: function(t, i) {
        return this._url === t && i === void 0 && (i = !0), this._url = t, i || this.redraw(), this;
      },
      // @method createTile(coords: Object, done?: Function): HTMLElement
      // Called only internally, overrides GridLayer's [`createTile()`](#gridlayer-createtile)
      // to return an `<img>` HTML element with the appropriate image URL given `coords`. The `done`
      // callback is called when the tile has been loaded.
      createTile: function(t, i) {
        var o = document.createElement("img");
        return et(o, "load", c(this._tileOnLoad, this, i, o)), et(o, "error", c(this._tileOnError, this, i, o)), (this.options.crossOrigin || this.options.crossOrigin === "") && (o.crossOrigin = this.options.crossOrigin === !0 ? "" : this.options.crossOrigin), typeof this.options.referrerPolicy == "string" && (o.referrerPolicy = this.options.referrerPolicy), o.alt = "", o.src = this.getTileUrl(t), o;
      },
      // @section Extension methods
      // @uninheritable
      // Layers extending `TileLayer` might reimplement the following method.
      // @method getTileUrl(coords: Object): String
      // Called only internally, returns the URL for a tile given its coordinates.
      // Classes extending `TileLayer` can override this function to provide custom tile URL naming schemes.
      getTileUrl: function(t) {
        var i = {
          r: $.retina ? "@2x" : "",
          s: this._getSubdomain(t),
          x: t.x,
          y: t.y,
          z: this._getZoomForUrl()
        };
        if (this._map && !this._map.options.crs.infinite) {
          var o = this._globalTileRange.max.y - t.y;
          this.options.tms && (i.y = o), i["-y"] = o;
        }
        return xt(this._url, l(i, this.options));
      },
      _tileOnLoad: function(t, i) {
        $.ielt9 ? setTimeout(c(t, this, null, i), 0) : t(null, i);
      },
      _tileOnError: function(t, i, o) {
        var r = this.options.errorTileUrl;
        r && i.getAttribute("src") !== r && (i.src = r), t(o, i);
      },
      _onTileRemove: function(t) {
        t.tile.onload = null;
      },
      _getZoomForUrl: function() {
        var t = this._tileZoom, i = this.options.maxZoom, o = this.options.zoomReverse, r = this.options.zoomOffset;
        return o && (t = i - t), t + r;
      },
      _getSubdomain: function(t) {
        var i = Math.abs(t.x + t.y) % this.options.subdomains.length;
        return this.options.subdomains[i];
      },
      // stops loading all tiles in the background layer
      _abortLoading: function() {
        var t, i;
        for (t in this._tiles)
          if (this._tiles[t].coords.z !== this._tileZoom && (i = this._tiles[t].el, i.onload = x, i.onerror = x, !i.complete)) {
            i.src = gt;
            var o = this._tiles[t].coords;
            kt(i), delete this._tiles[t], this.fire("tileabort", {
              tile: i,
              coords: o
            });
          }
      },
      _removeTile: function(t) {
        var i = this._tiles[t];
        if (i)
          return i.el.setAttribute("src", gt), cn.prototype._removeTile.call(this, t);
      },
      _tileReady: function(t, i, o) {
        if (!(!this._map || o && o.getAttribute("src") === gt))
          return cn.prototype._tileReady.call(this, t, i, o);
      }
    });
    function Gr(t, i) {
      return new ji(t, i);
    }
    var Yr = ji.extend({
      // @section
      // @aka TileLayer.WMS options
      // If any custom options not documented here are used, they will be sent to the
      // WMS server as extra parameters in each request URL. This can be useful for
      // [non-standard vendor WMS parameters](https://docs.geoserver.org/stable/en/user/services/wms/vendor.html).
      defaultWmsParams: {
        service: "WMS",
        request: "GetMap",
        // @option layers: String = ''
        // **(required)** Comma-separated list of WMS layers to show.
        layers: "",
        // @option styles: String = ''
        // Comma-separated list of WMS styles.
        styles: "",
        // @option format: String = 'image/jpeg'
        // WMS image format (use `'image/png'` for layers with transparency).
        format: "image/jpeg",
        // @option transparent: Boolean = false
        // If `true`, the WMS service will return images with transparency.
        transparent: !1,
        // @option version: String = '1.1.1'
        // Version of the WMS service to use
        version: "1.1.1"
      },
      options: {
        // @option crs: CRS = null
        // Coordinate Reference System to use for the WMS requests, defaults to
        // map CRS. Don't change this if you're not sure what it means.
        crs: null,
        // @option uppercase: Boolean = false
        // If `true`, WMS request parameter keys will be uppercase.
        uppercase: !1
      },
      initialize: function(t, i) {
        this._url = t;
        var o = l({}, this.defaultWmsParams);
        for (var r in i)
          r in this.options || (o[r] = i[r]);
        i = E(this, i);
        var h = i.detectRetina && $.retina ? 2 : 1, d = this.getTileSize();
        o.width = d.x * h, o.height = d.y * h, this.wmsParams = o;
      },
      onAdd: function(t) {
        this._crs = this.options.crs || t.options.crs, this._wmsVersion = parseFloat(this.wmsParams.version);
        var i = this._wmsVersion >= 1.3 ? "crs" : "srs";
        this.wmsParams[i] = this._crs.code, ji.prototype.onAdd.call(this, t);
      },
      getTileUrl: function(t) {
        var i = this._tileCoordsToNwSe(t), o = this._crs, r = Ct(o.project(i[0]), o.project(i[1])), h = r.min, d = r.max, m = (this._wmsVersion >= 1.3 && this._crs === Hr ? [h.y, h.x, d.y, d.x] : [h.x, h.y, d.x, d.y]).join(","), y = ji.prototype.getTileUrl.call(this, t);
        return y + tt(this.wmsParams, y, this.options.uppercase) + (this.options.uppercase ? "&BBOX=" : "&bbox=") + m;
      },
      // @method setParams(params: Object, noRedraw?: Boolean): this
      // Merges an object with the new parameters and re-requests tiles on the current screen (unless `noRedraw` was set to true).
      setParams: function(t, i) {
        return l(this.wmsParams, t), i || this.redraw(), this;
      }
    });
    function Ih(t, i) {
      return new Yr(t, i);
    }
    ji.WMS = Yr, Gr.wms = Ih;
    var Ue = xe.extend({
      // @section
      // @aka Renderer options
      options: {
        // @option padding: Number = 0.1
        // How much to extend the clip area around the map view (relative to its size)
        // e.g. 0.1 would be 10% of map view in each direction
        padding: 0.1
      },
      initialize: function(t) {
        E(this, t), p(this), this._layers = this._layers || {};
      },
      onAdd: function() {
        this._container || (this._initContainer(), this._zoomAnimated && ot(this._container, "leaflet-zoom-animated")), this.getPane().appendChild(this._container), this._update(), this.on("update", this._updatePaths, this);
      },
      onRemove: function() {
        this.off("update", this._updatePaths, this), this._destroyContainer();
      },
      getEvents: function() {
        var t = {
          viewreset: this._reset,
          zoom: this._onZoom,
          moveend: this._update,
          zoomend: this._onZoomEnd
        };
        return this._zoomAnimated && (t.zoomanim = this._onAnimZoom), t;
      },
      _onAnimZoom: function(t) {
        this._updateTransform(t.center, t.zoom);
      },
      _onZoom: function() {
        this._updateTransform(this._map.getCenter(), this._map.getZoom());
      },
      _updateTransform: function(t, i) {
        var o = this._map.getZoomScale(i, this._zoom), r = this._map.getSize().multiplyBy(0.5 + this.options.padding), h = this._map.project(this._center, i), d = r.multiplyBy(-o).add(h).subtract(this._map._getNewPixelOrigin(t, i));
        $.any3d ? pi(this._container, d, o) : Wt(this._container, d);
      },
      _reset: function() {
        this._update(), this._updateTransform(this._center, this._zoom);
        for (var t in this._layers)
          this._layers[t]._reset();
      },
      _onZoomEnd: function() {
        for (var t in this._layers)
          this._layers[t]._project();
      },
      _updatePaths: function() {
        for (var t in this._layers)
          this._layers[t]._update();
      },
      _update: function() {
        var t = this.options.padding, i = this._map.getSize(), o = this._map.containerPointToLayerPoint(i.multiplyBy(-t)).round();
        this._bounds = new mt(o, o.add(i.multiplyBy(1 + t * 2)).round()), this._center = this._map.getCenter(), this._zoom = this._map.getZoom();
      }
    }), Jr = Ue.extend({
      // @section
      // @aka Canvas options
      options: {
        // @option tolerance: Number = 0
        // How much to extend the click tolerance around a path/object on the map.
        tolerance: 0
      },
      getEvents: function() {
        var t = Ue.prototype.getEvents.call(this);
        return t.viewprereset = this._onViewPreReset, t;
      },
      _onViewPreReset: function() {
        this._postponeUpdatePaths = !0;
      },
      onAdd: function() {
        Ue.prototype.onAdd.call(this), this._draw();
      },
      _initContainer: function() {
        var t = this._container = document.createElement("canvas");
        et(t, "mousemove", this._onMouseMove, this), et(t, "click dblclick mousedown mouseup contextmenu", this._onClick, this), et(t, "mouseout", this._handleMouseOut, this), t._leaflet_disable_events = !0, this._ctx = t.getContext("2d");
      },
      _destroyContainer: function() {
        R(this._redrawRequest), delete this._ctx, kt(this._container), Lt(this._container), delete this._container;
      },
      _updatePaths: function() {
        if (!this._postponeUpdatePaths) {
          var t;
          this._redrawBounds = null;
          for (var i in this._layers)
            t = this._layers[i], t._update();
          this._redraw();
        }
      },
      _update: function() {
        if (!(this._map._animatingZoom && this._bounds)) {
          Ue.prototype._update.call(this);
          var t = this._bounds, i = this._container, o = t.getSize(), r = $.retina ? 2 : 1;
          Wt(i, t.min), i.width = r * o.x, i.height = r * o.y, i.style.width = o.x + "px", i.style.height = o.y + "px", $.retina && this._ctx.scale(2, 2), this._ctx.translate(-t.min.x, -t.min.y), this.fire("update");
        }
      },
      _reset: function() {
        Ue.prototype._reset.call(this), this._postponeUpdatePaths && (this._postponeUpdatePaths = !1, this._updatePaths());
      },
      _initPath: function(t) {
        this._updateDashArray(t), this._layers[p(t)] = t;
        var i = t._order = {
          layer: t,
          prev: this._drawLast,
          next: null
        };
        this._drawLast && (this._drawLast.next = i), this._drawLast = i, this._drawFirst = this._drawFirst || this._drawLast;
      },
      _addPath: function(t) {
        this._requestRedraw(t);
      },
      _removePath: function(t) {
        var i = t._order, o = i.next, r = i.prev;
        o ? o.prev = r : this._drawLast = r, r ? r.next = o : this._drawFirst = o, delete t._order, delete this._layers[p(t)], this._requestRedraw(t);
      },
      _updatePath: function(t) {
        this._extendRedrawBounds(t), t._project(), t._update(), this._requestRedraw(t);
      },
      _updateStyle: function(t) {
        this._updateDashArray(t), this._requestRedraw(t);
      },
      _updateDashArray: function(t) {
        if (typeof t.options.dashArray == "string") {
          var i = t.options.dashArray.split(/[, ]+/), o = [], r, h;
          for (h = 0; h < i.length; h++) {
            if (r = Number(i[h]), isNaN(r))
              return;
            o.push(r);
          }
          t.options._dashArray = o;
        } else
          t.options._dashArray = t.options.dashArray;
      },
      _requestRedraw: function(t) {
        this._map && (this._extendRedrawBounds(t), this._redrawRequest = this._redrawRequest || ut(this._redraw, this));
      },
      _extendRedrawBounds: function(t) {
        if (t._pxBounds) {
          var i = (t.options.weight || 0) + 1;
          this._redrawBounds = this._redrawBounds || new mt(), this._redrawBounds.extend(t._pxBounds.min.subtract([i, i])), this._redrawBounds.extend(t._pxBounds.max.add([i, i]));
        }
      },
      _redraw: function() {
        this._redrawRequest = null, this._redrawBounds && (this._redrawBounds.min._floor(), this._redrawBounds.max._ceil()), this._clear(), this._draw(), this._redrawBounds = null;
      },
      _clear: function() {
        var t = this._redrawBounds;
        if (t) {
          var i = t.getSize();
          this._ctx.clearRect(t.min.x, t.min.y, i.x, i.y);
        } else
          this._ctx.save(), this._ctx.setTransform(1, 0, 0, 1, 0, 0), this._ctx.clearRect(0, 0, this._container.width, this._container.height), this._ctx.restore();
      },
      _draw: function() {
        var t, i = this._redrawBounds;
        if (this._ctx.save(), i) {
          var o = i.getSize();
          this._ctx.beginPath(), this._ctx.rect(i.min.x, i.min.y, o.x, o.y), this._ctx.clip();
        }
        this._drawing = !0;
        for (var r = this._drawFirst; r; r = r.next)
          t = r.layer, (!i || t._pxBounds && t._pxBounds.intersects(i)) && t._updatePath();
        this._drawing = !1, this._ctx.restore();
      },
      _updatePoly: function(t, i) {
        if (this._drawing) {
          var o, r, h, d, m = t._parts, y = m.length, w = this._ctx;
          if (y) {
            for (w.beginPath(), o = 0; o < y; o++) {
              for (r = 0, h = m[o].length; r < h; r++)
                d = m[o][r], w[r ? "lineTo" : "moveTo"](d.x, d.y);
              i && w.closePath();
            }
            this._fillStroke(w, t);
          }
        }
      },
      _updateCircle: function(t) {
        if (!(!this._drawing || t._empty())) {
          var i = t._point, o = this._ctx, r = Math.max(Math.round(t._radius), 1), h = (Math.max(Math.round(t._radiusY), 1) || r) / r;
          h !== 1 && (o.save(), o.scale(1, h)), o.beginPath(), o.arc(i.x, i.y / h, r, 0, Math.PI * 2, !1), h !== 1 && o.restore(), this._fillStroke(o, t);
        }
      },
      _fillStroke: function(t, i) {
        var o = i.options;
        o.fill && (t.globalAlpha = o.fillOpacity, t.fillStyle = o.fillColor || o.color, t.fill(o.fillRule || "evenodd")), o.stroke && o.weight !== 0 && (t.setLineDash && t.setLineDash(i.options && i.options._dashArray || []), t.globalAlpha = o.opacity, t.lineWidth = o.weight, t.strokeStyle = o.color, t.lineCap = o.lineCap, t.lineJoin = o.lineJoin, t.stroke());
      },
      // Canvas obviously doesn't have mouse events for individual drawn objects,
      // so we emulate that by calculating what's under the mouse on mousemove/click manually
      _onClick: function(t) {
        for (var i = this._map.mouseEventToLayerPoint(t), o, r, h = this._drawFirst; h; h = h.next)
          o = h.layer, o.options.interactive && o._containsPoint(i) && (!(t.type === "click" || t.type === "preclick") || !this._map._draggableMoved(o)) && (r = o);
        this._fireEvent(r ? [r] : !1, t);
      },
      _onMouseMove: function(t) {
        if (!(!this._map || this._map.dragging.moving() || this._map._animatingZoom)) {
          var i = this._map.mouseEventToLayerPoint(t);
          this._handleMouseHover(t, i);
        }
      },
      _handleMouseOut: function(t) {
        var i = this._hoveredLayer;
        i && (jt(this._container, "leaflet-interactive"), this._fireEvent([i], t, "mouseout"), this._hoveredLayer = null, this._mouseHoverThrottled = !1);
      },
      _handleMouseHover: function(t, i) {
        if (!this._mouseHoverThrottled) {
          for (var o, r, h = this._drawFirst; h; h = h.next)
            o = h.layer, o.options.interactive && o._containsPoint(i) && (r = o);
          r !== this._hoveredLayer && (this._handleMouseOut(t), r && (ot(this._container, "leaflet-interactive"), this._fireEvent([r], t, "mouseover"), this._hoveredLayer = r)), this._fireEvent(this._hoveredLayer ? [this._hoveredLayer] : !1, t), this._mouseHoverThrottled = !0, setTimeout(c(function() {
            this._mouseHoverThrottled = !1;
          }, this), 32);
        }
      },
      _fireEvent: function(t, i, o) {
        this._map._fireDOMEvent(i, o || i.type, t);
      },
      _bringToFront: function(t) {
        var i = t._order;
        if (i) {
          var o = i.next, r = i.prev;
          if (o)
            o.prev = r;
          else
            return;
          r ? r.next = o : o && (this._drawFirst = o), i.prev = this._drawLast, this._drawLast.next = i, i.next = null, this._drawLast = i, this._requestRedraw(t);
        }
      },
      _bringToBack: function(t) {
        var i = t._order;
        if (i) {
          var o = i.next, r = i.prev;
          if (r)
            r.next = o;
          else
            return;
          o ? o.prev = r : r && (this._drawLast = r), i.prev = null, i.next = this._drawFirst, this._drawFirst.prev = i, this._drawFirst = i, this._requestRedraw(t);
        }
      }
    });
    function Xr(t) {
      return $.canvas ? new Jr(t) : null;
    }
    var fn = function() {
      try {
        return document.namespaces.add("lvml", "urn:schemas-microsoft-com:vml"), function(t) {
          return document.createElement("<lvml:" + t + ' class="lvml">');
        };
      } catch {
      }
      return function(t) {
        return document.createElement("<" + t + ' xmlns="urn:schemas-microsoft.com:vml" class="lvml">');
      };
    }(), kh = {
      _initContainer: function() {
        this._container = dt("div", "leaflet-vml-container");
      },
      _update: function() {
        this._map._animatingZoom || (Ue.prototype._update.call(this), this.fire("update"));
      },
      _initPath: function(t) {
        var i = t._container = fn("shape");
        ot(i, "leaflet-vml-shape " + (this.options.className || "")), i.coordsize = "1 1", t._path = fn("path"), i.appendChild(t._path), this._updateStyle(t), this._layers[p(t)] = t;
      },
      _addPath: function(t) {
        var i = t._container;
        this._container.appendChild(i), t.options.interactive && t.addInteractiveTarget(i);
      },
      _removePath: function(t) {
        var i = t._container;
        kt(i), t.removeInteractiveTarget(i), delete this._layers[p(t)];
      },
      _updateStyle: function(t) {
        var i = t._stroke, o = t._fill, r = t.options, h = t._container;
        h.stroked = !!r.stroke, h.filled = !!r.fill, r.stroke ? (i || (i = t._stroke = fn("stroke")), h.appendChild(i), i.weight = r.weight + "px", i.color = r.color, i.opacity = r.opacity, r.dashArray ? i.dashStyle = D(r.dashArray) ? r.dashArray.join(" ") : r.dashArray.replace(/( *, *)/g, " ") : i.dashStyle = "", i.endcap = r.lineCap.replace("butt", "flat"), i.joinstyle = r.lineJoin) : i && (h.removeChild(i), t._stroke = null), r.fill ? (o || (o = t._fill = fn("fill")), h.appendChild(o), o.color = r.fillColor || r.color, o.opacity = r.fillOpacity) : o && (h.removeChild(o), t._fill = null);
      },
      _updateCircle: function(t) {
        var i = t._point.round(), o = Math.round(t._radius), r = Math.round(t._radiusY || o);
        this._setPath(t, t._empty() ? "M0 0" : "AL " + i.x + "," + i.y + " " + o + "," + r + " 0," + 65535 * 360);
      },
      _setPath: function(t, i) {
        t._path.v = i;
      },
      _bringToFront: function(t) {
        zi(t._container);
      },
      _bringToBack: function(t) {
        Ni(t._container);
      }
    }, Qn = $.vml ? fn : S, dn = Ue.extend({
      _initContainer: function() {
        this._container = Qn("svg"), this._container.setAttribute("pointer-events", "none"), this._rootGroup = Qn("g"), this._container.appendChild(this._rootGroup);
      },
      _destroyContainer: function() {
        kt(this._container), Lt(this._container), delete this._container, delete this._rootGroup, delete this._svgSize;
      },
      _update: function() {
        if (!(this._map._animatingZoom && this._bounds)) {
          Ue.prototype._update.call(this);
          var t = this._bounds, i = t.getSize(), o = this._container;
          (!this._svgSize || !this._svgSize.equals(i)) && (this._svgSize = i, o.setAttribute("width", i.x), o.setAttribute("height", i.y)), Wt(o, t.min), o.setAttribute("viewBox", [t.min.x, t.min.y, i.x, i.y].join(" ")), this.fire("update");
        }
      },
      // methods below are called by vector layers implementations
      _initPath: function(t) {
        var i = t._path = Qn("path");
        t.options.className && ot(i, t.options.className), t.options.interactive && ot(i, "leaflet-interactive"), this._updateStyle(t), this._layers[p(t)] = t;
      },
      _addPath: function(t) {
        this._rootGroup || this._initContainer(), this._rootGroup.appendChild(t._path), t.addInteractiveTarget(t._path);
      },
      _removePath: function(t) {
        kt(t._path), t.removeInteractiveTarget(t._path), delete this._layers[p(t)];
      },
      _updatePath: function(t) {
        t._project(), t._update();
      },
      _updateStyle: function(t) {
        var i = t._path, o = t.options;
        i && (o.stroke ? (i.setAttribute("stroke", o.color), i.setAttribute("stroke-opacity", o.opacity), i.setAttribute("stroke-width", o.weight), i.setAttribute("stroke-linecap", o.lineCap), i.setAttribute("stroke-linejoin", o.lineJoin), o.dashArray ? i.setAttribute("stroke-dasharray", o.dashArray) : i.removeAttribute("stroke-dasharray"), o.dashOffset ? i.setAttribute("stroke-dashoffset", o.dashOffset) : i.removeAttribute("stroke-dashoffset")) : i.setAttribute("stroke", "none"), o.fill ? (i.setAttribute("fill", o.fillColor || o.color), i.setAttribute("fill-opacity", o.fillOpacity), i.setAttribute("fill-rule", o.fillRule || "evenodd")) : i.setAttribute("fill", "none"));
      },
      _updatePoly: function(t, i) {
        this._setPath(t, C(t._parts, i));
      },
      _updateCircle: function(t) {
        var i = t._point, o = Math.max(Math.round(t._radius), 1), r = Math.max(Math.round(t._radiusY), 1) || o, h = "a" + o + "," + r + " 0 1,0 ", d = t._empty() ? "M0 0" : "M" + (i.x - o) + "," + i.y + h + o * 2 + ",0 " + h + -o * 2 + ",0 ";
        this._setPath(t, d);
      },
      _setPath: function(t, i) {
        t._path.setAttribute("d", i);
      },
      // SVG does not have the concept of zIndex so we resort to changing the DOM order of elements
      _bringToFront: function(t) {
        zi(t._path);
      },
      _bringToBack: function(t) {
        Ni(t._path);
      }
    });
    $.vml && dn.include(kh);
    function Qr(t) {
      return $.svg || $.vml ? new dn(t) : null;
    }
    ht.include({
      // @namespace Map; @method getRenderer(layer: Path): Renderer
      // Returns the instance of `Renderer` that should be used to render the given
      // `Path`. It will ensure that the `renderer` options of the map and paths
      // are respected, and that the renderers do exist on the map.
      getRenderer: function(t) {
        var i = t.options.renderer || this._getPaneRenderer(t.options.pane) || this.options.renderer || this._renderer;
        return i || (i = this._renderer = this._createRenderer()), this.hasLayer(i) || this.addLayer(i), i;
      },
      _getPaneRenderer: function(t) {
        if (t === "overlayPane" || t === void 0)
          return !1;
        var i = this._paneRenderers[t];
        return i === void 0 && (i = this._createRenderer({ pane: t }), this._paneRenderers[t] = i), i;
      },
      _createRenderer: function(t) {
        return this.options.preferCanvas && Xr(t) || Qr(t);
      }
    });
    var ta = Di.extend({
      initialize: function(t, i) {
        Di.prototype.initialize.call(this, this._boundsToLatLngs(t), i);
      },
      // @method setBounds(latLngBounds: LatLngBounds): this
      // Redraws the rectangle with the passed bounds.
      setBounds: function(t) {
        return this.setLatLngs(this._boundsToLatLngs(t));
      },
      _boundsToLatLngs: function(t) {
        return t = $t(t), [
          t.getSouthWest(),
          t.getNorthWest(),
          t.getNorthEast(),
          t.getSouthEast()
        ];
      }
    });
    function Bh(t, i) {
      return new ta(t, i);
    }
    dn.create = Qn, dn.pointsToPath = C, We.geometryToLayer = Vn, We.coordsToLatLng = us, We.coordsToLatLngs = Kn, We.latLngToCoords = hs, We.latLngsToCoords = qn, We.getFeature = Fi, We.asFeature = Gn, ht.mergeOptions({
      // @option boxZoom: Boolean = true
      // Whether the map can be zoomed to a rectangular area specified by
      // dragging the mouse while pressing the shift key.
      boxZoom: !0
    });
    var ea = ke.extend({
      initialize: function(t) {
        this._map = t, this._container = t._container, this._pane = t._panes.overlayPane, this._resetStateTimeout = 0, t.on("unload", this._destroy, this);
      },
      addHooks: function() {
        et(this._container, "mousedown", this._onMouseDown, this);
      },
      removeHooks: function() {
        Lt(this._container, "mousedown", this._onMouseDown, this);
      },
      moved: function() {
        return this._moved;
      },
      _destroy: function() {
        kt(this._pane), delete this._pane;
      },
      _resetState: function() {
        this._resetStateTimeout = 0, this._moved = !1;
      },
      _clearDeferredResetState: function() {
        this._resetStateTimeout !== 0 && (clearTimeout(this._resetStateTimeout), this._resetStateTimeout = 0);
      },
      _onMouseDown: function(t) {
        if (!t.shiftKey || t.which !== 1 && t.button !== 1)
          return !1;
        this._clearDeferredResetState(), this._resetState(), on(), Ko(), this._startPoint = this._map.mouseEventToContainerPoint(t), et(document, {
          contextmenu: gi,
          mousemove: this._onMouseMove,
          mouseup: this._onMouseUp,
          keydown: this._onKeyDown
        }, this);
      },
      _onMouseMove: function(t) {
        this._moved || (this._moved = !0, this._box = dt("div", "leaflet-zoom-box", this._container), ot(this._container, "leaflet-crosshair"), this._map.fire("boxzoomstart")), this._point = this._map.mouseEventToContainerPoint(t);
        var i = new mt(this._point, this._startPoint), o = i.getSize();
        Wt(this._box, i.min), this._box.style.width = o.x + "px", this._box.style.height = o.y + "px";
      },
      _finish: function() {
        this._moved && (kt(this._box), jt(this._container, "leaflet-crosshair")), sn(), qo(), Lt(document, {
          contextmenu: gi,
          mousemove: this._onMouseMove,
          mouseup: this._onMouseUp,
          keydown: this._onKeyDown
        }, this);
      },
      _onMouseUp: function(t) {
        if (!(t.which !== 1 && t.button !== 1) && (this._finish(), !!this._moved)) {
          this._clearDeferredResetState(), this._resetStateTimeout = setTimeout(c(this._resetState, this), 0);
          var i = new Gt(
            this._map.containerPointToLatLng(this._startPoint),
            this._map.containerPointToLatLng(this._point)
          );
          this._map.fitBounds(i).fire("boxzoomend", { boxZoomBounds: i });
        }
      },
      _onKeyDown: function(t) {
        t.keyCode === 27 && (this._finish(), this._clearDeferredResetState(), this._resetState());
      }
    });
    ht.addInitHook("addHandler", "boxZoom", ea), ht.mergeOptions({
      // @option doubleClickZoom: Boolean|String = true
      // Whether the map can be zoomed in by double clicking on it and
      // zoomed out by double clicking while holding shift. If passed
      // `'center'`, double-click zoom will zoom to the center of the
      //  view regardless of where the mouse was.
      doubleClickZoom: !0
    });
    var ia = ke.extend({
      addHooks: function() {
        this._map.on("dblclick", this._onDoubleClick, this);
      },
      removeHooks: function() {
        this._map.off("dblclick", this._onDoubleClick, this);
      },
      _onDoubleClick: function(t) {
        var i = this._map, o = i.getZoom(), r = i.options.zoomDelta, h = t.originalEvent.shiftKey ? o - r : o + r;
        i.options.doubleClickZoom === "center" ? i.setZoom(h) : i.setZoomAround(t.containerPoint, h);
      }
    });
    ht.addInitHook("addHandler", "doubleClickZoom", ia), ht.mergeOptions({
      // @option dragging: Boolean = true
      // Whether the map is draggable with mouse/touch or not.
      dragging: !0,
      // @section Panning Inertia Options
      // @option inertia: Boolean = *
      // If enabled, panning of the map will have an inertia effect where
      // the map builds momentum while dragging and continues moving in
      // the same direction for some time. Feels especially nice on touch
      // devices. Enabled by default.
      inertia: !0,
      // @option inertiaDeceleration: Number = 3000
      // The rate with which the inertial movement slows down, in pixels/second².
      inertiaDeceleration: 3400,
      // px/s^2
      // @option inertiaMaxSpeed: Number = Infinity
      // Max speed of the inertial movement, in pixels/second.
      inertiaMaxSpeed: 1 / 0,
      // px/s
      // @option easeLinearity: Number = 0.2
      easeLinearity: 0.2,
      // TODO refactor, move to CRS
      // @option worldCopyJump: Boolean = false
      // With this option enabled, the map tracks when you pan to another "copy"
      // of the world and seamlessly jumps to the original one so that all overlays
      // like markers and vector layers are still visible.
      worldCopyJump: !1,
      // @option maxBoundsViscosity: Number = 0.0
      // If `maxBounds` is set, this option will control how solid the bounds
      // are when dragging the map around. The default value of `0.0` allows the
      // user to drag outside the bounds at normal speed, higher values will
      // slow down map dragging outside bounds, and `1.0` makes the bounds fully
      // solid, preventing the user from dragging outside the bounds.
      maxBoundsViscosity: 0
    });
    var na = ke.extend({
      addHooks: function() {
        if (!this._draggable) {
          var t = this._map;
          this._draggable = new Je(t._mapPane, t._container), this._draggable.on({
            dragstart: this._onDragStart,
            drag: this._onDrag,
            dragend: this._onDragEnd
          }, this), this._draggable.on("predrag", this._onPreDragLimit, this), t.options.worldCopyJump && (this._draggable.on("predrag", this._onPreDragWrap, this), t.on("zoomend", this._onZoomEnd, this), t.whenReady(this._onZoomEnd, this));
        }
        ot(this._map._container, "leaflet-grab leaflet-touch-drag"), this._draggable.enable(), this._positions = [], this._times = [];
      },
      removeHooks: function() {
        jt(this._map._container, "leaflet-grab"), jt(this._map._container, "leaflet-touch-drag"), this._draggable.disable();
      },
      moved: function() {
        return this._draggable && this._draggable._moved;
      },
      moving: function() {
        return this._draggable && this._draggable._moving;
      },
      _onDragStart: function() {
        var t = this._map;
        if (t._stop(), this._map.options.maxBounds && this._map.options.maxBoundsViscosity) {
          var i = $t(this._map.options.maxBounds);
          this._offsetLimit = Ct(
            this._map.latLngToContainerPoint(i.getNorthWest()).multiplyBy(-1),
            this._map.latLngToContainerPoint(i.getSouthEast()).multiplyBy(-1).add(this._map.getSize())
          ), this._viscosity = Math.min(1, Math.max(0, this._map.options.maxBoundsViscosity));
        } else
          this._offsetLimit = null;
        t.fire("movestart").fire("dragstart"), t.options.inertia && (this._positions = [], this._times = []);
      },
      _onDrag: function(t) {
        if (this._map.options.inertia) {
          var i = this._lastTime = +/* @__PURE__ */ new Date(), o = this._lastPos = this._draggable._absPos || this._draggable._newPos;
          this._positions.push(o), this._times.push(i), this._prunePositions(i);
        }
        this._map.fire("move", t).fire("drag", t);
      },
      _prunePositions: function(t) {
        for (; this._positions.length > 1 && t - this._times[0] > 50; )
          this._positions.shift(), this._times.shift();
      },
      _onZoomEnd: function() {
        var t = this._map.getSize().divideBy(2), i = this._map.latLngToLayerPoint([0, 0]);
        this._initialWorldOffset = i.subtract(t).x, this._worldWidth = this._map.getPixelWorldBounds().getSize().x;
      },
      _viscousLimit: function(t, i) {
        return t - (t - i) * this._viscosity;
      },
      _onPreDragLimit: function() {
        if (!(!this._viscosity || !this._offsetLimit)) {
          var t = this._draggable._newPos.subtract(this._draggable._startPos), i = this._offsetLimit;
          t.x < i.min.x && (t.x = this._viscousLimit(t.x, i.min.x)), t.y < i.min.y && (t.y = this._viscousLimit(t.y, i.min.y)), t.x > i.max.x && (t.x = this._viscousLimit(t.x, i.max.x)), t.y > i.max.y && (t.y = this._viscousLimit(t.y, i.max.y)), this._draggable._newPos = this._draggable._startPos.add(t);
        }
      },
      _onPreDragWrap: function() {
        var t = this._worldWidth, i = Math.round(t / 2), o = this._initialWorldOffset, r = this._draggable._newPos.x, h = (r - i + o) % t + i - o, d = (r + i + o) % t - i - o, m = Math.abs(h + o) < Math.abs(d + o) ? h : d;
        this._draggable._absPos = this._draggable._newPos.clone(), this._draggable._newPos.x = m;
      },
      _onDragEnd: function(t) {
        var i = this._map, o = i.options, r = !o.inertia || t.noInertia || this._times.length < 2;
        if (i.fire("dragend", t), r)
          i.fire("moveend");
        else {
          this._prunePositions(+/* @__PURE__ */ new Date());
          var h = this._lastPos.subtract(this._positions[0]), d = (this._lastTime - this._times[0]) / 1e3, m = o.easeLinearity, y = h.multiplyBy(m / d), w = y.distanceTo([0, 0]), O = Math.min(o.inertiaMaxSpeed, w), N = y.multiplyBy(O / w), G = O / (o.inertiaDeceleration * m), lt = N.multiplyBy(-G / 2).round();
          !lt.x && !lt.y ? i.fire("moveend") : (lt = i._limitOffset(lt, i.options.maxBounds), ut(function() {
            i.panBy(lt, {
              duration: G,
              easeLinearity: m,
              noMoveStart: !0,
              animate: !0
            });
          }));
        }
      }
    });
    ht.addInitHook("addHandler", "dragging", na), ht.mergeOptions({
      // @option keyboard: Boolean = true
      // Makes the map focusable and allows users to navigate the map with keyboard
      // arrows and `+`/`-` keys.
      keyboard: !0,
      // @option keyboardPanDelta: Number = 80
      // Amount of pixels to pan when pressing an arrow key.
      keyboardPanDelta: 80
    });
    var oa = ke.extend({
      keyCodes: {
        left: [37],
        right: [39],
        down: [40],
        up: [38],
        zoomIn: [187, 107, 61, 171],
        zoomOut: [189, 109, 54, 173]
      },
      initialize: function(t) {
        this._map = t, this._setPanDelta(t.options.keyboardPanDelta), this._setZoomDelta(t.options.zoomDelta);
      },
      addHooks: function() {
        var t = this._map._container;
        t.tabIndex <= 0 && (t.tabIndex = "0"), et(t, {
          focus: this._onFocus,
          blur: this._onBlur,
          mousedown: this._onMouseDown
        }, this), this._map.on({
          focus: this._addHooks,
          blur: this._removeHooks
        }, this);
      },
      removeHooks: function() {
        this._removeHooks(), Lt(this._map._container, {
          focus: this._onFocus,
          blur: this._onBlur,
          mousedown: this._onMouseDown
        }, this), this._map.off({
          focus: this._addHooks,
          blur: this._removeHooks
        }, this);
      },
      _onMouseDown: function() {
        if (!this._focused) {
          var t = document.body, i = document.documentElement, o = t.scrollTop || i.scrollTop, r = t.scrollLeft || i.scrollLeft;
          this._map._container.focus(), window.scrollTo(r, o);
        }
      },
      _onFocus: function() {
        this._focused = !0, this._map.fire("focus");
      },
      _onBlur: function() {
        this._focused = !1, this._map.fire("blur");
      },
      _setPanDelta: function(t) {
        var i = this._panKeys = {}, o = this.keyCodes, r, h;
        for (r = 0, h = o.left.length; r < h; r++)
          i[o.left[r]] = [-1 * t, 0];
        for (r = 0, h = o.right.length; r < h; r++)
          i[o.right[r]] = [t, 0];
        for (r = 0, h = o.down.length; r < h; r++)
          i[o.down[r]] = [0, t];
        for (r = 0, h = o.up.length; r < h; r++)
          i[o.up[r]] = [0, -1 * t];
      },
      _setZoomDelta: function(t) {
        var i = this._zoomKeys = {}, o = this.keyCodes, r, h;
        for (r = 0, h = o.zoomIn.length; r < h; r++)
          i[o.zoomIn[r]] = t;
        for (r = 0, h = o.zoomOut.length; r < h; r++)
          i[o.zoomOut[r]] = -t;
      },
      _addHooks: function() {
        et(document, "keydown", this._onKeyDown, this);
      },
      _removeHooks: function() {
        Lt(document, "keydown", this._onKeyDown, this);
      },
      _onKeyDown: function(t) {
        if (!(t.altKey || t.ctrlKey || t.metaKey)) {
          var i = t.keyCode, o = this._map, r;
          if (i in this._panKeys) {
            if (!o._panAnim || !o._panAnim._inProgress)
              if (r = this._panKeys[i], t.shiftKey && (r = K(r).multiplyBy(3)), o.options.maxBounds && (r = o._limitOffset(K(r), o.options.maxBounds)), o.options.worldCopyJump) {
                var h = o.wrapLatLng(o.unproject(o.project(o.getCenter()).add(r)));
                o.panTo(h);
              } else
                o.panBy(r);
          } else if (i in this._zoomKeys)
            o.setZoom(o.getZoom() + (t.shiftKey ? 3 : 1) * this._zoomKeys[i]);
          else if (i === 27 && o._popup && o._popup.options.closeOnEscapeKey)
            o.closePopup();
          else
            return;
          gi(t);
        }
      }
    });
    ht.addInitHook("addHandler", "keyboard", oa), ht.mergeOptions({
      // @section Mouse wheel options
      // @option scrollWheelZoom: Boolean|String = true
      // Whether the map can be zoomed by using the mouse wheel. If passed `'center'`,
      // it will zoom to the center of the view regardless of where the mouse was.
      scrollWheelZoom: !0,
      // @option wheelDebounceTime: Number = 40
      // Limits the rate at which a wheel can fire (in milliseconds). By default
      // user can't zoom via wheel more often than once per 40 ms.
      wheelDebounceTime: 40,
      // @option wheelPxPerZoomLevel: Number = 60
      // How many scroll pixels (as reported by [L.DomEvent.getWheelDelta](#domevent-getwheeldelta))
      // mean a change of one full zoom level. Smaller values will make wheel-zooming
      // faster (and vice versa).
      wheelPxPerZoomLevel: 60
    });
    var sa = ke.extend({
      addHooks: function() {
        et(this._map._container, "wheel", this._onWheelScroll, this), this._delta = 0;
      },
      removeHooks: function() {
        Lt(this._map._container, "wheel", this._onWheelScroll, this);
      },
      _onWheelScroll: function(t) {
        var i = Mr(t), o = this._map.options.wheelDebounceTime;
        this._delta += i, this._lastMousePos = this._map.mouseEventToContainerPoint(t), this._startTime || (this._startTime = +/* @__PURE__ */ new Date());
        var r = Math.max(o - (+/* @__PURE__ */ new Date() - this._startTime), 0);
        clearTimeout(this._timer), this._timer = setTimeout(c(this._performZoom, this), r), gi(t);
      },
      _performZoom: function() {
        var t = this._map, i = t.getZoom(), o = this._map.options.zoomSnap || 0;
        t._stop();
        var r = this._delta / (this._map.options.wheelPxPerZoomLevel * 4), h = 4 * Math.log(2 / (1 + Math.exp(-Math.abs(r)))) / Math.LN2, d = o ? Math.ceil(h / o) * o : h, m = t._limitZoom(i + (this._delta > 0 ? d : -d)) - i;
        this._delta = 0, this._startTime = null, m && (t.options.scrollWheelZoom === "center" ? t.setZoom(i + m) : t.setZoomAround(this._lastMousePos, i + m));
      }
    });
    ht.addInitHook("addHandler", "scrollWheelZoom", sa);
    var zh = 600;
    ht.mergeOptions({
      // @section Touch interaction options
      // @option tapHold: Boolean
      // Enables simulation of `contextmenu` event, default is `true` for mobile Safari.
      tapHold: $.touchNative && $.safari && $.mobile,
      // @option tapTolerance: Number = 15
      // The max number of pixels a user can shift his finger during touch
      // for it to be considered a valid tap.
      tapTolerance: 15
    });
    var ra = ke.extend({
      addHooks: function() {
        et(this._map._container, "touchstart", this._onDown, this);
      },
      removeHooks: function() {
        Lt(this._map._container, "touchstart", this._onDown, this);
      },
      _onDown: function(t) {
        if (clearTimeout(this._holdTimeout), t.touches.length === 1) {
          var i = t.touches[0];
          this._startPos = this._newPos = new Z(i.clientX, i.clientY), this._holdTimeout = setTimeout(c(function() {
            this._cancel(), this._isTapValid() && (et(document, "touchend", Yt), et(document, "touchend touchcancel", this._cancelClickPrevent), this._simulateEvent("contextmenu", i));
          }, this), zh), et(document, "touchend touchcancel contextmenu", this._cancel, this), et(document, "touchmove", this._onMove, this);
        }
      },
      _cancelClickPrevent: function t() {
        Lt(document, "touchend", Yt), Lt(document, "touchend touchcancel", t);
      },
      _cancel: function() {
        clearTimeout(this._holdTimeout), Lt(document, "touchend touchcancel contextmenu", this._cancel, this), Lt(document, "touchmove", this._onMove, this);
      },
      _onMove: function(t) {
        var i = t.touches[0];
        this._newPos = new Z(i.clientX, i.clientY);
      },
      _isTapValid: function() {
        return this._newPos.distanceTo(this._startPos) <= this._map.options.tapTolerance;
      },
      _simulateEvent: function(t, i) {
        var o = new MouseEvent(t, {
          bubbles: !0,
          cancelable: !0,
          view: window,
          // detail: 1,
          screenX: i.screenX,
          screenY: i.screenY,
          clientX: i.clientX,
          clientY: i.clientY
          // button: 2,
          // buttons: 2
        });
        o._simulated = !0, i.target.dispatchEvent(o);
      }
    });
    ht.addInitHook("addHandler", "tapHold", ra), ht.mergeOptions({
      // @section Touch interaction options
      // @option touchZoom: Boolean|String = *
      // Whether the map can be zoomed by touch-dragging with two fingers. If
      // passed `'center'`, it will zoom to the center of the view regardless of
      // where the touch events (fingers) were. Enabled for touch-capable web
      // browsers.
      touchZoom: $.touch,
      // @option bounceAtZoomLimits: Boolean = true
      // Set it to false if you don't want the map to zoom beyond min/max zoom
      // and then bounce back when pinch-zooming.
      bounceAtZoomLimits: !0
    });
    var aa = ke.extend({
      addHooks: function() {
        ot(this._map._container, "leaflet-touch-zoom"), et(this._map._container, "touchstart", this._onTouchStart, this);
      },
      removeHooks: function() {
        jt(this._map._container, "leaflet-touch-zoom"), Lt(this._map._container, "touchstart", this._onTouchStart, this);
      },
      _onTouchStart: function(t) {
        var i = this._map;
        if (!(!t.touches || t.touches.length !== 2 || i._animatingZoom || this._zooming)) {
          var o = i.mouseEventToContainerPoint(t.touches[0]), r = i.mouseEventToContainerPoint(t.touches[1]);
          this._centerPoint = i.getSize()._divideBy(2), this._startLatLng = i.containerPointToLatLng(this._centerPoint), i.options.touchZoom !== "center" && (this._pinchStartLatLng = i.containerPointToLatLng(o.add(r)._divideBy(2))), this._startDist = o.distanceTo(r), this._startZoom = i.getZoom(), this._moved = !1, this._zooming = !0, i._stop(), et(document, "touchmove", this._onTouchMove, this), et(document, "touchend touchcancel", this._onTouchEnd, this), Yt(t);
        }
      },
      _onTouchMove: function(t) {
        if (!(!t.touches || t.touches.length !== 2 || !this._zooming)) {
          var i = this._map, o = i.mouseEventToContainerPoint(t.touches[0]), r = i.mouseEventToContainerPoint(t.touches[1]), h = o.distanceTo(r) / this._startDist;
          if (this._zoom = i.getScaleZoom(h, this._startZoom), !i.options.bounceAtZoomLimits && (this._zoom < i.getMinZoom() && h < 1 || this._zoom > i.getMaxZoom() && h > 1) && (this._zoom = i._limitZoom(this._zoom)), i.options.touchZoom === "center") {
            if (this._center = this._startLatLng, h === 1)
              return;
          } else {
            var d = o._add(r)._divideBy(2)._subtract(this._centerPoint);
            if (h === 1 && d.x === 0 && d.y === 0)
              return;
            this._center = i.unproject(i.project(this._pinchStartLatLng, this._zoom).subtract(d), this._zoom);
          }
          this._moved || (i._moveStart(!0, !1), this._moved = !0), R(this._animRequest);
          var m = c(i._move, i, this._center, this._zoom, { pinch: !0, round: !1 }, void 0);
          this._animRequest = ut(m, this, !0), Yt(t);
        }
      },
      _onTouchEnd: function() {
        if (!this._moved || !this._zooming) {
          this._zooming = !1;
          return;
        }
        this._zooming = !1, R(this._animRequest), Lt(document, "touchmove", this._onTouchMove, this), Lt(document, "touchend touchcancel", this._onTouchEnd, this), this._map.options.zoomAnimation ? this._map._animateZoom(this._center, this._map._limitZoom(this._zoom), !0, this._map.options.zoomSnap) : this._map._resetView(this._center, this._map._limitZoom(this._zoom));
      }
    });
    ht.addInitHook("addHandler", "touchZoom", aa), ht.BoxZoom = ea, ht.DoubleClickZoom = ia, ht.Drag = na, ht.Keyboard = oa, ht.ScrollWheelZoom = sa, ht.TapHold = ra, ht.TouchZoom = aa, s.Bounds = mt, s.Browser = $, s.CRS = le, s.Canvas = Jr, s.Circle = ls, s.CircleMarker = Un, s.Class = It, s.Control = we, s.DivIcon = qr, s.DivOverlay = Be, s.DomEvent = Qu, s.DomUtil = Ju, s.Draggable = Je, s.Evented = bt, s.FeatureGroup = He, s.GeoJSON = We, s.GridLayer = cn, s.Handler = ke, s.Icon = Ri, s.ImageOverlay = Yn, s.LatLng = vt, s.LatLngBounds = Gt, s.Layer = xe, s.LayerGroup = Zi, s.LineUtil = ch, s.Map = ht, s.Marker = Wn, s.Mixin = rh, s.Path = Xe, s.Point = Z, s.PolyUtil = fh, s.Polygon = Di, s.Polyline = $e, s.Popup = Jn, s.PosAnimation = Er, s.Projection = dh, s.Rectangle = ta, s.Renderer = Ue, s.SVG = dn, s.SVGOverlay = Kr, s.TileLayer = ji, s.Tooltip = Xn, s.Transformation = di, s.Util = Tt, s.VideoOverlay = Vr, s.bind = c, s.bounds = Ct, s.canvas = Xr, s.circle = wh, s.circleMarker = bh, s.control = ln, s.divIcon = Eh, s.extend = l, s.featureGroup = gh, s.geoJSON = Ur, s.geoJson = Ph, s.gridLayer = Ah, s.icon = vh, s.imageOverlay = Th, s.latLng = at, s.latLngBounds = $t, s.layerGroup = _h, s.map = th, s.marker = yh, s.point = K, s.polygon = Lh, s.polyline = xh, s.popup = Sh, s.rectangle = Bh, s.setOptions = E, s.stamp = p, s.svg = Qr, s.svgOverlay = Oh, s.tileLayer = Gr, s.tooltip = Mh, s.transformation = _, s.version = a, s.videoOverlay = Ch;
    var Nh = window.L;
    s.noConflict = function() {
      return window.L = Nh, this;
    }, window.L = s;
  });
})(Rs, Rs.exports);
var sr = Rs.exports;
const vn = /* @__PURE__ */ Cd(sr), Od = /* @__PURE__ */ Uh({
  __proto__: null,
  default: vn
}, [sr]), Sd = "FeatureCollection", Md = [
  {
    type: "Feature",
    id: 300,
    properties: {
      ID_0: 86,
      ISO: "DEU",
      NAME_0: "Germany",
      ID_1: 10,
      NAME_1: "Nordrhein-Westfalen",
      ID_2: 28,
      NAME_2: "Munster",
      ID_3: 301,
      NAME_3: "Münster Städte",
      NL_NAME_3: null,
      VARNAME_3: null,
      TYPE_3: "Kreisfreie Städte",
      ENGTYPE_3: "Urban district"
    },
    geometry: {
      type: "Polygon",
      coordinates: [
        [
          [
            7.520258903503418,
            51.99682998657232
          ],
          [
            7.52623987197876,
            51.997009277343864
          ],
          [
            7.519031047821045,
            52.01177978515625
          ],
          [
            7.53668022155756,
            52.016059875488395
          ],
          [
            7.541749000549316,
            52.02746200561518
          ],
          [
            7.535449981689567,
            52.03102111816406
          ],
          [
            7.594130992889461,
            52.047748565673885
          ],
          [
            7.587530136108455,
            52.05504989624029
          ],
          [
            7.605199813842773,
            52.059318542480526
          ],
          [
            7.636680126190242,
            52.04153060913097
          ],
          [
            7.653759956359977,
            52.05326080322277
          ],
          [
            7.691520214080811,
            52.03190994262695
          ],
          [
            7.721499919891471,
            52.0327987670899
          ],
          [
            7.734660148620605,
            52.018199920654354
          ],
          [
            7.735819816589412,
            52.00326156616211
          ],
          [
            7.748380184173641,
            51.9961509704591
          ],
          [
            7.731540203094539,
            51.98067855834972
          ],
          [
            7.738939762115479,
            51.962200164795036
          ],
          [
            7.751480102539176,
            51.955089569091854
          ],
          [
            7.746048927307129,
            51.94744873046869
          ],
          [
            7.771089076995793,
            51.93323898315424
          ],
          [
            7.778171062469596,
            51.918498992920036
          ],
          [
            7.761837959289608,
            51.895591735839844
          ],
          [
            7.70721006393444,
            51.9051513671875
          ],
          [
            7.695528984069824,
            51.901062011718864
          ],
          [
            7.690659046173153,
            51.88597106933605
          ],
          [
            7.7026109695434,
            51.88632965087896
          ],
          [
            7.715649127960148,
            51.87178039550781
          ],
          [
            7.70396900177002,
            51.867691040039176
          ],
          [
            7.655368804931697,
            51.87741088867199
          ],
          [
            7.644248962402401,
            51.86585998535156
          ],
          [
            7.650781154632568,
            51.85858154296875
          ],
          [
            7.633420944213867,
            51.85058975219738
          ],
          [
            7.621209144592399,
            51.853939056396484
          ],
          [
            7.616068840026912,
            51.84257888793957
          ],
          [
            7.622319221496582,
            51.83903884887695
          ],
          [
            7.604421138763485,
            51.83848190307617
          ],
          [
            7.603859901428336,
            51.84593200683605
          ],
          [
            7.586257934570369,
            51.841651916503906
          ],
          [
            7.561571121215763,
            51.852088928222656
          ],
          [
            7.567249774932975,
            51.856010437011776
          ],
          [
            7.560440063476562,
            51.86701202392578
          ],
          [
            7.548520088195801,
            51.86664199829113
          ],
          [
            7.553907871246452,
            51.87429046630865
          ],
          [
            7.541409969329834,
            51.88138198852539
          ],
          [
            7.539968967437801,
            51.90003967285162
          ],
          [
            7.510428905487117,
            51.89538955688482
          ],
          [
            7.51610898971569,
            51.89929962158209
          ],
          [
            7.50895881652832,
            51.914070129394474
          ],
          [
            7.513440132141113,
            51.932918548583984
          ],
          [
            7.477028846740836,
            51.93928909301769
          ],
          [
            7.481781005859489,
            51.95442199707037
          ],
          [
            7.527769088745117,
            51.97830963134777
          ],
          [
            7.520258903503418,
            51.99682998657232
          ]
        ]
      ]
    }
  },
  {
    type: "Feature",
    id: 298,
    properties: {
      ID_0: 86,
      ISO: "DEU",
      NAME_0: "Germany",
      ID_1: 10,
      NAME_1: "Nordrhein-Westfalen",
      ID_2: 28,
      NAME_2: "Munster",
      ID_3: 299,
      NAME_3: "Coesfeld",
      NL_NAME_3: null,
      VARNAME_3: null,
      TYPE_3: "Landkreise",
      ENGTYPE_3: "Rural district"
    },
    geometry: {
      type: "Polygon",
      coordinates: [
        [
          [
            7.274960041046199,
            52.05712127685541
          ],
          [
            7.292599201202336,
            52.06138992309582
          ],
          [
            7.299290180206242,
            52.05406188964838
          ],
          [
            7.293640136718807,
            52.05012893676769
          ],
          [
            7.367189884185791,
            52.03350067138672
          ],
          [
            7.37382888793951,
            52.026191711425895
          ],
          [
            7.402790069580078,
            52.038311004638786
          ],
          [
            7.41540098190319,
            52.03118133544916
          ],
          [
            7.420750141143913,
            52.038841247558594
          ],
          [
            7.440299987793026,
            52.02067184448248
          ],
          [
            7.429277896881217,
            52.00907897949219
          ],
          [
            7.435907840728817,
            52.001770019531364
          ],
          [
            7.459519863128719,
            52.00624084472656
          ],
          [
            7.478711128234863,
            51.9918098449707
          ],
          [
            7.520258903503418,
            51.99682998657232
          ],
          [
            7.527769088745117,
            51.97830963134777
          ],
          [
            7.481781005859489,
            51.95442199707037
          ],
          [
            7.477028846740836,
            51.93928909301769
          ],
          [
            7.513440132141113,
            51.932918548583984
          ],
          [
            7.50895881652832,
            51.914070129394474
          ],
          [
            7.51610898971569,
            51.89929962158209
          ],
          [
            7.510428905487117,
            51.89538955688482
          ],
          [
            7.539968967437801,
            51.90003967285162
          ],
          [
            7.541409969329834,
            51.88138198852539
          ],
          [
            7.553907871246452,
            51.87429046630865
          ],
          [
            7.548520088195801,
            51.86664199829113
          ],
          [
            7.560440063476562,
            51.86701202392578
          ],
          [
            7.567249774932975,
            51.856010437011776
          ],
          [
            7.561571121215763,
            51.852088928222656
          ],
          [
            7.586257934570369,
            51.841651916503906
          ],
          [
            7.603859901428336,
            51.84593200683605
          ],
          [
            7.604421138763485,
            51.83848190307617
          ],
          [
            7.634789943695125,
            51.831958770751896
          ],
          [
            7.641589164733887,
            51.8209686279298
          ],
          [
            7.659208774566764,
            51.82524871826172
          ],
          [
            7.677908897399902,
            51.814640045166016
          ],
          [
            7.678720951080379,
            51.80347061157232
          ],
          [
            7.690918922424316,
            51.80012893676758
          ],
          [
            7.686289787292594,
            51.781341552734375
          ],
          [
            7.698229789733887,
            51.78170013427729
          ],
          [
            7.717689037322998,
            51.759971618652344
          ],
          [
            7.718739986419621,
            51.74510955810547
          ],
          [
            7.730409145355338,
            51.749198913574276
          ],
          [
            7.74337100982666,
            51.73472213745117
          ],
          [
            7.709157943725586,
            51.711311340331974
          ],
          [
            7.697238922119084,
            51.71092987060541
          ],
          [
            7.690238952636832,
            51.725589752197266
          ],
          [
            7.672627925872803,
            51.72130966186535
          ],
          [
            7.673150062561035,
            51.71387863159191
          ],
          [
            7.656058788299561,
            51.702171325683594
          ],
          [
            7.649600028991699,
            51.70941162109369
          ],
          [
            7.631979942321834,
            51.70510864257818
          ],
          [
            7.63146018981945,
            51.7125511169433
          ],
          [
            7.613598823547363,
            51.7119712829591
          ],
          [
            7.619819164276123,
            51.708450317382756
          ],
          [
            7.614389896392822,
            51.70082855224604
          ],
          [
            7.573009967803955,
            51.69575881958019
          ],
          [
            7.544329166412297,
            51.67992019653332
          ],
          [
            7.524919986724854,
            51.701641082763786
          ],
          [
            7.523580074310416,
            51.720230102539176
          ],
          [
            7.482279777526855,
            51.71514129638672
          ],
          [
            7.487410068511963,
            51.72650146484381
          ],
          [
            7.474988937377987,
            51.73355102539068
          ],
          [
            7.428929805755672,
            51.71335983276367
          ],
          [
            7.429738998413086,
            51.70219039916998
          ],
          [
            7.45451116561901,
            51.6881103515625
          ],
          [
            7.455570220947209,
            51.67324066162115
          ],
          [
            7.438329219818229,
            51.665199279785156
          ],
          [
            7.414400100708008,
            51.66810989379877
          ],
          [
            7.414928913116455,
            51.6606712341308
          ],
          [
            7.34814977645874,
            51.68445205688482
          ],
          [
            7.353219032287711,
            51.69583129882824
          ],
          [
            7.335820198059025,
            51.691478729248104
          ],
          [
            7.335268974304199,
            51.69892883300781
          ],
          [
            7.317609786987362,
            51.698299407958984
          ],
          [
            7.316760063171387,
            51.70949172973644
          ],
          [
            7.305560111999569,
            51.701610565185604
          ],
          [
            7.326807975768986,
            51.73226928710949
          ],
          [
            7.320038795471305,
            51.743259429931584
          ],
          [
            7.290268898010311,
            51.74594879150396
          ],
          [
            7.264557838440055,
            51.771270751953125
          ],
          [
            7.281939029693604,
            51.77561950683594
          ],
          [
            7.269847869873047,
            51.77894973754883
          ],
          [
            7.274798870086784,
            51.79035949707037
          ],
          [
            7.268269062042293,
            51.79763031005865
          ],
          [
            7.21934890747076,
            51.814739227295036
          ],
          [
            7.159379005432186,
            51.82032012939459
          ],
          [
            7.0903000831604,
            51.852249145507926
          ],
          [
            7.082828998565674,
            51.8671112060548
          ],
          [
            7.088829994201717,
            51.86727905273449
          ],
          [
            7.062269210815487,
            51.892921447753906
          ],
          [
            7.079160213470573,
            51.90468978881836
          ],
          [
            7.088960170745906,
            51.92757034301758
          ],
          [
            7.096189975738469,
            51.976749420166016
          ],
          [
            7.07145881652832,
            51.98360061645508
          ],
          [
            7.057629108428898,
            52.00205993652344
          ],
          [
            7.062889099121151,
            52.00973892211914
          ],
          [
            7.128879070282096,
            52.011558532714844
          ],
          [
            7.139420032501221,
            52.026920318603516
          ],
          [
            7.17360877990734,
            52.04671859741205
          ],
          [
            7.252389907836857,
            52.04140090942383
          ],
          [
            7.257328987121639,
            52.052841186523494
          ],
          [
            7.274960041046199,
            52.05712127685541
          ]
        ]
      ]
    }
  },
  {
    type: "Feature",
    id: 303,
    properties: {
      ID_0: 86,
      ISO: "DEU",
      NAME_0: "Germany",
      ID_1: 10,
      NAME_1: "Nordrhein-Westfalen",
      ID_2: 28,
      NAME_2: "Munster",
      ID_3: 304,
      NAME_3: "Warendorf",
      NL_NAME_3: null,
      VARNAME_3: null,
      TYPE_3: "Landkreise",
      ENGTYPE_3: "Rural district"
    },
    geometry: {
      type: "Polygon",
      coordinates: [
        [
          [
            7.886069774627686,
            52.08243942260742
          ],
          [
            7.924199104309196,
            52.057350158691406
          ],
          [
            7.918758869171256,
            52.049720764160156
          ],
          [
            7.974060058593864,
            52.03638076782238
          ],
          [
            7.986127853393555,
            52.03675079345703
          ],
          [
            7.997078895568904,
            52.052001953125
          ],
          [
            8.032528877258414,
            52.068290710449276
          ],
          [
            8.09704875946045,
            52.06864166259771
          ],
          [
            8.103867530822754,
            52.06151962280279
          ],
          [
            8.07947921752924,
            52.01063156127941
          ],
          [
            8.092378616333121,
            51.99993896484375
          ],
          [
            8.117248535156307,
            52.001152038574276
          ],
          [
            8.11127090454113,
            51.99328994750988
          ],
          [
            8.118048667907829,
            51.97850036621105
          ],
          [
            8.112910270690975,
            51.95566940307623
          ],
          [
            8.17542934417736,
            51.928001403808594
          ],
          [
            8.169388771057243,
            51.92401123046875
          ],
          [
            8.175918579101562,
            51.912929534912166
          ],
          [
            8.15219974517828,
            51.8969993591308
          ],
          [
            8.164149284362793,
            51.90119934082037
          ],
          [
            8.158598899841365,
            51.88970947265625
          ],
          [
            8.177089691162223,
            51.88283920288097
          ],
          [
            8.183979988098258,
            51.86426162719732
          ],
          [
            8.202129364013672,
            51.86487197875982
          ],
          [
            8.221068382263127,
            51.84666061401367
          ],
          [
            8.215288162231445,
            51.838951110839844
          ],
          [
            8.221489906310978,
            51.83539199829096
          ],
          [
            8.209248542785645,
            51.83876037597656
          ],
          [
            8.20374870300293,
            51.827308654785156
          ],
          [
            8.234029769897461,
            51.82448959350597
          ],
          [
            8.24064826965332,
            51.80963897705084
          ],
          [
            8.259079933166447,
            51.80266189575201
          ],
          [
            8.254119873046875,
            51.776222229003906
          ],
          [
            8.290710449218693,
            51.76218032836914
          ],
          [
            8.290940284729004,
            51.754669189453125
          ],
          [
            8.303059577941895,
            51.75498962402338
          ],
          [
            8.30348014831543,
            51.739971160888786
          ],
          [
            8.322138786315975,
            51.725421905517635
          ],
          [
            8.294589042663517,
            51.694980621338004
          ],
          [
            8.29599857330328,
            51.67636108398449
          ],
          [
            8.22009086608881,
            51.6559104919433
          ],
          [
            8.196008682250977,
            51.65901947021496
          ],
          [
            8.20715045928955,
            51.67050170898432
          ],
          [
            8.200419425964355,
            51.68151092529308
          ],
          [
            8.211319923400936,
            51.696708679199276
          ],
          [
            8.187790870666504,
            51.69235992431646
          ],
          [
            8.150668144226131,
            51.71372222900396
          ],
          [
            8.10878944396967,
            51.71625137329107
          ],
          [
            8.103799819946403,
            51.701198577880916
          ],
          [
            8.091900825500431,
            51.70085144042969
          ],
          [
            8.061188697814998,
            51.714828491210994
          ],
          [
            8.03120040893566,
            51.717628479003906
          ],
          [
            8.007888793945312,
            51.70949172973644
          ],
          [
            8.007398605346793,
            51.71691894531256
          ],
          [
            7.965469837188721,
            51.71932983398432
          ],
          [
            7.954779148101863,
            51.70040893554699
          ],
          [
            7.889000892639274,
            51.702091217041016
          ],
          [
            7.89421987533575,
            51.71340942382807
          ],
          [
            7.881529808044547,
            51.72417068481457
          ],
          [
            7.845510005950985,
            51.72676849365229
          ],
          [
            7.83256912231451,
            51.74124145507824
          ],
          [
            7.774209976196403,
            51.72080993652355
          ],
          [
            7.737151145935172,
            51.738239288330135
          ],
          [
            7.730409145355338,
            51.749198913574276
          ],
          [
            7.718739986419621,
            51.74510955810547
          ],
          [
            7.717689037322998,
            51.759971618652344
          ],
          [
            7.698229789733887,
            51.78170013427729
          ],
          [
            7.686289787292594,
            51.781341552734375
          ],
          [
            7.690918922424316,
            51.80012893676758
          ],
          [
            7.678720951080379,
            51.80347061157232
          ],
          [
            7.677908897399902,
            51.814640045166016
          ],
          [
            7.659208774566764,
            51.82524871826172
          ],
          [
            7.641589164733887,
            51.8209686279298
          ],
          [
            7.634789943695125,
            51.831958770751896
          ],
          [
            7.616630077362117,
            51.83512878417969
          ],
          [
            7.621209144592399,
            51.853939056396484
          ],
          [
            7.633420944213867,
            51.85058975219738
          ],
          [
            7.650781154632568,
            51.85858154296875
          ],
          [
            7.644248962402401,
            51.86585998535156
          ],
          [
            7.655368804931697,
            51.87741088867199
          ],
          [
            7.70396900177002,
            51.867691040039176
          ],
          [
            7.715649127960148,
            51.87178039550781
          ],
          [
            7.7026109695434,
            51.88632965087896
          ],
          [
            7.690659046173153,
            51.88597106933605
          ],
          [
            7.701229095459098,
            51.90497207641613
          ],
          [
            7.761837959289608,
            51.895591735839844
          ],
          [
            7.778171062469596,
            51.918498992920036
          ],
          [
            7.771089076995793,
            51.93323898315424
          ],
          [
            7.746048927307129,
            51.94744873046869
          ],
          [
            7.751480102539176,
            51.955089569091854
          ],
          [
            7.738939762115479,
            51.962200164795036
          ],
          [
            7.731540203094539,
            51.98067855834972
          ],
          [
            7.748380184173641,
            51.9961509704591
          ],
          [
            7.735819816589412,
            52.00326156616211
          ],
          [
            7.734660148620605,
            52.018199920654354
          ],
          [
            7.721499919891471,
            52.0327987670899
          ],
          [
            7.709508895874023,
            52.03245162963867
          ],
          [
            7.714619159698543,
            52.04383087158203
          ],
          [
            7.732019901275578,
            52.05183029174816
          ],
          [
            7.725131034851074,
            52.06285858154297
          ],
          [
            7.742228984832821,
            52.07460021972662
          ],
          [
            7.752429962158203,
            52.097358703613224
          ],
          [
            7.800138950347957,
            52.102489471435604
          ],
          [
            7.819069862365723,
            52.09178924560558
          ],
          [
            7.886069774627686,
            52.08243942260742
          ]
        ]
      ]
    }
  }
], Ed = {
  type: Sd,
  features: Md
};
const Ad = {
  name: "App",
  components: {},
  data: () => ({
    map: null,
    geojson: null,
    tooltips: [],
    url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    attribution: '&copy; <a target="_blank" href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    zoom: 9,
    center: [51.9625101, 7.6251879],
    icon: sr.icon({
      iconUrl: "/static/images/marker.png",
      iconSize: [32, 37],
      iconAnchor: [16, 37]
    }),
    counties: Ed,
    geoStyler: (e, n) => ({
      color: "rgba(241, 128, 141, 1)",
      fillColor: "rgba(255, 255, 255, 0)"
    }),
    searchTimer: null,
    form: {
      region: null,
      query: null
    },
    options: {
      regions: [
        { id: "Münster Städte", name: "Münster Städte" },
        { id: "Coesfeld", name: "Coesfeld" },
        { id: "Warendorf", name: "Warendorf" }
      ]
    },
    results: [],
    current: null,
    selected: null,
    locations: [
      {
        id: 1,
        name: "Heimatverein Münster Eins",
        county: "Münster Städte",
        district: "Munster",
        latLng: [51.96712864046486, 7.62597836860118],
        marker: null
      },
      {
        id: 2,
        name: "Heimatverein Münster Zwei",
        county: "Münster Städte",
        district: "Munster",
        latLng: [51.90951440337593, 7.621249915368067],
        marker: null
      },
      {
        id: 3,
        name: "Heimatverein Münster Drei",
        county: "Münster Städte",
        district: "Munster",
        latLng: [51.96912674322523, 7.574989305635977],
        marker: null
      },
      {
        id: 4,
        name: "Heimatverein Coesfeld Eins",
        county: "Coesfeld",
        district: "Munster",
        latLng: [51.9253833653372, 7.177231225787015],
        marker: null
      },
      {
        id: 5,
        name: "Heimatverein Coesfeld Zwei",
        county: "Coesfeld",
        district: "Munster",
        latLng: [51.86174077434947, 7.475767488942785],
        marker: null
      },
      {
        id: 6,
        name: "Heimatverein Warendorf Eins",
        county: "Warendorf",
        district: "Munster",
        latLng: [51.95612164190155, 7.966718723492161],
        marker: null
      },
      {
        id: 7,
        name: "Heimatverein Warendorf Zwei",
        county: "Warendorf",
        district: "Munster",
        latLng: [51.91715246771387, 7.967085164517754],
        marker: null
      },
      {
        id: 8,
        name: "Heimatverein Warendorf Drei",
        county: "Warendorf",
        district: "Munster",
        latLng: [51.928319308905245, 7.849775834795684],
        marker: null
      },
      {
        id: 9,
        name: "Heimatverein Warendorf Vier",
        county: "Warendorf",
        district: "Munster",
        latLng: [51.98901734697654, 8.03682821301583],
        marker: null
      },
      {
        id: 10,
        name: "Heimatverein Warendorf Fünf",
        county: "Warendorf",
        district: "Munster",
        latLng: [51.76448965454254, 8.036933046623792],
        marker: null
      }
    ]
  }),
  mounted() {
    this.map = vn.map("osmap-map"), this.map.setView(this.center, this.zoom), this.map.on("zoomend", this.onZoomChange), vn.tileLayer(this.url, {
      attribution: this.attribution,
      maxZoom: 18
    }).addTo(this.map);
    for (var e of this.locations) {
      const n = vn.marker(e.latLng, { icon: this.icon });
      n.addTo(this.map), n.on("mouseover", this.onMarkerOver.bind(this, e)), n.on("mouseout", this.onMarkerOut), n.on("click", this.selectLocation.bind(this, e)), e.marker = n;
    }
    this.geojson = vn.geoJson(this.counties, { style: this.geoStyler, onEachFeature: this.onEachFeature }), this.geojson.addTo(this.map), this.doSearch();
  },
  methods: {
    clearAll() {
      this.form.region = null, this.form.query = null;
    },
    doSearch() {
      this.results = this.locations.filter((e) => this.form.region && e.county == this.form.region && (!this.form.query || e.name.toLowerCase().includes(this.form.query.toLowerCase())));
      for (const e of this.locations)
        e.marker._icon.style.display = this.results.includes(e) ? "" : "none";
    },
    highlightFeature(e) {
      var n = e.target;
      n.setStyle({
        fillColor: "rgba(228, 0, 26, 0.26)"
      }), n.bringToFront();
    },
    onCountyClick(e) {
      this.map.fitBounds(e.target.getBounds()), this.form.region = e.target.feature.properties.NAME_3;
    },
    resetHighlight(e) {
      this.geojson.resetStyle(e.target);
    },
    onEachFeature(e, n) {
      n.on({
        mouseover: this.highlightFeature,
        mouseout: this.resetHighlight,
        click: this.onCountyClick
      });
      const s = n.bindTooltip(
        String(e.properties.NAME_3),
        {
          permanent: !0,
          direction: "center",
          className: "osmap-label"
        }
      );
      s.openTooltip(), this.tooltips.push(s);
    },
    onZoomChange(e) {
      this.map.getZoom() > 7 ? this.tooltips.forEach((n) => {
        n.openTooltip();
      }) : this.tooltips.forEach((n) => {
        n.closeTooltip();
      });
    },
    onMarkerOver(e) {
      this.current = e;
    },
    onMarkerOut() {
      this.current = null;
    },
    selectLocation(e) {
      this.selected = e;
    },
    closeSidebar() {
      this.selected = null;
    }
  },
  watch: {
    form: {
      handler: function(e, n) {
        this.searchTimer && clearTimeout(this.searchTimer), this.searchTimer = setTimeout(() => {
          this.doSearch();
        }, 500);
      },
      deep: !0
    }
  }
}, Id = "/static/images/location.png", kd = (e, n) => {
  const s = e.__vccOpts || e;
  for (const [a, l] of n)
    s[a] = l;
  return s;
}, Bd = { class: "container osmap-container" }, zd = /* @__PURE__ */ Y("h1", { class: "my-4" }, "Location finder", -1), Nd = { class: "osmap-inner" }, Zd = { class: "osmap-left d-flex flex-column" }, Rd = { class: "osmap-card px-3 py-4" }, Dd = /* @__PURE__ */ Y("h4", { class: "pb-3" }, " Find local history and tourist offices ", -1), Fd = { class: "form-group" }, jd = /* @__PURE__ */ Y("label", { for: "id_regions" }, "Select region", -1), Hd = { class: "form-group" }, $d = { class: "form-group input-search" }, Wd = /* @__PURE__ */ Y("label", { for: "id_search" }, "Search", -1), Ud = { class: "input-group input-search" }, Vd = /* @__PURE__ */ Y("div", { class: "search-icon" }, [
  /* @__PURE__ */ Y("svg", {
    width: "16",
    height: "16",
    viewBox: "0 0 16 16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, [
    /* @__PURE__ */ Y("path", {
      d: "M7.22222 13.4444C10.6587 13.4444 13.4444 10.6587 13.4444 7.22222C13.4444 3.78578 10.6587 1 7.22222 1C3.78578 1 1 3.78578 1 7.22222C1 10.6587 3.78578 13.4444 7.22222 13.4444Z",
      stroke: "#333333",
      "stroke-width": "2",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }),
    /* @__PURE__ */ Y("path", {
      d: "M15 15.0001L11.6166 11.6168",
      stroke: "#333333",
      "stroke-width": "2",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    })
  ])
], -1), Kd = { class: "osmap-card osmap-list flex-fill no-border-top" }, qd = ["onClick"], Gd = { class: "d-flex justify-content-between" }, Yd = { class: "d-flex" }, Jd = { class: "d-flex flex-column" }, Xd = /* @__PURE__ */ Y("div", { class: "d-flex align-items-center" }, [
  /* @__PURE__ */ Y("svg", {
    width: "18",
    height: "10",
    viewBox: "0 0 18 10",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, [
    /* @__PURE__ */ Y("path", {
      d: "M1 5H17",
      stroke: "#E4001A",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }),
    /* @__PURE__ */ Y("path", {
      d: "M13 1L17 5L13 9",
      stroke: "#E4001A",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    })
  ])
], -1), Qd = { class: "osmap-right d-flex" }, tp = /* @__PURE__ */ Y("div", {
  class: "osmap-card flex-fill",
  id: "osmap-map"
}, null, -1), ep = {
  key: 0,
  class: "osmap-sidebar"
}, ip = /* @__PURE__ */ Y("img", {
  class: "img-fluid",
  src: Id
}, null, -1), np = { class: "p-4" }, op = { class: "text-black text-strong" };
function sp(e, n) {
  const s = mf("v-select");
  return Vt(), re("div", Bd, [
    zd,
    Y("div", Nd, [
      Y("div", Zd, [
        Y("div", Rd, [
          Dd,
          Y("div", Fd, [
            jd,
            ne(s, {
              name: "regions",
              modelValue: e.form.region,
              "onUpdate:modelValue": n[0] || (n[0] = (a) => e.form.region = a),
              options: e.options.regions,
              reduce: (a) => a.id,
              label: "name"
            }, null, 8, ["modelValue", "options", "reduce"])
          ]),
          Y("div", Hd, [
            Y("div", $d, [
              Wd,
              Y("div", Ud, [
                po(Y("input", {
                  name: "query",
                  "onUpdate:modelValue": n[1] || (n[1] = (a) => e.form.query = a),
                  type: "text",
                  class: "form-control dropdown-toggle",
                  placeholder: "",
                  id: "id_search"
                }, null, 512), [
                  [vd, e.form.query]
                ]),
                Vd
              ])
            ])
          ]),
          Y("div", null, [
            Y("a", {
              href: "#",
              onClick: n[2] || (n[2] = Lo((...a) => e.clearAll && e.clearAll(...a), ["prevent"]))
            }, "Clear all")
          ])
        ]),
        Y("div", Kd, [
          (Vt(!0), re(ae, null, Es(e.results, (a, l) => (Vt(), re("div", {
            key: l,
            class: Ai(["osmap-list-item px-3 py-4", { active: e.current === a }]),
            onClick: (u) => e.selectLocation(a)
          }, [
            Y("div", Gd, [
              Y("div", Yd, [
                Y("div", Jd, [
                  Y("strong", null, ni(a.name), 1),
                  Y("p", null, ni(a.county) + ", " + ni(a.district), 1)
                ])
              ]),
              Xd
            ])
          ], 10, qd))), 128))
        ])
      ]),
      Y("div", Qd, [
        tp,
        e.selected ? (Vt(), re("div", ep, [
          ip,
          Y("div", np, [
            Y("h5", op, ni(e.selected.name), 1),
            Y("p", null, ni(e.selected.county) + ", " + ni(e.selected.district), 1),
            Y("p", null, [
              Y("a", {
                href: "#",
                onClick: n[3] || (n[3] = Lo((...a) => e.closeSidebar && e.closeSidebar(...a), ["prevent"]))
              }, "Close")
            ])
          ])
        ])) : _o("", !0)
      ])
    ])
  ]);
}
const rp = /* @__PURE__ */ kd(Ad, [["render", sp]]);
const ap = (e, n) => {
  for (const s of Object.keys(n))
    e.on(s, n[s]);
}, Yl = (e) => {
  for (const n of Object.keys(e)) {
    const s = e[n];
    s && si(s.cancel) && s.cancel();
  }
}, lp = (e) => !e || typeof e.charAt != "function" ? e : e.charAt(0).toUpperCase() + e.slice(1), si = (e) => typeof e == "function", Dt = (e, n, s) => {
  for (const a in s) {
    const l = "set" + lp(a);
    e[l] ? yn(
      () => s[a],
      (u, c) => {
        e[l](u, c);
      }
    ) : n[l] && yn(
      () => s[a],
      (u) => {
        n[l](u);
      }
    );
  }
}, zt = (e, n, s = {}) => {
  const a = { ...s };
  for (const l in e) {
    const u = n[l], c = e[l];
    u && (u && u.custom === !0 || c !== void 0 && (a[l] = c));
  }
  return a;
}, Qt = (e) => {
  const n = {};
  for (const s in e)
    if (s.startsWith("on") && !s.startsWith("onUpdate") && s !== "onReady") {
      const a = s.slice(2).toLocaleLowerCase();
      n[a] = e[s];
    }
  return n;
}, up = async (e) => {
  const n = await Promise.all([
    import("./marker-icon-2x-b9d2094a.mjs"),
    import("./marker-icon-b2ca459f.mjs"),
    import("./marker-shadow-eea80fe0.mjs")
  ]);
  delete e.Default.prototype._getIconUrl, e.Default.mergeOptions({
    iconRetinaUrl: n[0].default,
    iconUrl: n[1].default,
    shadowUrl: n[2].default
  });
}, lo = (e) => {
  const n = st(
    (...a) => console.warn(`Method ${e} has been invoked without being replaced`)
  ), s = (...a) => n.value(...a);
  return s.wrapped = n, ge(e, s), s;
}, uo = (e, n) => e.wrapped.value = n, Mt = typeof self == "object" && self.self === self && self || typeof global == "object" && global.global === global && global || globalThis, wt = (e) => {
  const n = Pt(e);
  if (!n)
    throw new Error(`Attempt to inject ${e} before it was provided.`);
  return n;
}, Ft = Symbol(), oe = Symbol(), Do = Symbol(), An = Symbol(), Jl = Symbol(), Xl = Symbol(), Ql = Symbol(), tu = Symbol(), eu = Symbol(), iu = Symbol(), nu = Symbol(), ou = Symbol(), In = {
  options: {
    type: Object,
    default: () => ({}),
    custom: !0
  }
}, kn = (e) => ({ options: e.options, methods: {} }), Qi = {
  ...In,
  pane: {
    type: String
  },
  attribution: {
    type: String
  },
  name: {
    type: String,
    custom: !0
  },
  layerType: {
    type: String,
    custom: !0
  },
  visible: {
    type: Boolean,
    custom: !0,
    default: !0
  }
}, Bn = (e, n, s) => {
  const a = wt(oe), l = wt(Do), { options: u, methods: c } = kn(e), f = zt(
    e,
    Qi,
    u
  ), p = () => a({ leafletObject: n.value }), g = () => l({ leafletObject: n.value }), b = {
    ...c,
    setAttribution(x) {
      g(), n.value.options.attribution = x, e.visible && p();
    },
    setName() {
      g(), e.visible && p();
    },
    setLayerType() {
      g(), e.visible && p();
    },
    setVisible(x) {
      n.value && (x ? p() : g());
    },
    bindPopup(x) {
      if (!n.value || !si(n.value.bindPopup)) {
        console.warn(
          "Attempt to bind popup before bindPopup method available on layer."
        );
        return;
      }
      n.value.bindPopup(x);
    },
    bindTooltip(x) {
      if (!n.value || !si(n.value.bindTooltip)) {
        console.warn(
          "Attempt to bind tooltip before bindTooltip method available on layer."
        );
        return;
      }
      n.value.bindTooltip(x);
    },
    unbindTooltip() {
      n.value && (si(n.value.closeTooltip) && n.value.closeTooltip(), si(n.value.unbindTooltip) && n.value.unbindTooltip());
    },
    unbindPopup() {
      n.value && (si(n.value.closePopup) && n.value.closePopup(), si(n.value.unbindPopup) && n.value.unbindPopup());
    },
    updateVisibleProp(x) {
      s.emit("update:visible", x);
    }
  };
  return ge(eu, b.bindPopup), ge(iu, b.bindTooltip), ge(nu, b.unbindPopup), ge(ou, b.unbindTooltip), En(() => {
    b.unbindPopup(), b.unbindTooltip(), g();
  }), { options: f, methods: b };
}, De = (e, n) => {
  if (e && n.default)
    return hi("div", { style: { display: "none" } }, n.default());
}, su = {
  ...Qi,
  interactive: {
    type: Boolean,
    default: void 0
  },
  bubblingMouseEvents: {
    type: Boolean,
    default: void 0
  }
}, hp = (e, n, s) => {
  const { options: a, methods: l } = Bn(
    e,
    n,
    s
  );
  return { options: zt(
    e,
    su,
    a
  ), methods: l };
}, rr = {
  ...su,
  stroke: {
    type: Boolean,
    default: void 0
  },
  color: {
    type: String
  },
  weight: {
    type: Number
  },
  opacity: {
    type: Number
  },
  lineCap: {
    type: String
  },
  lineJoin: {
    type: String
  },
  dashArray: {
    type: String
  },
  dashOffset: {
    type: String
  },
  fill: {
    type: Boolean,
    default: void 0
  },
  fillColor: {
    type: String
  },
  fillOpacity: {
    type: Number
  },
  fillRule: {
    type: String
  },
  className: {
    type: String
  }
}, ru = (e, n, s) => {
  const { options: a, methods: l } = hp(e, n, s), u = zt(
    e,
    rr,
    a
  ), c = wt(Do), f = {
    ...l,
    setStroke(p) {
      n.value.setStyle({ stroke: p });
    },
    setColor(p) {
      n.value.setStyle({ color: p });
    },
    setWeight(p) {
      n.value.setStyle({ weight: p });
    },
    setOpacity(p) {
      n.value.setStyle({ opacity: p });
    },
    setLineCap(p) {
      n.value.setStyle({ lineCap: p });
    },
    setLineJoin(p) {
      n.value.setStyle({ lineJoin: p });
    },
    setDashArray(p) {
      n.value.setStyle({ dashArray: p });
    },
    setDashOffset(p) {
      n.value.setStyle({ dashOffset: p });
    },
    setFill(p) {
      n.value.setStyle({ fill: p });
    },
    setFillColor(p) {
      n.value.setStyle({ fillColor: p });
    },
    setFillOpacity(p) {
      n.value.setStyle({ fillOpacity: p });
    },
    setFillRule(p) {
      n.value.setStyle({ fillRule: p });
    },
    setClassName(p) {
      n.value.setStyle({ className: p });
    }
  };
  return Ii(() => {
    c({ leafletObject: n.value });
  }), { options: u, methods: f };
}, ar = {
  ...rr,
  /**
   * Radius of the marker in pixels.
   */
  radius: {
    type: Number
  },
  latLng: {
    type: [Object, Array],
    required: !0,
    custom: !0
  }
}, au = (e, n, s) => {
  const { options: a, methods: l } = ru(
    e,
    n,
    s
  ), u = zt(
    e,
    ar,
    a
  ), c = {
    ...l,
    setRadius(f) {
      n.value.setRadius(f);
    },
    setLatLng(f) {
      n.value.setLatLng(f);
    }
  };
  return { options: u, methods: c };
}, lu = {
  ...ar,
  /**
   * Radius of the circle in meters.
   */
  radius: {
    type: Number
  }
}, cp = (e, n, s) => {
  const { options: a, methods: l } = au(e, n, s), u = zt(
    e,
    lu,
    a
  ), c = {
    ...l
  };
  return { options: u, methods: c };
};
Rt({
  name: "LCircle",
  props: lu,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { options: c, methods: f } = cp(e, s, n);
    return At(async () => {
      const { circle: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(p(e.latLng, c));
      const g = Qt(n.attrs);
      s.value.on(g), Dt(f, s.value, e), u({
        ...e,
        ...f,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
Rt({
  name: "LCircleMarker",
  props: ar,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { options: c, methods: f } = au(
      e,
      s,
      n
    );
    return At(async () => {
      const { circleMarker: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        p(e.latLng, c)
      );
      const g = Qt(n.attrs);
      s.value.on(g), Dt(f, s.value, e), u({
        ...e,
        ...f,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
const tn = {
  ...In,
  position: {
    type: String
  }
}, zn = (e, n) => {
  const { options: s, methods: a } = kn(e), l = zt(
    e,
    tn,
    s
  ), u = {
    ...a,
    setPosition(c) {
      n.value && n.value.setPosition(c);
    }
  };
  return En(() => {
    n.value && n.value.remove();
  }), { options: l, methods: u };
}, fp = (e) => e.default ? hi("div", { ref: "root" }, e.default()) : null;
Rt({
  name: "LControl",
  props: {
    ...tn,
    disableClickPropagation: {
      type: Boolean,
      custom: !0,
      default: !0
    },
    disableScrollPropagation: {
      type: Boolean,
      custom: !0,
      default: !1
    }
  },
  setup(e, n) {
    const s = st(), a = st(), l = Pt(Ft), u = wt(An), { options: c, methods: f } = zn(e, s);
    return At(async () => {
      const { Control: p, DomEvent: g } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs"), b = p.extend({
        onAdd() {
          return a.value;
        }
      });
      s.value = Bt(new b(c)), Dt(f, s.value, e), u({ leafletObject: s.value }), e.disableClickPropagation && a.value && g.disableClickPropagation(a.value), e.disableScrollPropagation && a.value && g.disableScrollPropagation(a.value), Et(() => n.emit("ready", s.value));
    }), { root: a, leafletObject: s };
  },
  render() {
    return fp(this.$slots);
  }
});
const uu = {
  ...tn,
  prefix: {
    type: String
  }
}, dp = (e, n) => {
  const { options: s, methods: a } = zn(
    e,
    n
  ), l = zt(
    e,
    uu,
    s
  ), u = {
    ...a,
    setPrefix(c) {
      n.value.setPrefix(c);
    }
  };
  return { options: l, methods: u };
};
Rt({
  name: "LControlAttribution",
  props: uu,
  setup(e, n) {
    const s = st(), a = Pt(Ft), l = wt(An), { options: u, methods: c } = dp(e, s);
    return At(async () => {
      const { control: f } = a ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        f.attribution(u)
      ), Dt(c, s.value, e), l({ leafletObject: s.value }), Et(() => n.emit("ready", s.value));
    }), { leafletObject: s };
  },
  render() {
    return null;
  }
});
const hu = {
  ...tn,
  collapsed: {
    type: Boolean,
    default: void 0
  },
  autoZIndex: {
    type: Boolean,
    default: void 0
  },
  hideSingleBase: {
    type: Boolean,
    default: void 0
  },
  sortLayers: {
    type: Boolean,
    default: void 0
  },
  sortFunction: {
    type: Function
  }
}, pp = (e, n) => {
  const { options: s } = zn(e, n);
  return { options: zt(
    e,
    hu,
    s
  ), methods: {
    addLayer(a) {
      a.layerType === "base" ? n.value.addBaseLayer(a.leafletObject, a.name) : a.layerType === "overlay" && n.value.addOverlay(a.leafletObject, a.name);
    },
    removeLayer(a) {
      n.value.removeLayer(a.leafletObject);
    }
  } };
};
Rt({
  name: "LControlLayers",
  props: hu,
  setup(e, n) {
    const s = st(), a = Pt(Ft), l = wt(Jl), { options: u, methods: c } = pp(e, s);
    return At(async () => {
      const { control: f } = a ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        f.layers(void 0, void 0, u)
      ), Dt(c, s.value, e), l({
        ...e,
        ...c,
        leafletObject: s.value
      }), Et(() => n.emit("ready", s.value));
    }), { leafletObject: s };
  },
  render() {
    return null;
  }
});
const cu = {
  ...tn,
  maxWidth: {
    type: Number
  },
  metric: {
    type: Boolean,
    default: void 0
  },
  imperial: {
    type: Boolean,
    default: void 0
  },
  updateWhenIdle: {
    type: Boolean,
    default: void 0
  }
}, mp = (e, n) => {
  const { options: s, methods: a } = zn(
    e,
    n
  );
  return { options: zt(
    e,
    cu,
    s
  ), methods: a };
};
Rt({
  name: "LControlScale",
  props: cu,
  setup(e, n) {
    const s = st(), a = Pt(Ft), l = wt(An), { options: u, methods: c } = mp(e, s);
    return At(async () => {
      const { control: f } = a ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(f.scale(u)), Dt(c, s.value, e), l({ leafletObject: s.value }), Et(() => n.emit("ready", s.value));
    }), { leafletObject: s };
  },
  render() {
    return null;
  }
});
const fu = {
  ...tn,
  zoomInText: {
    type: String
  },
  zoomInTitle: {
    type: String
  },
  zoomOutText: {
    type: String
  },
  zoomOutTitle: {
    type: String
  }
}, _p = (e, n) => {
  const { options: s, methods: a } = zn(
    e,
    n
  );
  return { options: zt(
    e,
    fu,
    s
  ), methods: a };
};
Rt({
  name: "LControlZoom",
  props: fu,
  setup(e, n) {
    const s = st(), a = Pt(Ft), l = wt(An), { options: u, methods: c } = _p(e, s);
    return At(async () => {
      const { control: f } = a ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(f.zoom(u)), Dt(c, s.value, e), l({ leafletObject: s.value }), Et(() => n.emit("ready", s.value));
    }), { leafletObject: s };
  },
  render() {
    return null;
  }
});
const Fo = {
  ...Qi
}, lr = (e, n, s) => {
  const { options: a, methods: l } = Bn(
    e,
    n,
    s
  ), u = zt(
    e,
    Fo,
    a
  ), c = {
    ...l,
    addLayer(f) {
      n.value.addLayer(f.leafletObject);
    },
    removeLayer(f) {
      n.value.removeLayer(f.leafletObject);
    }
  };
  return ge(oe, c.addLayer), ge(Do, c.removeLayer), { options: u, methods: c };
}, du = {
  ...Fo
}, gp = (e, n, s) => {
  const { options: a, methods: l } = lr(
    e,
    n,
    s
  ), u = zt(
    e,
    du,
    a
  ), c = {
    ...l
  };
  return { options: u, methods: c };
};
Rt({
  props: du,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { methods: c, options: f } = gp(
      e,
      s,
      n
    );
    return At(async () => {
      const { featureGroup: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        p(void 0, f)
      );
      const g = Qt(n.attrs);
      s.value.on(g), Dt(c, s.value, e), u({
        ...e,
        ...c,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
const pu = {
  ...Fo,
  geojson: {
    type: [Object, Array],
    custom: !0
  },
  optionsStyle: {
    type: Function,
    custom: !0
  }
}, vp = (e, n, s) => {
  const { options: a, methods: l } = lr(
    e,
    n,
    s
  ), u = zt(
    e,
    pu,
    a
  );
  Object.prototype.hasOwnProperty.call(e, "optionsStyle") && (u.style = e.optionsStyle);
  const c = {
    ...l,
    setGeojson(f) {
      n.value.clearLayers(), n.value.addData(f);
    },
    setOptionsStyle(f) {
      n.value.setStyle(f);
    },
    getGeoJSONData() {
      return n.value.toGeoJSON();
    },
    getBounds() {
      return n.value.getBounds();
    }
  };
  return { options: u, methods: c };
}, yp = Rt({
  props: pu,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { methods: c, options: f } = vp(e, s, n);
    return At(async () => {
      const { geoJSON: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(p(e.geojson, f));
      const g = Qt(n.attrs);
      s.value.on(g), Dt(c, s.value, e), u({
        ...e,
        ...c,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
}), ur = {
  ...Qi,
  opacity: {
    type: Number
  },
  zIndex: {
    type: Number
  },
  tileSize: {
    type: [Number, Array, Object]
  },
  noWrap: {
    type: Boolean,
    default: void 0
  },
  minZoom: {
    type: Number
  },
  maxZoom: {
    type: Number
  }
}, mu = (e, n, s) => {
  const { options: a, methods: l } = Bn(
    e,
    n,
    s
  ), u = zt(
    e,
    ur,
    a
  ), c = {
    ...l,
    setTileComponent() {
      var f;
      (f = n.value) == null || f.redraw();
    }
  };
  return En(() => {
    n.value.off();
  }), { options: u, methods: c };
}, bp = (e, n, s, a) => e.extend({
  initialize(l) {
    this.tileComponents = {}, this.on("tileunload", this._unloadTile), s.setOptions(this, l);
  },
  createTile(l) {
    const u = this._tileCoordsToKey(l);
    this.tileComponents[u] = n.create("div");
    const c = hi({ setup: a, props: ["coords"] }, { coords: l });
    return xd(c, this.tileComponents[u]), this.tileComponents[u];
  },
  _unloadTile(l) {
    const u = this._tileCoordsToKey(l.coords);
    this.tileComponents[u] && (this.tileComponents[u].innerHTML = "", this.tileComponents[u] = void 0);
  }
});
Rt({
  props: {
    ...ur,
    childRender: {
      type: Function,
      required: !0
    }
  },
  setup(e, n) {
    const s = st(), a = st(null), l = st(!1), u = Pt(Ft), c = wt(oe), { options: f, methods: p } = mu(e, s, n);
    return At(async () => {
      const { GridLayer: g, DomUtil: b, Util: x } = u ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs"), T = bp(
        g,
        b,
        x,
        e.childRender
      );
      s.value = Bt(new T(f));
      const k = Qt(n.attrs);
      s.value.on(k), Dt(p, s.value, e), c({
        ...e,
        ...p,
        leafletObject: s.value
      }), l.value = !0, Et(() => n.emit("ready", s.value));
    }), { root: a, ready: l, leafletObject: s };
  },
  render() {
    return this.ready ? hi("div", { style: { display: "none" }, ref: "root" }) : null;
  }
});
const Ka = {
  iconUrl: {
    type: String
  },
  iconRetinaUrl: {
    type: String
  },
  iconSize: {
    type: [Object, Array]
  },
  iconAnchor: {
    type: [Object, Array]
  },
  popupAnchor: {
    type: [Object, Array]
  },
  tooltipAnchor: {
    type: [Object, Array]
  },
  shadowUrl: {
    type: String
  },
  shadowRetinaUrl: {
    type: String
  },
  shadowSize: {
    type: [Object, Array]
  },
  shadowAnchor: {
    type: [Object, Array]
  },
  bgPos: {
    type: [Object, Array]
  },
  className: {
    type: String
  }
};
Rt({
  name: "LIcon",
  props: {
    ...Ka,
    ...In
  },
  setup(e, n) {
    const s = st(), a = Pt(Ft), l = wt(Xl), u = wt(Ql), c = wt(tu);
    let f, p, g, b, x;
    const T = (tt, X, xt) => {
      const D = tt && tt.innerHTML;
      if (!X) {
        xt && x && l() && u(D);
        return;
      }
      const rt = Qt(n.attrs);
      x && p(x, rt);
      const { options: gt } = kn(e), Kt = zt(
        e,
        Ka,
        gt
      );
      D && (Kt.html = D), x = Kt.html ? g(Kt) : b(Kt), f(x, rt), c(x);
    }, k = () => {
      Et(() => T(s.value, !0, !1));
    }, U = () => {
      Et(() => T(s.value, !1, !0));
    }, E = {
      setIconUrl: k,
      setIconRetinaUrl: k,
      setIconSize: k,
      setIconAnchor: k,
      setPopupAnchor: k,
      setTooltipAnchor: k,
      setShadowUrl: k,
      setShadowRetinaUrl: k,
      setShadowAnchor: k,
      setBgPos: k,
      setClassName: k,
      setHtml: k
    };
    return At(async () => {
      const {
        DomEvent: tt,
        divIcon: X,
        icon: xt
      } = a ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      f = tt.on, p = tt.off, g = X, b = xt, Dt(E, {}, e), new MutationObserver(U).observe(s.value, {
        attributes: !0,
        childList: !0,
        characterData: !0,
        subtree: !0
      }), k();
    }), { root: s };
  },
  render() {
    const e = this.$slots.default ? this.$slots.default() : void 0;
    return hi("div", { ref: "root" }, e);
  }
});
const _u = {
  ...Qi,
  opacity: {
    type: Number
  },
  alt: {
    type: String
  },
  interactive: {
    type: Boolean,
    default: void 0
  },
  crossOrigin: {
    type: Boolean,
    default: void 0
  },
  errorOverlayUrl: {
    type: String
  },
  zIndex: {
    type: Number
  },
  className: {
    type: String
  },
  url: {
    type: String,
    required: !0,
    custom: !0
  },
  bounds: {
    type: [Array, Object],
    required: !0,
    custom: !0
  }
}, wp = (e, n, s) => {
  const { options: a, methods: l } = Bn(
    e,
    n,
    s
  ), u = zt(
    e,
    _u,
    a
  ), c = {
    ...l,
    /**
     * Sets the opacity of the overlay.
     * @param {number} opacity
     */
    setOpacity(f) {
      return n.value.setOpacity(f);
    },
    /**
     * Changes the URL of the image.
     * @param {string} url
     */
    setUrl(f) {
      return n.value.setUrl(f);
    },
    /**
     * Update the bounds that this ImageOverlay covers
     * @param {LatLngBounds | Array<Array<number>>} bounds
     */
    setBounds(f) {
      return n.value.setBounds(f);
    },
    /**
     * Get the bounds that this ImageOverlay covers
     * @returns {LatLngBounds}
     */
    getBounds() {
      return n.value.getBounds();
    },
    /**
     * Returns the instance of HTMLImageElement used by this overlay.
     * @returns {HTMLElement}
     */
    getElement() {
      return n.value.getElement();
    },
    /**
     * Brings the layer to the top of all overlays.
     */
    bringToFront() {
      return n.value.bringToFront();
    },
    /**
     * Brings the layer to the bottom of all overlays.
     */
    bringToBack() {
      return n.value.bringToBack();
    },
    /**
     * Changes the zIndex of the image overlay.
     * @param {number} zIndex
     */
    setZIndex(f) {
      return n.value.setZIndex(f);
    }
  };
  return { options: u, methods: c };
};
Rt({
  name: "LImageOverlay",
  props: _u,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { options: c, methods: f } = wp(
      e,
      s,
      n
    );
    return At(async () => {
      const { imageOverlay: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        p(e.url, e.bounds, c)
      );
      const g = Qt(n.attrs);
      s.value.on(g), Dt(f, s.value, e), u({
        ...e,
        ...f,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
Rt({
  props: Fo,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { methods: c } = lr(e, s, n);
    return At(async () => {
      const { layerGroup: f } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        f(void 0, e.options)
      );
      const p = Qt(n.attrs);
      s.value.on(p), Dt(c, s.value, e), u({
        ...e,
        ...c,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
function gu(e, n, s) {
  var a, l, u;
  n === void 0 && (n = 50), s === void 0 && (s = {});
  var c = (a = s.isImmediate) != null && a, f = (l = s.callback) != null && l, p = s.maxWait, g = Date.now(), b = [];
  function x() {
    if (p !== void 0) {
      var k = Date.now() - g;
      if (k + n >= p)
        return p - k;
    }
    return n;
  }
  var T = function() {
    var k = [].slice.call(arguments), U = this;
    return new Promise(function(E, tt) {
      var X = c && u === void 0;
      if (u !== void 0 && clearTimeout(u), u = setTimeout(function() {
        if (u = void 0, g = Date.now(), !c) {
          var D = e.apply(U, k);
          f && f(D), b.forEach(function(rt) {
            return (0, rt.resolve)(D);
          }), b = [];
        }
      }, x()), X) {
        var xt = e.apply(U, k);
        return f && f(xt), E(xt);
      }
      b.push({ resolve: E, reject: tt });
    });
  };
  return T.cancel = function(k) {
    u !== void 0 && clearTimeout(u), b.forEach(function(U) {
      return (0, U.reject)(k);
    }), b = [];
  }, T;
}
const qa = {
  ...In,
  /**
   * The center of the map, supports .sync modifier
   */
  center: {
    type: [Object, Array]
  },
  /**
   * The bounds of the map, supports .sync modifier
   */
  bounds: {
    type: [Array, Object]
  },
  /**
   * The max bounds of the map
   */
  maxBounds: {
    type: [Array, Object]
  },
  /**
   * The zoom of the map, supports .sync modifier
   */
  zoom: {
    type: Number
  },
  /**
   * The minZoom of the map
   */
  minZoom: {
    type: Number
  },
  /**
   * The maxZoom of the map
   */
  maxZoom: {
    type: Number
  },
  /**
   * The paddingBottomRight of the map
   */
  paddingBottomRight: {
    type: [Object, Array]
  },
  /**
   * The paddingTopLeft of the map
   */
  paddingTopLeft: {
    type: Object
  },
  /**
   * The padding of the map
   */
  padding: {
    type: Object
  },
  /**
   * The worldCopyJump option for the map
   */
  worldCopyJump: {
    type: Boolean,
    default: void 0
  },
  /**
   * The CRS to use for the map. Can be an object that defines a coordinate reference
   * system for projecting geographical points into screen coordinates and back
   * (see https://leafletjs.com/reference-1.7.1.html#crs-l-crs-base), or a string
   * name identifying one of Leaflet's defined CRSs, such as "EPSG4326".
   */
  crs: {
    type: [String, Object]
  },
  maxBoundsViscosity: {
    type: Number
  },
  inertia: {
    type: Boolean,
    default: void 0
  },
  inertiaDeceleration: {
    type: Number
  },
  inertiaMaxSpeed: {
    type: Number
  },
  easeLinearity: {
    type: Number
  },
  zoomAnimation: {
    type: Boolean,
    default: void 0
  },
  zoomAnimationThreshold: {
    type: Number
  },
  fadeAnimation: {
    type: Boolean,
    default: void 0
  },
  markerZoomAnimation: {
    type: Boolean,
    default: void 0
  },
  noBlockingAnimations: {
    type: Boolean,
    default: void 0
  },
  useGlobalLeaflet: {
    type: Boolean,
    default: !0,
    custom: !0
  }
}, xp = Rt({
  emits: ["ready", "update:zoom", "update:center", "update:bounds"],
  props: qa,
  setup(e, n) {
    const s = st(), a = Ao({
      ready: !1,
      layersToAdd: [],
      layersInControl: []
    }), { options: l } = kn(e), u = zt(
      e,
      qa,
      l
    ), c = lo(oe), f = lo(Do), p = lo(An), g = lo(
      Jl
    );
    ge(Ft, e.useGlobalLeaflet);
    const b = gn(() => {
      const E = {};
      return e.noBlockingAnimations && (E.animate = !1), E;
    }), x = gn(() => {
      const E = b.value;
      return e.padding && (E.padding = e.padding), e.paddingTopLeft && (E.paddingTopLeft = e.paddingTopLeft), e.paddingBottomRight && (E.paddingBottomRight = e.paddingBottomRight), E;
    }), T = {
      moveend: gu((E) => {
        a.leafletRef && (n.emit("update:zoom", a.leafletRef.getZoom()), n.emit("update:center", a.leafletRef.getCenter()), n.emit("update:bounds", a.leafletRef.getBounds()));
      }),
      overlayadd(E) {
        const tt = a.layersInControl.find((X) => X.name === E.name);
        tt && tt.updateVisibleProp(!0);
      },
      overlayremove(E) {
        const tt = a.layersInControl.find((X) => X.name === E.name);
        tt && tt.updateVisibleProp(!1);
      }
    };
    At(async () => {
      e.useGlobalLeaflet && (Mt.L = Mt.L || await Promise.resolve().then(() => Od));
      const {
        map: E,
        CRS: tt,
        Icon: X,
        latLngBounds: xt,
        latLng: D,
        DomEvent: rt,
        stamp: gt
      } = e.useGlobalLeaflet ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      try {
        u.beforeMapMount && await u.beforeMapMount();
      } catch (j) {
        console.error(
          `The following error occurred running the provided beforeMapMount hook ${j.message}`
        );
      }
      await up(X);
      const Kt = typeof u.crs == "string" ? tt[u.crs] : u.crs;
      u.crs = Kt || tt.EPSG3857;
      const qt = {
        addLayer(j) {
          j.layerType !== void 0 && (a.layerControl === void 0 ? a.layersToAdd.push(j) : a.layersInControl.find(
            (V) => gt(V.leafletObject) === gt(j.leafletObject)
          ) || (a.layerControl.addLayer(j), a.layersInControl.push(j))), j.visible !== !1 && a.leafletRef.addLayer(j.leafletObject);
        },
        removeLayer(j) {
          j.layerType !== void 0 && (a.layerControl === void 0 ? a.layersToAdd = a.layersToAdd.filter(
            (V) => V.name !== j.name
          ) : (a.layerControl.removeLayer(j.leafletObject), a.layersInControl = a.layersInControl.filter(
            (V) => gt(V.leafletObject) !== gt(j.leafletObject)
          ))), a.leafletRef.removeLayer(j.leafletObject);
        },
        registerLayerControl(j) {
          a.layerControl = j, a.layersToAdd.forEach((V) => {
            a.layerControl.addLayer(V);
          }), a.layersToAdd = [], p(j);
        },
        registerControl(j) {
          a.leafletRef.addControl(j.leafletObject);
        },
        setZoom(j) {
          const V = a.leafletRef.getZoom();
          j !== V && a.leafletRef.setZoom(j, b.value);
        },
        setCrs(j) {
          const V = a.leafletRef.getBounds();
          a.leafletRef.options.crs = j, a.leafletRef.fitBounds(V, {
            animate: !1,
            padding: [0, 0]
          });
        },
        fitBounds(j) {
          a.leafletRef.fitBounds(j, x.value);
        },
        setBounds(j) {
          if (!j)
            return;
          const V = xt(j);
          V.isValid() && !(a.lastSetBounds || a.leafletRef.getBounds()).equals(V, 0) && (a.lastSetBounds = V, a.leafletRef.fitBounds(V));
        },
        setCenter(j) {
          if (j == null)
            return;
          const V = D(j), ut = a.lastSetCenter || a.leafletRef.getCenter();
          (ut.lat !== V.lat || ut.lng !== V.lng) && (a.lastSetCenter = V, a.leafletRef.panTo(V, b.value));
        }
      };
      uo(c, qt.addLayer), uo(f, qt.removeLayer), uo(p, qt.registerControl), uo(g, qt.registerLayerControl), a.leafletRef = Bt(E(s.value, u)), Dt(qt, a.leafletRef, e);
      const Q = Qt(n.attrs);
      ap(a.leafletRef, T), rt.on(a.leafletRef.getContainer(), Q), a.ready = !0, Et(() => n.emit("ready", a.leafletRef));
    }), Ii(() => {
      Yl(T), a.leafletRef && (a.leafletRef.off(), a.leafletRef.remove());
    });
    const k = gn(() => a.leafletRef), U = gn(() => a.ready);
    return { root: s, ready: U, leafletObject: k };
  },
  render() {
    return hi(
      "div",
      { style: { width: "100%", height: "100%" }, ref: "root" },
      this.ready && this.$slots.default ? this.$slots.default() : {}
    );
  }
}), Lp = ["Symbol(Comment)", "Symbol(Text)"], Pp = ["LTooltip", "LPopup"], vu = {
  ...Qi,
  draggable: {
    type: Boolean,
    default: void 0
  },
  icon: {
    type: [Object]
  },
  zIndexOffset: {
    type: Number
  },
  latLng: {
    type: [Object, Array],
    custom: !0,
    required: !0
  }
}, Tp = (e, n, s) => {
  const { options: a, methods: l } = Bn(
    e,
    n,
    s
  ), u = zt(
    e,
    vu,
    a
  ), c = {
    ...l,
    setDraggable(f) {
      n.value.dragging && (f ? n.value.dragging.enable() : n.value.dragging.disable());
    },
    latLngSync(f) {
      s.emit("update:latLng", f.latlng), s.emit("update:lat-lng", f.latlng);
    },
    setLatLng(f) {
      if (f != null && n.value) {
        const p = n.value.getLatLng();
        (!p || !p.equals(f)) && n.value.setLatLng(f);
      }
    }
  };
  return { options: u, methods: c };
}, Cp = (e, n) => {
  const s = n.slots.default && n.slots.default();
  return s && s.length && s.some(Op);
};
function Op(e) {
  return !(Lp.includes(e.type.toString()) || Pp.includes(e.type.name));
}
const Sp = Rt({
  name: "LMarker",
  props: vu,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe);
    ge(
      Xl,
      () => {
        var g;
        return !!((g = s.value) != null && g.getElement());
      }
    ), ge(Ql, (g) => {
      var b, x;
      const T = si((b = s.value) == null ? void 0 : b.getElement) && ((x = s.value) == null ? void 0 : x.getElement());
      T && (T.innerHTML = g);
    }), ge(
      tu,
      (g) => {
        var b;
        return ((b = s.value) == null ? void 0 : b.setIcon) && s.value.setIcon(g);
      }
    );
    const { options: c, methods: f } = Tp(e, s, n), p = {
      moveHandler: gu(f.latLngSync)
    };
    return At(async () => {
      const { marker: g, divIcon: b } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      Cp(c, n) && (c.icon = b({ className: "" })), s.value = Bt(g(e.latLng, c));
      const x = Qt(n.attrs);
      s.value.on(x), s.value.on("move", p.moveHandler), Dt(f, s.value, e), u({
        ...e,
        ...f,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), Ii(() => Yl(p)), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
}), hr = {
  ...rr,
  smoothFactor: {
    type: Number
  },
  noClip: {
    type: Boolean,
    default: void 0
  },
  latLngs: {
    type: Array,
    required: !0,
    custom: !0
  }
}, yu = (e, n, s) => {
  const { options: a, methods: l } = ru(
    e,
    n,
    s
  ), u = zt(
    e,
    hr,
    a
  ), c = {
    ...l,
    setSmoothFactor(f) {
      n.value.setStyle({ smoothFactor: f });
    },
    setNoClip(f) {
      n.value.setStyle({ noClip: f });
    },
    addLatLng(f) {
      n.value.addLatLng(f);
    }
  };
  return { options: u, methods: c };
}, Po = {
  ...hr
}, bu = (e, n, s) => {
  const { options: a, methods: l } = yu(
    e,
    n,
    s
  ), u = zt(
    e,
    Po,
    a
  ), c = {
    ...l,
    toGeoJSON(f) {
      return n.value.toGeoJSON(f);
    }
  };
  return { options: u, methods: c };
}, Mp = Rt({
  name: "LPolygon",
  props: Po,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { options: c, methods: f } = bu(e, s, n);
    return At(async () => {
      const { polygon: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(p(e.latLngs, c));
      const g = Qt(n.attrs);
      s.value.on(g), Dt(f, s.value, e), u({
        ...e,
        ...f,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
Rt({
  name: "LPolyline",
  props: hr,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { options: c, methods: f } = yu(e, s, n);
    return At(async () => {
      const { polyline: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        p(e.latLngs, c)
      );
      const g = Qt(n.attrs);
      s.value.on(g), Dt(f, s.value, e), u({
        ...e,
        ...f,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
const wu = {
  ...In,
  content: {
    type: String,
    default: null
  }
}, xu = (e, n) => {
  const { options: s, methods: a } = kn(e), l = {
    ...a,
    setContent(u) {
      n.value && u !== null && u !== void 0 && n.value.setContent(u);
    }
  };
  return { options: s, methods: l };
}, Lu = (e) => e.default ? hi("div", { ref: "root" }, e.default()) : null, Ep = {
  ...wu,
  latLng: {
    type: [Object, Array],
    default: () => []
  }
}, Ap = (e, n) => {
  const { options: s, methods: a } = xu(e, n);
  return { options: s, methods: a };
};
Rt({
  name: "LPopup",
  props: Ep,
  setup(e, n) {
    const s = st(), a = st(null), l = Pt(Ft), u = wt(eu), c = wt(nu), { options: f, methods: p } = Ap(e, s);
    return At(async () => {
      const { popup: g } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(g(f)), e.latLng !== void 0 && s.value.setLatLng(e.latLng), Dt(p, s.value, e);
      const b = Qt(n.attrs);
      s.value.on(b), s.value.setContent(e.content || a.value || ""), u(s.value), Et(() => n.emit("ready", s.value));
    }), Ii(() => {
      c();
    }), { root: a, leafletObject: s };
  },
  render() {
    return Lu(this.$slots);
  }
});
const Pu = {
  ...Po,
  latLngs: {
    ...Po.latLngs,
    required: !1
  },
  bounds: {
    type: Object,
    custom: !0
  }
}, Ip = (e, n, s) => {
  const { options: a, methods: l } = bu(
    e,
    n,
    s
  ), u = zt(
    e,
    Pu,
    a
  ), c = {
    ...l,
    setBounds(f) {
      n.value.setBounds(f);
    },
    setLatLngs(f) {
      n.value.setBounds(f);
    }
  };
  return { options: u, methods: c };
};
Rt({
  name: "LRectangle",
  props: Pu,
  setup(e, n) {
    const s = st(), a = st(!1), l = Pt(Ft), u = wt(oe), { options: c, methods: f } = Ip(e, s, n);
    return At(async () => {
      const { rectangle: p, latLngBounds: g } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs"), b = e.bounds ? g(e.bounds) : g(e.latLngs || []);
      s.value = Bt(p(b, c));
      const x = Qt(n.attrs);
      s.value.on(x), Dt(f, s.value, e), u({
        ...e,
        ...f,
        leafletObject: s.value
      }), a.value = !0, Et(() => n.emit("ready", s.value));
    }), { ready: a, leafletObject: s };
  },
  render() {
    return De(this.ready, this.$slots);
  }
});
const cr = {
  ...ur,
  tms: {
    type: Boolean,
    default: void 0
  },
  subdomains: {
    type: [String, Array],
    validator: (e) => typeof e == "string" ? !0 : Array.isArray(e) ? e.every((n) => typeof n == "string") : !1
  },
  detectRetina: {
    type: Boolean,
    default: void 0
  },
  url: {
    type: String,
    required: !0,
    custom: !0
  }
}, Tu = (e, n, s) => {
  const { options: a, methods: l } = mu(e, n, s), u = zt(
    e,
    cr,
    a
  ), c = {
    ...l
  };
  return { options: u, methods: c };
}, kp = Rt({
  props: cr,
  setup(e, n) {
    const s = st(), a = Pt(Ft), l = wt(oe), { options: u, methods: c } = Tu(e, s, n);
    return At(async () => {
      const { tileLayer: f } = a ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(f(e.url, u));
      const p = Qt(n.attrs);
      s.value.on(p), Dt(c, s.value, e), l({
        ...e,
        ...c,
        leafletObject: s.value
      }), Et(() => n.emit("ready", s.value));
    }), { leafletObject: s };
  },
  render() {
    return null;
  }
}), Bp = {
  ...wu
}, zp = (e, n) => {
  const { options: s, methods: a } = xu(e, n), l = wt(ou);
  return Ii(() => {
    l();
  }), { options: s, methods: a };
};
Rt({
  name: "LTooltip",
  props: Bp,
  setup(e, n) {
    const s = st(), a = st(null), l = Pt(Ft), u = wt(iu), { options: c, methods: f } = zp(e, s);
    return At(async () => {
      const { tooltip: p } = l ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(p(c)), Dt(f, s.value, e);
      const g = Qt(n.attrs);
      s.value.on(g), s.value.setContent(e.content || a.value || ""), u(s.value), Et(() => n.emit("ready", s.value));
    }), { root: a, leafletObject: s };
  },
  render() {
    return Lu(this.$slots);
  }
});
const Cu = {
  ...cr,
  layers: {
    type: String,
    required: !0
  },
  styles: {
    type: String
  },
  format: {
    type: String
  },
  transparent: {
    type: Boolean,
    default: void 0
  },
  version: {
    type: String
  },
  crs: {
    type: Object
  },
  uppercase: {
    type: Boolean,
    default: void 0
  }
}, Np = (e, n, s) => {
  const { options: a, methods: l } = Tu(e, n, s);
  return {
    options: zt(
      e,
      Cu,
      a
    ),
    methods: {
      ...l
    }
  };
};
Rt({
  props: Cu,
  setup(e, n) {
    const s = st(), a = Pt(Ft), l = wt(oe), { options: u, methods: c } = Np(
      e,
      s,
      n
    );
    return At(async () => {
      const { tileLayer: f } = a ? Mt.L : await import("./leaflet-src.esm-271ccf8c.mjs");
      s.value = Bt(
        f.wms(e.url, u)
      );
      const p = Qt(n.attrs);
      s.value.on(p), Dt(c, s.value, e), l({
        ...e,
        ...c,
        leafletObject: s.value
      }), Et(() => n.emit("ready", s.value));
    }), { leafletObject: s };
  },
  render() {
    return null;
  }
});
var Zp = Object.defineProperty, Rp = Object.defineProperties, Dp = Object.getOwnPropertyDescriptors, Ga = Object.getOwnPropertySymbols, Fp = Object.prototype.hasOwnProperty, jp = Object.prototype.propertyIsEnumerable, Ya = (e, n, s) => n in e ? Zp(e, n, { enumerable: !0, configurable: !0, writable: !0, value: s }) : e[n] = s, Hi = (e, n) => {
  for (var s in n || (n = {}))
    Fp.call(n, s) && Ya(e, s, n[s]);
  if (Ga)
    for (var s of Ga(n))
      jp.call(n, s) && Ya(e, s, n[s]);
  return e;
}, Ja = (e, n) => Rp(e, Dp(n));
const Hp = {
  props: {
    autoscroll: {
      type: Boolean,
      default: !0
    }
  },
  watch: {
    typeAheadPointer() {
      this.autoscroll && this.maybeAdjustScroll();
    },
    open(e) {
      this.autoscroll && e && this.$nextTick(() => this.maybeAdjustScroll());
    }
  },
  methods: {
    maybeAdjustScroll() {
      var e;
      const n = ((e = this.$refs.dropdownMenu) == null ? void 0 : e.children[this.typeAheadPointer]) || !1;
      if (n) {
        const s = this.getDropdownViewport(), { top: a, bottom: l, height: u } = n.getBoundingClientRect();
        if (a < s.top)
          return this.$refs.dropdownMenu.scrollTop = n.offsetTop;
        if (l > s.bottom)
          return this.$refs.dropdownMenu.scrollTop = n.offsetTop - (s.height - u);
      }
    },
    getDropdownViewport() {
      return this.$refs.dropdownMenu ? this.$refs.dropdownMenu.getBoundingClientRect() : {
        height: 0,
        top: 0,
        bottom: 0
      };
    }
  }
}, $p = {
  data() {
    return {
      typeAheadPointer: -1
    };
  },
  watch: {
    filteredOptions() {
      for (let e = 0; e < this.filteredOptions.length; e++)
        if (this.selectable(this.filteredOptions[e])) {
          this.typeAheadPointer = e;
          break;
        }
    },
    open(e) {
      e && this.typeAheadToLastSelected();
    },
    selectedValue() {
      this.open && this.typeAheadToLastSelected();
    }
  },
  methods: {
    typeAheadUp() {
      for (let e = this.typeAheadPointer - 1; e >= 0; e--)
        if (this.selectable(this.filteredOptions[e])) {
          this.typeAheadPointer = e;
          break;
        }
    },
    typeAheadDown() {
      for (let e = this.typeAheadPointer + 1; e < this.filteredOptions.length; e++)
        if (this.selectable(this.filteredOptions[e])) {
          this.typeAheadPointer = e;
          break;
        }
    },
    typeAheadSelect() {
      const e = this.filteredOptions[this.typeAheadPointer];
      e && this.selectable(e) && this.select(e);
    },
    typeAheadToLastSelected() {
      this.typeAheadPointer = this.selectedValue.length !== 0 ? this.filteredOptions.indexOf(this.selectedValue[this.selectedValue.length - 1]) : -1;
    }
  }
}, Wp = {
  props: {
    loading: {
      type: Boolean,
      default: !1
    }
  },
  data() {
    return {
      mutableLoading: !1
    };
  },
  watch: {
    search() {
      this.$emit("search", this.search, this.toggleLoading);
    },
    loading(e) {
      this.mutableLoading = e;
    }
  },
  methods: {
    toggleLoading(e = null) {
      return e == null ? this.mutableLoading = !this.mutableLoading : this.mutableLoading = e;
    }
  }
}, fr = (e, n) => {
  const s = e.__vccOpts || e;
  for (const [a, l] of n)
    s[a] = l;
  return s;
}, Up = {}, Vp = {
  xmlns: "http://www.w3.org/2000/svg",
  width: "10",
  height: "10"
}, Kp = /* @__PURE__ */ Y("path", { d: "M6.895455 5l2.842897-2.842898c.348864-.348863.348864-.914488 0-1.263636L9.106534.261648c-.348864-.348864-.914489-.348864-1.263636 0L5 3.104545 2.157102.261648c-.348863-.348864-.914488-.348864-1.263636 0L.261648.893466c-.348864.348864-.348864.914489 0 1.263636L3.104545 5 .261648 7.842898c-.348864.348863-.348864.914488 0 1.263636l.631818.631818c.348864.348864.914773.348864 1.263636 0L5 6.895455l2.842898 2.842897c.348863.348864.914772.348864 1.263636 0l.631818-.631818c.348864-.348864.348864-.914489 0-1.263636L6.895455 5z" }, null, -1), qp = [
  Kp
];
function Gp(e, n) {
  return Vt(), re("svg", Vp, qp);
}
const Yp = /* @__PURE__ */ fr(Up, [["render", Gp]]), Jp = {}, Xp = {
  xmlns: "http://www.w3.org/2000/svg",
  width: "14",
  height: "10"
}, Qp = /* @__PURE__ */ Y("path", { d: "M9.211364 7.59931l4.48338-4.867229c.407008-.441854.407008-1.158247 0-1.60046l-.73712-.80023c-.407008-.441854-1.066904-.441854-1.474243 0L7 5.198617 2.51662.33139c-.407008-.441853-1.066904-.441853-1.474243 0l-.737121.80023c-.407008.441854-.407008 1.158248 0 1.600461l4.48338 4.867228L7 10l2.211364-2.40069z" }, null, -1), tm = [
  Qp
];
function em(e, n) {
  return Vt(), re("svg", Xp, tm);
}
const im = /* @__PURE__ */ fr(Jp, [["render", em]]), Xa = {
  Deselect: Yp,
  OpenIndicator: im
}, nm = {
  mounted(e, { instance: n }) {
    if (n.appendToBody) {
      const {
        height: s,
        top: a,
        left: l,
        width: u
      } = n.$refs.toggle.getBoundingClientRect();
      let c = window.scrollX || window.pageXOffset, f = window.scrollY || window.pageYOffset;
      e.unbindPosition = n.calculatePosition(e, n, {
        width: u + "px",
        left: c + l + "px",
        top: f + a + s + "px"
      }), document.body.appendChild(e);
    }
  },
  unmounted(e, { instance: n }) {
    n.appendToBody && (e.unbindPosition && typeof e.unbindPosition == "function" && e.unbindPosition(), e.parentNode && e.parentNode.removeChild(e));
  }
};
function om(e) {
  const n = {};
  return Object.keys(e).sort().forEach((s) => {
    n[s] = e[s];
  }), JSON.stringify(n);
}
let sm = 0;
function rm() {
  return ++sm;
}
const am = {
  components: Hi({}, Xa),
  directives: { appendToBody: nm },
  mixins: [Hp, $p, Wp],
  compatConfig: {
    MODE: 3
  },
  emits: [
    "open",
    "close",
    "update:modelValue",
    "search",
    "search:compositionstart",
    "search:compositionend",
    "search:keydown",
    "search:blur",
    "search:focus",
    "search:input",
    "option:created",
    "option:selecting",
    "option:selected",
    "option:deselecting",
    "option:deselected"
  ],
  props: {
    modelValue: {},
    components: {
      type: Object,
      default: () => ({})
    },
    options: {
      type: Array,
      default() {
        return [];
      }
    },
    disabled: {
      type: Boolean,
      default: !1
    },
    clearable: {
      type: Boolean,
      default: !0
    },
    deselectFromDropdown: {
      type: Boolean,
      default: !1
    },
    searchable: {
      type: Boolean,
      default: !0
    },
    multiple: {
      type: Boolean,
      default: !1
    },
    placeholder: {
      type: String,
      default: ""
    },
    transition: {
      type: String,
      default: "vs__fade"
    },
    clearSearchOnSelect: {
      type: Boolean,
      default: !0
    },
    closeOnSelect: {
      type: Boolean,
      default: !0
    },
    label: {
      type: String,
      default: "label"
    },
    autocomplete: {
      type: String,
      default: "off"
    },
    reduce: {
      type: Function,
      default: (e) => e
    },
    selectable: {
      type: Function,
      default: (e) => !0
    },
    getOptionLabel: {
      type: Function,
      default(e) {
        return typeof e == "object" ? e.hasOwnProperty(this.label) ? e[this.label] : console.warn(`[vue-select warn]: Label key "option.${this.label}" does not exist in options object ${JSON.stringify(e)}.
https://vue-select.org/api/props.html#getoptionlabel`) : e;
      }
    },
    getOptionKey: {
      type: Function,
      default(e) {
        if (typeof e != "object")
          return e;
        try {
          return e.hasOwnProperty("id") ? e.id : om(e);
        } catch (n) {
          return console.warn(`[vue-select warn]: Could not stringify this option to generate unique key. Please provide'getOptionKey' prop to return a unique key for each option.
https://vue-select.org/api/props.html#getoptionkey`, e, n);
        }
      }
    },
    onTab: {
      type: Function,
      default: function() {
        this.selectOnTab && !this.isComposing && this.typeAheadSelect();
      }
    },
    taggable: {
      type: Boolean,
      default: !1
    },
    tabindex: {
      type: Number,
      default: null
    },
    pushTags: {
      type: Boolean,
      default: !1
    },
    filterable: {
      type: Boolean,
      default: !0
    },
    filterBy: {
      type: Function,
      default(e, n, s) {
        return (n || "").toLocaleLowerCase().indexOf(s.toLocaleLowerCase()) > -1;
      }
    },
    filter: {
      type: Function,
      default(e, n) {
        return e.filter((s) => {
          let a = this.getOptionLabel(s);
          return typeof a == "number" && (a = a.toString()), this.filterBy(s, a, n);
        });
      }
    },
    createOption: {
      type: Function,
      default(e) {
        return typeof this.optionList[0] == "object" ? { [this.label]: e } : e;
      }
    },
    resetOnOptionsChange: {
      default: !1,
      validator: (e) => ["function", "boolean"].includes(typeof e)
    },
    clearSearchOnBlur: {
      type: Function,
      default: function({ clearSearchOnSelect: e, multiple: n }) {
        return e && !n;
      }
    },
    noDrop: {
      type: Boolean,
      default: !1
    },
    inputId: {
      type: String
    },
    dir: {
      type: String,
      default: "auto"
    },
    selectOnTab: {
      type: Boolean,
      default: !1
    },
    selectOnKeyCodes: {
      type: Array,
      default: () => [13]
    },
    searchInputQuerySelector: {
      type: String,
      default: "[type=search]"
    },
    mapKeydown: {
      type: Function,
      default: (e, n) => e
    },
    appendToBody: {
      type: Boolean,
      default: !1
    },
    calculatePosition: {
      type: Function,
      default(e, n, { width: s, top: a, left: l }) {
        e.style.top = a, e.style.left = l, e.style.width = s;
      }
    },
    dropdownShouldOpen: {
      type: Function,
      default({ noDrop: e, open: n, mutableLoading: s }) {
        return e ? !1 : n && !s;
      }
    },
    uid: {
      type: [String, Number],
      default: () => rm()
    }
  },
  data() {
    return {
      search: "",
      open: !1,
      isComposing: !1,
      pushedTags: [],
      _value: [],
      deselectButtons: []
    };
  },
  computed: {
    isReducingValues() {
      return this.$props.reduce !== this.$options.props.reduce.default;
    },
    isTrackingValues() {
      return typeof this.modelValue > "u" || this.isReducingValues;
    },
    selectedValue() {
      let e = this.modelValue;
      return this.isTrackingValues && (e = this.$data._value), e != null && e !== "" ? [].concat(e) : [];
    },
    optionList() {
      return this.options.concat(this.pushTags ? this.pushedTags : []);
    },
    searchEl() {
      return this.$slots.search ? this.$refs.selectedOptions.querySelector(this.searchInputQuerySelector) : this.$refs.search;
    },
    scope() {
      const e = {
        search: this.search,
        loading: this.loading,
        searching: this.searching,
        filteredOptions: this.filteredOptions
      };
      return {
        search: {
          attributes: Hi({
            disabled: this.disabled,
            placeholder: this.searchPlaceholder,
            tabindex: this.tabindex,
            readonly: !this.searchable,
            id: this.inputId,
            "aria-autocomplete": "list",
            "aria-labelledby": `vs${this.uid}__combobox`,
            "aria-controls": `vs${this.uid}__listbox`,
            ref: "search",
            type: "search",
            autocomplete: this.autocomplete,
            value: this.search
          }, this.dropdownOpen && this.filteredOptions[this.typeAheadPointer] ? {
            "aria-activedescendant": `vs${this.uid}__option-${this.typeAheadPointer}`
          } : {}),
          events: {
            compositionstart: () => this.isComposing = !0,
            compositionend: () => this.isComposing = !1,
            keydown: this.onSearchKeyDown,
            blur: this.onSearchBlur,
            focus: this.onSearchFocus,
            input: (n) => this.search = n.target.value
          }
        },
        spinner: {
          loading: this.mutableLoading
        },
        noOptions: {
          search: this.search,
          loading: this.mutableLoading,
          searching: this.searching
        },
        openIndicator: {
          attributes: {
            ref: "openIndicator",
            role: "presentation",
            class: "vs__open-indicator"
          }
        },
        listHeader: e,
        listFooter: e,
        header: Ja(Hi({}, e), { deselect: this.deselect }),
        footer: Ja(Hi({}, e), { deselect: this.deselect })
      };
    },
    childComponents() {
      return Hi(Hi({}, Xa), this.components);
    },
    stateClasses() {
      return {
        "vs--open": this.dropdownOpen,
        "vs--single": !this.multiple,
        "vs--multiple": this.multiple,
        "vs--searching": this.searching && !this.noDrop,
        "vs--searchable": this.searchable && !this.noDrop,
        "vs--unsearchable": !this.searchable,
        "vs--loading": this.mutableLoading,
        "vs--disabled": this.disabled
      };
    },
    searching() {
      return !!this.search;
    },
    dropdownOpen() {
      return this.dropdownShouldOpen(this);
    },
    searchPlaceholder() {
      return this.isValueEmpty && this.placeholder ? this.placeholder : void 0;
    },
    filteredOptions() {
      const e = [].concat(this.optionList);
      if (!this.filterable && !this.taggable)
        return e;
      const n = this.search.length ? this.filter(e, this.search, this) : e;
      if (this.taggable && this.search.length) {
        const s = this.createOption(this.search);
        this.optionExists(s) || n.unshift(s);
      }
      return n;
    },
    isValueEmpty() {
      return this.selectedValue.length === 0;
    },
    showClearButton() {
      return !this.multiple && this.clearable && !this.open && !this.isValueEmpty;
    }
  },
  watch: {
    options(e, n) {
      const s = () => typeof this.resetOnOptionsChange == "function" ? this.resetOnOptionsChange(e, n, this.selectedValue) : this.resetOnOptionsChange;
      !this.taggable && s() && this.clearSelection(), this.modelValue && this.isTrackingValues && this.setInternalValueFromOptions(this.modelValue);
    },
    modelValue: {
      immediate: !0,
      handler(e) {
        this.isTrackingValues && this.setInternalValueFromOptions(e);
      }
    },
    multiple() {
      this.clearSelection();
    },
    open(e) {
      this.$emit(e ? "open" : "close");
    }
  },
  created() {
    this.mutableLoading = this.loading;
  },
  methods: {
    setInternalValueFromOptions(e) {
      Array.isArray(e) ? this.$data._value = e.map((n) => this.findOptionFromReducedValue(n)) : this.$data._value = this.findOptionFromReducedValue(e);
    },
    select(e) {
      this.$emit("option:selecting", e), this.isOptionSelected(e) ? this.deselectFromDropdown && (this.clearable || this.multiple && this.selectedValue.length > 1) && this.deselect(e) : (this.taggable && !this.optionExists(e) && (this.$emit("option:created", e), this.pushTag(e)), this.multiple && (e = this.selectedValue.concat(e)), this.updateValue(e), this.$emit("option:selected", e)), this.onAfterSelect(e);
    },
    deselect(e) {
      this.$emit("option:deselecting", e), this.updateValue(this.selectedValue.filter((n) => !this.optionComparator(n, e))), this.$emit("option:deselected", e);
    },
    clearSelection() {
      this.updateValue(this.multiple ? [] : null);
    },
    onAfterSelect(e) {
      this.closeOnSelect && (this.open = !this.open, this.searchEl.blur()), this.clearSearchOnSelect && (this.search = "");
    },
    updateValue(e) {
      typeof this.modelValue > "u" && (this.$data._value = e), e !== null && (Array.isArray(e) ? e = e.map((n) => this.reduce(n)) : e = this.reduce(e)), this.$emit("update:modelValue", e);
    },
    toggleDropdown(e) {
      const n = e.target !== this.searchEl;
      n && e.preventDefault();
      const s = [
        ...this.deselectButtons || [],
        this.$refs.clearButton
      ];
      if (this.searchEl === void 0 || s.filter(Boolean).some((a) => a.contains(e.target) || a === e.target)) {
        e.preventDefault();
        return;
      }
      this.open && n ? this.searchEl.blur() : this.disabled || (this.open = !0, this.searchEl.focus());
    },
    isOptionSelected(e) {
      return this.selectedValue.some((n) => this.optionComparator(n, e));
    },
    isOptionDeselectable(e) {
      return this.isOptionSelected(e) && this.deselectFromDropdown;
    },
    optionComparator(e, n) {
      return this.getOptionKey(e) === this.getOptionKey(n);
    },
    findOptionFromReducedValue(e) {
      const n = (a) => JSON.stringify(this.reduce(a)) === JSON.stringify(e), s = [...this.options, ...this.pushedTags].filter(n);
      return s.length === 1 ? s[0] : s.find((a) => this.optionComparator(a, this.$data._value)) || e;
    },
    closeSearchOptions() {
      this.open = !1, this.$emit("search:blur");
    },
    maybeDeleteValue() {
      if (!this.searchEl.value.length && this.selectedValue && this.selectedValue.length && this.clearable) {
        let e = null;
        this.multiple && (e = [
          ...this.selectedValue.slice(0, this.selectedValue.length - 1)
        ]), this.updateValue(e);
      }
    },
    optionExists(e) {
      return this.optionList.some((n) => this.optionComparator(n, e));
    },
    normalizeOptionForSlot(e) {
      return typeof e == "object" ? e : { [this.label]: e };
    },
    pushTag(e) {
      this.pushedTags.push(e);
    },
    onEscape() {
      this.search.length ? this.search = "" : this.searchEl.blur();
    },
    onSearchBlur() {
      if (this.mousedown && !this.searching)
        this.mousedown = !1;
      else {
        const { clearSearchOnSelect: e, multiple: n } = this;
        this.clearSearchOnBlur({ clearSearchOnSelect: e, multiple: n }) && (this.search = ""), this.closeSearchOptions();
        return;
      }
      if (this.search.length === 0 && this.options.length === 0) {
        this.closeSearchOptions();
        return;
      }
    },
    onSearchFocus() {
      this.open = !0, this.$emit("search:focus");
    },
    onMousedown() {
      this.mousedown = !0;
    },
    onMouseUp() {
      this.mousedown = !1;
    },
    onSearchKeyDown(e) {
      const n = (l) => (l.preventDefault(), !this.isComposing && this.typeAheadSelect()), s = {
        8: (l) => this.maybeDeleteValue(),
        9: (l) => this.onTab(),
        27: (l) => this.onEscape(),
        38: (l) => (l.preventDefault(), this.typeAheadUp()),
        40: (l) => (l.preventDefault(), this.typeAheadDown())
      };
      this.selectOnKeyCodes.forEach((l) => s[l] = n);
      const a = this.mapKeydown(s, this);
      if (typeof a[e.keyCode] == "function")
        return a[e.keyCode](e);
    }
  }
}, lm = ["dir"], um = ["id", "aria-expanded", "aria-owns"], hm = {
  ref: "selectedOptions",
  class: "vs__selected-options"
}, cm = ["disabled", "title", "aria-label", "onClick"], fm = {
  ref: "actions",
  class: "vs__actions"
}, dm = ["disabled"], pm = { class: "vs__spinner" }, mm = ["id"], _m = ["id", "aria-selected", "onMouseover", "onClick"], gm = {
  key: 0,
  class: "vs__no-options"
}, vm = /* @__PURE__ */ xo(" Sorry, no matching options. "), ym = ["id"];
function bm(e, n, s, a, l, u) {
  const c = _f("append-to-body");
  return Vt(), re("div", {
    dir: s.dir,
    class: Ai(["v-select", u.stateClasses])
  }, [
    Pe(e.$slots, "header", Le(Te(u.scope.header))),
    Y("div", {
      id: `vs${s.uid}__combobox`,
      ref: "toggle",
      class: "vs__dropdown-toggle",
      role: "combobox",
      "aria-expanded": u.dropdownOpen.toString(),
      "aria-owns": `vs${s.uid}__listbox`,
      "aria-label": "Search for option",
      onMousedown: n[1] || (n[1] = (f) => u.toggleDropdown(f))
    }, [
      Y("div", hm, [
        (Vt(!0), re(ae, null, Es(u.selectedValue, (f, p) => Pe(e.$slots, "selected-option-container", {
          option: u.normalizeOptionForSlot(f),
          deselect: u.deselect,
          multiple: s.multiple,
          disabled: s.disabled
        }, () => [
          (Vt(), re("span", {
            key: s.getOptionKey(f),
            class: "vs__selected"
          }, [
            Pe(e.$slots, "selected-option", Le(Te(u.normalizeOptionForSlot(f))), () => [
              xo(ni(s.getOptionLabel(f)), 1)
            ]),
            s.multiple ? (Vt(), re("button", {
              key: 0,
              ref_for: !0,
              ref: (g) => l.deselectButtons[p] = g,
              disabled: s.disabled,
              type: "button",
              class: "vs__deselect",
              title: `Deselect ${s.getOptionLabel(f)}`,
              "aria-label": `Deselect ${s.getOptionLabel(f)}`,
              onClick: (g) => u.deselect(f)
            }, [
              (Vt(), Ln(_s(u.childComponents.Deselect)))
            ], 8, cm)) : _o("", !0)
          ]))
        ])), 256)),
        Pe(e.$slots, "search", Le(Te(u.scope.search)), () => [
          Y("input", Ns({ class: "vs__search" }, u.scope.search.attributes, gf(u.scope.search.events)), null, 16)
        ])
      ], 512),
      Y("div", fm, [
        po(Y("button", {
          ref: "clearButton",
          disabled: s.disabled,
          type: "button",
          class: "vs__clear",
          title: "Clear Selected",
          "aria-label": "Clear Selected",
          onClick: n[0] || (n[0] = (...f) => u.clearSelection && u.clearSelection(...f))
        }, [
          (Vt(), Ln(_s(u.childComponents.Deselect)))
        ], 8, dm), [
          [Ua, u.showClearButton]
        ]),
        Pe(e.$slots, "open-indicator", Le(Te(u.scope.openIndicator)), () => [
          s.noDrop ? _o("", !0) : (Vt(), Ln(_s(u.childComponents.OpenIndicator), Le(Ns({ key: 0 }, u.scope.openIndicator.attributes)), null, 16))
        ]),
        Pe(e.$slots, "spinner", Le(Te(u.scope.spinner)), () => [
          po(Y("div", pm, "Loading...", 512), [
            [Ua, e.mutableLoading]
          ])
        ])
      ], 512)
    ], 40, um),
    ne(or, { name: s.transition }, {
      default: Ol(() => [
        u.dropdownOpen ? po((Vt(), re("ul", {
          id: `vs${s.uid}__listbox`,
          ref: "dropdownMenu",
          key: `vs${s.uid}__listbox`,
          class: "vs__dropdown-menu",
          role: "listbox",
          tabindex: "-1",
          onMousedown: n[2] || (n[2] = Lo((...f) => u.onMousedown && u.onMousedown(...f), ["prevent"])),
          onMouseup: n[3] || (n[3] = (...f) => u.onMouseUp && u.onMouseUp(...f))
        }, [
          Pe(e.$slots, "list-header", Le(Te(u.scope.listHeader))),
          (Vt(!0), re(ae, null, Es(u.filteredOptions, (f, p) => (Vt(), re("li", {
            id: `vs${s.uid}__option-${p}`,
            key: s.getOptionKey(f),
            role: "option",
            class: Ai(["vs__dropdown-option", {
              "vs__dropdown-option--deselect": u.isOptionDeselectable(f) && p === e.typeAheadPointer,
              "vs__dropdown-option--selected": u.isOptionSelected(f),
              "vs__dropdown-option--highlight": p === e.typeAheadPointer,
              "vs__dropdown-option--disabled": !s.selectable(f)
            }]),
            "aria-selected": p === e.typeAheadPointer ? !0 : null,
            onMouseover: (g) => s.selectable(f) ? e.typeAheadPointer = p : null,
            onClick: Lo((g) => s.selectable(f) ? u.select(f) : null, ["prevent", "stop"])
          }, [
            Pe(e.$slots, "option", Le(Te(u.normalizeOptionForSlot(f))), () => [
              xo(ni(s.getOptionLabel(f)), 1)
            ])
          ], 42, _m))), 128)),
          u.filteredOptions.length === 0 ? (Vt(), re("li", gm, [
            Pe(e.$slots, "no-options", Le(Te(u.scope.noOptions)), () => [
              vm
            ])
          ])) : _o("", !0),
          Pe(e.$slots, "list-footer", Le(Te(u.scope.listFooter)))
        ], 40, mm)), [
          [c]
        ]) : (Vt(), re("ul", {
          key: 1,
          id: `vs${s.uid}__listbox`,
          role: "listbox",
          style: { display: "none", visibility: "hidden" }
        }, null, 8, ym))
      ]),
      _: 3
    }, 8, ["name"]),
    Pe(e.$slots, "footer", Le(Te(u.scope.footer)))
  ], 10, lm);
}
const wm = /* @__PURE__ */ fr(am, [["render", bm]]);
const xm = {
  install(e) {
    e.component("l-map", xp), e.component("l-tile-layer", kp), e.component("l-marker", Sp), e.component("l-polygon", Mp), e.component("l-geo-json", yp), e.component("v-select", wm);
  }
}, Ou = Ld(rp);
Ou.use(xm);
Ou.mount("#osmapplugin");
export {
  Ou as default
};
